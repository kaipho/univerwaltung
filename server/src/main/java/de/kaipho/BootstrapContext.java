package de.kaipho; // keep

import de.kaipho.core.db.DbConnector;
import de.kaipho.core.security.SecurityContext;
import de.kaipho.core.transaction.Callable;
import de.kaipho.domain.MetadataKeys;
import de.kaipho.core.transaction.Transaction;
import de.kaipho.domain.category.*;
import de.kaipho.domain.constant.uni.RepVorlesung;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.security.CustomUser;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.domain.security.User;
import de.kaipho.domain.uni.*;
import de.kaipho.service.security.BCryptProvider;
import de.kaipho.service.core.CategoryService;
import de.kaipho.service.core.CategoryNotExistInClassException;
import de.kaipho.service.uni.UniService;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Context to load all needed data into the db.
 */
public class BootstrapContext {

    /**
     * boots up the application.
     */
    public static void bootstrap() {
        DbConnector.createInMemoryDbConnection();
        Transaction.getInstance()
                   .run(Callable.DEFAULT, () -> {
                       SecurityContext.setUser(TechnicalUser.create("tu", BCryptProvider.getInstance().hashPassword("tu"), Language.empty(), ""));
                       initLangs();
                       initRoles();
                       initAdminUser();
                       initDev();
                       return null;
                   })
                   .resolveAndCommit();
    }

    private static void initAdminUser() {
        String name = "admin";
        List<User> founding = User.findBy(MetadataKeys.SECURITY_USER_USERNAME, name);
        if (founding.isEmpty()) {
            TechnicalUser user = TechnicalUser.create(name, BCryptProvider.getInstance().hashPassword(name), Language.DE, "");
            user.addSingleToRoles(Role.ADMIN);
            user.addSingleToRoles(Role.EDIT);
        }
    }

    private static void initLangs() {
        Language.DE = initLang("de");
        Language.EN = initLang("en");
    }

    private static Language initLang(String name) {
        try {
            return Language.from(name);
        } catch (CategoryNotExistInClassException e) {
            CategoryService.getInstance().addValueTo(name, Language.ID);
            return Language.from(name);
        }
    }

    private static void initRoles() {
        Role.ADMIN = initRole("ADMIN");
        Role.EDIT = initRole("EDIT");
        Role.VERWALTUNG = initRole("VERWALTUNG");
        Role.DOZENT = initRole("DOZENT");
    }

    private static Role initRole(String name) {
        try {
            return Role.from(name);
        } catch (CategoryNotExistInClassException e) {
            CategoryService.getInstance().addValueTo(name, Role.ID);
            return Role.from(name);
        }
    }

    private static void initDev() {
        CategoryService.getInstance().addValueTo("Dr.", Titel.ID);
        CategoryService.getInstance().addValueTo("Prof.", Titel.ID);
        CategoryService.getInstance().addValueTo("Kein Titel", Titel.ID);

        CategoryService.getInstance().addValueTo("1", Semester.ID);
        CategoryService.getInstance().addValueTo("2", Semester.ID);
        CategoryService.getInstance().addValueTo("3", Semester.ID);
        CategoryService.getInstance().addValueTo("4", Semester.ID);
        CategoryService.getInstance().addValueTo("5", Semester.ID);
        CategoryService.getInstance().addValueTo("6", Semester.ID);

        Fachbereich informatik = Fachbereich.create("Informatik");
        Fachbereich.create("BWL");
        Fachbereich vwl = Fachbereich.create("VWL");

        Dozent dozent1 = Dozent.create(Titel.from("Dr."), "25569", Optional.of(informatik));
        Person.create(
                "loe",
                BCryptProvider.getInstance().hashPassword("loe"),
                Language.DE,
                "Löwe",
                "M.",
                Optional.of(dozent1)
        );
        Dozent dozent2 = Dozent.create(Titel.from("Kein Titel"), "25570", Optional.of(vwl));
        Person.create("zwk",
                BCryptProvider.getInstance().hashPassword("zwk"),
                Language.DE,
                "Zwecker",
                "?",
                Optional.of(dozent2)
        );

        Raum a113 = Raum.create("A113");

        Vorlesung vwl1 = Vorlesung.create("VWL 1", Optional.of(dozent2), Optional.of(vwl), Optional.of(a113));
        Vorlesung db21 = Vorlesung.create("Datenbanken 2.1", Optional.of(dozent1), Optional.of(informatik), Optional.of(a113));

        Student sTom = Student.create(112222L, Semester.from("6"));
        Person.create("tom",
                BCryptProvider.getInstance().hashPassword("tom"),
                Language.DE,
                "Schmidt",
                "Tom",
                Optional.of(sTom)
        );
        Student sTim = Student.create(112223L, Semester.from("4"));
        Person.create("tim",
                BCryptProvider.getInstance().hashPassword("tim"),
                Language.DE,
                "Kräuter",
                "Tim",
                Optional.of(sTim)
        );

        UniService.getInstance().addVorlesung(sTom, vwl1);
        UniService.getInstance().addVorlesung(sTom, db21);
        UniService.getInstance().addVorlesung(sTim, vwl1);

        RepVorlesung.i18nModel().update(Language.DE, "{bezeichnung} bei {dozent}|{anzStudenten} Studenten");
        RepVorlesung.i18nModel().update(Language.EN, "{bezeichnung}, {dozent}, {anzStudenten}");

        Notification n1 = Notification.create("test", "testing...", LocalDate.now(), NotificationLevel.empty(), false);
        User.findBy(MetadataKeys.SECURITY_USER_USERNAME, "admin").get(0).addSingleToNotifications(n1);
    }
}
