package de.kaipho.controller;

import de.kaipho.core.Constants;
import de.kaipho.core.exceptions.ValidationException;
import de.kaipho.core.transaction.TransactionContext;
import de.kaipho.domain.core.primitive.UnitNotCovered;
import de.kaipho.domain.exception.ErrorVM;
import de.kaipho.service.security.PasswordsNotMatchException;
import de.kaipho.service.security.WrongLoginException;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * Exceptionhandling to convert the exceptions for better handling on the ui.
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler implements ResponseBodyAdvice<Object> {


    @ExceptionHandler(value = WrongLoginException.class)
    public ResponseEntity<Object> wrongLogin(WrongLoginException e) throws Exception {
        ErrorVM error = ErrorVM.createUnchecked();
        error.setCode((long) HttpStatus.UNAUTHORIZED.value());
        error.setMessage("Falsche Logindaten!");
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(error);
    }

    @ExceptionHandler(value = PasswordsNotMatchException.class)
    public ResponseEntity<Object> passwordsDoNotMatch(PasswordsNotMatchException e) throws Exception {
        ErrorVM error = ErrorVM.createUnchecked();
        error.setCode((long) HttpStatus.UNAUTHORIZED.value());
        error.setMessage("Passwörter stimmen nicht überein!");
        e.printStackTrace();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @ExceptionHandler(value = {ValidationException.class, UnitNotCovered.class})
    public ResponseEntity<Object> validationError(PasswordsNotMatchException e) throws Exception {
        ErrorVM error = ErrorVM.createUnchecked();
        error.setCode((long) HttpStatus.BAD_REQUEST.value());
        error.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> standard(Exception e) throws Exception {
        ErrorVM error = ErrorVM.createUnchecked();
        error.setCode((long) HttpStatus.UNAUTHORIZED.value());
        error.setMessage("Serverfehler: " + e.getMessage());
        e.printStackTrace();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if (TransactionContext.isInLongTransaction()) {
            response.getHeaders().add(Constants.TRANSACTION_HEADER, Long.toString(TransactionContext.getLongTransaction().getId()));
            response.getHeaders().add("Access-Control-Expose-Headers", Constants.TRANSACTION_HEADER);
            TransactionContext.reset();
        }
        if(body == null) {
            return "OK!";
        }
        return body;
    }
}
