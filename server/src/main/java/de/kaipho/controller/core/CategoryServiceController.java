package de.kaipho.controller.core;

import de.kaipho.core.Constants;
import de.kaipho.core.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import de.kaipho.domain.core.CategoryClassVM;
import de.kaipho.service.core.CategoryService;
import java.util.ArrayList;

import static de.kaipho.core.converter.FunctionParam.*;
import static de.kaipho.core.transaction.Callable.*;

@RestController
@RequestMapping("/core")
public class CategoryServiceController {
    @Autowired
    private CategoryService categoryService;

    

    @PostMapping("/getValuesFor")
    public List<String> getValuesFor(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(CATEGORYSERVICE_GETVALUESFOR, () -> {
           return categoryService.getValuesFor(PARAM_INTEGER.apply(obj.get("categoryClassId")));
       }).resolve();
    }
    @PostMapping("/addValueTo")
    public void addValueTo(@RequestBody Map<String, Object> obj) {
       Transaction.getInstance().run(CATEGORYSERVICE_ADDVALUETO, () -> {
           categoryService.addValueTo(PARAM_STRING.apply(obj.get("value")), PARAM_INTEGER.apply(obj.get("categoryClassId")));
           return null;
       }).resolve();
    }
    @PostMapping("/deleteValue")
    public void deleteValue(@RequestBody Map<String, Object> obj) {
       Transaction.getInstance().run(CATEGORYSERVICE_DELETEVALUE, () -> {
           categoryService.deleteValue(PARAM_STRING.apply(obj.get("value")), PARAM_INTEGER.apply(obj.get("categoryClassId")));
           return null;
       }).resolve();
    }
    @PostMapping("/getAllCategoryClasses")
    public List<CategoryClassVM> getAllCategoryClasses(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(CATEGORYSERVICE_GETALLCATEGORYCLASSES, () -> {
           return categoryService.getAllCategoryClasses();
       }).resolve();
    }
}
