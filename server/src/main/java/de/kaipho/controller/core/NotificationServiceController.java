package de.kaipho.controller.core;

import de.kaipho.core.Constants;
import de.kaipho.core.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import de.kaipho.service.core.NotificationService;
import de.kaipho.domain.core.Notification;
import java.util.ArrayList;
import de.kaipho.domain.security.User;

import static de.kaipho.core.converter.FunctionParam.*;
import static de.kaipho.core.transaction.Callable.*;

@RestController
@RequestMapping("/core")
public class NotificationServiceController {
    @Autowired
    private NotificationService notificationService;

    

    @PostMapping("/addNotification")
    public void addNotification(@RequestBody Map<String, Object> obj) {
       User user1 = User.findOneById(Long.parseLong(obj.get("user").toString()));
       Notification notification1 = Notification.findOneById(Long.parseLong(obj.get("notification").toString()));
       Transaction.getInstance().run(NOTIFICATIONSERVICE_ADDNOTIFICATION, () -> {
           notificationService.addNotification(user1, notification1);
           return null;
       }).resolve();
    }
    @PostMapping("/getNotifications")
    public List<Notification> getNotifications(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(NOTIFICATIONSERVICE_GETNOTIFICATIONS, () -> {
           return notificationService.getNotifications();
       }).resolve();
    }
}
