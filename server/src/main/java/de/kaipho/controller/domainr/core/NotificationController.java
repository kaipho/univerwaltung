package de.kaipho.controller.domainr.core;

import de.kaipho.controller.domainr.JsonConverter;
import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;
import de.kaipho.controller.domainr.ToJsonVisitor;
import de.kaipho.controller.domainr.ResolvePathVisitor;
import de.kaipho.core.transaction.Transaction;
import de.kaipho.core.converter.*;

import org.springframework.web.bind.annotation.*;

import javax.json.*;
import java.util.HashMap;
import java.util.Map;

import de.kaipho.domain.core.Notification;
import de.kaipho.domain.category.NotificationLevel;
import java.time.LocalDate;
import de.kaipho.service.security.BCryptProvider;

import static de.kaipho.core.converter.Updater.*;
import static de.kaipho.core.converter.UpdaterCategory.updaterCategory;
import static de.kaipho.domain.MetadataKeys.*;

@RestController
@RequestMapping("api/domainr/13")
public class NotificationController {
    private DataRequestService<Notification> requestService = DataRequestService.getInstance(Notification.class);

    @GetMapping
    public String findAll() throws DbException {
        ListVM<Notification> objects = requestService.findAll();
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/find")
    public String findAllWithAssociationLike(Long associationId, @RequestParam("val") Object val) throws DbException {
        ListVM<Notification> objects = requestService.findAllWithAssociationLike(associationId, val);
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/{id}")
    public String findSingle(@PathVariable Long id) throws DbException {
        Notification it = Notification.findOneById(id);
        JsonObject obj = it.accept(ToJsonVisitor.getInstance());
        JsonObjectBuilder links = Json.createObjectBuilder();
        links.add("self", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId());
        links.add("type", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId() + "/" + CORE_NOTIFICATION_TYPE);

        JsonObjectBuilder objVm = Json.createObjectBuilder().add("_embedded", obj)
                                                            .add("_links", links.build());
        return objVm.build().toString();
    }

    private static final HashMap<String, CrudUpdater<Notification>> updater = new HashMap<>();

    static {
        updater.put("subject", updater(UPDATER_STRING, Notification::setSubject, Notification::getSubject));
        updater.put("message", updater(UPDATER_STRING, Notification::setMessage, Notification::getMessage));
        updater.put("creation", updater(UPDATER_DATE, Notification::setCreation, Notification::getCreation));
        updater.put("type", updaterCategory(NotificationLevel::from, Notification::setType, Notification::getType));
        updater.put("isRead", updater(UPDATER_BOOLEAN, Notification::setIsRead, Notification::getIsRead));
    }

    @PutMapping("/{id}")
    public void updateSingle(@PathVariable("id") Long id, @RequestBody HashMap<String, Object> obj) throws DbException {
        Transaction.getInstance()
                    .run(()->Updater.updateAll(Notification.findOneById(id),obj,updater))
                    .resolve();
    }

    @GetMapping("/create")
    public String createObj() throws DbException {
        Notification obj = Transaction.getInstance().run(() -> Notification.builder().build()).resolve();
        return "domainr/13/" + obj.getId();
    }
}
