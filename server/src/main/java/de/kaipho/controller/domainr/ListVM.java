package de.kaipho.controller.domainr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO
 */
public class ListVM<T> {
    private Map<String, List<ObjVM<T>>> _embedded;

    public ListVM() {
        this._embedded = new HashMap<>();
    }

    public void addSingleToData(String className, ObjVM<T> data) {
        this._embedded.putIfAbsent(className, new ArrayList<>());
        this._embedded.get(className).add(data);
    }

    public Map<String, List<ObjVM<T>>> getData() {
        return _embedded;
    }
}
