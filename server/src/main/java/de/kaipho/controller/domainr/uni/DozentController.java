package de.kaipho.controller.domainr.uni;

import de.kaipho.controller.domainr.JsonConverter;
import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;
import de.kaipho.controller.domainr.ToJsonVisitor;
import de.kaipho.controller.domainr.ResolvePathVisitor;
import de.kaipho.core.transaction.Transaction;
import de.kaipho.core.converter.*;

import org.springframework.web.bind.annotation.*;

import javax.json.*;
import java.util.HashMap;
import java.util.Map;

import de.kaipho.domain.category.Titel;
import java.time.LocalDate;
import de.kaipho.service.security.BCryptProvider;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.domain.uni.Dozent;

import static de.kaipho.core.converter.Updater.*;
import static de.kaipho.core.converter.UpdaterCategory.updaterCategory;
import static de.kaipho.domain.MetadataKeys.*;

@RestController
@RequestMapping("api/domainr/23")
public class DozentController {
    private DataRequestService<Dozent> requestService = DataRequestService.getInstance(Dozent.class);

    @GetMapping
    public String findAll() throws DbException {
        ListVM<Dozent> objects = requestService.findAll();
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/find")
    public String findAllWithAssociationLike(Long associationId, @RequestParam("val") Object val) throws DbException {
        ListVM<Dozent> objects = requestService.findAllWithAssociationLike(associationId, val);
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/{id}")
    public String findSingle(@PathVariable Long id) throws DbException {
        Dozent it = Dozent.findOneById(id);
        JsonObject obj = it.accept(ToJsonVisitor.getInstance());
        JsonObjectBuilder links = Json.createObjectBuilder();
        links.add("self", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId());
        links.add("titel", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId() + "/" + UNI_DOZENT_TITEL);

        JsonObjectBuilder objVm = Json.createObjectBuilder().add("_embedded", obj)
                                                            .add("_links", links.build());
        return objVm.build().toString();
    }

    private static final HashMap<String, CrudUpdater<Dozent>> updater = new HashMap<>();

    static {
        updater.put("titel", updaterCategory(Titel::from, Dozent::setTitel, Dozent::getTitel));
        updater.put("personalnummer", updater(UPDATER_STRING, Dozent::setPersonalnummer, Dozent::getPersonalnummer));
    }

    @PutMapping("/{id}")
    public void updateSingle(@PathVariable("id") Long id, @RequestBody HashMap<String, Object> obj) throws DbException {
        Transaction.getInstance()
                    .run(()->Updater.updateAll(Dozent.findOneById(id),obj,updater))
                    .resolve();
    }

    @GetMapping("/create")
    public String createObj() throws DbException {
        Dozent obj = Transaction.getInstance().run(() -> Dozent.builder().build()).resolve();
        return "domainr/23/" + obj.getId();
    }
}
