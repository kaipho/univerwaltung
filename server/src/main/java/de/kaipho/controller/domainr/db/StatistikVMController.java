package de.kaipho.controller.domainr.db;

import de.kaipho.controller.domainr.JsonConverter;
import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;
import de.kaipho.controller.domainr.ToJsonVisitor;
import de.kaipho.controller.domainr.ResolvePathVisitor;
import de.kaipho.core.transaction.Transaction;
import de.kaipho.core.converter.*;

import org.springframework.web.bind.annotation.*;

import javax.json.*;
import java.util.HashMap;
import java.util.Map;

import de.kaipho.domain.db.StatistikVM;
import java.time.LocalDate;
import de.kaipho.service.security.BCryptProvider;

import static de.kaipho.core.converter.Updater.*;
import static de.kaipho.core.converter.UpdaterCategory.updaterCategory;
import static de.kaipho.domain.MetadataKeys.*;

@RestController
@RequestMapping("api/domainr/31")
public class StatistikVMController {
    private DataRequestService<StatistikVM> requestService = DataRequestService.getInstance(StatistikVM.class);

    @GetMapping
    public String findAll() throws DbException {
        ListVM<StatistikVM> objects = requestService.findAll();
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/find")
    public String findAllWithAssociationLike(Long associationId, @RequestParam("val") Object val) throws DbException {
        ListVM<StatistikVM> objects = requestService.findAllWithAssociationLike(associationId, val);
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/{id}")
    public String findSingle(@PathVariable Long id) throws DbException {
        StatistikVM it = StatistikVM.findOneById(id);
        JsonObject obj = it.accept(ToJsonVisitor.getInstance());
        JsonObjectBuilder links = Json.createObjectBuilder();
        links.add("self", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId());

        JsonObjectBuilder objVm = Json.createObjectBuilder().add("_embedded", obj)
                                                            .add("_links", links.build());
        return objVm.build().toString();
    }

    private static final HashMap<String, CrudUpdater<StatistikVM>> updater = new HashMap<>();

    static {
        updater.put("name", updater(UPDATER_STRING, StatistikVM::setName, StatistikVM::getName));
        updater.put("value", updater(UPDATER_INTEGER, StatistikVM::setValue, StatistikVM::getValue));
    }

    @PutMapping("/{id}")
    public void updateSingle(@PathVariable("id") Long id, @RequestBody HashMap<String, Object> obj) throws DbException {
        Transaction.getInstance()
                    .run(()->Updater.updateAll(StatistikVM.findOneById(id),obj,updater))
                    .resolve();
    }

    @GetMapping("/create")
    public String createObj() throws DbException {
        StatistikVM obj = Transaction.getInstance().run(() -> StatistikVM.builder().build()).resolve();
        return "domainr/31/" + obj.getId();
    }
}
