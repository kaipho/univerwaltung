package de.kaipho.controller.domainr.core;

import de.kaipho.controller.domainr.JsonConverter;
import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;
import de.kaipho.controller.domainr.ToJsonVisitor;
import de.kaipho.controller.domainr.ResolvePathVisitor;
import de.kaipho.core.transaction.Transaction;
import de.kaipho.core.converter.*;

import org.springframework.web.bind.annotation.*;

import javax.json.*;
import java.util.HashMap;
import java.util.Map;

import java.time.LocalDate;
import de.kaipho.domain.core.CategoryClassVM;
import de.kaipho.service.security.BCryptProvider;

import static de.kaipho.core.converter.Updater.*;
import static de.kaipho.core.converter.UpdaterCategory.updaterCategory;
import static de.kaipho.domain.MetadataKeys.*;

@RestController
@RequestMapping("api/domainr/7")
public class CategoryClassVMController {
    private DataRequestService<CategoryClassVM> requestService = DataRequestService.getInstance(CategoryClassVM.class);

    @GetMapping
    public String findAll() throws DbException {
        ListVM<CategoryClassVM> objects = requestService.findAll();
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/find")
    public String findAllWithAssociationLike(Long associationId, @RequestParam("val") Object val) throws DbException {
        ListVM<CategoryClassVM> objects = requestService.findAllWithAssociationLike(associationId, val);
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/{id}")
    public String findSingle(@PathVariable Long id) throws DbException {
        CategoryClassVM it = CategoryClassVM.findOneById(id);
        JsonObject obj = it.accept(ToJsonVisitor.getInstance());
        JsonObjectBuilder links = Json.createObjectBuilder();
        links.add("self", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId());

        JsonObjectBuilder objVm = Json.createObjectBuilder().add("_embedded", obj)
                                                            .add("_links", links.build());
        return objVm.build().toString();
    }

    private static final HashMap<String, CrudUpdater<CategoryClassVM>> updater = new HashMap<>();

    static {
        updater.put("dbId", updater(UPDATER_INTEGER, CategoryClassVM::setDbId, CategoryClassVM::getDbId));
        updater.put("name", updater(UPDATER_STRING, CategoryClassVM::setName, CategoryClassVM::getName));
    }

    @PutMapping("/{id}")
    public void updateSingle(@PathVariable("id") Long id, @RequestBody HashMap<String, Object> obj) throws DbException {
        Transaction.getInstance()
                    .run(()->Updater.updateAll(CategoryClassVM.findOneById(id),obj,updater))
                    .resolve();
    }

    @GetMapping("/create")
    public String createObj() throws DbException {
        CategoryClassVM obj = Transaction.getInstance().run(() -> CategoryClassVM.builder().build()).resolve();
        return "domainr/7/" + obj.getId();
    }
}
