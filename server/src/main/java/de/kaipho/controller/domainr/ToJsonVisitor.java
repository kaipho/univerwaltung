package de.kaipho.controller.domainr;

import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.exception.ErrorVM;
import de.kaipho.domain.core.CategoryClassVM;
import de.kaipho.domain.security.AnonymUser;
import de.kaipho.domain.uni.Dozent;
import de.kaipho.domain.transaction.LongTransactionVM;
import de.kaipho.domain.core.CategoryVM;
import de.kaipho.domain.db.StatistikVM;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.domain.security.LoginVM;

import de.kaipho.domain.category.PersistentCategory;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.core.Constants;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.primitive.PersistentPrimitive;
import de.kaipho.core.db.blob.Blob;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonArrayBuilder;

/**
 * Visitor to map a persistent element to its json representation.
 */
public class ToJsonVisitor implements PersistentElementCompleteVisitor<JsonObject> {
    private static ToJsonVisitor INSTANCE = new ToJsonVisitor(false);
    private static ToJsonVisitor INNER_INSTANCE = new ToJsonVisitor(true);
    public static ToJsonVisitor getInstance() {
        return INSTANCE;
    }

    private final boolean isInner;

    private ToJsonVisitor(boolean isInner) {
        this.isInner = isInner;
    }

    private JsonObjectBuilder visitPersistentPrimitive(PersistentPrimitive primitive) {
        JsonObjectBuilder obj = Json.createObjectBuilder();
        obj.add("unit_id", primitive.getUnitInstance().getId());
        obj.add("unit_class_id", primitive.getUnitClass().getId());
        obj.add("value", primitive.resolve().toDouble());
        obj.add("id", primitive.getId());
        return obj;
    }

    private JsonObjectBuilder visitBlob(Blob blob) {
        JsonObjectBuilder obj = Json.createObjectBuilder();
        obj.add("id", blob.getId());
        if(blob.getName() != null) obj.add("name", blob.getName());
        if(blob.getBlobId() != null) obj.add("blobId", blob.getBlobId());
        return obj;
    }

    private JsonObject visitPersistentElement(PersistentElement persistentElement) {
        if(isInner) {
            JsonObjectBuilder obj = Json.createObjectBuilder();
            if(persistentElement.getId() != null) obj.add("id", persistentElement.getId());
            if(persistentElement.getId() != null) obj.add("_rep", persistentElement.toString());
            obj.add("_type", 6);
            obj.add("_self", "/domainr/6/" + persistentElement.getId());
            return obj.build();
        }
        return persistentElement.accept(INNER_INSTANCE);
    }

    @Override
    public JsonObject visit(Person person) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(person.getId() != null) obj.add("id", person.getId());
       if(person.getId() != null) obj.add("_rep", person.toString());
       obj.add("_type", 21);
       obj.add("_self", "/domainr/21/" + person.getId());
       if(person.getUsername() != null) obj.add("username", person.getUsername());
       if(person.getPassword() != null) obj.add("password", person.getPassword());
       if(person.getName() != null) obj.add("name", person.getName());
       if(person.getVorname() != null) obj.add("vorname", person.getVorname());
       if(person.getRolle().isPresent()) obj.add("rolle", visitPersistentElement(person.getRolle().get()));
       if(person.getLang() != null && !person.getLang().isEmpty()) obj.add("lang", person.getLang().getValue());
       JsonArrayBuilder roles = Json.createArrayBuilder();
       person.getRoles().stream().map(PersistentCategory::getValue).forEach(roles::add);
       obj.add("roles", roles);
       JsonArrayBuilder notifications = Json.createArrayBuilder();
       person.getNotifications()
                     .stream()
                     .map(this::visitPersistentElement)
                     .forEach(notifications::add);
       obj.add("notifications", notifications);
       return obj.build();
    }
    @Override
    public JsonObject visit(Student student) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(student.getId() != null) obj.add("id", student.getId());
       if(student.getId() != null) obj.add("_rep", student.toString());
       obj.add("_type", 16);
       obj.add("_self", "/domainr/16/" + student.getId());
       if(student.getMatrikelnummer() != null) obj.add("matrikelnummer", student.getMatrikelnummer());
       if(student.getPerson().isPresent()) obj.add("person", visitPersistentElement(student.getPerson().get()));
       if(student.getSemester() != null && !student.getSemester().isEmpty()) obj.add("semester", student.getSemester().getValue());
       JsonArrayBuilder besuchteVorlesungen = Json.createArrayBuilder();
       student.getBesuchteVorlesungen()
                     .stream()
                     .map(this::visitPersistentElement)
                     .forEach(besuchteVorlesungen::add);
       obj.add("besuchteVorlesungen", besuchteVorlesungen);
       return obj.build();
    }
    @Override
    public JsonObject visit(Dozent dozent) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(dozent.getId() != null) obj.add("id", dozent.getId());
       if(dozent.getId() != null) obj.add("_rep", dozent.toString());
       obj.add("_type", 23);
       obj.add("_self", "/domainr/23/" + dozent.getId());
       if(dozent.getPersonalnummer() != null) obj.add("personalnummer", dozent.getPersonalnummer());
       if(dozent.getPerson().isPresent()) obj.add("person", visitPersistentElement(dozent.getPerson().get()));
       if(dozent.getFachbereich().isPresent()) obj.add("fachbereich", visitPersistentElement(dozent.getFachbereich().get()));
       if(dozent.getTitel() != null && !dozent.getTitel().isEmpty()) obj.add("titel", dozent.getTitel().getValue());
       return obj.build();
    }
    @Override
    public JsonObject visit(Vorlesung vorlesung) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(vorlesung.getId() != null) obj.add("id", vorlesung.getId());
       if(vorlesung.getId() != null) obj.add("_rep", vorlesung.toString());
       obj.add("_type", 17);
       obj.add("_self", "/domainr/17/" + vorlesung.getId());
       if(vorlesung.getBezeichnung() != null) obj.add("bezeichnung", vorlesung.getBezeichnung());
       if(vorlesung.getDozent().isPresent()) obj.add("dozent", visitPersistentElement(vorlesung.getDozent().get()));
       if(vorlesung.getFachbereich().isPresent()) obj.add("fachbereich", visitPersistentElement(vorlesung.getFachbereich().get()));
       if(vorlesung.getRaum().isPresent()) obj.add("raum", visitPersistentElement(vorlesung.getRaum().get()));
       JsonArrayBuilder studenten = Json.createArrayBuilder();
       vorlesung.getStudenten()
                     .stream()
                     .map(this::visitPersistentElement)
                     .forEach(studenten::add);
       obj.add("studenten", studenten);
       return obj.build();
    }
    @Override
    public JsonObject visit(Fachbereich fachbereich) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(fachbereich.getId() != null) obj.add("id", fachbereich.getId());
       if(fachbereich.getId() != null) obj.add("_rep", fachbereich.toString());
       obj.add("_type", 24);
       obj.add("_self", "/domainr/24/" + fachbereich.getId());
       if(fachbereich.getBezeichnung() != null) obj.add("bezeichnung", fachbereich.getBezeichnung());
       JsonArrayBuilder dozenten = Json.createArrayBuilder();
       fachbereich.getDozenten()
                     .stream()
                     .map(this::visitPersistentElement)
                     .forEach(dozenten::add);
       obj.add("dozenten", dozenten);
       JsonArrayBuilder vorlesungen = Json.createArrayBuilder();
       fachbereich.getVorlesungen()
                     .stream()
                     .map(this::visitPersistentElement)
                     .forEach(vorlesungen::add);
       obj.add("vorlesungen", vorlesungen);
       return obj.build();
    }
    @Override
    public JsonObject visit(Raum raum) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(raum.getId() != null) obj.add("id", raum.getId());
       if(raum.getId() != null) obj.add("_rep", raum.toString());
       obj.add("_type", 25);
       obj.add("_self", "/domainr/25/" + raum.getId());
       if(raum.getBezeichnung() != null) obj.add("bezeichnung", raum.getBezeichnung());
       return obj.build();
    }
    @Override
    public JsonObject visit(ErrorVM errorVM) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(errorVM.getId() != null) obj.add("id", errorVM.getId());
       if(errorVM.getId() != null) obj.add("_rep", errorVM.toString());
       obj.add("_type", 6);
       obj.add("_self", "/domainr/6/" + errorVM.getId());
       if(errorVM.getCode() != null) obj.add("code", errorVM.getCode());
       if(errorVM.getMessage() != null) obj.add("message", errorVM.getMessage());
       return obj.build();
    }
    @Override
    public JsonObject visit(LongTransactionVM longTransactionVM) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(longTransactionVM.getId() != null) obj.add("id", longTransactionVM.getId());
       if(longTransactionVM.getId() != null) obj.add("_rep", longTransactionVM.toString());
       obj.add("_type", 29);
       obj.add("_self", "/domainr/29/" + longTransactionVM.getId());
       if(longTransactionVM.getLongTaId() != null) obj.add("longTaId", longTransactionVM.getLongTaId());
       return obj.build();
    }
    @Override
    public JsonObject visit(CategoryClassVM categoryClassVM) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(categoryClassVM.getId() != null) obj.add("id", categoryClassVM.getId());
       if(categoryClassVM.getId() != null) obj.add("_rep", categoryClassVM.toString());
       obj.add("_type", 7);
       obj.add("_self", "/domainr/7/" + categoryClassVM.getId());
       if(categoryClassVM.getDbId() != null) obj.add("dbId", categoryClassVM.getDbId());
       if(categoryClassVM.getName() != null) obj.add("name", categoryClassVM.getName());
       JsonArrayBuilder values = Json.createArrayBuilder();
       categoryClassVM.getValues()
                     .stream()
                     .map(this::visitPersistentElement)
                     .forEach(values::add);
       obj.add("values", values);
       return obj.build();
    }
    @Override
    public JsonObject visit(CategoryVM categoryVM) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(categoryVM.getId() != null) obj.add("id", categoryVM.getId());
       if(categoryVM.getId() != null) obj.add("_rep", categoryVM.toString());
       obj.add("_type", 8);
       obj.add("_self", "/domainr/8/" + categoryVM.getId());
       if(categoryVM.getDbId() != null) obj.add("dbId", categoryVM.getDbId());
       if(categoryVM.getName() != null) obj.add("name", categoryVM.getName());
       if(categoryVM.getIsDerived() != null) obj.add("isDerived", categoryVM.getIsDerived());
       return obj.build();
    }
    @Override
    public JsonObject visit(AnonymUser anonymUser) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(anonymUser.getId() != null) obj.add("id", anonymUser.getId());
       if(anonymUser.getId() != null) obj.add("_rep", anonymUser.toString());
       obj.add("_type", 11);
       obj.add("_self", "/domainr/11/" + anonymUser.getId());
       if(anonymUser.getUsername() != null) obj.add("username", anonymUser.getUsername());
       if(anonymUser.getPassword() != null) obj.add("password", anonymUser.getPassword());
       if(anonymUser.getLang() != null && !anonymUser.getLang().isEmpty()) obj.add("lang", anonymUser.getLang().getValue());
       JsonArrayBuilder roles = Json.createArrayBuilder();
       anonymUser.getRoles().stream().map(PersistentCategory::getValue).forEach(roles::add);
       obj.add("roles", roles);
       JsonArrayBuilder notifications = Json.createArrayBuilder();
       anonymUser.getNotifications()
                     .stream()
                     .map(this::visitPersistentElement)
                     .forEach(notifications::add);
       obj.add("notifications", notifications);
       return obj.build();
    }
    @Override
    public JsonObject visit(TechnicalUser technicalUser) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(technicalUser.getId() != null) obj.add("id", technicalUser.getId());
       if(technicalUser.getId() != null) obj.add("_rep", technicalUser.toString());
       obj.add("_type", 35);
       obj.add("_self", "/domainr/35/" + technicalUser.getId());
       if(technicalUser.getUsername() != null) obj.add("username", technicalUser.getUsername());
       if(technicalUser.getPassword() != null) obj.add("password", technicalUser.getPassword());
       if(technicalUser.getDescription() != null) obj.add("description", technicalUser.getDescription());
       if(technicalUser.getLang() != null && !technicalUser.getLang().isEmpty()) obj.add("lang", technicalUser.getLang().getValue());
       JsonArrayBuilder roles = Json.createArrayBuilder();
       technicalUser.getRoles().stream().map(PersistentCategory::getValue).forEach(roles::add);
       obj.add("roles", roles);
       JsonArrayBuilder notifications = Json.createArrayBuilder();
       technicalUser.getNotifications()
                     .stream()
                     .map(this::visitPersistentElement)
                     .forEach(notifications::add);
       obj.add("notifications", notifications);
       return obj.build();
    }
    @Override
    public JsonObject visit(LoginVM loginVM) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(loginVM.getId() != null) obj.add("id", loginVM.getId());
       if(loginVM.getId() != null) obj.add("_rep", loginVM.toString());
       obj.add("_type", 12);
       obj.add("_self", "/domainr/12/" + loginVM.getId());
       if(loginVM.getToken() != null) obj.add("token", loginVM.getToken());
       return obj.build();
    }
    @Override
    public JsonObject visit(Notification notification) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(notification.getId() != null) obj.add("id", notification.getId());
       if(notification.getId() != null) obj.add("_rep", notification.toString());
       obj.add("_type", 13);
       obj.add("_self", "/domainr/13/" + notification.getId());
       if(notification.getSubject() != null) obj.add("subject", notification.getSubject());
       if(notification.getMessage() != null) obj.add("message", notification.getMessage());
       if(notification.getCreation() != null) obj.add("creation", notification.getCreation().format(Constants.DATE_FORMAT));
       if(notification.getIsRead() != null) obj.add("isRead", notification.getIsRead());
       if(notification.getType() != null && !notification.getType().isEmpty()) obj.add("type", notification.getType().getValue());
       return obj.build();
    }
    @Override
    public JsonObject visit(StatistikVM statistikVM) {
       JsonObjectBuilder obj = Json.createObjectBuilder();
       if(statistikVM.getId() != null) obj.add("id", statistikVM.getId());
       if(statistikVM.getId() != null) obj.add("_rep", statistikVM.toString());
       obj.add("_type", 31);
       obj.add("_self", "/domainr/31/" + statistikVM.getId());
       if(statistikVM.getName() != null) obj.add("name", statistikVM.getName());
       if(statistikVM.getValue() != null) obj.add("value", statistikVM.getValue());
       return obj.build();
    }
}
