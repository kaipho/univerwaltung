package de.kaipho.controller.domainr;

import de.kaipho.domain.core.PersistentElement;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public enum JsonConverter {
    ;
    public static String listVmToJson(ListVM<? extends PersistentElement> objects) {
        JsonObjectBuilder data = Json.createObjectBuilder();

        objects.getData().forEach((key, val) -> {
            JsonArrayBuilder jsonArray = Json.createArrayBuilder();
            val.stream().map(ObjVM::getObj).forEach(it -> {
                JsonObjectBuilder objBuilder = Json.createObjectBuilder();
                if(it.getId() != null) objBuilder.add("id", it.getId());
                if(it.getId() != null) objBuilder.add("_rep", it.toString());
                JsonObject obj = objBuilder.build();

                JsonObjectBuilder links = Json.createObjectBuilder();
                links.add("self", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId());

                JsonObjectBuilder objVm = Json.createObjectBuilder().add("_embedded", obj)
                                              .add("_links", links.build());
                jsonArray.add(objVm.build());
            });
            data.add(key, jsonArray.build());
        });

        return Json.createObjectBuilder().add("_embedded", data.build()).build().toString();
    }
}
