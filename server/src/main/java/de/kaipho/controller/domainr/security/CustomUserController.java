package de.kaipho.controller.domainr.security;

import de.kaipho.controller.domainr.JsonConverter;
import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;
import de.kaipho.controller.domainr.ToJsonVisitor;
import de.kaipho.controller.domainr.ResolvePathVisitor;
import de.kaipho.core.transaction.Transaction;
import de.kaipho.core.converter.*;

import org.springframework.web.bind.annotation.*;

import javax.json.*;
import java.util.HashMap;
import java.util.Map;

import java.time.LocalDate;
import de.kaipho.service.security.BCryptProvider;
import de.kaipho.domain.category.Language;
import de.kaipho.domain.security.CustomUser;
import de.kaipho.domain.security.User;

import static de.kaipho.core.converter.Updater.*;
import static de.kaipho.core.converter.UpdaterCategory.updaterCategory;
import static de.kaipho.domain.MetadataKeys.*;

@RestController
@RequestMapping("api/domainr/10")
public class CustomUserController {
    private DataRequestService<CustomUser> requestService = DataRequestService.getInstance(CustomUser.class);

    @GetMapping
    public String findAll() throws DbException {
        ListVM<CustomUser> objects = requestService.findAll();
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/find")
    public String findAllWithAssociationLike(Long associationId, @RequestParam("val") Object val) throws DbException {
        ListVM<CustomUser> objects = requestService.findAllWithAssociationLike(associationId, val);
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/{id}")
    public String findSingle(@PathVariable Long id) throws DbException {
        CustomUser it = CustomUser.findOneById(id);
        JsonObject obj = it.accept(ToJsonVisitor.getInstance());
        JsonObjectBuilder links = Json.createObjectBuilder();
        links.add("self", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId());
        links.add("lang", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId() + "/" + SECURITY_USER_LANG);

        JsonObjectBuilder objVm = Json.createObjectBuilder().add("_embedded", obj)
                                                            .add("_links", links.build());
        return objVm.build().toString();
    }

    private static final HashMap<String, CrudUpdater<CustomUser>> updater = new HashMap<>();

    static {
        updater.put("username", updater(UPDATER_STRING, User::setUsername, User::getUsername));
        updater.put("password", updater(UPDATER_PASSWORD, User::setPassword, User::getPassword));
        updater.put("lang", updaterCategory(Language::from, User::setLang, User::getLang));
    }

    @PutMapping("/{id}")
    public void updateSingle(@PathVariable("id") Long id, @RequestBody HashMap<String, Object> obj) throws DbException {
        Transaction.getInstance()
                    .run(()->Updater.updateAll(CustomUser.findOneById(id),obj,updater))
                    .resolve();
    }

}
