package de.kaipho.controller.domainr;

import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.exception.ErrorVM;
import de.kaipho.domain.core.CategoryClassVM;
import de.kaipho.domain.security.AnonymUser;
import de.kaipho.domain.uni.Dozent;
import de.kaipho.domain.transaction.LongTransactionVM;
import de.kaipho.domain.core.CategoryVM;
import de.kaipho.domain.db.StatistikVM;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.domain.security.LoginVM;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;

import static de.kaipho.domain.MetadataKeys.*;

public class ResolvePathVisitor implements PersistentElementCompleteVisitor<Long> {
    @Override
    public Long visit(Person person) {
       return UNI_PERSON;
    }
    @Override
    public Long visit(Student student) {
       return UNI_STUDENT;
    }
    @Override
    public Long visit(Dozent dozent) {
       return UNI_DOZENT;
    }
    @Override
    public Long visit(Vorlesung vorlesung) {
       return UNI_VORLESUNG;
    }
    @Override
    public Long visit(Fachbereich fachbereich) {
       return UNI_FACHBEREICH;
    }
    @Override
    public Long visit(Raum raum) {
       return UNI_RAUM;
    }
    @Override
    public Long visit(ErrorVM errorVM) {
       return EXCEPTION_ERRORVM;
    }
    @Override
    public Long visit(LongTransactionVM longTransactionVM) {
       return TRANSACTION_LONGTRANSACTIONVM;
    }
    @Override
    public Long visit(CategoryClassVM categoryClassVM) {
       return CORE_CATEGORYCLASSVM;
    }
    @Override
    public Long visit(CategoryVM categoryVM) {
       return CORE_CATEGORYVM;
    }
    @Override
    public Long visit(AnonymUser anonymUser) {
       return SECURITY_ANONYMUSER;
    }
    @Override
    public Long visit(TechnicalUser technicalUser) {
       return SECURITY_TECHNICALUSER;
    }
    @Override
    public Long visit(LoginVM loginVM) {
       return SECURITY_LOGINVM;
    }
    @Override
    public Long visit(Notification notification) {
       return CORE_NOTIFICATION;
    }
    @Override
    public Long visit(StatistikVM statistikVM) {
       return DB_STATISTIKVM;
    }
}
