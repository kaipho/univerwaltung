package de.kaipho.controller.domainr.core;

import de.kaipho.controller.domainr.JsonConverter;
import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;
import de.kaipho.controller.domainr.ToJsonVisitor;
import de.kaipho.controller.domainr.ResolvePathVisitor;
import de.kaipho.core.transaction.Transaction;
import de.kaipho.core.converter.*;

import org.springframework.web.bind.annotation.*;

import javax.json.*;
import java.util.HashMap;
import java.util.Map;

import de.kaipho.domain.core.CategoryVM;
import java.time.LocalDate;
import de.kaipho.service.security.BCryptProvider;

import static de.kaipho.core.converter.Updater.*;
import static de.kaipho.core.converter.UpdaterCategory.updaterCategory;
import static de.kaipho.domain.MetadataKeys.*;

@RestController
@RequestMapping("api/domainr/8")
public class CategoryVMController {
    private DataRequestService<CategoryVM> requestService = DataRequestService.getInstance(CategoryVM.class);

    @GetMapping
    public String findAll() throws DbException {
        ListVM<CategoryVM> objects = requestService.findAll();
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/find")
    public String findAllWithAssociationLike(Long associationId, @RequestParam("val") Object val) throws DbException {
        ListVM<CategoryVM> objects = requestService.findAllWithAssociationLike(associationId, val);
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/{id}")
    public String findSingle(@PathVariable Long id) throws DbException {
        CategoryVM it = CategoryVM.findOneById(id);
        JsonObject obj = it.accept(ToJsonVisitor.getInstance());
        JsonObjectBuilder links = Json.createObjectBuilder();
        links.add("self", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId());

        JsonObjectBuilder objVm = Json.createObjectBuilder().add("_embedded", obj)
                                                            .add("_links", links.build());
        return objVm.build().toString();
    }

    private static final HashMap<String, CrudUpdater<CategoryVM>> updater = new HashMap<>();

    static {
        updater.put("dbId", updater(UPDATER_INTEGER, CategoryVM::setDbId, CategoryVM::getDbId));
        updater.put("name", updater(UPDATER_STRING, CategoryVM::setName, CategoryVM::getName));
        updater.put("isDerived", updater(UPDATER_BOOLEAN, CategoryVM::setIsDerived, CategoryVM::getIsDerived));
    }

    @PutMapping("/{id}")
    public void updateSingle(@PathVariable("id") Long id, @RequestBody HashMap<String, Object> obj) throws DbException {
        Transaction.getInstance()
                    .run(()->Updater.updateAll(CategoryVM.findOneById(id),obj,updater))
                    .resolve();
    }

    @GetMapping("/create")
    public String createObj() throws DbException {
        CategoryVM obj = Transaction.getInstance().run(() -> CategoryVM.builder().build()).resolve();
        return "domainr/8/" + obj.getId();
    }
}
