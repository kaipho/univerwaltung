package de.kaipho.controller.primitive;

import de.kaipho.core.Constants;
import de.kaipho.core.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import de.kaipho.service.primitive.PrimitivesService;

import static de.kaipho.core.converter.FunctionParam.*;
import static de.kaipho.core.transaction.Callable.*;

@RestController
@RequestMapping("/primitive")
public class PrimitivesServiceController {
    @Autowired
    private PrimitivesService primitivesService;

    

    @PostMapping("/convertTo")
    public Double convertTo(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(PRIMITIVESSERVICE_CONVERTTO, () -> {
           return primitivesService.convertTo(PARAM_INTEGER.apply(obj.get("primitiveId")), PARAM_INTEGER.apply(obj.get("unitId")));
       }).resolve();
    }
}
