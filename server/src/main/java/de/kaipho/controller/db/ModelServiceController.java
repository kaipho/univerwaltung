package de.kaipho.controller.db;

import de.kaipho.core.Constants;
import de.kaipho.core.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import de.kaipho.domain.db.StatistikVM;
import java.util.ArrayList;
import de.kaipho.service.db.ModelService;

import static de.kaipho.core.converter.FunctionParam.*;
import static de.kaipho.core.transaction.Callable.*;

@RestController
@RequestMapping("/db")
public class ModelServiceController {
    @Autowired
    private ModelService modelService;

    

    @PostMapping("/getModelForClass")
    public String getModelForClass(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(MODELSERVICE_GETMODELFORCLASS, () -> {
           return modelService.getModelForClass(PARAM_STRING.apply(obj.get("clazz")));
       }).resolve();
    }
    @PostMapping("/getAllClasses")
    public List<String> getAllClasses(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(MODELSERVICE_GETALLCLASSES, () -> {
           return modelService.getAllClasses();
       }).resolve();
    }
    @PostMapping("/modelStatistics")
    public List<StatistikVM> modelStatistics(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(MODELSERVICE_MODELSTATISTICS, () -> {
           return modelService.modelStatistics();
       }).resolve();
    }
}
