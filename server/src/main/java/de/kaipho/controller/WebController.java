package de.kaipho.controller;

import de.kaipho.core.db.DbConnector;
import de.kaipho.core.db.DbFacade;
import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class WebController {

    @GetMapping("/web/**")
    public String mappingWeb() {
        return "/index.html";
    }

    @GetMapping("/")
    public String mapping() {
        return "/index.html";
    }

    @GetMapping("/shutdown")
    public void shutdown() {
        System.exit(0);
    }

    InputStream f;

    @PostMapping("/api/file")
    public ResponseEntity<Long> uploadFile(MultipartFile file) throws IOException {
        Long key = DbConnector.getConnector()
                              .getBuilderFor("INSERT INTO val_blob (val) VALUES (?)")
                              .asManipulation()
                              .withObj(file.getInputStream())
                              .resolveKey();
        return ResponseEntity.ok(key);
    }

    @GetMapping("/api/file/{id}")
    public void getFile(@PathVariable Long id, HttpServletResponse response) throws IOException {
        InputStream is = DbFacade.loadBlobForId(id);
        IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
    }
}
