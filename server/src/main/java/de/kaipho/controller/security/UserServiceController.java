package de.kaipho.controller.security;

import de.kaipho.core.Constants;
import de.kaipho.core.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import de.kaipho.service.security.UserService;
import de.kaipho.domain.security.LoginVM;
import de.kaipho.domain.security.User;

import static de.kaipho.core.converter.FunctionParam.*;
import static de.kaipho.core.transaction.Callable.*;

@RestController
@RequestMapping("/security")
public class UserServiceController {
    @Autowired
    private UserService userService;

    

    @PostMapping("/login")
    public LoginVM login(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(USERSERVICE_LOGIN, () -> {
           return userService.login(PARAM_STRING.apply(obj.get("username")), PARAM_PASSWORD.apply(obj.get("password")));
       }).resolve();
    }
    @PostMapping("/logout")
    public void logout(@RequestBody Map<String, Object> obj) {
       Transaction.getInstance().run(USERSERVICE_LOGOUT, () -> {
           userService.logout();
           return null;
       }).resolve();
    }
    @PostMapping("/changePassword")
    public void changePassword(@RequestBody Map<String, Object> obj) {
       Transaction.getInstance().run(USERSERVICE_CHANGEPASSWORD, () -> {
           userService.changePassword(PARAM_PASSWORD.apply(obj.get("old")), PARAM_PASSWORD.apply(obj.get("newPassword")), PARAM_PASSWORD.apply(obj.get("repeatIt")));
           return null;
       }).resolve();
    }
    @PostMapping("/resetPassword")
    public String resetPassword(@RequestBody Map<String, Object> obj) {
       User user1 = User.findOneById(Long.parseLong(obj.get("user").toString()));
       return Transaction.getInstance().run(USERSERVICE_RESETPASSWORD, () -> {
           return userService.resetPassword(user1);
       }).resolve();
    }
    @PostMapping("/getUserFromToken")
    public LoginVM getUserFromToken(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(USERSERVICE_GETUSERFROMTOKEN, () -> {
           return userService.getUserFromToken(PARAM_STRING.apply(obj.get("token")));
       }).resolve();
    }
}
