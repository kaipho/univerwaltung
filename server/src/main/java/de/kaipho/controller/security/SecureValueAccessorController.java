package de.kaipho.controller.security;

import de.kaipho.core.Constants;
import de.kaipho.core.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import de.kaipho.service.security.SecureValueAccessor;

import static de.kaipho.core.converter.FunctionParam.*;
import static de.kaipho.core.transaction.Callable.*;

@RestController
@RequestMapping("/security")
public class SecureValueAccessorController {
    @Autowired
    private SecureValueAccessor secureValueAccessor;

    

    @PostMapping("/decrypt")
    public String decrypt(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(SECUREVALUEACCESSOR_DECRYPT, () -> {
           return secureValueAccessor.decrypt(PARAM_PASSWORD.apply(obj.get("text")), PARAM_PASSWORD.apply(obj.get("password")));
       }).resolve();
    }
    @PostMapping("/encrypt")
    public String encrypt(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(SECUREVALUEACCESSOR_ENCRYPT, () -> {
           return secureValueAccessor.encrypt(PARAM_STRING.apply(obj.get("text")), PARAM_PASSWORD.apply(obj.get("password")));
       }).resolve();
    }
}
