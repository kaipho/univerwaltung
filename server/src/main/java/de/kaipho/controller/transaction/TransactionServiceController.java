package de.kaipho.controller.transaction;

import de.kaipho.core.Constants;
import de.kaipho.core.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import de.kaipho.service.transaction.TransactionService;
import de.kaipho.domain.db.StatistikVM;
import java.util.ArrayList;
import de.kaipho.domain.transaction.LongTransactionVM;

import static de.kaipho.core.converter.FunctionParam.*;
import static de.kaipho.core.transaction.Callable.*;

@RestController
@RequestMapping("/transaction")
public class TransactionServiceController {
    @Autowired
    private TransactionService transactionService;

    

    @PostMapping("/commit")
    public void commit(@RequestBody Map<String, Object> obj) {
       Transaction.getInstance().run(TRANSACTIONSERVICE_COMMIT, () -> {
           transactionService.commit();
           return null;
       }).resolve();
    }
    @PostMapping("/startTransaction")
    public void startTransaction(@RequestBody Map<String, Object> obj) {
       Transaction.getInstance().run(TRANSACTIONSERVICE_STARTTRANSACTION, () -> {
           transactionService.startTransaction();
           return null;
       }).resolve();
    }
    @PostMapping("/openTransactions")
    public List<LongTransactionVM> openTransactions(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(TRANSACTIONSERVICE_OPENTRANSACTIONS, () -> {
           return transactionService.openTransactions();
       }).resolve();
    }
    @PostMapping("/transactionStatistics")
    public List<StatistikVM> transactionStatistics(@RequestBody Map<String, Object> obj) {
       return Transaction.getInstance().run(TRANSACTIONSERVICE_TRANSACTIONSTATISTICS, () -> {
           return transactionService.transactionStatistics();
       }).resolve();
    }
}
