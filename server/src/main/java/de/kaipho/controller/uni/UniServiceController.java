package de.kaipho.controller.uni;

import de.kaipho.core.Constants;
import de.kaipho.core.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.service.uni.UniService;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.domain.uni.Dozent;

import static de.kaipho.core.converter.FunctionParam.*;
import static de.kaipho.core.transaction.Callable.*;

@RestController
@RequestMapping("/uni")
public class UniServiceController {
    @Autowired
    private UniService uniService;

    

    @PostMapping("/setFachbereich")
    public void setFachbereich(@RequestBody Map<String, Object> obj) {
       Dozent dozent1 = Dozent.findOneById(Long.parseLong(obj.get("dozent").toString()));
       Fachbereich fachbereich1 = Fachbereich.findOneById(Long.parseLong(obj.get("fachbereich").toString()));
       Transaction.getInstance().run(UNISERVICE_SETFACHBEREICH, () -> {
           uniService.setFachbereich(dozent1, fachbereich1);
           return null;
       }).resolve();
    }
    @PostMapping("/setRolle")
    public void setRolle(@RequestBody Map<String, Object> obj) {
       Person person1 = Person.findOneById(Long.parseLong(obj.get("person").toString()));
       PersonRolle rolle1 = PersonRolle.findOneById(Long.parseLong(obj.get("rolle").toString()));
       Transaction.getInstance().run(UNISERVICE_SETROLLE, () -> {
           uniService.setRolle(person1, rolle1);
           return null;
       }).resolve();
    }
    @PostMapping("/addVorlesung")
    public void addVorlesung(@RequestBody Map<String, Object> obj) {
       Student student1 = Student.findOneById(Long.parseLong(obj.get("student").toString()));
       Vorlesung vorlesung1 = Vorlesung.findOneById(Long.parseLong(obj.get("vorlesung").toString()));
       Transaction.getInstance().run(UNISERVICE_ADDVORLESUNG, () -> {
           uniService.addVorlesung(student1, vorlesung1);
           return null;
       }).resolve();
    }
    @PostMapping("/setRaum")
    public void setRaum(@RequestBody Map<String, Object> obj) {
       Vorlesung vorlesung1 = Vorlesung.findOneById(Long.parseLong(obj.get("vorlesung").toString()));
       Raum raum1 = Raum.findOneById(Long.parseLong(obj.get("raum").toString()));
       Transaction.getInstance().run(UNISERVICE_SETRAUM, () -> {
           uniService.setRaum(vorlesung1, raum1);
           return null;
       }).resolve();
    }
    @PostMapping("/setFachbereichVorlesung")
    public void setFachbereichVorlesung(@RequestBody Map<String, Object> obj) {
       Vorlesung vorlesung1 = Vorlesung.findOneById(Long.parseLong(obj.get("vorlesung").toString()));
       Fachbereich fachbereich1 = Fachbereich.findOneById(Long.parseLong(obj.get("fachbereich").toString()));
       Transaction.getInstance().run(UNISERVICE_SETFACHBEREICHVORLESUNG, () -> {
           uniService.setFachbereichVorlesung(vorlesung1, fachbereich1);
           return null;
       }).resolve();
    }
    @PostMapping("/setDozent")
    public void setDozent(@RequestBody Map<String, Object> obj) {
       Vorlesung vorlesung1 = Vorlesung.findOneById(Long.parseLong(obj.get("vorlesung").toString()));
       Dozent dozent1 = Dozent.findOneById(Long.parseLong(obj.get("dozent").toString()));
       Transaction.getInstance().run(UNISERVICE_SETDOZENT, () -> {
           uniService.setDozent(vorlesung1, dozent1);
           return null;
       }).resolve();
    }
}
