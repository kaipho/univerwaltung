package de.kaipho.i18n;

import de.kaipho.domain.category.Language;

/**
 * A context with functionality to resolve i18n.
 */
public interface I18nContext {

    void update(Language language, String newRep);

    String get();

    String get(Language language);
}
