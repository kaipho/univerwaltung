package de.kaipho.i18n;

/**
 * A interface representing i18n-functionality for a instance item.
 */
public interface I18nInstance {
    I18nContext i18n();
}
