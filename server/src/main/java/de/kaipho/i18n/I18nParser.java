package de.kaipho.i18n;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Parser for i18n strings, not ThreadSave!. Processes and replaces placeholders: <br>
 * - {varname} gets an lookup in the given hashmap and replaces the string.
 */
public class I18nParser {
    private final String s;
    private final Map<String, Object> data;

    private StringContainer actual = EmptyContainer.INSTANCE;
    private List<StringContainer> result = new ArrayList<>();

    public I18nParser(String s, Map<String, Object> data) {
        this.s = s;
        this.data = data;
    }

    private void setActual(StringContainer actual) {
        result.add(actual);
        this.actual = actual;
    }

    String parseString() {
        return splitString(s).stream()
                             .map(it -> it.accept(new ContainerReturnVisitor<StringBuilder>() {
                                 @Override
                                 public StringBuilder visit(SimpleContainer simpleContainer) {
                                     return simpleContainer.getContent();
                                 }

                                 @Override
                                 public StringBuilder visit(PlaceholderContainer placeholderContainer) {
                                     String placeholder = placeholderContainer.getPlaceholder().toString();
                                     if (!data.containsKey(placeholder)) {
                                         throw new RuntimeException("Constant not parsable!");
                                     }
                                     return new StringBuilder(data.get(placeholder).toString());
                                 }

                                 @Override
                                 public StringBuilder visit(EmptyContainer emptyContainer) {
                                     return new StringBuilder();
                                 }
                             }))
                             .reduce(new StringBuilder(), StringBuilder::append)
                             .toString();
    }

    private List<StringContainer> splitString(String s) {
        for (char c : s.toCharArray()) {
            actual.accept(new ContainerVisitor() {
                @Override
                public void visit(SimpleContainer simpleContainer) {
                    if (c == '{') {
                        setActual(new PlaceholderContainer());
                    } else {
                        simpleContainer.getContent().append(c);
                    }
                }

                @Override
                public void visit(PlaceholderContainer placeholderContainer) {
                    if (c == '}') {
                        setActual(EmptyContainer.INSTANCE);
                    } else {
                        placeholderContainer.getPlaceholder().append(c);
                    }
                }

                @Override
                public void visit(EmptyContainer emptyContainer) {
                    if (c == '{') {
                        setActual(new PlaceholderContainer());
                    } else {
                        SimpleContainer container = new SimpleContainer();
                        container.getContent().append(c);
                        setActual(container);
                    }
                }
            });
        }
        return result;
    }

    interface ContainerVisitor {
        void visit(SimpleContainer simpleContainer);

        void visit(PlaceholderContainer placeholderContainer);

        void visit(EmptyContainer emptyContainer);
    }

    interface ContainerReturnVisitor<T> {
        T visit(SimpleContainer simpleContainer);

        T visit(PlaceholderContainer placeholderContainer);

        T visit(EmptyContainer emptyContainer);
    }

    interface StringContainer {
        void accept(ContainerVisitor visitor);

        <T> T accept(ContainerReturnVisitor<T> visitor);
    }

    static class EmptyContainer implements StringContainer {
        static EmptyContainer INSTANCE = new EmptyContainer();

        private EmptyContainer() {
        }

        @Override
        public void accept(ContainerVisitor visitor) {
            visitor.visit(this);
        }

        @Override
        public <T> T accept(ContainerReturnVisitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    static class SimpleContainer implements StringContainer {
        private final StringBuilder content;

        SimpleContainer() {
            this.content = new StringBuilder();
        }

        public StringBuilder getContent() {
            return content;
        }

        @Override
        public void accept(ContainerVisitor visitor) {
            visitor.visit(this);
        }

        @Override
        public <T> T accept(ContainerReturnVisitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    static class PlaceholderContainer implements StringContainer {
        private final StringBuilder placeholder;

        PlaceholderContainer() {
            this.placeholder = new StringBuilder();
        }

        public StringBuilder getPlaceholder() {
            return placeholder;
        }

        @Override
        public void accept(ContainerVisitor visitor) {
            visitor.visit(this);
        }

        @Override
        public <T> T accept(ContainerReturnVisitor<T> visitor) {
            return visitor.visit(this);
        }
    }
}
