package de.kaipho.core.converter;

public interface CrudUpdater<O> {
    void execute(O obj, Object value);
}
