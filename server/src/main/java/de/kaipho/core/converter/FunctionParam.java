package de.kaipho.core.converter;

import de.kaipho.core.db.blob.Blob;
import de.kaipho.domain.core.primitive.PersistentPrimitive;
import de.kaipho.domain.core.primitive.Rational;
import de.kaipho.service.primitive.Primitives;

import java.time.LocalDate;
import java.util.function.Function;
import java.util.Map;

/**
 * Converter for function params
 */
public class FunctionParam {
    public static final Function<Object, String> PARAM_STRING = Object::toString;
    public static final Function<Object, String> PARAM_PASSWORD = Object::toString;
    public static final Function<Object, Long> PARAM_INTEGER = val -> Long.valueOf(val.toString());
    public static final Function<Object, Double> PARAM_DOUBLE = val -> Double.valueOf(val.toString());
    public static final Function<Object, Boolean> PARAM_BOOLEAN = val -> (boolean) val;
    public static final Function<Object, PersistentPrimitive> PARAM_PRIMITIVE = val -> {
        Map<String, Object> values = (Map<String, Object>) val;
        Long unit = Long.valueOf(values.get("unit_id").toString());
        Double value = Double.valueOf(values.get("value").toString());
        return new PersistentPrimitive(Rational.of(value), Primitives.PRIMITIVES.get(unit));
    };
    public static final Function<Object, Blob> PARAM_BLOB = val -> {
        Map<String, Object> values = (Map<String, Object>) val;
        Long blobId = Long.valueOf(values.get("blobId").toString());
        String name= values.get("name").toString();
        return new Blob(null, name, blobId);
    };
    public static final Function<Object, LocalDate> PARAM_DATE = val -> {
        if (val.toString()
               .trim()
               .isEmpty()) {
            return null;
        }
        return LocalDate.parse(val.toString());
    };
}
