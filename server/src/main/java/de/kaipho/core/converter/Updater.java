package de.kaipho.core.converter;

import de.kaipho.core.db.blob.Blob;
import de.kaipho.domain.core.primitive.PersistentPrimitive;
import de.kaipho.domain.core.primitive.Rational;
import de.kaipho.service.primitive.Primitives;
import de.kaipho.service.security.BCryptProvider;

import java.time.LocalDate;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Updater for domainr objects.
 */
public class Updater<O, T> implements CrudUpdater<O> {
    public static final Function<Object, String> UPDATER_STRING = Object::toString;
    public static final Function<Object, String> UPDATER_PASSWORD = val -> BCryptProvider.getInstance()
                                                                                         .hashPassword(val.toString());
    public static final Function<Object, Long> UPDATER_INTEGER = val -> Long.valueOf(val.toString());
    public static final Function<Object, Double> UPDATER_DOUBLE = val -> Double.valueOf(val.toString());
    public static final Function<Object, Boolean> UPDATER_BOOLEAN = val -> (boolean) val;
    public static final Function<Object, PersistentPrimitive> UPDATER_PRIMITIVE = val -> {
        Map<String, Object> values = (Map<String, Object>) val;
        Long id = Long.valueOf(values.get("id").toString());
        Long unit = Long.valueOf(values.get("unit_id").toString());
        Double value = Double.valueOf(values.get("value").toString());
        return new PersistentPrimitive(id, Rational.of(value), Primitives.PRIMITIVES.get(unit));
    };
    public static final Function<Object, LocalDate> UPDATER_DATE = val -> {
        if (val.toString()
               .trim()
               .isEmpty()) {
            return null;
        }
        return LocalDate.parse(val.toString());
    };
    public static final Function<Object, Blob> UPDATER_BLOB = val -> {
        Map<String, Object> values = (Map<String, Object>) val;
        Long id = Long.valueOf(values.get("id").toString());
        Long blobId = Long.valueOf(values.get("blobId").toString());
        String name= values.get("name").toString();
        return new Blob(id, name, blobId);
    };

    private final Function<Object, T> converter;
    private final BiConsumer<O, T> setter;
    private final Function<O, T> getter;

    Updater(Function<Object, T> converter, BiConsumer<O, T> setter, Function<O, T> getter) {
        this.converter = converter;
        this.setter = setter;
        this.getter = getter;
    }

    public static <O, T> CrudUpdater<O> updater(Function<Object, T> converter, BiConsumer<O, T> setter, Function<O, T> getter) {
        return new Updater<>(converter, setter, getter);
    }

    public static <O> Void updateAll(O applyOn, Map<String, Object> values, Map<String, CrudUpdater<O>> updater) {
        values.forEach((key, value) -> {
            CrudUpdater<O> singleUpdater = updater.get(key);
            if(singleUpdater != null)
                singleUpdater.execute(applyOn, value);
        });
        return null;
    }

    @Override
    public void execute(O obj, Object value) {
        Object oldValue = getter.apply(obj);
        if (value == oldValue) {
            return;
        }
        if (value == null) {
            value = "";
        }
        if (!value.equals(oldValue)) {
            setter.accept(obj, converter.apply(value));
        }
    }
}
