package de.kaipho.core.db;

/**
 * Db operations to fetch statistic data.
 */
public class DbStatistic {
    private DbStatistic() {
    }

    public static long fetchOverallObjCount() {
        return fetchTableRows("obj");
    }

    public static long fetchOverallLinkCount() {
        return fetchTableRows("link");
    }

    public static long fetchOverallIntegerCount() {
        return fetchTableRows("val_integer");
    }

    public static long fetchOverallStringCount() {
        return fetchTableRows("val_string");
    }

    public static long fetchOverallDateTimeCount() {
        return fetchTableRows("val_datetime");
    }

    public static long fetchOverallDoubleCount() {
        return fetchTableRows("val_double");
    }

    public static long fetchOverallBlobCount() {
        return fetchTableRows("val_blob");
    }
    public static long fetchOverallRationalCount() {
        return fetchTableRows("val_rational");
    }

    public static long fetchObjCountFor(long id) {
        DbConnectionBuilder builder = DbConnector.getConnector()
                                                 .getBuilderFor("SELECT COUNT(1) FROM obj WHERE instanceof = ?")
                                                 .asQuery()
                                                 .withLong(id);
        return builder.forSingle(rs -> rs.getLong(1));
    }

    /**
     * Fetches the count of rows in the given table.
     */
    private static long fetchTableRows(String tableName) {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT COUNT(1) FROM " + tableName)
                          .asQuery()
                          .forSingle(rs -> rs.getLong(1));
    }

    public static long fetchOpenLongTransactions() {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT COUNT(1) FROM LONGTRANSACTION WHERE COMMITED IS NULL")
                          .asQuery()
                          .forSingle(rs -> rs.getLong(1));
    }

    public static long fetchCommitedLongTransactions() {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT COUNT(1) FROM LONGTRANSACTION WHERE COMMITED IS NOT NULL")
                          .asQuery()
                          .forSingle(rs -> rs.getLong(1));
    }
}
