package de.kaipho.core.db.primitives;

import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.primitive.PersistentPrimitive;
import de.kaipho.domain.core.primitive.UnitClass;

public interface PrimitivesStore {
    static de.kaipho.core.db.primitives.PrimitivesStore getStore() {
        return PrimitivesStoreH2.INSTANCE;
    }

    void savePrimitive(PersistentPrimitive<?> primitive, PersistentElement owner, Long linkInstanceOf);
    <RANGE extends UnitClass> PersistentPrimitive<RANGE> loadPersistentPrimitive(Long id, RANGE range);
    <RANGE extends UnitClass> PersistentPrimitive<RANGE> loadPersistentPrimitive(PersistentElement owner, Long linkInstanceOf, RANGE range);
}
