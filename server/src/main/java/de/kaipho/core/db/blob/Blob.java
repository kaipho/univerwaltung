package de.kaipho.core.db.blob;

/**
 * System for storing BLOB in java.
 */
public class Blob {
    private Long id;
    private String name;
    private Long blobId;

    public Blob(Long id, String name, Long blobId) {
        this.id = id;
        this.name = name;
        this.blobId = blobId;
    }

    public Blob(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBlobId() {
        return blobId;
    }

    public void setBlobId(Long blobId) {
        this.blobId = blobId;
    }
}
