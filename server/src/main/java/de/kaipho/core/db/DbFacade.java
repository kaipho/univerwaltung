package de.kaipho.core.db;

import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.security.SecurityContext;
import de.kaipho.core.transaction.LongTransaction;
import de.kaipho.core.transaction.ShortTransaction;
import de.kaipho.core.transaction.TransactionContext;
import de.kaipho.domain.category.PersistentCategory;
import de.kaipho.domain.core.CategoryClassVM;
import de.kaipho.domain.core.CategoryVM;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.security.AnonymUser;
import de.kaipho.domain.security.User;
import de.kaipho.service.core.CategoryAlreadyExistInHierarchyException;
import de.kaipho.service.core.CategoryInUseException;
import de.kaipho.service.security.WrongLoginException;

import java.io.InputStream;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * Facade to access data in the database (stored in the meta-model)
 */
public abstract class DbFacade {
    private static DbFacadeImpl impl;

    public static void setImpl(DbFacadeImpl impl) {
        DbFacade.impl = impl;
    }

    protected Long createObjInstance(Long key) throws DbException {
        return createObjInstance(key, false);
    }

    protected Long createObjInstance(Long key, boolean userAssociation) throws DbException {
        DbConnector connector = DbConnector.getConnector();
        PreparedStatement statement = connector.getStatementFor("INSERT INTO obj (instanceOf, CREATEDBY) VALUES (?,?)");
        try {
            statement.setLong(1, key);
            statement.setLong(2, TransactionContext.getTransaction().getId());
            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            long id;
            if (keys.next()) {
                id = keys.getLong(1);
            } else throw new DbException();
            statement.close();

            if (userAssociation) {
                User user = SecurityContext.getUser();
                if (user instanceof AnonymUser) {
                    connector.rollback();
                    throw new WrongLoginException();
                }
                PreparedStatement uA = connector.getStatementFor("INSERT INTO link (pos, fromObj, toObj, instanceOf, CREATEDBY) VALUES (?,?,?,?,?)");
                uA.setLong(1, -1);
                uA.setLong(2, user.getId());
                uA.setLong(3, id);
                uA.setLong(4, -1);
                uA.setLong(5, TransactionContext.getTransaction().getId());
                uA.execute();
            }

            connector.commit();
            return id;
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    /**
     * Gets the id of the class the instance is typed in.
     */
    public static OptionalInt getClassIdForInstanceId(Long id) {
        DbConnector connector = DbConnector.getConnector();
        PreparedStatement statement = connector.getStatementFor("SELECT INSTANCEOF FROM obj WHERE ID = ?");
        try {
            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            OptionalInt instanceOfId;
            if (result.next()) {
                instanceOfId = OptionalInt.of(result.getInt("INSTANCEOF"));
            } else throw new DbException();
            statement.close();
            connector.commit();
            return instanceOfId;
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public static List<Long> getByIdAndAssociationLike(Long classId, Long linkID, Object association) {
        return DbConnector.getConnector()
                          .getBuilderForFile("getByIdAndAssociationLike")
                          .withLong(linkID)
                          .withObj(association)
                          .withLong(classId)
                          .withLong(TransactionContext.getLongTaId())
                          .withLong(TransactionContext.getLongTaId())
                          .forEach(row -> row.getLong(1));
    }

    public static <T> List<T> getAll(Long id, BiFunction<Integer, Long, T> mapper) {
        return DbConnector.getConnector()
                          .getBuilderForFile("findAll")
                          .withLong(id)
                          .withLong(TransactionContext.getLongTaId())
                          .withLong(TransactionContext.getLongTaId())
                          .forEach(rs -> mapper.apply(rs.getInt("instanceof"), rs.getLong("id")));
    }

    /**
     * Deletes the given object and all links from and to this object.
     *
     * @param id The id of the Object
     */
    public static void deleteObj(Long id) {
        impl.deleteObj(id);
    }

    public static <T> T resolveParent(Long id, Long instanceOf, Function<Long, T> resolver) {
        PreparedStatement resolveParent = DbConnector.getConnector()
                                                     .getStatementFor("SELECT fromObj FROM link WHERE link.toObj = ? AND link.instanceOf = ?");
        try {
            resolveParent.setObject(1, id);
            resolveParent.setObject(2, instanceOf);
            ResultSet resultSet = resolveParent.executeQuery();
            if (resultSet.next()) {
                Long toId = resultSet.getLong("fromObj");
                if (resultSet.next()) {
                    throw new DbException("Too many results");
                }
                return resolver.apply(toId);
            }
            throw new DbException("No results");
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public static Map<Long, List<DbLink>> loadLinks(Long fromObj) throws DbException {
        return DbConnector.getConnector()
                          .getBuilderForFile("loadLinks")
                          .withLong(fromObj)
                          .withLong(TransactionContext.getLongTaId())
                          .withLong(TransactionContext.getLongTaId())
                          .asQuery()
                          .forEach(DbFacade::mapLink)
                          .stream()
                          .collect(Collectors.groupingBy(DbLink::getInstanceOf));
    }

    private static DbLink mapLink(ResultSet row) throws SQLException {
        return new DbLink(
                row.getLong("id"),
                row.getLong("toObj"),
                row.getLong("instanceof"),
                row.getLong("pos"),
                row.getLong("named")
        );
    }

    // GET ALL
    // select id from object where instanceof in (19,20,21)

    // GET BY X
    // select * from object, val_string, link  where object.id = link.fromobj and link.toobj = val_string.id
    // and link.instanceof = 0 and val_string.val like '%' and object.instanceof IN (19,20,21)

    protected Long setAssociation(Long from, Long val, Long instanceOf) throws DbException {
        Long id = setAssociation(from, val, "val_integer", instanceOf, -1L);
        SingletonCache.getInstance().setInteger(id, val);
        return id;
    }

    protected Long setAssociation(Long from, String val, Long instanceOf) throws DbException {
        Long id = setAssociation(from, val, "val_string", instanceOf, -1L);
        SingletonCache.getInstance().setString(id, val);
        return id;
    }

    protected Long setAssociation(Long from, PersistentCategory val, Long instanceOf) throws DbException {
        if (val.isEmpty()) return null;
        Long id = setAssociation(from, val.getValue(), "val_string", instanceOf, -1L);
        SingletonCache.getInstance().setString(id, val.getValue());
        return id;
    }

    public static Long setAssociation(Long from, InputStream val, Long instanceOf) throws DbException {
        return setAssociation(from, val, "val_blob", instanceOf, -1L);
    }

    protected Long setAssociation(Long from, LocalDate val, Long instanceOf) throws DbException {
        if (val == null) {
            saveLink(from, null, instanceOf, -1L);
            return -3L;
        }
        Long id = setAssociation(from, java.sql.Date.valueOf(val), "val_datetime", instanceOf, -1L);
        SingletonCache.getInstance().setLocalDate(id, val);
        return id;
    }

    protected Long setAssociation(Long from, Boolean val, Long instanceOf) throws DbException {
        Long id = setAssociation(from, val ? 1 : 0, "val_integer", instanceOf, -1L);
        SingletonCache.getInstance().setInteger(id, val ? 1L : 0L);
        return id;
    }

    protected Long setAssociation(Long from, Double val, Long instanceOf) throws DbException {
        Long id = setAssociation(from, val, "val_double", instanceOf, -1L);
        SingletonCache.getInstance().setDouble(id, val);
        return id;
    }

    protected void setAssociation(Long from, PersistentElement val, Long instanceOf) throws DbException {
        if (val == null) {
            saveLink(from, null, instanceOf, -1L);
            return;
        }
        saveLink(from, val.getId(), instanceOf, -1L);
    }

    protected void setAssociation(Long from, List<?> val, Long instanceOf) throws DbException {
        for (long i = 0; i < val.size(); i++) {
            Object it = val.get((int) i);
            addAssociation(from, it, instanceOf, i);
        }
    }

    protected Long setAssociation(Long from, de.kaipho.core.db.blob.Blob val, Long instanceOf) throws DbException {
        Long nameId = saveToSingletonTable(val.getName(), "val_string");
        DbConnector.getConnector()
                   .getBuilderFor("UPDATE link SET named = ?, link.toobj = ? WHERE link.id = ?")
                   .withLong(nameId)
                   .withLong(val.getBlobId())
                   .withLong(val.getId())
                   .asManipulation()
                   .submit();
        return val.getId();
    }

    /**
     * inserts a map into the db. The structure is the following: owner - link1 -> key - link2 -> value.
     * link1 has an order, link2 is selectable with pos == -2.
     */
    protected void setAssociation(Long from, Map<?, ?> val, Long instanceOf) throws DbException {
        Long i = 0L;
        for (Object o : val.keySet()) {
            Long targetId = addAssociation(from, o, instanceOf, i);

            Object it = val.get(o);
            addAssociation(targetId, it, instanceOf, -2L);

            i++;
        }
    }

    protected Long addAssociation(Long from, Object it, Long instanceOf, Long pos) throws DbException {
        if (it instanceof Integer) {
            return setAssociation(from, it, "val_integer", instanceOf, pos);
        } else if (it instanceof String) {
            return setAssociation(from, it, "val_string", instanceOf, pos);
        } else if (it instanceof Double) {
            return setAssociation(from, it, "val_double", instanceOf, pos);
        } else if (it instanceof LocalDate) {
            return setAssociation(from, java.sql.Date.valueOf((LocalDate) it), "val_datetime", instanceOf, pos);
        } else if (it instanceof Boolean) {
            return setAssociation(from, (Boolean) it ? 1 : 0, "val_integer", instanceOf, pos);
        } else if (it instanceof PersistentElement) {
            saveLink(from, ((PersistentElement) it).getId(), instanceOf, pos);
            return ((PersistentElement) it).getId();
        } else if (it instanceof PersistentCategory) {
            return setAssociation(from, ((PersistentCategory) it).getValue(), "val_string", instanceOf, pos);
        }
        throw new DbException(it + " could not mapped.");
    }

    private static Long setAssociation(Long from, Object val, String table, Long instanceOf, Long optionalPos) throws DbException {
        Long singletonId = saveToSingletonTable(val, table);
        saveLink(from, singletonId, instanceOf, optionalPos);
        return singletonId;
    }

    /**
     * Saves the given val to the given table. Returns the id of the stored object.
     * If there ist already a val like this, return the id of the val.
     */
    private static Long saveToSingletonTable(Object val, String table) {
        Optional<Long> optionalId = DbConnector.getConnector()
                                               .getBuilderFor("SELECT id FROM " + table + " WHERE val = ?")
                                               .withObj(val)
                                               .forSingleOptional(row -> row.getLong(1));
        if (optionalId.isPresent()) return optionalId.get();
        return DbConnector.getConnector()
                          .getBuilderFor("INSERT INTO " + table + " (val) VALUES (?)")
                          .withObj(val)
                          .resolveKey();
    }

    public static Long saveString(Object val) {
        return saveToSingletonTable(val, "val_string");
    }

    private static void saveLink(Long from, Long to, Long instanceOf, Long pos) {
        try {
            // Don't delete, mark as deleted!
            Boolean linkExist = DbConnector.getConnector()
                                           .getBuilderFor("SELECT COUNT(link.id) FROM link " +
                                                   "WHERE fromObj = ? " +
                                                   "AND instanceof = ? " +
                                                   "AND pos = ? " +
                                                   "AND link.ID IN (SELECT id FROM LINKSNOTDELETED)")
                                           .withLong(from)
                                           .withLong(instanceOf)
                                           .withLong(pos)
                                           .forSingle(row -> row.getLong(1) > 0);

            if (linkExist) {
                DbConnector.getConnector()
                           .getBuilderFor("INSERT INTO accessed(TRANSACTION, OBJECT, TYPE) VALUES (?, " +
                                   "SELECT link.id FROM link WHERE fromObj = ? " +
                                   "AND instanceof = ? " +
                                   "AND pos = ? " +
                                   "AND link.ID IN (SELECT id FROM LINKSNOTDELETED), 'D')")
                           .withLong(TransactionContext.getTransaction().getId())
                           .withLong(from)
                           .withLong(instanceOf)
                           .withLong(pos)
                           .submit();
            }

            if (to == null) {
                return;
            }

            PreparedStatement insertNewValue = DbConnector.getConnector()
                                                          .getStatementFor("INSERT INTO link (pos, fromObj, toObj, instanceof, CREATEDBY) VALUES (?, ?, ?, ?, ?)");
            insertNewValue.setLong(1, pos);
            insertNewValue.setLong(2, from);
            insertNewValue.setLong(3, to);
            insertNewValue.setLong(4, instanceOf);
            insertNewValue.setLong(5, TransactionContext.getTransaction().getId());
            insertNewValue.executeUpdate();
            insertNewValue.close();
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public static InputStream loadBlobForId(Long id) throws DbException {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT val FROM val_blob WHERE id = ?")
                          .withLong(id)
                          .asQuery()
                          .forSingle(row -> row.getBinaryStream(1));
    }

    protected Map<Long, Long> getObjList(Long from, Long instanceOf) throws DbException {
        Map<Long, Long> result = new HashMap<>();
        DbConnector connector = DbConnector.getConnector();
        PreparedStatement statementObjList = connector.getStatementFor("SELECT toObj, pos FROM link WHERE link.fromObj = ? AND instanceOf = ?");
        try {
            statementObjList.setLong(1, from);
            statementObjList.setLong(2, instanceOf);
            ResultSet resultSet = statementObjList.executeQuery();
            while (resultSet.next()) {
                Long val = resultSet.getLong("toObj");
                Long pos = resultSet.getLong("pos");
                result.put(pos, val);
            }
            statementObjList.close();
        } catch (SQLException e) {
            throw new DbException(e);
        }
        return result;
    }

    private void processSingletonSelectListStatement(Long from, PreparedStatement statement, TriConsumer<Object, Long, Long> consumer) throws DbException {
        try {
            statement.setLong(1, from);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Object val = resultSet.getObject("val");
                Long instanceOf = resultSet.getLong("instanceof");
                Long pos = resultSet.getLong("pos");
                consumer.accept(val, instanceOf, pos);
            }
            statement.close();
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    protected Long getObjectForAssociations(Long from, Long instanceOf) throws DbException {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT link.toObj FROM link WHERE link.fromObj = ? AND link.instanceOf = ?")
                          .withLong(from)
                          .withLong(instanceOf)
                          .forSingle(rs -> rs.getLong("toObj"));
    }

    protected Long getObjectForAssociationsOptional(Long from, Long instanceOf) throws DbException {
        return DbConnector.getConnector()
                          .getBuilderForFile("getObjectForAssociationsOptional")
                          .withLong(from)
                          .withLong(instanceOf)
                          .withLong(TransactionContext.getLongTaId())
                          .withLong(TransactionContext.getLongTaId())
                          .forSingleOptional(rs -> rs.getLong("toObj"))
                          .orElse(null);
    }

    public static Long initLink(Long from, Long instanceOf) throws DbException {
        Long id = DbConnector.getConnector()
                             .getBuilderFor("INSERT INTO link (pos, fromobj, instanceof, CREATEDBY) VALUES (-1, ?, ?, ?)")
                             .withLong(from)
                             .withLong(instanceOf)
                             .withLong(TransactionContext.getTransaction().getId())
                             .asManipulation()
                             .resolveKey();
        DbConnector.getConnector().commit();
        return id;
    }

    /**
     * Checks the given String against the Table category. Return false if the Category is not instance of the CategoryClass.
     */
    public static boolean validateStringReachedFromCategoryClass(Long categoryClassId, String category) throws DbException {
        Long results = DbConnector.getConnector()
                                  .getBuilderFor("SELECT count(val) FROM val_string, category " +
                                          "WHERE val_string.id = category.fk_string " +
                                          "AND category.instanceOf IN (SELECT sub FROM hierarchy WHERE super = ?) " +
                                          "AND val_string.val = ?")
                                  .withLong(categoryClassId)
                                  .withString(category)
                                  .forSingle(row -> row.getLong(1));
        return results == 1;
    }

    public static void addCategoryToCategoryClass(Long categoryClassId, String category) {
        Long id = saveToSingletonTable(category, "val_string");

        Long existingRows = DbConnector.getConnector()
                                       .getBuilderFor("SELECT count(*) FROM hierarchy, category " +
                                               "WHERE fk_string = ? " +
                                               "AND category.instanceOf IN (SELECT sub FROM hierarchy WHERE super = ?)")
                                       .withLong(id)
                                       .withLong(categoryClassId)
                                       .forSingle(row -> row.getLong(1));
        if (existingRows > 0) throw new CategoryAlreadyExistInHierarchyException(category);

        DbConnector.getConnector()
                   .getBuilderFor("INSERT INTO category(instanceOf, fk_string) VALUES (?, ?)")
                   .withLong(categoryClassId)
                   .withLong(id)
                   .submit();
    }

    public static void deletesCategoryFromCategoryClass(Long categoryClassId, String category) {
        Boolean isInUse = DbConnector.getConnector()
                                     .getBuilderFor("SELECT COUNT(1) FROM link, association, CLASS, VAL_STRING " +
                                             "WHERE link.TOOBJ = VAL_STRING.ID " +
                                             "AND VAL_STRING.VAL = ? " +
                                             "AND link.INSTANCEOF = ASSOCIATION.ID " +
                                             "AND ASSOCIATION.TARGET = ?")
                                     .withString(category)
                                     .withLong(categoryClassId)
                                     .forSingle(row -> row.getLong(1) > 0);
        if (isInUse)
            throw new CategoryInUseException();

        DbConnector.getConnector()
                   .getBuilderFor("DELETE FROM category WHERE fk_string = (SELECT id FROM val_string WHERE val = ?) AND instanceOf = ?")
                   .withString(category)
                   .withLong(categoryClassId)
                   .submit();
    }

    public static List<String> loadCategoriesInstanceOfCategoryClass(Long categoryClassId) {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT val_string.val FROM val_string, category " +
                                  "WHERE val_string.id = category.fk_string " +
                                  "AND category.instanceOf IN (SELECT sub FROM hierarchy WHERE super = ?)")
                          .withLong(categoryClassId)
                          .forEach(row -> row.getString(1));
    }

    public static List<CategoryVM> loadCategoriesInstanceOfCategoryClass(String categoryClassName) {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT val_string.val, FALSE AS derived FROM val_string, category " +
                                  "WHERE val_string.id = category.fk_string " +
                                  "AND category.instanceOf = (SELECT id FROM class WHERE class.name = ?) " +
                                  "UNION SELECT val_string.val, TRUE AS derived FROM val_string, category " +
                                  "WHERE val_string.id = category.fk_string " +
                                  "AND category.instanceOf IN (SELECT sub FROM hierarchy, class WHERE super = class.id AND class.name = ? AND sub != super) ")
                          .withString(categoryClassName)
                          .withString(categoryClassName)
                          .forEach(row -> {
                              CategoryVM result = CategoryVM.createUnchecked();
                              result.setName(row.getString(1));
                              result.setIsDerived(row.getBoolean(2));
                              return result;
                          });
    }

    public static List<CategoryClassVM> loadAllCategoryClasses() {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT class.name, class.id FROM class WHERE class.type = 'category_class'")
                          .forEach(row -> {
                              CategoryClassVM result = CategoryClassVM.createUnchecked();
                              result.setName(row.getString(1));
                              result.setDbId(row.getLong(2));
                              return result;
                          });
    }


    /**
     * Berechnet die umkehrassociation einer Association vom typ collection.
     * Also alle objekte, die mittels der gegebenen Association auf das Objekt zeigen
     */
    public static <T> List<T> deriveBacklinkCollection(Long idTo, Long association, Function<Long, T> mapper) {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT fromObj FROM link WHERE toObj = ? AND instanceof = ?")
                          .withLong(idTo)
                          .withLong(association)
                          .forEach(row -> row.getLong(1))
                          .stream()
                          .map(mapper::apply)
                          .collect(Collectors.toList());
    }

    /**
     * Berechnet die umkehrassociation einer Association vom typ optional.
     * Also alle objekte, die mittels der gegebenen Association auf das Objekt zeigen
     */
    public static <T> Optional<T> deriveBacklinkOptional(Long idTo, Long association, Function<Long, T> mapper) {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT fromObj FROM link WHERE toObj = ? AND instanceof = ?")
                          .withLong(idTo)
                          .withLong(association)
                          .forSingleOptional(row -> row.getLong(1))
                          .map(mapper::apply);
    }

    public static LongTransaction createLongTransaction() {
        Long key = DbConnector.getConnector()
                              .getBuilderFor("INSERT INTO LONGTRANSACTION (COMMITED, USERID) VALUES (NULL, ?)")
                              .withLong(SecurityContext.getId())
                              .resolveKey();
        return new LongTransaction(key);
    }

    public static ShortTransaction createShortTransaction(LongTransaction forLongTransaction, int id) {
        Long key = DbConnector.getConnector()
                              .getBuilderFor("INSERT INTO SHORTTRANSACTION (BELONGSTO, INSTANCEOF) VALUES (?, ?)")
                              .withLong(forLongTransaction.getId())
                              .withLong(id)
                              .resolveKey();
        return new ShortTransaction(key, forLongTransaction);
    }

    public static void commitLongTransaction(LongTransaction longTransaction) {
        DbConnector.getConnector()
                   .getBuilderFor("UPDATE LONGTRANSACTION SET COMMITED = ? WHERE ID = ?")
                   .withTimestamp(Timestamp.valueOf(LocalDateTime.now()))
                   .withLong(longTransaction.getId())
                   .submit();
    }

    public static List<Long> loadOpenLongTransactions() {
        return DbConnector.getConnector()
                          .getBuilderForFile("findOpenLongTransactionsForUser")
                          .withLong(SecurityContext.getId())
                          .forEach(row -> row.getLong(1));
    }
}
