package de.kaipho.core.db;

/**
 * Created by Neo on 08.02.17.
 */
public class DbFacadeImplH2 implements de.kaipho.core.db.DbFacadeImpl {
    @Override
    public void deleteObj(Long id) {
        DbConnector.getConnector()
                   .getBuilderFor("DELETE FROM object WHERE id = ?;")
                   .asManipulation()
                   .withLong(id)
                   .submit();

        DbConnector.getConnector()
                   .getBuilderFor("DELETE FROM link" +
                           "  WHERE id IN (SELECT link.id" +
                           "               FROM link, association" +
                           "               WHERE link.instanceof = association.id AND association.source = (SELECT name FROM class WHERE id = (SELECT instanceof FROM object WHERE id = ?))" +
                           "               AND link.fromobj = ?" +
                           "               UNION" +
                           "               SELECT link.id" +
                           "               FROM link, association" +
                           "               WHERE link.instanceof = association.id AND association.target = (SELECT name FROM class WHERE id = (SELECT instanceof FROM object WHERE id = ?)) " +
                           "               AND link.toobj = ?" +
                           "               UNION" +
                           "               SELECT link.id" +
                           "               FROM link" +
                           "               WHERE link.toobj = ? AND link.instanceof = -1)")
                   .asManipulation()
                   .withLong(id)
                   .withLong(id)
                   .withLong(id)
                   .withLong(id)
                   .withLong(id)
                   .submit();
    }
}
