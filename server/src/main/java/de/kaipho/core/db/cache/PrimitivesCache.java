package de.kaipho.core.db.cache;

import de.kaipho.domain.core.primitive.Rational;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Local caching of eg rationals.
 */
public class PrimitivesCache {
    private static de.kaipho.core.db.cache.PrimitivesCache instance = new de.kaipho.core.db.cache.PrimitivesCache();
    public static de.kaipho.core.db.cache.PrimitivesCache getInstance() {
        return instance;
    }


    private final Map<Long, Rational> rationals = new HashMap<>();

    public Optional<Long> findRational(Long numerator, Long denominator) {
        return rationals.keySet()
                        .stream()
                        .filter(it -> rationals.get(it).equals(new Rational(numerator, denominator)))
                        .findFirst();
    }

    public Optional<Rational> findRational(Long id) {
        System.out.println("Cache find rational: " + id);
        return Optional.ofNullable(rationals.get(id));
    }

    public void cacheRational(Long id, Rational r) {
        System.out.println("Cache put rational: " + id);
        rationals.put(id, r);
    }

    public void reset() {
        rationals.clear();
    }
}
