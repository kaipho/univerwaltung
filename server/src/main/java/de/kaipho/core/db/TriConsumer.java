package de.kaipho.core.db;

public interface TriConsumer<T, T1, T2> {
    void accept(T t, T1 u, T2 t2);
}
