package de.kaipho.core.db;

import java.util.ArrayList;
import java.util.List;

/**
 * Overrides the ArrayList to add the functionality to store data.
 */
public class StoredList<T> extends ArrayList<T> {
    private final Long association;
    private final DbFacade dbFacade;
    private final Long realId;

    public StoredList(Long association, DbFacade dbFacade, Long realId, List<T> l) {
        this.association = association;
        this.dbFacade = dbFacade;
        this.realId = realId;
        this.addAll(l);
    }

    @Override
    public boolean add(T t) {
        dbFacade.addAssociation(realId, t, association, (long) this.size());
        return super.add(t);
    }
}
