package de.kaipho.core.db;

import de.kaipho.core.Pair;

import java.util.List;
import java.util.Map;

/**
 * Functions to extract the class Model from the db.
 */
public class DbModel {
    public static List<String> getSubClassesOf(String className) {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT c2.name, c2.abstract FROM class as c1, class as c2, hierarchy as h where c1.id = h.super and c2.id = h.sub and c1.name = ? and h.derived = 0")
                          .asQuery()
                          .withString(className)
                          .forEach(rs -> {
                              String result = "";
                              if (rs.getInt("abstract") == 1) {
                                  result += "<<a>>\n";
                              }
                              result += rs.getString("name");
                              return result;
                          });
    }

    public static List<String> getAllDomainClasses() {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT name, abstract FROM class WHERE type = 'class' ORDER BY name ASC")
                          .asQuery()
                          .forEach(rs -> {
                              String result = "";
                              result += rs.getString("name");
                              if (rs.getInt("abstract") == 1) {
                                  result += " (abstract)";
                              }
                              return result;
                          });
    }

    public static List<String> getSimpleAssociationsOf(String clazz) {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT a.name, c2.name AS type FROM class AS c1, class AS c2, association AS a WHERE c1.name = ? AND a.source = c1.id AND a.target = c2.id AND c2.type != 'class'")
                          .asQuery()
                          .withString(clazz)
                          .forEach(rs -> new StringBuilder(rs.getString("name")).append(": ")
                                                                                .append(rs.getString("type"))
                                                                                .toString());
    }


    public static List<Pair<String, String>> getUserAssociationsOf(String clazz) {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT a.name, c2.name AS type, c2.abstract FROM class AS c1, class AS c2, association AS a WHERE c1.name = ? AND a.source = c1.id AND a.target = c2.id AND c2.type = 'class'")
                          .asQuery()
                          .withString(clazz)
                          .forEach(rs -> {
                              String result = "";
                              if (rs.getInt("abstract") == 1) {
                                  result += "<<a>>\n";
                              }
                              result += rs.getString("type");
                              return new Pair<>(rs.getString("name"), result);
                          });
    }
}
