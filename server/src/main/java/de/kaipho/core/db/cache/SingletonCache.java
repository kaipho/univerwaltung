package de.kaipho.core.db.cache;

import de.kaipho.core.db.DbConnector;
import de.kaipho.core.db.DbException;
import de.kaipho.core.db.DbLink;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Cache for Singleton Tables (Integer, String, ...)
 * The data in this tables is immutable!
 */
public class SingletonCache {
    private static de.kaipho.core.db.cache.SingletonCache ourInstance = new de.kaipho.core.db.cache.SingletonCache();

    public static de.kaipho.core.db.cache.SingletonCache getInstance() {
        return ourInstance;
    }

    private SingletonCache() {
        integer = new HashMap<>();
        strings = new HashMap<>();
        dates = new HashMap<>();
        doubles = new HashMap<>();
    }

    private final Map<Long, Long> integer;
    private final Map<Long, String> strings;
    private final Map<Long, LocalDate> dates;
    private final Map<Long, Double> doubles;

    public Long getInteger(Map<Long, List<DbLink>> links, Long instanceOf) {
        return get(links, instanceOf, this::getInteger);
    }

    public Long getInteger(Long id) {
        if (id == null) return null;
        Long result = integer.computeIfAbsent(id, key -> DbConnector.getConnector()
                                                                       .getBuilderFor("SELECT val FROM val_integer WHERE id = ?")
                                                                       .withLong(id)
                                                                       .asQuery()
                                                                       .forSingle(row -> row.getLong(1)));
        System.out.println("Cache find int: " + id + " | " + result);
        return result;
    }

    public void setInteger(Long id, Long integer) {
        System.out.println("Cache put int: " + id);
        this.integer.put(id, integer);
    }

    public String getString(Map<Long, List<DbLink>> links, Long instanceOf) {
        return get(links, instanceOf, this::getString);
    }

    public String getString(Long id) {
        if (id == null || id == 0) return null;
        String result = strings.computeIfAbsent(id, key -> DbConnector.getConnector()
                                                                      .getBuilderFor("SELECT val FROM val_string WHERE id = ?")
                                                                      .withLong(id)
                                                                      .asQuery()
                                                                      .forSingle(row -> row.getString(1)));
        System.out.println("Cache find strings: " + id + " | " + result);
        return result;
    }

    public void setString(Long id, String string) {
        System.out.println("Cache put strings: " + id);
        this.strings.put(id, string);
    }

    public LocalDate getLocalDate(Map<Long, List<DbLink>> links, Long instanceOf) {
        return get(links, instanceOf, this::getLocalDate);
    }

    public LocalDate getLocalDate(Long id) {
        if (id == null) return null;
        LocalDate result = dates.computeIfAbsent(id, key -> DbConnector.getConnector()
                                                                       .getBuilderFor("SELECT val FROM val_datetime WHERE id = ?")
                                                                       .withLong(id)
                                                                       .asQuery()
                                                                       .forSingle(row -> row.getDate(1).toLocalDate()));
        System.out.println("Cache find LocalDate: " + id + " | " + result);
        return result;
    }

    public void setLocalDate(Long id, LocalDate date) {
        System.out.println("Cache put LocalDate: " + id);
        this.dates.put(id, date);
    }

    public Double getDouble(Map<Long, List<DbLink>> links, Long instanceOf) {
        return get(links, instanceOf, this::getDouble);
    }

    public Double getDouble(Long id) {
        if (id == null) return null;
        Double result = doubles.computeIfAbsent(id, key -> DbConnector.getConnector()
                                                                      .getBuilderFor("SELECT val FROM val_double WHERE id = ?")
                                                                      .withLong(id)
                                                                      .asQuery()
                                                                      .forSingle(row -> row.getDouble(1)));
        System.out.println("Cache find double: " + id + " | " + result);
        return result;
    }

    public void setDouble(Long id, double d) {
        System.out.println("Cache put double: " + id);
        this.doubles.put(id, d);
    }

    public Boolean getBoolean(Map<Long, List<DbLink>> links, Long instanceOf) {
        return get(links, instanceOf, this::getBoolean);
    }

    public Boolean getBoolean(Long id) {
        if (id == null) return null;
        boolean result = integer.computeIfAbsent(id, key -> DbConnector.getConnector()
                                                                       .getBuilderFor("SELECT val FROM val_double WHERE id = ?")
                                                                       .withLong(id)
                                                                       .asQuery()
                                                                       .forSingle(row -> row.getLong(1))) != 0;
        System.out.println("Cache find boolean: " + id + " | " + result);
        return result;
    }

    public void setBoolean(Long id, boolean b) {
        System.out.println("Cache put boolean: " + id);
        this.integer.put(id, b ? 1L : 0L);
    }

    public de.kaipho.core.db.blob.Blob getBlob(Map<Long, List<DbLink>> linksMap, Long instanceOf) {
        List<DbLink> links = linksMap.get(instanceOf);
        if(links.isEmpty()) {
            return null;
        } if(links.size() > 1) {
            throw new DbException("Uniques violated!");
        }
        DbLink link = links.get(0);
        String name = de.kaipho.core.db.cache.SingletonCache.getInstance().getString(link.getName());
        return new de.kaipho.core.db.blob.Blob(link.getId(), name, link.getToObj());
    }

    private  <T> T get(Map<Long, List<DbLink>> linksMap, Long instanceOf, Function<Long, T> f) {
        List<DbLink> links = linksMap.get(instanceOf);
        if(links == null || links.isEmpty()) {
            return null;
        } if(links.size() > 1) {
            throw new DbException("Uniques violated!");
        }
        return f.apply(links.get(0).getToObj());
    }
}
