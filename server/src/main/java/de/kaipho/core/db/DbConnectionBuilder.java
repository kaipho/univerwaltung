package de.kaipho.core.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Builder to build a db access.
 */
public class DbConnectionBuilder {
    private Function<PreparedStatement, ResultSet> accessFunction;
    private Consumer<ResultSet> resultConsumer;
    private final PreparedStatement preparedStatement;
    private int posCounter = 1;

    DbConnectionBuilder(PreparedStatement preparedStatement) {
        this.preparedStatement = preparedStatement;
    }

    public DbConnectionBuilder asQuery() {
        accessFunction = (ps) -> {
            try {
                return ps.executeQuery();
            } catch (SQLException e) {
                throw new DbException(e);
            }
        };
        return this;
    }

    public DbConnectionBuilder asManipulation() {
        accessFunction = (ps) -> {
            try {
                ps.executeUpdate();
                return null;
            } catch (SQLException e) {
                throw new DbException(e);
            }
        };
        return this;
    }

    public DbConnectionBuilder withLong(long obj) {
        try {
            preparedStatement.setLong(posCounter, obj);
            posCounter++;
        } catch (SQLException e) {
            throw new DbException(e);
        }
        return this;
    }

    public DbConnectionBuilder withTimestamp(Timestamp obj) {
        try {
            preparedStatement.setTimestamp(posCounter, obj);
            posCounter++;
        } catch (SQLException e) {
            throw new DbException(e);
        }
        return this;
    }

    public DbConnectionBuilder withObj(Object obj) {
        try {
            preparedStatement.setObject(posCounter, obj);
            posCounter++;
        } catch (SQLException e) {
            throw new DbException(e);
        }
        return this;
    }

    public DbConnectionBuilder withString(String obj) {
        try {
            preparedStatement.setString(posCounter, obj);
            posCounter++;
        } catch (SQLException e) {
            throw new DbException(e);
        }
        return this;
    }


    public <R> List<R> forEach(SqlFunction<R> consumer) {
        asQuery();
        ResultSet rs = accessFunction.apply(preparedStatement);
        List<R> result = new ArrayList<>();
        try {
            while (rs.next()) {
                result.add(consumer.apply(rs));
            }
            rs.close();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new DbException(e);
        }
        return result;
    }

    public <R> R forSingle(SqlFunction<R> consumer) {
        asQuery();
        ResultSet rs = accessFunction.apply(preparedStatement);
        try {
            if (rs.next()) {
                R result = consumer.apply(rs);
                rs.close();
                preparedStatement.close();
                return result;
            } else {
                throw new DbException("No Result!");
            }
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public <R> Optional<R> forSingleOptional(SqlFunction<R> consumer) {
        asQuery();
        ResultSet rs = accessFunction.apply(preparedStatement);
        try {
            if (rs.next()) {
                R result = consumer.apply(rs);
                rs.close();
                preparedStatement.close();
                return Optional.of(result);
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public Long resolveKey() {
        asManipulation();
        accessFunction.apply(preparedStatement);
        try {
            ResultSet keys = preparedStatement.getGeneratedKeys();
            if (keys.next()) {
                Long key = keys.getLong(1);
                keys.close();
                preparedStatement.close();
                return key;
            } else throw new DbException();
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public void submit() {
        asManipulation();
        try {
            accessFunction.apply(preparedStatement);
            preparedStatement.close();
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }
}
