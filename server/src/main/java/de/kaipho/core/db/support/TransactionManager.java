package de.kaipho.core.db.support;

import de.kaipho.core.db.DbConnector;

import java.util.function.Consumer;
import java.util.function.Function;

public class TransactionManager {

    public static <R> de.kaipho.core.db.support.TransactionContext<R> execute(Function<de.kaipho.core.db.support.TransactionContext<R>, R> executable) {
        throw new RuntimeException("Not Implemented!");
    }

    public static de.kaipho.core.db.support.TransactionContext<Void> execute(String name, Consumer<de.kaipho.core.db.support.TransactionContext<Void>> executable) {
        DbConnector.getConnector().startTransaction();
        System.out.println("Transaction gestartet");
        de.kaipho.core.db.support.TransactionContext<Void> context = new de.kaipho.core.db.support.TransactionContext<>();

        try {
            executable.accept(context);
            System.out.println("commit");
            DbConnector.getConnector().commitTransaction();
            return context;
        } catch (RuntimeException e) {
            context.setE(e);
            e.printStackTrace();
            System.out.println("rollback");
            DbConnector.getConnector().rollback();
            return context;
        }
    }

}
