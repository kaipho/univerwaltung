package de.kaipho.core.db;

import java.nio.charset.Charset;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Connector used to get connections to the DB.
 */
public class DbConnector {
    private final Connection connection;
    private static DbConnector connector;

    // H2
    private static final String url = "jdbc:postgresql://172.17.0.4:5432/top";
    private static final String username = "gen";
    private static final String password = "nyd4vmlKjbEMibdJ";

    private int counter = 0;

    private DbConnector(Connection connection) {
        this.connection = connection;
    }

    /**
     * Creates a new {@link DbConnector}. Also validates, that all needed tables exist.
     */
    public static DbConnector createDbConnection() {
        try {
            Class.forName("org.h2.Driver");
            Connection connection = DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(false);
            connector = new DbConnector(connection);
            connector.createTablesIfNotExist("PgSql");
            return connector;
        } catch (SQLException | ClassNotFoundException | URISyntaxException | IOException e) {
            connector.rollback();
            return connector;
        }
    }

    /**
     * Creates a new {@link DbConnector}. Also validates, that all needed tables exist.
     */
    public static DbConnector createInMemoryDbConnection() {
        try {
            Class.forName("org.h2.Driver");
            Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE", "sa", "");
            connection.setAutoCommit(false);
            connector = new DbConnector(connection);
            connector.createTablesIfNotExist("");
            connector.commit();
            return connector;
        } catch (SQLException | ClassNotFoundException | URISyntaxException | IOException e) {
            e.printStackTrace();
            connector.rollback();
            throw new DbException();
        }
    }

    public static DbConnector getConnector() throws DbException {
        if (connector == null) {
            throw new DbException("No connector initialized!");
        }
        return connector;
    }

    public PreparedStatement getStatementFor(String sql) throws DbException {
//        System.out.println(counter++);
        try {
            return connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public CallableStatement getFunctionCall(String sql) throws DbException {
//        System.out.println(counter++);
        try {
            return connection.prepareCall(sql);
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    /**
     * Returns a builder to build db calls.
     */
    public DbConnectionBuilder getBuilderFor(String sql) throws DbException {
//        System.out.println(counter++);
        try {
            return new DbConnectionBuilder(connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS));
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    private static Map<String, String> sqlFiles = new HashMap<>();
    /**
     * Returns a builder to build db calls.
     */
    public DbConnectionBuilder getBuilderForFile(String filename) throws DbException {
        try {
            if (!sqlFiles.containsKey(filename)) {
                sqlFiles.put(filename, IOUtils.toString(getClass().getResourceAsStream("/database/h2/" + filename + ".sql"), Charset.defaultCharset()));
            }
            return new DbConnectionBuilder(connection.prepareStatement(sqlFiles.get(filename), Statement.RETURN_GENERATED_KEYS));
        } catch (IOException | SQLException e) {
            throw new DbException(e);
        }
    }

    private boolean inTransaction = false;

    public void startTransaction() {
        inTransaction = true;
    }

    public void commit() throws DbException {
        if (inTransaction)
            return;
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public void commitTransaction() throws DbException {
        inTransaction = false;
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public void rollback() throws DbException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    public void closeConnection() throws DbException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }

    /**
     * Creates all needed tables if they do not exist.
     */
    private void createTablesIfNotExist(String type) throws SQLException, URISyntaxException, IOException {
        InputStream stream = getClass().getResourceAsStream("/database/init" + type + ".sql");
        PreparedStatement statement = connection.prepareStatement(new String(IOUtils.toByteArray(stream)));
        statement.execute();
        statement.close();
    }
}
