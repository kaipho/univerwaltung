package de.kaipho.core.transaction;

import de.kaipho.service.transaction.TransactionService;

import java.util.function.Supplier;

public class Transaction {
    private static Transaction ourInstance = new Transaction();

    public static Transaction getInstance() {
        return ourInstance;
    }

    private Transaction() {}

    /**
     * Executes the transaction logic. To get the result, call transaction.resolve().
     * @param calledMethod The method (method must be modelled in the metamodel), on which the logic get called.
     * @param logic The inner logic of the transaction.
     * @param <R> The result type of the transaction.
     * @return the {@link TransactionExecutor}
     */
    public <R> TransactionExecutor<R> run(Callable calledMethod, Supplier<R> logic) {
        if(!TransactionContext.isInLongTransaction()) {
            TransactionService.getInstance().startTransaction();
        }
        TransactionExecutor<R> transaction = new TransactionExecutor<>(logic, TransactionContext.getLongTransaction(), calledMethod);
        transaction.execute();
        return transaction;
    }

    @Deprecated
    public <R> TransactionExecutor<R> run(Supplier<R> logic) {
        if(!TransactionContext.isInLongTransaction()) {
            TransactionContext.startLongTransaction();
        }
        TransactionExecutor<R> transaction = new TransactionExecutor<>(logic, TransactionContext.getLongTransaction(), Callable.DEFAULT);
        transaction.execute();
        return transaction;
    }
}
