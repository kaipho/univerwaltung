package de.kaipho.core.transaction;

import de.kaipho.core.db.DbFacade;

/**
 * Representation of a long transaction from the metamodell.
 */
public class LongTransaction {
    private final long id;


    public LongTransaction(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void commit() {
        DbFacade.commitLongTransaction(this);
        TransactionContext.reset();
    }
}
