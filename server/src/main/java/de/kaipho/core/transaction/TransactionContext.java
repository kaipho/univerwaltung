package de.kaipho.core.transaction;

import de.kaipho.core.db.DbFacade;

/**
 * Context to store transaction information. The information should be set in the controller based on the request session.
 * The information is stored only for the thread!
 */
public class TransactionContext {
    private static ThreadLocal<ShortTransaction> transaction = new ThreadLocal<>();
    private static ThreadLocal<LongTransaction> longTransaction = new ThreadLocal<>();

    public static ShortTransaction getTransaction() {
        return TransactionContext.transaction.get();
    }

    public static LongTransaction getLongTransaction() {
        return TransactionContext.longTransaction.get();
    }

    public static void setTransaction(ShortTransaction transaction) {
        TransactionContext.transaction = ThreadLocal.withInitial(() -> transaction);
    }

    public static void setLongTransaction(LongTransaction longTransaction) {
        TransactionContext.longTransaction = ThreadLocal.withInitial(() -> longTransaction);
    }

    public static boolean isInTransaction() {
        return transaction.get() != null;
    }

    public static void reset() {
        transaction.set(null);
        longTransaction.set(null);
    }

    public static boolean isInLongTransaction() {
        return longTransaction.get() != null;
    }

    public static void startLongTransaction() {
        if (isInTransaction()) throw new RuntimeException("Already in Transaction!");
        LongTransaction transaction = DbFacade.createLongTransaction();
        setLongTransaction(transaction);
    }

    public static long getLongTaId() {
        if (getLongTransaction() == null) {
            if (getTransaction() == null) return -1;
            return getTransaction().getBelongsTo().getId();
        }
        return getLongTransaction().getId();
    }
}
