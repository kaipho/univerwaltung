package de.kaipho.core.transaction;

import de.kaipho.core.Constants;
import de.kaipho.core.db.DbFacade;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TransactionFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String transactionId = ((HttpServletRequest) servletRequest).getHeader(Constants.TRANSACTION_HEADER);
        if (transactionId != null && !"".equals(transactionId)) {
            TransactionContext.setLongTransaction(new LongTransaction(Long.parseLong(transactionId)));
        } else {
            TransactionContext.reset();
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
