package de.kaipho.service.transaction;

import java.util.List;
import de.kaipho.domain.db.StatistikVM;
import java.util.ArrayList;
import de.kaipho.domain.transaction.LongTransactionVM;

public interface TransactionService {

    /**
     * Commits the actual long-transaction from the users session.
     */
    void commit();
    /**
     * Starts a long-transaction.
     */
    void startTransaction();
    /**
     * Loads all uncommitted transactions of the user.
     */
    List<LongTransactionVM> openTransactions();
    /**
     * Fetch some statistics about transactions.
     */
    List<StatistikVM> transactionStatistics();

    public static TransactionService getInstance() {
        TransactionService instance = new TransactionServiceImpl();
        return new TransactionServiceSecurityProxy(instance);
    }
}
