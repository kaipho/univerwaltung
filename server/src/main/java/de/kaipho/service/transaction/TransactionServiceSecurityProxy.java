package de.kaipho.service.transaction;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;

import java.util.List;
import de.kaipho.domain.db.StatistikVM;
import java.util.ArrayList;
import de.kaipho.domain.transaction.LongTransactionVM;

class TransactionServiceSecurityProxy implements TransactionService {

    private final TransactionService real;
    
    TransactionServiceSecurityProxy(TransactionService real) {
        this.real = real;
    }

    public void commit() {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.commit();
    }
    public void startTransaction() {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.startTransaction();
    }
    public List<LongTransactionVM> openTransactions() {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.openTransactions();
    }
    public List<StatistikVM> transactionStatistics() {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.transactionStatistics();
    }
}
