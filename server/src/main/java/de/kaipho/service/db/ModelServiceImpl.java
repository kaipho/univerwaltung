package de.kaipho.service.db;

import org.springframework.stereotype.Service;
import java.util.List;
import de.kaipho.domain.db.StatistikVM;
import java.util.ArrayList;
import de.kaipho.core.db.DbStatistic;
import guru.nidi.graphviz.attribute.Attributes;
import java.util.*;
import de.kaipho.core.Pair;
import de.kaipho.core.db.DbModel;
import guru.nidi.graphviz.attribute.Records;
import guru.nidi.graphviz.model.Graph;
import guru.nidi.graphviz.model.Link;
import guru.nidi.graphviz.model.Node;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static guru.nidi.graphviz.attribute.Records.rec;
import static guru.nidi.graphviz.attribute.Records.turn;
import static guru.nidi.graphviz.model.Factory.graph;
import static guru.nidi.graphviz.model.Factory.node;
import static guru.nidi.graphviz.model.Factory.to;
// imports_end

@Service
public class ModelServiceImpl implements ModelService {

    private ModelService self() {
        return this;
    }

    // editable_area_start
    public List<StatistikVM> modelStatistics() {
        List<StatistikVM> result = new ArrayList<>();
        StatistikVM t1 = StatistikVM.createUnchecked();
        t1.setName("obj");
        t1.setValue(DbStatistic.fetchOverallObjCount());
        result.add(t1);
        t1 = StatistikVM.createUnchecked();
        t1.setName("link");
        t1.setValue(DbStatistic.fetchOverallLinkCount());
        result.add(t1);
        t1 = StatistikVM.createUnchecked();
        t1.setName("integer");
        t1.setValue(DbStatistic.fetchOverallIntegerCount());
        result.add(t1);
        t1 = StatistikVM.createUnchecked();
        t1.setName("double");
        t1.setValue(DbStatistic.fetchOverallDoubleCount());
        result.add(t1);
        t1 = StatistikVM.createUnchecked();
        t1.setName("date");
        t1.setValue(DbStatistic.fetchOverallDateTimeCount());
        result.add(t1);
        t1 = StatistikVM.createUnchecked();
        t1.setName("string");
        t1.setValue(DbStatistic.fetchOverallStringCount());
        result.add(t1);
        t1 = StatistikVM.createUnchecked();
        t1.setName("blob");
        t1.setValue(DbStatistic.fetchOverallBlobCount());
        result.add(t1);
        t1 = StatistikVM.createUnchecked();
        t1.setName("rational");
        t1.setValue(DbStatistic.fetchOverallRationalCount());
        result.add(t1);
        return result;
    }
    public String getModelForClass(String clazz) {
       // TODO implement String getModelForClass(...)
       throw new RuntimeException("Not implemented");
    }
    public List<String> getAllClasses() {
        return DbModel.getAllDomainClasses();
    }

    // editable_area_end

    // private_area_start
    private Map<String, Node> nodes = new HashMap<>();

    private Node getNode(String name) {
        if(nodes.containsKey("<<a>>\n" + name))
            return nodes.get("<<a>>\n" + name);
        return nodes.computeIfAbsent(name, (key) -> node(key).with(Records.of(turn(rec(key), getSimpleAssociationAsRecord(key)))));
    }

    private Graph calculateSubgraph(Graph g, Node actual, String name) {
        List<Node> nodes = new ArrayList<>();
        List<String> children = DbModel.getSubClassesOf(name.replace("<<a>>\n", ""));
        if (!children.isEmpty()) {
            for (String it : children) {
                if(this.nodes.containsKey(it)) {
                    Node actualNode = getNode(it);
                    calculateAssociations(g, actualNode, it, nodes);
                    continue;
                }
                Node actualNode = getNode(it);
                nodes.add(actualNode);
                g = g.with(actualNode);
                g = calculateSubgraph(g, actualNode, it);
            }
        }
        g = calculateAssociations(g, actual, name, nodes);
        return g;
    }

    private Graph calculateAssociations(Graph g, Node actual, String name, List<Node> nodes) {
        List<Pair<String, String>> associations = DbModel.getUserAssociationsOf(name.replace("<<a>>\n", ""));
        Link[] nodesArr = new Link[nodes.size() + associations.size()];
        g = g.with(actual.link(Stream.concat(getSuperClassesStream(nodes), getAssociationStream(associations))
                                     .collect(Collectors.toList())
                                     .toArray(nodesArr)));
        return g;
    }

    private String getSimpleAssociationAsRecord(String clazzName) {
        return rec(DbModel.getSimpleAssociationsOf(clazzName.replace("<<a>>\n", ""))
                          .stream()
                          .map(it -> it + "\\l")
                          .reduce("", (s1, s2) -> s1 + s2));
    }

    private Stream<Link> getAssociationStream(List<Pair<String, String>> associations) {
        return associations.stream()
                           .map(it -> to(getNode(it.getT2())).with(Attributes.attr("arrowtail", "vee"), Attributes.attr("xlabel", it.getT1())));
    }

    private Stream<Link> getSuperClassesStream(List<Node> nodes) {
        return nodes.stream()
                    .map(it -> to(it).with(Attributes.attr("dir", "back"), Attributes.attr("arrowtail", "empty")));
    }

    // private_area_end
}
