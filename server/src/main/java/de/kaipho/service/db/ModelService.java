package de.kaipho.service.db;

import java.util.List;
import de.kaipho.domain.db.StatistikVM;
import java.util.ArrayList;

public interface ModelService {

    String getModelForClass(String clazz);
    List<String> getAllClasses();
    List<StatistikVM> modelStatistics();

    public static ModelService getInstance() {
        ModelService instance = new ModelServiceImpl();
        return new ModelServiceSecurityProxy(instance);
    }
}
