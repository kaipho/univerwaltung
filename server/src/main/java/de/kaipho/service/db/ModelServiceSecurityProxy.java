package de.kaipho.service.db;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;

import java.util.List;
import de.kaipho.domain.db.StatistikVM;
import java.util.ArrayList;

class ModelServiceSecurityProxy implements ModelService {

    private final ModelService real;
    
    ModelServiceSecurityProxy(ModelService real) {
        this.real = real;
    }

    public String getModelForClass(String clazz) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.getModelForClass(clazz);
    }
    public List<String> getAllClasses() {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.getAllClasses();
    }
    public List<StatistikVM> modelStatistics() {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.modelStatistics();
    }
}
