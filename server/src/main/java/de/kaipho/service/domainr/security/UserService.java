package de.kaipho.service.domainr.security;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.uni.Person;
import de.kaipho.domain.security.UserCompleteVisitor;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.domain.security.AnonymUser;
import de.kaipho.domain.security.User;

/**
 * Implements a DataRequestService for Azubi
 */
public class UserService implements DataRequestService<User> {
    @Override
    public ListVM<User> findAll() throws DbException {
        ListVM<User> result = new ListVM<>();

        User.findAll().forEach(it ->
            it.accept(new UserCompleteVisitor<User>() {
            @Override
            public User visit(Person it) {
                result.addSingleToData("person", new ObjVM<>(it));
                return it;
            }
            @Override
            public User visit(AnonymUser it) {
                result.addSingleToData("anonymUser", new ObjVM<>(it));
                return it;
            }
            @Override
            public User visit(TechnicalUser it) {
                result.addSingleToData("technicalUser", new ObjVM<>(it));
                return it;
            }
            }
        ));

        return result;
    }

    @Override
    public ListVM<User> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<User> result = new ListVM<>();

        User.findBy(associationId, val).forEach(it ->
            it.accept(new UserCompleteVisitor<User>() {
            @Override
            public User visit(Person it) {
                result.addSingleToData("person", new ObjVM<>(it));
                return it;
            }
            @Override
            public User visit(AnonymUser it) {
                result.addSingleToData("anonymUser", new ObjVM<>(it));
                return it;
            }
            @Override
            public User visit(TechnicalUser it) {
                result.addSingleToData("technicalUser", new ObjVM<>(it));
                return it;
            }
            }
        ));

        return result;
    }

    @Override
    public ObjVM<User> findSingle(Long id) throws DbException {
        return new ObjVM<>(User.findOneById(id));
    }
}
