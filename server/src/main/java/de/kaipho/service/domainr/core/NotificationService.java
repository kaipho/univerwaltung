package de.kaipho.service.domainr.core;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.core.Notification;

/**
 * Implements a DataRequestService for Azubi
 */
public class NotificationService implements DataRequestService<Notification> {
    @Override
    public ListVM<Notification> findAll() throws DbException {
        ListVM<Notification> result = new ListVM<>();

        Notification.findAll().forEach(it ->
            result.addSingleToData("notification", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<Notification> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<Notification> result = new ListVM<>();

        Notification.findBy(associationId, val).forEach(it ->
            result.addSingleToData("notification", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<Notification> findSingle(Long id) throws DbException {
        return new ObjVM<>(Notification.findOneById(id));
    }
}
