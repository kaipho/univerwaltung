package de.kaipho.service.domainr.transaction;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.transaction.LongTransactionVM;

/**
 * Implements a DataRequestService for Azubi
 */
public class LongTransactionVMService implements DataRequestService<LongTransactionVM> {
    @Override
    public ListVM<LongTransactionVM> findAll() throws DbException {
        ListVM<LongTransactionVM> result = new ListVM<>();

        LongTransactionVM.findAll().forEach(it ->
            result.addSingleToData("longTransactionVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<LongTransactionVM> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<LongTransactionVM> result = new ListVM<>();

        LongTransactionVM.findBy(associationId, val).forEach(it ->
            result.addSingleToData("longTransactionVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<LongTransactionVM> findSingle(Long id) throws DbException {
        return new ObjVM<>(LongTransactionVM.findOneById(id));
    }
}
