package de.kaipho.service.domainr.core;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.core.CategoryClassVM;

/**
 * Implements a DataRequestService for Azubi
 */
public class CategoryClassVMService implements DataRequestService<CategoryClassVM> {
    @Override
    public ListVM<CategoryClassVM> findAll() throws DbException {
        ListVM<CategoryClassVM> result = new ListVM<>();

        CategoryClassVM.findAll().forEach(it ->
            result.addSingleToData("categoryClassVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<CategoryClassVM> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<CategoryClassVM> result = new ListVM<>();

        CategoryClassVM.findBy(associationId, val).forEach(it ->
            result.addSingleToData("categoryClassVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<CategoryClassVM> findSingle(Long id) throws DbException {
        return new ObjVM<>(CategoryClassVM.findOneById(id));
    }
}
