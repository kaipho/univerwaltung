package de.kaipho.service.domainr.security;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.uni.Person;
import de.kaipho.domain.security.CustomUserCompleteVisitor;
import de.kaipho.domain.security.CustomUser;

/**
 * Implements a DataRequestService for Azubi
 */
public class CustomUserService implements DataRequestService<CustomUser> {
    @Override
    public ListVM<CustomUser> findAll() throws DbException {
        ListVM<CustomUser> result = new ListVM<>();

        CustomUser.findAll().forEach(it ->
            it.accept(new CustomUserCompleteVisitor<CustomUser>() {
            @Override
            public CustomUser visit(Person it) {
                result.addSingleToData("person", new ObjVM<>(it));
                return it;
            }
            }
        ));

        return result;
    }

    @Override
    public ListVM<CustomUser> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<CustomUser> result = new ListVM<>();

        CustomUser.findBy(associationId, val).forEach(it ->
            it.accept(new CustomUserCompleteVisitor<CustomUser>() {
            @Override
            public CustomUser visit(Person it) {
                result.addSingleToData("person", new ObjVM<>(it));
                return it;
            }
            }
        ));

        return result;
    }

    @Override
    public ObjVM<CustomUser> findSingle(Long id) throws DbException {
        return new ObjVM<>(CustomUser.findOneById(id));
    }
}
