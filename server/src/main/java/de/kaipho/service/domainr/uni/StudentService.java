package de.kaipho.service.domainr.uni;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.uni.Student;

/**
 * Implements a DataRequestService for Azubi
 */
public class StudentService implements DataRequestService<Student> {
    @Override
    public ListVM<Student> findAll() throws DbException {
        ListVM<Student> result = new ListVM<>();

        Student.findAll().forEach(it ->
            result.addSingleToData("student", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<Student> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<Student> result = new ListVM<>();

        Student.findBy(associationId, val).forEach(it ->
            result.addSingleToData("student", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<Student> findSingle(Long id) throws DbException {
        return new ObjVM<>(Student.findOneById(id));
    }
}
