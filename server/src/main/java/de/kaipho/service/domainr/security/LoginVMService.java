package de.kaipho.service.domainr.security;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.security.LoginVM;

/**
 * Implements a DataRequestService for Azubi
 */
public class LoginVMService implements DataRequestService<LoginVM> {
    @Override
    public ListVM<LoginVM> findAll() throws DbException {
        ListVM<LoginVM> result = new ListVM<>();

        LoginVM.findAll().forEach(it ->
            result.addSingleToData("loginVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<LoginVM> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<LoginVM> result = new ListVM<>();

        LoginVM.findBy(associationId, val).forEach(it ->
            result.addSingleToData("loginVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<LoginVM> findSingle(Long id) throws DbException {
        return new ObjVM<>(LoginVM.findOneById(id));
    }
}
