package de.kaipho.service.domainr.core;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.core.CategoryVM;

/**
 * Implements a DataRequestService for Azubi
 */
public class CategoryVMService implements DataRequestService<CategoryVM> {
    @Override
    public ListVM<CategoryVM> findAll() throws DbException {
        ListVM<CategoryVM> result = new ListVM<>();

        CategoryVM.findAll().forEach(it ->
            result.addSingleToData("categoryVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<CategoryVM> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<CategoryVM> result = new ListVM<>();

        CategoryVM.findBy(associationId, val).forEach(it ->
            result.addSingleToData("categoryVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<CategoryVM> findSingle(Long id) throws DbException {
        return new ObjVM<>(CategoryVM.findOneById(id));
    }
}
