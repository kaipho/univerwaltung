package de.kaipho.service.domainr.exception;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.exception.ErrorVM;

/**
 * Implements a DataRequestService for Azubi
 */
public class ErrorVMService implements DataRequestService<ErrorVM> {
    @Override
    public ListVM<ErrorVM> findAll() throws DbException {
        ListVM<ErrorVM> result = new ListVM<>();

        ErrorVM.findAll().forEach(it ->
            result.addSingleToData("errorVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<ErrorVM> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<ErrorVM> result = new ListVM<>();

        ErrorVM.findBy(associationId, val).forEach(it ->
            result.addSingleToData("errorVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<ErrorVM> findSingle(Long id) throws DbException {
        return new ObjVM<>(ErrorVM.findOneById(id));
    }
}
