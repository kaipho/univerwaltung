package de.kaipho.service.domainr.security;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.security.AnonymUser;

/**
 * Implements a DataRequestService for Azubi
 */
public class AnonymUserService implements DataRequestService<AnonymUser> {
    @Override
    public ListVM<AnonymUser> findAll() throws DbException {
        ListVM<AnonymUser> result = new ListVM<>();

        AnonymUser.findAll().forEach(it ->
            result.addSingleToData("anonymUser", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<AnonymUser> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<AnonymUser> result = new ListVM<>();

        AnonymUser.findBy(associationId, val).forEach(it ->
            result.addSingleToData("anonymUser", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<AnonymUser> findSingle(Long id) throws DbException {
        return new ObjVM<>(AnonymUser.findOneById(id));
    }
}
