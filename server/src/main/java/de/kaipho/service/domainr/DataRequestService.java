package de.kaipho.service.domainr;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.domain.core.PersistentElement;

import java.util.HashMap;
import java.util.List;

import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.core.Notification;
import de.kaipho.service.domainr.exception.ErrorVMService;
import de.kaipho.domain.uni.Person;
import de.kaipho.service.domainr.security.LoginVMService;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.service.domainr.uni.DozentService;
import de.kaipho.service.domainr.core.CategoryClassVMService;
import de.kaipho.service.domainr.security.AnonymUserService;
import de.kaipho.domain.uni.Dozent;
import de.kaipho.domain.transaction.LongTransactionVM;
import de.kaipho.service.domainr.core.NotificationService;
import de.kaipho.service.domainr.security.CustomUserService;
import de.kaipho.service.domainr.uni.VorlesungService;
import de.kaipho.service.domainr.uni.PersonRolleService;
import de.kaipho.domain.core.CategoryVM;
import de.kaipho.domain.db.StatistikVM;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.service.domainr.transaction.LongTransactionVMService;
import de.kaipho.domain.security.LoginVM;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.exception.ErrorVM;
import de.kaipho.domain.core.CategoryClassVM;
import de.kaipho.service.domainr.security.UserService;
import de.kaipho.service.domainr.uni.PersonService;
import de.kaipho.service.domainr.uni.StudentService;
import de.kaipho.domain.security.AnonymUser;
import de.kaipho.service.domainr.uni.FachbereichService;
import de.kaipho.service.domainr.security.TechnicalUserService;
import de.kaipho.service.domainr.core.CategoryVMService;
import de.kaipho.service.domainr.db.StatistikVMService;
import de.kaipho.domain.security.CustomUser;
import de.kaipho.service.domainr.uni.RaumService;
import de.kaipho.domain.security.User;

/**
 * Service definition to make requesting/deleting data possible.
 * Create and update actions will be avaible while manipulation the object via a proxy.
 */
public interface DataRequestService<T extends PersistentElement> {
    /**
     * Gets all elements of the given type.
     * TODO paging
     */
    ListVM<T> findAll() throws DbException;

    /**
     * Limits the search with an check on one association.
     */
    ListVM<T> findAllWithAssociationLike(Long associationId, Object val) throws DbException;

    /**
     * Finds the element to the given id.
     */
    ObjVM<T> findSingle(Long id) throws DbException;

    // TODO delete

    HashMap<Class<? extends PersistentElement>, DataRequestService> requestServices = new HashMap<>();

    public static <R extends PersistentElement> DataRequestService<R> getInstance(Class<R> forClass) {
        if(requestServices.isEmpty()) {
            requestServices.put(Person.class, new PersonService());
            requestServices.put(PersonRolle.class, new PersonRolleService());
            requestServices.put(Student.class, new StudentService());
            requestServices.put(Dozent.class, new DozentService());
            requestServices.put(Vorlesung.class, new VorlesungService());
            requestServices.put(Fachbereich.class, new FachbereichService());
            requestServices.put(Raum.class, new RaumService());
            requestServices.put(ErrorVM.class, new ErrorVMService());
            requestServices.put(LongTransactionVM.class, new LongTransactionVMService());
            requestServices.put(CategoryClassVM.class, new CategoryClassVMService());
            requestServices.put(CategoryVM.class, new CategoryVMService());
            requestServices.put(User.class, new UserService());
            requestServices.put(CustomUser.class, new CustomUserService());
            requestServices.put(AnonymUser.class, new AnonymUserService());
            requestServices.put(TechnicalUser.class, new TechnicalUserService());
            requestServices.put(LoginVM.class, new LoginVMService());
            requestServices.put(Notification.class, new NotificationService());
            requestServices.put(StatistikVM.class, new StatistikVMService());
        }
        return requestServices.get(forClass);
    }
}
