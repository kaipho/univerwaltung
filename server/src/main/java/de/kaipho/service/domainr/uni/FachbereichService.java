package de.kaipho.service.domainr.uni;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.uni.Fachbereich;

/**
 * Implements a DataRequestService for Azubi
 */
public class FachbereichService implements DataRequestService<Fachbereich> {
    @Override
    public ListVM<Fachbereich> findAll() throws DbException {
        ListVM<Fachbereich> result = new ListVM<>();

        Fachbereich.findAll().forEach(it ->
            result.addSingleToData("fachbereich", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<Fachbereich> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<Fachbereich> result = new ListVM<>();

        Fachbereich.findBy(associationId, val).forEach(it ->
            result.addSingleToData("fachbereich", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<Fachbereich> findSingle(Long id) throws DbException {
        return new ObjVM<>(Fachbereich.findOneById(id));
    }
}
