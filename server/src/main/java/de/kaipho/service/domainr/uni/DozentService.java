package de.kaipho.service.domainr.uni;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.uni.Dozent;

/**
 * Implements a DataRequestService for Azubi
 */
public class DozentService implements DataRequestService<Dozent> {
    @Override
    public ListVM<Dozent> findAll() throws DbException {
        ListVM<Dozent> result = new ListVM<>();

        Dozent.findAll().forEach(it ->
            result.addSingleToData("dozent", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<Dozent> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<Dozent> result = new ListVM<>();

        Dozent.findBy(associationId, val).forEach(it ->
            result.addSingleToData("dozent", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<Dozent> findSingle(Long id) throws DbException {
        return new ObjVM<>(Dozent.findOneById(id));
    }
}
