package de.kaipho.service.domainr.uni;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.uni.Student;
import de.kaipho.domain.uni.PersonRolleCompleteVisitor;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.domain.uni.Dozent;

/**
 * Implements a DataRequestService for Azubi
 */
public class PersonRolleService implements DataRequestService<PersonRolle> {
    @Override
    public ListVM<PersonRolle> findAll() throws DbException {
        ListVM<PersonRolle> result = new ListVM<>();

        PersonRolle.findAll().forEach(it ->
            it.accept(new PersonRolleCompleteVisitor<PersonRolle>() {
            @Override
            public PersonRolle visit(Dozent it) {
                result.addSingleToData("dozent", new ObjVM<>(it));
                return it;
            }
            @Override
            public PersonRolle visit(Student it) {
                result.addSingleToData("student", new ObjVM<>(it));
                return it;
            }
            }
        ));

        return result;
    }

    @Override
    public ListVM<PersonRolle> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<PersonRolle> result = new ListVM<>();

        PersonRolle.findBy(associationId, val).forEach(it ->
            it.accept(new PersonRolleCompleteVisitor<PersonRolle>() {
            @Override
            public PersonRolle visit(Dozent it) {
                result.addSingleToData("dozent", new ObjVM<>(it));
                return it;
            }
            @Override
            public PersonRolle visit(Student it) {
                result.addSingleToData("student", new ObjVM<>(it));
                return it;
            }
            }
        ));

        return result;
    }

    @Override
    public ObjVM<PersonRolle> findSingle(Long id) throws DbException {
        return new ObjVM<>(PersonRolle.findOneById(id));
    }
}
