package de.kaipho.service.domainr.security;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.security.TechnicalUser;

/**
 * Implements a DataRequestService for Azubi
 */
public class TechnicalUserService implements DataRequestService<TechnicalUser> {
    @Override
    public ListVM<TechnicalUser> findAll() throws DbException {
        ListVM<TechnicalUser> result = new ListVM<>();

        TechnicalUser.findAll().forEach(it ->
            result.addSingleToData("technicalUser", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<TechnicalUser> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<TechnicalUser> result = new ListVM<>();

        TechnicalUser.findBy(associationId, val).forEach(it ->
            result.addSingleToData("technicalUser", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<TechnicalUser> findSingle(Long id) throws DbException {
        return new ObjVM<>(TechnicalUser.findOneById(id));
    }
}
