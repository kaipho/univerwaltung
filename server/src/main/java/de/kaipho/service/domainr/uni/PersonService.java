package de.kaipho.service.domainr.uni;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.uni.Person;

/**
 * Implements a DataRequestService for Azubi
 */
public class PersonService implements DataRequestService<Person> {
    @Override
    public ListVM<Person> findAll() throws DbException {
        ListVM<Person> result = new ListVM<>();

        Person.findAll().forEach(it ->
            result.addSingleToData("person", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<Person> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<Person> result = new ListVM<>();

        Person.findBy(associationId, val).forEach(it ->
            result.addSingleToData("person", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<Person> findSingle(Long id) throws DbException {
        return new ObjVM<>(Person.findOneById(id));
    }
}
