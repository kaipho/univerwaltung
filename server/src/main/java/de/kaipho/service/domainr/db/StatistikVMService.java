package de.kaipho.service.domainr.db;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.db.StatistikVM;

/**
 * Implements a DataRequestService for Azubi
 */
public class StatistikVMService implements DataRequestService<StatistikVM> {
    @Override
    public ListVM<StatistikVM> findAll() throws DbException {
        ListVM<StatistikVM> result = new ListVM<>();

        StatistikVM.findAll().forEach(it ->
            result.addSingleToData("statistikVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<StatistikVM> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<StatistikVM> result = new ListVM<>();

        StatistikVM.findBy(associationId, val).forEach(it ->
            result.addSingleToData("statistikVM", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<StatistikVM> findSingle(Long id) throws DbException {
        return new ObjVM<>(StatistikVM.findOneById(id));
    }
}
