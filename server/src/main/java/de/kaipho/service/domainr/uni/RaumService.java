package de.kaipho.service.domainr.uni;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.uni.Raum;

/**
 * Implements a DataRequestService for Azubi
 */
public class RaumService implements DataRequestService<Raum> {
    @Override
    public ListVM<Raum> findAll() throws DbException {
        ListVM<Raum> result = new ListVM<>();

        Raum.findAll().forEach(it ->
            result.addSingleToData("raum", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<Raum> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<Raum> result = new ListVM<>();

        Raum.findBy(associationId, val).forEach(it ->
            result.addSingleToData("raum", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<Raum> findSingle(Long id) throws DbException {
        return new ObjVM<>(Raum.findOneById(id));
    }
}
