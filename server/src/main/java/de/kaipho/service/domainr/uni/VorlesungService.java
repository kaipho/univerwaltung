package de.kaipho.service.domainr.uni;

import de.kaipho.controller.domainr.ListVM;
import de.kaipho.controller.domainr.ObjVM;
import de.kaipho.core.db.DbException;
import de.kaipho.service.domainr.DataRequestService;

import de.kaipho.domain.uni.Vorlesung;

/**
 * Implements a DataRequestService for Azubi
 */
public class VorlesungService implements DataRequestService<Vorlesung> {
    @Override
    public ListVM<Vorlesung> findAll() throws DbException {
        ListVM<Vorlesung> result = new ListVM<>();

        Vorlesung.findAll().forEach(it ->
            result.addSingleToData("vorlesung", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ListVM<Vorlesung> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<Vorlesung> result = new ListVM<>();

        Vorlesung.findBy(associationId, val).forEach(it ->
            result.addSingleToData("vorlesung", new ObjVM<>(it)
        ));

        return result;
    }

    @Override
    public ObjVM<Vorlesung> findSingle(Long id) throws DbException {
        return new ObjVM<>(Vorlesung.findOneById(id));
    }
}
