package de.kaipho.service.primitive;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;


class PrimitivesServiceSecurityProxy implements PrimitivesService {

    private final PrimitivesService real;
    
    PrimitivesServiceSecurityProxy(PrimitivesService real) {
        this.real = real;
    }

    public Double convertTo(Long primitiveId, Long unitId) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.convertTo(primitiveId, unitId);
    }
}
