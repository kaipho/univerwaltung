package de.kaipho.service.primitive;


public interface PrimitivesService {

    Double convertTo(Long primitiveId, Long unitId);

    public static PrimitivesService getInstance() {
        PrimitivesService instance = new PrimitivesServiceImpl();
        return new PrimitivesServiceSecurityProxy(instance);
    }
}
