package de.kaipho.service.uni;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;

import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.domain.uni.Dozent;

class UniServiceSecurityProxy implements UniService {

    private final UniService real;
    
    UniServiceSecurityProxy(UniService real) {
        this.real = real;
    }

    public void setFachbereich(Dozent dozent, Fachbereich fachbereich) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.setFachbereich(dozent, fachbereich);
    }
    public void setRolle(Person person, PersonRolle rolle) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.setRolle(person, rolle);
    }
    public void addVorlesung(Student student, Vorlesung vorlesung) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.addVorlesung(student, vorlesung);
    }
    public void setRaum(Vorlesung vorlesung, Raum raum) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.setRaum(vorlesung, raum);
    }
    public void setFachbereichVorlesung(Vorlesung vorlesung, Fachbereich fachbereich) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.setFachbereichVorlesung(vorlesung, fachbereich);
    }
    public void setDozent(Vorlesung vorlesung, Dozent dozent) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.setDozent(vorlesung, dozent);
    }
}
