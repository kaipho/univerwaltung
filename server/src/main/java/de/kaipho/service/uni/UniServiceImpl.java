package de.kaipho.service.uni;

import org.springframework.stereotype.Service;
import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.domain.uni.Dozent;
import de.kaipho.domain.category.NotificationLevel;
import de.kaipho.core.security.SecurityContext;
import de.kaipho.core.transaction.Transaction;
import de.kaipho.domain.core.Notification;
import de.kaipho.service.core.NotificationService;
import java.time.LocalDate;
// imports_end

@Service
public class UniServiceImpl implements UniService {

    private UniService self() {
        return this;
    }

    // editable_area_start
    public void setFachbereichVorlesung(Vorlesung vorlesung, Fachbereich fachbereich) {
        vorlesung.setFachbereich(fachbereich);
    }

    public void setDozent(Vorlesung vorlesung, Dozent dozent) {
        vorlesung.setDozent(dozent);
    }

    public void setRaum(Vorlesung vorlesung, Raum raum) {
        vorlesung.setRaum(raum);
    }

    public void addVorlesung(Student student, Vorlesung vorlesung) {
        if (vorlesung.getStudenten().stream().noneMatch(it -> it.getId().equals(student.getId())))
            vorlesung.addSingleToStudenten(student);
    }

    public void setRolle(Person person, PersonRolle rolle) {
        Transaction.getInstance().run(null, () -> {
            person.setRolle(rolle);
            Notification n1 = Notification.create(rolle.toString(), "testing...", LocalDate.now(), NotificationLevel.empty(), false);
            NotificationService.getInstance().addNotification(SecurityContext.getUser(), n1);
            return null;
        });
    }

    public void setFachbereich(Dozent dozent, Fachbereich fachbereich) {
        dozent.setFachbereich(fachbereich);
    }

    // editable_area_end

    // private_area_start

    // private_area_end
}
