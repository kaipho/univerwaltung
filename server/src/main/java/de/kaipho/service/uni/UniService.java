package de.kaipho.service.uni;

import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.domain.uni.Dozent;

public interface UniService {

    void setFachbereich(Dozent dozent, Fachbereich fachbereich);
    void setRolle(Person person, PersonRolle rolle);
    void addVorlesung(Student student, Vorlesung vorlesung);
    void setRaum(Vorlesung vorlesung, Raum raum);
    void setFachbereichVorlesung(Vorlesung vorlesung, Fachbereich fachbereich);
    void setDozent(Vorlesung vorlesung, Dozent dozent);

    public static UniService getInstance() {
        UniService instance = new UniServiceImpl();
        return new UniServiceSecurityProxy(instance);
    }
}
