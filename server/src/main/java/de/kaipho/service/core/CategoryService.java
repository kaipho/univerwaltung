package de.kaipho.service.core;

import java.util.List;
import de.kaipho.domain.core.CategoryClassVM;
import java.util.ArrayList;

public interface CategoryService {

    /**
     * Loads all categories of the categoryClass
     */
    List<String> getValuesFor(Long categoryClassId);
    /**
     * Adds a single value to the categoryClass
     */
    void addValueTo(String value, Long categoryClassId);
    /**
     * Deletes a value from the categoryClass
     */
    void deleteValue(String value, Long categoryClassId);
    /**
     * Get all classes
     */
    // Secured by proxy...
    List<CategoryClassVM> getAllCategoryClasses();

    public static CategoryService getInstance() {
        CategoryService instance = new CategoryServiceImpl();
        return new CategoryServiceSecurityProxy(instance);
    }
}
