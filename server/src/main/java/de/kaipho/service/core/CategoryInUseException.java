package de.kaipho.service.core;

/**
 * Category is in use, so it can't be deleted.
 */
public class CategoryInUseException extends RuntimeException {
}
