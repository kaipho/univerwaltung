package de.kaipho.service.core;

import java.util.List;
import de.kaipho.domain.core.Notification;
import java.util.ArrayList;
import de.kaipho.domain.security.User;

public interface NotificationService {

    void addNotification(User user, Notification notification);
    List<Notification> getNotifications();

    public static NotificationService getInstance() {
        NotificationService instance = new NotificationServiceImpl();
        return new NotificationServiceSecurityProxy(instance);
    }
}
