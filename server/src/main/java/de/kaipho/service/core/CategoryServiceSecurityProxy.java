package de.kaipho.service.core;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;

import java.util.List;
import de.kaipho.domain.core.CategoryClassVM;
import java.util.ArrayList;

class CategoryServiceSecurityProxy implements CategoryService {

    private final CategoryService real;
    
    CategoryServiceSecurityProxy(CategoryService real) {
        this.real = real;
    }

    public List<String> getValuesFor(Long categoryClassId) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.getValuesFor(categoryClassId);
    }
    public void addValueTo(String value, Long categoryClassId) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.addValueTo(value, categoryClassId);
    }
    public void deleteValue(String value, Long categoryClassId) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.deleteValue(value, categoryClassId);
    }
    public List<CategoryClassVM> getAllCategoryClasses() {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.getAllCategoryClasses();
    }
}
