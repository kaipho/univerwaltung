package de.kaipho.service.core;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;

import java.util.List;
import de.kaipho.domain.core.Notification;
import java.util.ArrayList;
import de.kaipho.domain.security.User;

class NotificationServiceSecurityProxy implements NotificationService {

    private final NotificationService real;
    
    NotificationServiceSecurityProxy(NotificationService real) {
        this.real = real;
    }

    public void addNotification(User user, Notification notification) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.addNotification(user, notification);
    }
    public List<Notification> getNotifications() {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.getNotifications();
    }
}
