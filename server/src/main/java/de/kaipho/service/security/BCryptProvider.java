package de.kaipho.service.security;


public interface BCryptProvider {

    String hashPassword(String password);
    Boolean matchPasswordWithHash(String plaintext, String hash);

    public static BCryptProvider getInstance() {
        BCryptProvider instance = new BCryptProviderImpl();
        return new BCryptProviderSecurityProxy(instance);
    }
}
