package de.kaipho.service.security;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;

import de.kaipho.domain.security.User;

class JwtTokenServiceSecurityProxy implements JwtTokenService {

    private final JwtTokenService real;
    
    JwtTokenServiceSecurityProxy(JwtTokenService real) {
        this.real = real;
    }

    public String getToken(User user) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.getToken(user);
    }
    public User parseToken(String token) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.parseToken(token);
    }
}
