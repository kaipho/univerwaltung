package de.kaipho.service.security;

/**
 * Login information of the current user is wrong or there is no information.
 */
public class WrongLoginException extends RuntimeException {
}
