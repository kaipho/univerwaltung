package de.kaipho.service.security;

import de.kaipho.domain.security.User;

public interface JwtTokenService {

    String getToken(User user);
    User parseToken(String token);

    public static JwtTokenService getInstance() {
        JwtTokenService instance = new JwtTokenServiceImpl();
        return new JwtTokenServiceSecurityProxy(instance);
    }
}
