package de.kaipho.service.security;


public interface AESProvider {

    String decrypt(String text, String password);
    String encrypt(String text, String password);
    String getKey();

    public static AESProvider getInstance() {
        AESProvider instance = new AESProviderImpl();
        return new AESProviderSecurityProxy(instance);
    }
}
