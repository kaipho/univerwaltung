package de.kaipho.service.security;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;


class AESProviderSecurityProxy implements AESProvider {

    private final AESProvider real;
    
    AESProviderSecurityProxy(AESProvider real) {
        this.real = real;
    }

    public String decrypt(String text, String password) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.decrypt(text, password);
    }
    public String encrypt(String text, String password) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.encrypt(text, password);
    }
    public String getKey() {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.getKey();
    }
}
