package de.kaipho.service.security;

import java.util.stream.Collectors;

import de.kaipho.domain.category.PersistentCategory;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.stereotype.Service;
import de.kaipho.domain.security.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.stream.Collectors;
import static de.kaipho.core.Constants.*;
// imports_end

@Service
public class JwtTokenServiceImpl implements JwtTokenService {

    private JwtTokenService self() {
        return this;
    }

    // editable_area_start

    public String getToken(User user) {
        String authorities = user.getRoles()
                                 .stream()
                                 .map(PersistentCategory::getValue)
                                 .collect(Collectors.joining(separator));

        long now = new Date().getTime();
        Date validity;
        validity = new Date(now + this.tokenValidityInSeconds);
        // TODO Save language?
        return BEARER_TOKEN + Jwts.builder()
                                  .setSubject(user.getUsername())
                                  .claim(AUTHORITIES_KEY, authorities)
                                  .signWith(SignatureAlgorithm.HS512, SECURITY_KEY)
                                  .setExpiration(validity)
                                  .compact();
    }

    public User parseToken(String token) {
        try {
            Claims claims = Jwts.parser()
                                .setSigningKey(SECURITY_KEY)
                                .parseClaimsJws(token)
                                .getBody();

            // User should be cached for 15 min!
            return UserCache.getInstance().getUserByNameCached(claims.getSubject());
        } catch (ExpiredJwtException e) {
            throw new WrongLoginException();
        }
    }

    // editable_area_end

    // private_area_start
    private long tokenValidityInSeconds = 60000 * 60 * 12;
    private static final String separator = ",";

    // private_area_end
}
