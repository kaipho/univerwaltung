package de.kaipho.service.security;


public interface SecureValueAccessor {

    String decrypt(String text, String password);
    String encrypt(String text, String password);

    public static SecureValueAccessor getInstance() {
        SecureValueAccessor instance = new SecureValueAccessorImpl();
        return new SecureValueAccessorSecurityProxy(instance);
    }
}
