package de.kaipho.service.security;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;

import de.kaipho.domain.security.LoginVM;
import de.kaipho.domain.security.User;

class UserServiceSecurityProxy implements UserService {

    private final UserService real;
    
    UserServiceSecurityProxy(UserService real) {
        this.real = real;
    }

    public LoginVM login(String username, String password) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.login(username, password);
    }
    public void logout() {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.logout();
    }
    public void changePassword(String old, String newPassword, String repeatIt) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       real.changePassword(old, newPassword, repeatIt);
    }
    public String resetPassword(User user) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.resetPassword(user);
    }
    public LoginVM getUserFromToken(String token) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.getUserFromToken(token);
    }
}
