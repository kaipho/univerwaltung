package de.kaipho.service.security; //keep

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import de.kaipho.core.Constants;
import de.kaipho.service.security.AESProvider;
import de.kaipho.service.security.JwtTokenService;
import de.kaipho.service.security.BCryptProvider;
import de.kaipho.domain.security.LoginVM;
import de.kaipho.domain.security.User;
import de.kaipho.core.security.SecurityContext;
import de.kaipho.domain.MetadataKeys;
import de.kaipho.domain.security.CustomUser;
import java.util.List;
// imports_end

@Service
public class UserServiceImpl implements UserService {
    private BCryptProvider bCryptProvider = BCryptProvider.getInstance();
    private JwtTokenService jwtTokenService = JwtTokenService.getInstance();
    private AESProvider aESProvider = AESProvider.getInstance();

    private UserService self() {
        return this;
    }

    // editable_area_start
    public void changePassword(String old, String newPassword, String repeatIt) {
        if (!newPassword.equals(repeatIt)) {
            throw new PasswordsNotMatchException();
        }
        String username = SecurityContext.getUser()
                                         .getUsername();
        List<CustomUser> user = CustomUser.findBy(MetadataKeys.SECURITY_USER_USERNAME, username);
        if (user.size() != 1) {
            throw new PasswordsNotMatchException();
        }
        CustomUser single = user.get(0);
        if (!this.bCryptProvider.matchPasswordWithHash(old, single.getPassword())) {
            throw new PasswordsNotMatchException();
        }
        String password = BCryptProvider.getInstance()
                                        .hashPassword(newPassword);

        single.setPassword(password);
    }

    public LoginVM getUserFromToken(String token) {
        if (!StringUtils.hasText(token) || !token.startsWith(Constants.BEARER_TOKEN)) {
            throw new WrongLoginException();
        }
        LoginVM loginVM = LoginVM.createUnchecked();
        loginVM.setToken(token);
        loginVM.setUser(JwtTokenService.getInstance().parseToken(token.substring(7)));
        return loginVM;
    }

    /**
     * performes the login, returns a token to authorize future requests and detailed information about the user.
     */
    public LoginVM login(String username, String password) {
        User user = User.findBy(MetadataKeys.SECURITY_USER_USERNAME, username)
                        .stream()
                        .findFirst()
                        .orElseThrow(WrongLoginException::new);

        Boolean isPasswordOk = this.bCryptProvider.matchPasswordWithHash(password, user.getPassword());
        if (!isPasswordOk) {
            throw new WrongLoginException();
        }
        String token = jwtTokenService.getToken(user);
        LoginVM loginVM = LoginVM.createUnchecked();
        loginVM.setToken(token);
        loginVM.setUser(user);
        return loginVM;
    }

    public void logout() {
        // TODO implement void logout(...)
        throw new RuntimeException("Not implemented");
    }

    // Secured by proxy...
    public String resetPassword(User user) {
        // TODO implement String resetPassword(...)
        throw new RuntimeException("Not implemented");
    }

    // editable_area_end

    // private_area_start

    // private_area_end
}
