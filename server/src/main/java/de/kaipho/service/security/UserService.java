package de.kaipho.service.security;

import de.kaipho.domain.security.LoginVM;
import de.kaipho.domain.security.User;

public interface UserService {

    /**
     * performes the login, returns a token to authorize future requests and detailed information about the user.
     */
    LoginVM login(String username, String password);
    void logout();
    void changePassword(String old, String newPassword, String repeatIt);
    // Secured by proxy...
    String resetPassword(User user);
    LoginVM getUserFromToken(String token);

    public static UserService getInstance() {
        UserService instance = new UserServiceImpl();
        return new UserServiceSecurityProxy(instance);
    }
}
