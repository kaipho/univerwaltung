package de.kaipho.service.security;

import org.springframework.stereotype.Service;
import de.kaipho.service.security.AESProvider;
import de.kaipho.service.security.BCryptProvider;
// imports_end

@Service
public class SecureValueAccessorImpl implements SecureValueAccessor {
    private AESProvider aESProvider = AESProvider.getInstance();
    private BCryptProvider bCryptProvider = BCryptProvider.getInstance();

    private SecureValueAccessor self() {
        return this;
    }

    // editable_area_start

    public String decrypt(String text, String password) {
       // TODO implement String decrypt(...)
       throw new RuntimeException("Not implemented");
    }
    public String encrypt(String text, String password) {
       // TODO implement String encrypt(...)
       throw new RuntimeException("Not implemented");
    }

    // editable_area_end

    // private_area_start

    // private_area_end
}
