package de.kaipho.service.security;

import de.kaipho.domain.MetadataKeys;
import de.kaipho.domain.security.User;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Caching for User Objects, this allows thar user objects gets only loaded once every n minutes.
 * As Disadvantage, changes will not appear instantly.
 * TODO solution for notifications!? There can only be one User object!!
 */
public class UserCache {
    private static de.kaipho.service.security.UserCache ourInstance = new de.kaipho.service.security.UserCache();

    public static de.kaipho.service.security.UserCache getInstance() {
        return ourInstance;
    }

    private UserCache() {
        new Thread(this::clearMap).start();
    }

    private Map<String, User> userCache = new ConcurrentHashMap<>();

    private void clearMap() {
        while (true) {
            try {
                Thread.sleep(300000);
                System.out.println("Clear User Cache");
                userCache.clear();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public User getUserByNameCached(String name) {
        return userCache.computeIfAbsent(name, key -> {
            List<User> result = User.findBy(MetadataKeys.SECURITY_USER_USERNAME, name);
            if(result.isEmpty()) {
                throw new RuntimeException();
            }
            return result.get(0);
        });
    }
}
