package de.kaipho.service.security;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;


class SecureValueAccessorSecurityProxy implements SecureValueAccessor {

    private final SecureValueAccessor real;
    
    SecureValueAccessorSecurityProxy(SecureValueAccessor real) {
        this.real = real;
    }

    public String decrypt(String text, String password) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.decrypt(text, password);
    }
    public String encrypt(String text, String password) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.encrypt(text, password);
    }
}
