package de.kaipho.service.security;

import de.kaipho.core.exceptions.UnauthorizedException;
import de.kaipho.core.security.SecurityContext;


class BCryptProviderSecurityProxy implements BCryptProvider {

    private final BCryptProvider real;
    
    BCryptProviderSecurityProxy(BCryptProvider real) {
        this.real = real;
    }

    public String hashPassword(String password) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.hashPassword(password);
    }
    public Boolean matchPasswordWithHash(String plaintext, String hash) {
       if(!SecurityContext.isLoggedIn()) throw new UnauthorizedException();
       return real.matchPasswordWithHash(plaintext, hash);
    }
}
