package de.kaipho.service.security;

/**
 * Password do not match saved version or retype.
 */
public class PasswordsNotMatchException extends RuntimeException {
}
