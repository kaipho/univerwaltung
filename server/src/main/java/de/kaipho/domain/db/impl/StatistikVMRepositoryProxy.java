package de.kaipho.domain.db.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.db.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;


public class StatistikVMRepositoryProxy extends DbFacade implements StatistikVM {
    private StatistikVMImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public StatistikVMRepositoryProxy() {
        Long id= super.createObjInstance(DB_STATISTIKVM);
        real = new StatistikVMImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public StatistikVMRepositoryProxy(Long id) {
        this.real = new StatistikVMImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setName(cache.getString(links, DB_STATISTIKVM_NAME));
        real.setValue(cache.getInteger(links, DB_STATISTIKVM_VALUE));
        initLists();
    }

    private void initLists() {
    }

    public void constructAll() {
    }

    @Override
    public String getName() {
       return real.getName();
    }
    @Override
    public Long getValue() {
       return real.getValue();
    }
    @Override
    public void setName(String name) {
        setAssociation(real.getId(), name, DB_STATISTIKVM_NAME);
        real.setName(name);
    }
    @Override
    public void setValue(Long value) {
        setAssociation(real.getId(), value, DB_STATISTIKVM_VALUE);
        real.setValue(value);
    }
    @Override
    public void addValue(Long value) {
       real.addValue(value);
    }


    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof StatistikVMRepositoryProxy) return getId().equals(((StatistikVMRepositoryProxy) obj).getId());
        return false;
    }
}
