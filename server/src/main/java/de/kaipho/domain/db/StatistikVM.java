package de.kaipho.domain.db;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.db.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import de.kaipho.domain.db.impl.StatistikVMRepositoryProxy;
import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.core.db.DbConnector;
import java.sql.PreparedStatement;
import de.kaipho.core.db.DbException;

public interface StatistikVM extends PersistentElement {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static StatistikVM createUnchecked() {
            return new StatistikVMImpl();
    }

    static StatistikVM create(
            String name, Long value
    ) {
        StatistikVM result = new StatistikVMRepositoryProxy();
        result.setName(name);
        result.setValue(value);
        return result;
    }

	static StatistikVM findOneById(Long id) {
	   if(id == null) return null;
	   return new StatistikVMRepositoryProxy(id);
	}
	static List<StatistikVM> findAll() throws DbException {
	    return DbFacade.getAll(31L, (classId, id) -> new StatistikVMRepositoryProxy(id));
	}
	static List<StatistikVM> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(31L, association, s)
	                   .stream()
	                   .map(StatistikVM::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    String getName();
    
    Long getValue();
    
    void setName(String name);
    
    void setValue(Long value);
    
    void addValue(Long value);
    

    public static StatistikVMBuilder builder() {
        return new StatistikVMBuilder();
    }
    
    public static class DoneableStatistikVMBuilder<R> extends StatistikVMBuilder {
        private MasterBuilder<R> master;
        public DoneableStatistikVMBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableStatistikVMBuilder<R> name(String name) {
           super.name(name);
           return this;
        }
        @Override
        public DoneableStatistikVMBuilder<R> value(Long value) {
           super.value(value);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class StatistikVMBuilder extends MasterBuilder<StatistikVMBuilder> {
        private StatistikVMRepositoryProxy statistikVMRepositoryProxy;
        private StatistikVM statistikVM;
    
        StatistikVMBuilder() {
            this.statistikVMRepositoryProxy = new StatistikVMRepositoryProxy();
            this.statistikVM = this.statistikVMRepositoryProxy;
        }
    
        public StatistikVMBuilder name(String name) {
            this.statistikVM.setName(name);
            return this;
        }
        public StatistikVMBuilder value(Long value) {
            this.statistikVM.setValue(value);
            return this;
        }
    
        public StatistikVM build() {
            this.statistikVMRepositoryProxy.constructAll();
            return this.statistikVM;
        }
    }

}
