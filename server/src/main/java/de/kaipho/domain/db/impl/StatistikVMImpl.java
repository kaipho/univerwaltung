package de.kaipho.domain.db.impl;

import de.kaipho.domain.db.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;

public class StatistikVMImpl implements StatistikVM {

	private Long id;
    private String name;
    private Long value;

    public StatistikVMImpl() {
    }

    @Override
    public String getName() {
        return this.name;
    }
    @Override
    public Long getValue() {
        return this.value;
    }
    @Override
    public void addValue(Long value) {
       if(this.value == null) this.value = 0L;
       this.value += value;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public void setValue(Long value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        StatistikVM statistikVM = (StatistikVM) o;
    
        if (getName() != null ? !getName().equals(statistikVM.getName()) : statistikVM.getName() != null) return false;
        if (getValue() != null ? !getValue().equals(statistikVM.getValue()) : statistikVM.getValue() != null) return false;
    
        return true;
    }


    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
