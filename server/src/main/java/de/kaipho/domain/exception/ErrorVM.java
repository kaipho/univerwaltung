package de.kaipho.domain.exception;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.exception.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.core.db.DbConnector;
import java.sql.PreparedStatement;
import de.kaipho.core.db.DbException;
import de.kaipho.domain.exception.impl.ErrorVMRepositoryProxy;

public interface ErrorVM extends PersistentElement {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static ErrorVM createUnchecked() {
            return new ErrorVMImpl();
    }

    static ErrorVM create(
            Long code, String message
    ) {
        ErrorVM result = new ErrorVMRepositoryProxy();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

	static ErrorVM findOneById(Long id) {
	   if(id == null) return null;
	   return new ErrorVMRepositoryProxy(id);
	}
	static List<ErrorVM> findAll() throws DbException {
	    return DbFacade.getAll(6L, (classId, id) -> new ErrorVMRepositoryProxy(id));
	}
	static List<ErrorVM> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(6L, association, s)
	                   .stream()
	                   .map(ErrorVM::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    Long getCode();
    
    String getMessage();
    
    void setCode(Long code);
    
    void addCode(Long code);
    
    void setMessage(String message);
    

    public static ErrorVMBuilder builder() {
        return new ErrorVMBuilder();
    }
    
    public static class DoneableErrorVMBuilder<R> extends ErrorVMBuilder {
        private MasterBuilder<R> master;
        public DoneableErrorVMBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableErrorVMBuilder<R> code(Long code) {
           super.code(code);
           return this;
        }
        @Override
        public DoneableErrorVMBuilder<R> message(String message) {
           super.message(message);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class ErrorVMBuilder extends MasterBuilder<ErrorVMBuilder> {
        private ErrorVMRepositoryProxy errorVMRepositoryProxy;
        private ErrorVM errorVM;
    
        ErrorVMBuilder() {
            this.errorVMRepositoryProxy = new ErrorVMRepositoryProxy();
            this.errorVM = this.errorVMRepositoryProxy;
        }
    
        public ErrorVMBuilder code(Long code) {
            this.errorVM.setCode(code);
            return this;
        }
        public ErrorVMBuilder message(String message) {
            this.errorVM.setMessage(message);
            return this;
        }
    
        public ErrorVM build() {
            this.errorVMRepositoryProxy.constructAll();
            return this.errorVM;
        }
    }

}
