package de.kaipho.domain.exception.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.exception.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;


public class ErrorVMRepositoryProxy extends DbFacade implements ErrorVM {
    private ErrorVMImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public ErrorVMRepositoryProxy() {
        Long id= super.createObjInstance(EXCEPTION_ERRORVM);
        real = new ErrorVMImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public ErrorVMRepositoryProxy(Long id) {
        this.real = new ErrorVMImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setCode(cache.getInteger(links, EXCEPTION_ERRORVM_CODE));
        real.setMessage(cache.getString(links, EXCEPTION_ERRORVM_MESSAGE));
        initLists();
    }

    private void initLists() {
    }

    public void constructAll() {
    }

    @Override
    public Long getCode() {
       return real.getCode();
    }
    @Override
    public String getMessage() {
       return real.getMessage();
    }
    @Override
    public void setCode(Long code) {
        setAssociation(real.getId(), code, EXCEPTION_ERRORVM_CODE);
        real.setCode(code);
    }
    @Override
    public void addCode(Long code) {
       real.addCode(code);
    }
    @Override
    public void setMessage(String message) {
        setAssociation(real.getId(), message, EXCEPTION_ERRORVM_MESSAGE);
        real.setMessage(message);
    }


    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof ErrorVMRepositoryProxy) return getId().equals(((ErrorVMRepositoryProxy) obj).getId());
        return false;
    }
}
