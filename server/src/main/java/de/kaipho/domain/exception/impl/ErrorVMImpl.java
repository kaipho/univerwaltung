package de.kaipho.domain.exception.impl;

import de.kaipho.domain.exception.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;

public class ErrorVMImpl implements ErrorVM {

	private Long id;
    private Long code;
    private String message;

    public ErrorVMImpl() {
    }

    @Override
    public Long getCode() {
        return this.code;
    }
    @Override
    public void addCode(Long code) {
       if(this.code == null) this.code = 0L;
       this.code += code;
    }
    @Override
    public String getMessage() {
        return this.message;
    }
    @Override
    public void setCode(Long code) {
        this.code = code;
    }
    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getCode() != null ? getCode().hashCode() : 0);
        result = 31 * result + (getMessage() != null ? getMessage().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        ErrorVM errorVM = (ErrorVM) o;
    
        if (getCode() != null ? !getCode().equals(errorVM.getCode()) : errorVM.getCode() != null) return false;
        if (getMessage() != null ? !getMessage().equals(errorVM.getMessage()) : errorVM.getMessage() != null) return false;
    
        return true;
    }


    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
