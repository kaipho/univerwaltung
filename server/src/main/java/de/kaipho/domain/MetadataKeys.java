package de.kaipho.domain;

import javax.annotation.Generated;

/**
 * metadata keys for DB
 */
@Generated("de.kaipho.gen+")
public interface MetadataKeys {
    final Long UNI_PERSON = 21L;
    final Long UNI_PERSON_NAME = 32L;
    final Long UNI_PERSON_VORNAME = 33L;
    final Long UNI_PERSON_ROLLE = 34L;
    
    final Long UNI_PERSONROLLE = 22L;
    final Long UNI_PERSONROLLE_PERSON = 46L;
    
    final Long UNI_STUDENT = 16L;
    final Long UNI_STUDENT_MATRIKELNUMMER = 35L;
    final Long UNI_STUDENT_SEMESTER = 36L;
    final Long UNI_STUDENT_BESUCHTEVORLESUNGEN = 28L;
    
    final Long UNI_DOZENT = 23L;
    final Long UNI_DOZENT_TITEL = 37L;
    final Long UNI_DOZENT_PERSONALNUMMER = 38L;
    final Long UNI_DOZENT_FACHBEREICH = 39L;
    
    final Long UNI_VORLESUNG = 17L;
    final Long UNI_VORLESUNG_BEZEICHNUNG = 29L;
    final Long UNI_VORLESUNG_DOZENT = 40L;
    final Long UNI_VORLESUNG_FACHBEREICH = 41L;
    final Long UNI_VORLESUNG_RAUM = 42L;
    final Long UNI_VORLESUNG_STUDENTEN = 30L;
    
    final Long UNI_FACHBEREICH = 24L;
    final Long UNI_FACHBEREICH_BEZEICHNUNG = 43L;
    final Long UNI_FACHBEREICH_DOZENTEN = 48L;
    final Long UNI_FACHBEREICH_VORLESUNGEN = 49L;
    
    final Long UNI_RAUM = 25L;
    final Long UNI_RAUM_BEZEICHNUNG = 44L;
    
    final Long EXCEPTION_ERRORVM = 6L;
    final Long EXCEPTION_ERRORVM_CODE = 1L;
    final Long EXCEPTION_ERRORVM_MESSAGE = 2L;
    
    final Long TRANSACTION_LONGTRANSACTIONVM = 29L;
    final Long TRANSACTION_LONGTRANSACTIONVM_LONGTAID = 51L;
    
    final Long CORE_CATEGORYCLASSVM = 7L;
    final Long CORE_CATEGORYCLASSVM_DBID = 3L;
    final Long CORE_CATEGORYCLASSVM_NAME = 4L;
    final Long CORE_CATEGORYCLASSVM_VALUES = 5L;
    
    final Long CORE_CATEGORYVM = 8L;
    final Long CORE_CATEGORYVM_DBID = 6L;
    final Long CORE_CATEGORYVM_NAME = 7L;
    final Long CORE_CATEGORYVM_ISDERIVED = 8L;
    
    final Long SECURITY_USER = 9L;
    final Long SECURITY_USER_USERNAME = 9L;
    final Long SECURITY_USER_PASSWORD = 10L;
    final Long SECURITY_USER_LANG = 11L;
    final Long SECURITY_USER_ROLES = 12L;
    final Long SECURITY_USER_NOTIFICATIONS = 13L;
    
    final Long SECURITY_CUSTOMUSER = 10L;
    
    final Long SECURITY_ANONYMUSER = 11L;
    
    final Long SECURITY_TECHNICALUSER = 35L;
    final Long SECURITY_TECHNICALUSER_DESCRIPTION = 56L;
    
    final Long SECURITY_LOGINVM = 12L;
    final Long SECURITY_LOGINVM_TOKEN = 14L;
    final Long SECURITY_LOGINVM_USER = 15L;
    
    final Long CORE_NOTIFICATION = 13L;
    final Long CORE_NOTIFICATION_SUBJECT = 16L;
    final Long CORE_NOTIFICATION_MESSAGE = 17L;
    final Long CORE_NOTIFICATION_CREATION = 18L;
    final Long CORE_NOTIFICATION_TYPE = 19L;
    final Long CORE_NOTIFICATION_ISREAD = 20L;
    
    final Long DB_STATISTIKVM = 31L;
    final Long DB_STATISTIKVM_NAME = 53L;
    final Long DB_STATISTIKVM_VALUE = 54L;
    
}
