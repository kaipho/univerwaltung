package de.kaipho.domain.core;

import java.util.List;
import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import java.util.Optional;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.uni.PersonRolle;
import java.util.ArrayList;
import de.kaipho.domain.uni.Dozent;

import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.MetadataKeys;
// imports_end

/**
 * Manager containing all derived associations.
 */
public class DerivedManager {
    private static DerivedManager ourInstance = new DerivedManager();

    public static DerivedManager getInstance() {
        return ourInstance;
    }

    private DerivedManager() {
    }

    // editable_area_start
    public List<Dozent> derivedDozenten(Fachbereich fachbereich) {
        return DbFacade.deriveBacklinkCollection(fachbereich.getId(), MetadataKeys.UNI_DOZENT_FACHBEREICH, Dozent::findOneById);
    }
    public List<Vorlesung> derivedVorlesungen(Fachbereich fachbereich) {
        return DbFacade.deriveBacklinkCollection(fachbereich.getId(), MetadataKeys.UNI_VORLESUNG_FACHBEREICH, Vorlesung::findOneById);
    }
    public Optional<Person> derivedPerson(PersonRolle personRolle) {
        return DbFacade.deriveBacklinkOptional(personRolle.getId(), MetadataKeys.UNI_PERSON_ROLLE, Person::findOneById);
    }

    public List<Vorlesung> derivedBesuchteVorlesungen(Student student) {
        return DbFacade.deriveBacklinkCollection(student.getId(), MetadataKeys.UNI_VORLESUNG_STUDENTEN, Vorlesung::findOneById);
    }

//     public Long derivedDouble(Statistik statistik) {
//         return DbStatistic.fetchOverallDoubleCount();
//     }

//     public Long derivedDate(Statistik statistik) {
//         return DbStatistic.fetchOverallDateTimeCount();
//     }

//     public Long derivedInteger(Statistik statistik) {
//         return DbStatistic.fetchOverallIntegerCount();
//     }

//     public Long derivedObjects(Statistik statistik) {
//         return DbStatistic.fetchOverallObjCount();
//     }

//     public Long derivedLinks(Statistik statistik) {
//         return DbStatistic.fetchOverallLinkCount();
//     }

//     public Long derivedStrings(Statistik statistik) {
//         return DbStatistic.fetchOverallStringCount();
//     }

    // editable_area_end

    // private_area_start

    // private_area_end
}
