package de.kaipho.domain.core.impl;

import de.kaipho.domain.core.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.domain.security.UserVisitor;
import de.kaipho.domain.category.NotificationLevel;
import java.time.LocalDate;
import de.kaipho.domain.security.CustomUserSimpleVisitor;
import de.kaipho.domain.security.CustomUserVisitor;
import de.kaipho.domain.security.UserSimpleVisitor;

/**
 * A $Notification belongs to one to n $User. A Notification should inform about errors, events and so on.
 */
public class NotificationImpl implements Notification {

	private Long id;
    private String subject;
    private String message;
    private LocalDate creation;
    private NotificationLevel type;
    private Boolean isRead;

    public NotificationImpl() {
    }

    @Override
    public String getSubject() {
        return this.subject;
    }
    @Override
    public String getMessage() {
        return this.message;
    }
    @Override
    public LocalDate getCreation() {
        return this.creation;
    }
    @Override
    public NotificationLevel getType() {
        return this.type;
    }
    @Override
    public Boolean getIsRead() {
        return this.isRead;
    }
    @Override
    public void setSubject(String subject) {
        this.subject = subject;
    }
    @Override
    public void setMessage(String message) {
        this.message = message;
    }
    @Override
    public void setCreation(LocalDate creation) {
        this.creation = creation;
    }
    @Override
    public void setType(NotificationLevel type) {
        this.type = type;
    }
    @Override
    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getSubject() != null ? getSubject().hashCode() : 0);
        result = 31 * result + (getMessage() != null ? getMessage().hashCode() : 0);
        result = 31 * result + (getCreation() != null ? getCreation().hashCode() : 0);
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getIsRead() != null ? getIsRead().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        Notification notification = (Notification) o;
    
        if (getSubject() != null ? !getSubject().equals(notification.getSubject()) : notification.getSubject() != null) return false;
        if (getMessage() != null ? !getMessage().equals(notification.getMessage()) : notification.getMessage() != null) return false;
        if (getCreation() != null ? !getCreation().equals(notification.getCreation()) : notification.getCreation() != null) return false;
        if (getType() != null ? !getType().equals(notification.getType()) : notification.getType() != null) return false;
        if (getIsRead() != null ? !getIsRead().equals(notification.getIsRead()) : notification.getIsRead() != null) return false;
    
        return true;
    }

    @Override
    public <D> D accept(UserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    @Override
    public void accept(UserSimpleVisitor visitor) {
       visitor.visit(this);
    }
    @Override
    public <D> D accept(CustomUserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    @Override
    public void accept(CustomUserSimpleVisitor visitor) {
       visitor.visit(this);
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
