package de.kaipho.domain.core;

import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.exception.ErrorVM;
import de.kaipho.domain.core.CategoryClassVM;
import de.kaipho.domain.security.AnonymUser;
import de.kaipho.domain.uni.Dozent;
import de.kaipho.domain.transaction.LongTransactionVM;
import de.kaipho.domain.core.CategoryVM;
import de.kaipho.domain.db.StatistikVM;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.domain.security.LoginVM;

public class MasterBuilder<R> implements PersistentElementCompleteVisitor<R> {
    @Override
    public R visit(Person person) {
       return null;
    }
    @Override
    public R visit(Student student) {
       return null;
    }
    @Override
    public R visit(Dozent dozent) {
       return null;
    }
    @Override
    public R visit(Vorlesung vorlesung) {
       return null;
    }
    @Override
    public R visit(Fachbereich fachbereich) {
       return null;
    }
    @Override
    public R visit(Raum raum) {
       return null;
    }
    @Override
    public R visit(ErrorVM errorVM) {
       return null;
    }
    @Override
    public R visit(LongTransactionVM longTransactionVM) {
       return null;
    }
    @Override
    public R visit(CategoryClassVM categoryClassVM) {
       return null;
    }
    @Override
    public R visit(CategoryVM categoryVM) {
       return null;
    }
    @Override
    public R visit(AnonymUser anonymUser) {
       return null;
    }
    @Override
    public R visit(TechnicalUser technicalUser) {
       return null;
    }
    @Override
    public R visit(LoginVM loginVM) {
       return null;
    }
    @Override
    public R visit(Notification notification) {
       return null;
    }
    @Override
    public R visit(StatistikVM statistikVM) {
       return null;
    }
}
