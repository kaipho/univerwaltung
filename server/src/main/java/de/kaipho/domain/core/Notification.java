package de.kaipho.domain.core;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.core.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import java.util.List;
import de.kaipho.domain.security.UserVisitor;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.category.NotificationLevel;
import de.kaipho.core.db.DbConnector;
import java.time.LocalDate;
import de.kaipho.domain.security.CustomUserSimpleVisitor;
import java.sql.PreparedStatement;
import de.kaipho.domain.security.CustomUserVisitor;
import de.kaipho.domain.core.impl.NotificationRepositoryProxy;
import de.kaipho.core.db.DbException;
import de.kaipho.domain.security.UserSimpleVisitor;

/**
 * A $Notification belongs to one to n $User. A Notification should inform about errors, events and so on.
 */
public interface Notification extends PersistentElement {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static Notification createUnchecked() {
            return new NotificationImpl();
    }

    static Notification create(
            String subject, String message, LocalDate creation, NotificationLevel type, Boolean isRead
    ) {
        Notification result = new NotificationRepositoryProxy();
        result.setSubject(subject);
        result.setMessage(message);
        result.setCreation(creation);
        result.setType(type);
        result.setIsRead(isRead);
        return result;
    }

	static Notification findOneById(Long id) {
	   if(id == null) return null;
	   return new NotificationRepositoryProxy(id);
	}
	static List<Notification> findAll() throws DbException {
	    return DbFacade.getAll(13L, (classId, id) -> new NotificationRepositoryProxy(id));
	}
	static List<Notification> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(13L, association, s)
	                   .stream()
	                   .map(Notification::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    String getSubject();
    
    String getMessage();
    
    LocalDate getCreation();
    
    NotificationLevel getType();
    
    Boolean getIsRead();
    
    void setSubject(String subject);
    
    void setMessage(String message);
    
    void setCreation(LocalDate creation);
    
    void setType(NotificationLevel type);
    
    void setIsRead(Boolean isRead);
    

    public static NotificationBuilder builder() {
        return new NotificationBuilder();
    }
    
    public static class DoneableNotificationBuilder<R> extends NotificationBuilder {
        private MasterBuilder<R> master;
        public DoneableNotificationBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableNotificationBuilder<R> subject(String subject) {
           super.subject(subject);
           return this;
        }
        @Override
        public DoneableNotificationBuilder<R> message(String message) {
           super.message(message);
           return this;
        }
        @Override
        public DoneableNotificationBuilder<R> creation(LocalDate creation) {
           super.creation(creation);
           return this;
        }
        @Override
        public DoneableNotificationBuilder<R> isRead(Boolean isRead) {
           super.isRead(isRead);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class NotificationBuilder extends MasterBuilder<NotificationBuilder> {
        private NotificationRepositoryProxy notificationRepositoryProxy;
        private Notification notification;
    
        NotificationBuilder() {
            this.notificationRepositoryProxy = new NotificationRepositoryProxy();
            this.notification = this.notificationRepositoryProxy;
        }
    
        public NotificationBuilder subject(String subject) {
            this.notification.setSubject(subject);
            return this;
        }
        public NotificationBuilder message(String message) {
            this.notification.setMessage(message);
            return this;
        }
        public NotificationBuilder creation(LocalDate creation) {
            this.notification.setCreation(creation);
            return this;
        }
        public NotificationBuilder type(NotificationLevel type) {
            this.notification.setType(type);
            return this;
        }
        public NotificationBuilder isRead(Boolean isRead) {
            this.notification.setIsRead(isRead);
            return this;
        }
    
        public Notification build() {
            this.notificationRepositoryProxy.constructAll();
            return this.notification;
        }
    }

    public <D> D accept(UserVisitor<D> visitor);
    public void accept(UserSimpleVisitor visitor);
    public <D> D accept(CustomUserVisitor<D> visitor);
    public void accept(CustomUserSimpleVisitor visitor);
}
