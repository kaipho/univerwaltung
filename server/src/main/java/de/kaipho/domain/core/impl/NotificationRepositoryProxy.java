package de.kaipho.domain.core.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.core.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;

import de.kaipho.domain.security.UserVisitor;
import de.kaipho.domain.category.NotificationLevel;
import java.time.LocalDate;
import de.kaipho.domain.security.CustomUserSimpleVisitor;
import de.kaipho.domain.security.CustomUserVisitor;
import de.kaipho.domain.security.UserSimpleVisitor;

/**
 * A $Notification belongs to one to n $User. A Notification should inform about errors, events and so on.
 */
public class NotificationRepositoryProxy extends DbFacade implements Notification {
    private NotificationImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public NotificationRepositoryProxy() {
        Long id= super.createObjInstance(CORE_NOTIFICATION);
        real = new NotificationImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public NotificationRepositoryProxy(Long id) {
        this.real = new NotificationImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setSubject(cache.getString(links, CORE_NOTIFICATION_SUBJECT));
        real.setMessage(cache.getString(links, CORE_NOTIFICATION_MESSAGE));
        real.setCreation(cache.getLocalDate(links, CORE_NOTIFICATION_CREATION));
        real.setType(NotificationLevel.from(cache.getString(links, CORE_NOTIFICATION_TYPE)));
        real.setIsRead(cache.getBoolean(links, CORE_NOTIFICATION_ISREAD));
        initLists();
    }

    private void initLists() {
    }

    public void constructAll() {
    }

    @Override
    public String getSubject() {
       return real.getSubject();
    }
    @Override
    public String getMessage() {
       return real.getMessage();
    }
    @Override
    public LocalDate getCreation() {
       return real.getCreation();
    }
    @Override
    public NotificationLevel getType() {
       return real.getType();
    }
    @Override
    public Boolean getIsRead() {
       return real.getIsRead();
    }
    @Override
    public void setSubject(String subject) {
        setAssociation(real.getId(), subject, CORE_NOTIFICATION_SUBJECT);
        real.setSubject(subject);
    }
    @Override
    public void setMessage(String message) {
        setAssociation(real.getId(), message, CORE_NOTIFICATION_MESSAGE);
        real.setMessage(message);
    }
    @Override
    public void setCreation(LocalDate creation) {
        setAssociation(real.getId(), creation, CORE_NOTIFICATION_CREATION);
        real.setCreation(creation);
    }
    @Override
    public void setType(NotificationLevel type) {
        setAssociation(real.getId(), type, CORE_NOTIFICATION_TYPE);
        real.setType(type);
    }
    @Override
    public void setIsRead(Boolean isRead) {
        setAssociation(real.getId(), isRead, CORE_NOTIFICATION_ISREAD);
        real.setIsRead(isRead);
    }

    public <D> D accept(UserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(UserSimpleVisitor visitor) {
       visitor.visit(this);
    }
    public <D> D accept(CustomUserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(CustomUserSimpleVisitor visitor) {
       visitor.visit(this);
    }

    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof NotificationRepositoryProxy) return getId().equals(((NotificationRepositoryProxy) obj).getId());
        return false;
    }
}
