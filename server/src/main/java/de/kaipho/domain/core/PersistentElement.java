package de.kaipho.domain.core;

import java.io.Serializable;

/**
 * A PersistentElement is a domain class, which can be stored in a database.
 */
public interface PersistentElement extends Serializable {
    Long getId();

    <T> T accept(PersistentElementCompleteVisitor<T> visitor);
    String toString();
}
