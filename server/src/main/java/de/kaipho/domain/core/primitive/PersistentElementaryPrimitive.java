package de.kaipho.domain.core.primitive;

/**
 * Created by Neo on 03.03.17.
 */
public class PersistentElementaryPrimitive<RANGE extends de.kaipho.domain.core.primitive.UnitClass> extends de.kaipho.domain.core.primitive.PrimitiveState<RANGE> {
    private final de.kaipho.domain.core.primitive.Rational fraction;
    private final de.kaipho.domain.core.primitive.UnitInstance<RANGE> unit;

    PersistentElementaryPrimitive(de.kaipho.domain.core.primitive.Rational fraction, de.kaipho.domain.core.primitive.UnitInstance<RANGE> unit, de.kaipho.domain.core.primitive.PersistentPrimitive<RANGE> of) {
        super(of);
        this.fraction = fraction;
        this.unit = unit;
    }

    @Override
    public de.kaipho.domain.core.primitive.PersistentPrimitive<RANGE> to(de.kaipho.domain.core.primitive.UnitInstance<RANGE> unit) {
        return new de.kaipho.domain.core.primitive.PersistentPrimitive<>(unit.fromDefault(this.unit.toDefault(fraction)), unit);
    }

    @Override
    public de.kaipho.domain.core.primitive.Rational resolve() {
        return fraction;
    }

    @Override
    public de.kaipho.domain.core.primitive.PersistentPrimitive<RANGE> add(de.kaipho.domain.core.primitive.PersistentPrimitive<RANGE> primitive) {
        return primitive.visit(new PrimitiveStateVisitor<de.kaipho.domain.core.primitive.PersistentPrimitive<RANGE>>() {
            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<RANGE> accept(de.kaipho.domain.core.primitive.PersistentCombinedPrimitive rangePersistentCombinedPrimitive) {
                return null;
            }

            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<RANGE> accept(de.kaipho.domain.core.primitive.PersistentElementaryPrimitive rangePersistentElementaryPrimitive) {
                return new de.kaipho.domain.core.primitive.PersistentPrimitive<>(fraction.plus(rangePersistentElementaryPrimitive.fraction), unit);
            }

            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<RANGE> accept(de.kaipho.domain.core.primitive.PersistentEmptyPrimitive rangePersistentEmptyPrimitive) {
                return primitive;
            }
        });
    }

    @Override
    public de.kaipho.domain.core.primitive.PersistentPrimitive<?> mult(de.kaipho.domain.core.primitive.PersistentPrimitive<?> primitive) {
        return primitive.visit(new PrimitiveStateVisitor<de.kaipho.domain.core.primitive.PersistentPrimitive<?>>() {
            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<?> accept(de.kaipho.domain.core.primitive.PersistentCombinedPrimitive rangePersistentCombinedPrimitive) {
                return null;
            }

            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<?> accept(de.kaipho.domain.core.primitive.PersistentElementaryPrimitive rangePersistentElementaryPrimitive) {
                // TODO units do not match
                return new de.kaipho.domain.core.primitive.PersistentPrimitive<>(fraction.times(rangePersistentElementaryPrimitive.fraction), unit);
            }

            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<?> accept(de.kaipho.domain.core.primitive.PersistentEmptyPrimitive rangePersistentEmptyPrimitive) {
                return primitive;
            }
        });
    }

    @Override
    public de.kaipho.domain.core.primitive.PersistentPrimitive<?> minus(de.kaipho.domain.core.primitive.PersistentPrimitive<?> primitive) {
        return primitive.visit(new PrimitiveStateVisitor<de.kaipho.domain.core.primitive.PersistentPrimitive<?>>() {
            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<?> accept(de.kaipho.domain.core.primitive.PersistentCombinedPrimitive rangePersistentCombinedPrimitive) {
                return null;
            }

            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<?> accept(de.kaipho.domain.core.primitive.PersistentElementaryPrimitive rangePersistentElementaryPrimitive) {
                // TODO units do not match
                return new de.kaipho.domain.core.primitive.PersistentPrimitive<>(fraction.minus(rangePersistentElementaryPrimitive.fraction), unit);
            }

            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<?> accept(de.kaipho.domain.core.primitive.PersistentEmptyPrimitive rangePersistentEmptyPrimitive) {
                return primitive;
            }
        });
    }

    @Override
    public de.kaipho.domain.core.primitive.PersistentPrimitive<?> div(de.kaipho.domain.core.primitive.PersistentPrimitive<?> primitive) {
        return primitive.visit(new PrimitiveStateVisitor<de.kaipho.domain.core.primitive.PersistentPrimitive<?>>() {
            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<?> accept(de.kaipho.domain.core.primitive.PersistentCombinedPrimitive rangePersistentCombinedPrimitive) {
                return null;
            }

            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<?> accept(de.kaipho.domain.core.primitive.PersistentElementaryPrimitive rangePersistentElementaryPrimitive) {
                // TODO units do not match
                return new de.kaipho.domain.core.primitive.PersistentPrimitive<>(fraction.divides(rangePersistentElementaryPrimitive.fraction), unit);
            }

            @Override
            public de.kaipho.domain.core.primitive.PersistentPrimitive<?> accept(de.kaipho.domain.core.primitive.PersistentEmptyPrimitive rangePersistentEmptyPrimitive) {
                return primitive;
            }
        });
    }


    @Override
    public de.kaipho.domain.core.primitive.UnitInstance<RANGE> getUnitInstance() {
        return unit;
    }

    @Override
    public String toString() {
        return fraction.toDouble() + " (" + unit.toString() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        de.kaipho.domain.core.primitive.PersistentElementaryPrimitive<?> that = (de.kaipho.domain.core.primitive.PersistentElementaryPrimitive<?>) o;

        if (fraction != null ? !fraction.equals(that.fraction) : that.fraction != null) return false;
        return unit != null ? unit.equals(that.unit) : that.unit == null;
    }

    @Override
    public int hashCode() {
        int result = fraction != null ? fraction.hashCode() : 0;
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        return result;
    }

    @Override
    public <R> R visit(PrimitiveStateVisitor<R> visitor) {
        return visitor.accept(this);
    }
}
