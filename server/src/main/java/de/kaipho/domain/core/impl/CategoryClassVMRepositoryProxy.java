package de.kaipho.domain.core.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.core.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;

import java.util.List;
import de.kaipho.domain.core.CategoryVM;
import java.util.ArrayList;

/**
 * VM für eine Kategorie Klasse.
 */
public class CategoryClassVMRepositoryProxy extends DbFacade implements CategoryClassVM {
    private CategoryClassVMImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public CategoryClassVMRepositoryProxy() {
        Long id= super.createObjInstance(CORE_CATEGORYCLASSVM);
        real = new CategoryClassVMImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public CategoryClassVMRepositoryProxy(Long id) {
        this.real = new CategoryClassVMImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setDbId(cache.getInteger(links, CORE_CATEGORYCLASSVM_DBID));
        real.setName(cache.getString(links, CORE_CATEGORYCLASSVM_NAME));
        getObjList(real.getId(), CORE_CATEGORYCLASSVM_VALUES).forEach((key, val) ->
               real.getValues().add(CategoryVM.findOneById(val))
        );
        initLists();
    }

    private void initLists() {
        real.setValues(new StoredList<>(CORE_CATEGORYCLASSVM_VALUES, this, real.getId(), real.getValues()));
    }

    public void constructAll() {
    }

    @Override
    public Long getDbId() {
       return real.getDbId();
    }
    @Override
    public String getName() {
       return real.getName();
    }
    @Override
    public List<CategoryVM> getValues() {
       return real.getValues();
    }
    @Override
    public void setDbId(Long dbId) {
        setAssociation(real.getId(), dbId, CORE_CATEGORYCLASSVM_DBID);
        real.setDbId(dbId);
    }
    @Override
    public void addDbId(Long dbId) {
       real.addDbId(dbId);
    }
    @Override
    public void setName(String name) {
        setAssociation(real.getId(), name, CORE_CATEGORYCLASSVM_NAME);
        real.setName(name);
    }
    @Override
    public void setValues(List<CategoryVM> values) {
        setAssociation(real.getId(), values, CORE_CATEGORYCLASSVM_VALUES);
        real.setValues(values);
    }
    @Override
    public void addSingleToValues(CategoryVM values) {
       real.addSingleToValues(values);
    }


    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof CategoryClassVMRepositoryProxy) return getId().equals(((CategoryClassVMRepositoryProxy) obj).getId());
        return false;
    }
}
