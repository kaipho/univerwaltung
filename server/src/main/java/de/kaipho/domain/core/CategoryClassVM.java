package de.kaipho.domain.core;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.core.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.core.CategoryVM;
import de.kaipho.core.db.DbConnector;
import java.sql.PreparedStatement;
import de.kaipho.core.db.DbException;
import java.util.ArrayList;
import de.kaipho.domain.core.impl.CategoryClassVMRepositoryProxy;

/**
 * VM für eine Kategorie Klasse.
 */
public interface CategoryClassVM extends PersistentElement {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static CategoryClassVM createUnchecked() {
            return new CategoryClassVMImpl();
    }

    static CategoryClassVM create(
            Long dbId, String name
    ) {
        CategoryClassVM result = new CategoryClassVMRepositoryProxy();
        result.setDbId(dbId);
        result.setName(name);
        return result;
    }

	static CategoryClassVM findOneById(Long id) {
	   if(id == null) return null;
	   return new CategoryClassVMRepositoryProxy(id);
	}
	static List<CategoryClassVM> findAll() throws DbException {
	    return DbFacade.getAll(7L, (classId, id) -> new CategoryClassVMRepositoryProxy(id));
	}
	static List<CategoryClassVM> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(7L, association, s)
	                   .stream()
	                   .map(CategoryClassVM::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    Long getDbId();
    
    String getName();
    
    List<CategoryVM> getValues();
    
    void setDbId(Long dbId);
    
    void addDbId(Long dbId);
    
    void setName(String name);
    
    void setValues(List<CategoryVM> values);
    
    void addSingleToValues(CategoryVM values);
    

    public static CategoryClassVMBuilder builder() {
        return new CategoryClassVMBuilder();
    }
    
    public static class DoneableCategoryClassVMBuilder<R> extends CategoryClassVMBuilder {
        private MasterBuilder<R> master;
        public DoneableCategoryClassVMBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableCategoryClassVMBuilder<R> dbId(Long dbId) {
           super.dbId(dbId);
           return this;
        }
        @Override
        public DoneableCategoryClassVMBuilder<R> name(String name) {
           super.name(name);
           return this;
        }
        @Override
        public DoneableCategoryClassVMBuilder<R> values(List<CategoryVM> values) {
           super.values(values);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class CategoryClassVMBuilder extends MasterBuilder<CategoryClassVMBuilder> {
        private CategoryClassVMRepositoryProxy categoryClassVMRepositoryProxy;
        private CategoryClassVM categoryClassVM;
    
        CategoryClassVMBuilder() {
            this.categoryClassVMRepositoryProxy = new CategoryClassVMRepositoryProxy();
            this.categoryClassVM = this.categoryClassVMRepositoryProxy;
        }
    
        public CategoryClassVMBuilder dbId(Long dbId) {
            this.categoryClassVM.setDbId(dbId);
            return this;
        }
        public CategoryClassVMBuilder name(String name) {
            this.categoryClassVM.setName(name);
            return this;
        }
        public CategoryClassVMBuilder values(List<CategoryVM> values) {
            this.categoryClassVM.setValues(values);
            return this;
        }
    
        public CategoryClassVM build() {
            this.categoryClassVMRepositoryProxy.constructAll();
            return this.categoryClassVM;
        }
    }

}
