package de.kaipho.domain.core.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.core.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;


/**
 * VM für eine einzelne Kategorie.
 */
public class CategoryVMRepositoryProxy extends DbFacade implements CategoryVM {
    private CategoryVMImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public CategoryVMRepositoryProxy() {
        Long id= super.createObjInstance(CORE_CATEGORYVM);
        real = new CategoryVMImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public CategoryVMRepositoryProxy(Long id) {
        this.real = new CategoryVMImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setDbId(cache.getInteger(links, CORE_CATEGORYVM_DBID));
        real.setName(cache.getString(links, CORE_CATEGORYVM_NAME));
        real.setIsDerived(cache.getBoolean(links, CORE_CATEGORYVM_ISDERIVED));
        initLists();
    }

    private void initLists() {
    }

    public void constructAll() {
    }

    @Override
    public Long getDbId() {
       return real.getDbId();
    }
    @Override
    public String getName() {
       return real.getName();
    }
    @Override
    public Boolean getIsDerived() {
       return real.getIsDerived();
    }
    @Override
    public void setDbId(Long dbId) {
        setAssociation(real.getId(), dbId, CORE_CATEGORYVM_DBID);
        real.setDbId(dbId);
    }
    @Override
    public void addDbId(Long dbId) {
       real.addDbId(dbId);
    }
    @Override
    public void setName(String name) {
        setAssociation(real.getId(), name, CORE_CATEGORYVM_NAME);
        real.setName(name);
    }
    @Override
    public void setIsDerived(Boolean isDerived) {
        setAssociation(real.getId(), isDerived, CORE_CATEGORYVM_ISDERIVED);
        real.setIsDerived(isDerived);
    }


    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof CategoryVMRepositoryProxy) return getId().equals(((CategoryVMRepositoryProxy) obj).getId());
        return false;
    }
}
