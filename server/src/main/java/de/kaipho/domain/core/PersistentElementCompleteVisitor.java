package de.kaipho.domain.core;

import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.exception.ErrorVM;
import de.kaipho.domain.core.CategoryClassVM;
import de.kaipho.domain.security.AnonymUser;
import de.kaipho.domain.uni.Dozent;
import de.kaipho.domain.transaction.LongTransactionVM;
import de.kaipho.domain.core.CategoryVM;
import de.kaipho.domain.db.StatistikVM;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.domain.security.LoginVM;

public interface PersistentElementCompleteVisitor<T> {
    T visit(Person person);
    T visit(Student student);
    T visit(Dozent dozent);
    T visit(Vorlesung vorlesung);
    T visit(Fachbereich fachbereich);
    T visit(Raum raum);
    T visit(ErrorVM errorVM);
    T visit(LongTransactionVM longTransactionVM);
    T visit(CategoryClassVM categoryClassVM);
    T visit(CategoryVM categoryVM);
    T visit(AnonymUser anonymUser);
    T visit(TechnicalUser technicalUser);
    T visit(LoginVM loginVM);
    T visit(Notification notification);
    T visit(StatistikVM statistikVM);
}
