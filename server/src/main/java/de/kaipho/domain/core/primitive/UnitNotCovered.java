package de.kaipho.domain.core.primitive;

public class UnitNotCovered extends RuntimeException {
    UnitNotCovered(String s) {
        super(s);
    }

}
