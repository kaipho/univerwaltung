package de.kaipho.domain.core.primitive;

/**
 * A instance of a UnitClass
 */
public abstract class UnitInstance<FOR extends de.kaipho.domain.core.primitive.UnitClass> {
    private final FOR instanceOf;
    private final Long id;

    public UnitInstance(FOR instanceOf, Long id) {
        this.instanceOf = instanceOf;
        this.id = id;
    }

    /**
     * Checks if the rational is a valid number for this instance.
     */
    public abstract boolean isCovered(de.kaipho.domain.core.primitive.Rational r);

    public abstract de.kaipho.domain.core.primitive.Rational toDefault(de.kaipho.domain.core.primitive.Rational r);

    public abstract de.kaipho.domain.core.primitive.Rational fromDefault(de.kaipho.domain.core.primitive.Rational r);

    public abstract String toString();

    public FOR getInstanceOf() {
        return instanceOf;
    }

    public Long getId() {
        return id;
    }
}
