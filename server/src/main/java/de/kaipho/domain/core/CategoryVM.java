package de.kaipho.domain.core;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.core.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.core.impl.CategoryVMRepositoryProxy;
import de.kaipho.core.db.DbConnector;
import java.sql.PreparedStatement;
import de.kaipho.core.db.DbException;

/**
 * VM für eine einzelne Kategorie.
 */
public interface CategoryVM extends PersistentElement {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static CategoryVM createUnchecked() {
            return new CategoryVMImpl();
    }

    static CategoryVM create(
            Long dbId, String name, Boolean isDerived
    ) {
        CategoryVM result = new CategoryVMRepositoryProxy();
        result.setDbId(dbId);
        result.setName(name);
        result.setIsDerived(isDerived);
        return result;
    }

	static CategoryVM findOneById(Long id) {
	   if(id == null) return null;
	   return new CategoryVMRepositoryProxy(id);
	}
	static List<CategoryVM> findAll() throws DbException {
	    return DbFacade.getAll(8L, (classId, id) -> new CategoryVMRepositoryProxy(id));
	}
	static List<CategoryVM> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(8L, association, s)
	                   .stream()
	                   .map(CategoryVM::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    Long getDbId();
    
    String getName();
    
    Boolean getIsDerived();
    
    void setDbId(Long dbId);
    
    void addDbId(Long dbId);
    
    void setName(String name);
    
    void setIsDerived(Boolean isDerived);
    

    public static CategoryVMBuilder builder() {
        return new CategoryVMBuilder();
    }
    
    public static class DoneableCategoryVMBuilder<R> extends CategoryVMBuilder {
        private MasterBuilder<R> master;
        public DoneableCategoryVMBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableCategoryVMBuilder<R> dbId(Long dbId) {
           super.dbId(dbId);
           return this;
        }
        @Override
        public DoneableCategoryVMBuilder<R> name(String name) {
           super.name(name);
           return this;
        }
        @Override
        public DoneableCategoryVMBuilder<R> isDerived(Boolean isDerived) {
           super.isDerived(isDerived);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class CategoryVMBuilder extends MasterBuilder<CategoryVMBuilder> {
        private CategoryVMRepositoryProxy categoryVMRepositoryProxy;
        private CategoryVM categoryVM;
    
        CategoryVMBuilder() {
            this.categoryVMRepositoryProxy = new CategoryVMRepositoryProxy();
            this.categoryVM = this.categoryVMRepositoryProxy;
        }
    
        public CategoryVMBuilder dbId(Long dbId) {
            this.categoryVM.setDbId(dbId);
            return this;
        }
        public CategoryVMBuilder name(String name) {
            this.categoryVM.setName(name);
            return this;
        }
        public CategoryVMBuilder isDerived(Boolean isDerived) {
            this.categoryVM.setIsDerived(isDerived);
            return this;
        }
    
        public CategoryVM build() {
            this.categoryVMRepositoryProxy.constructAll();
            return this.categoryVM;
        }
    }

}
