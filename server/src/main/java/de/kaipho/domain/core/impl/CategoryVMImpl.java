package de.kaipho.domain.core.impl;

import de.kaipho.domain.core.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;

/**
 * VM für eine einzelne Kategorie.
 */
public class CategoryVMImpl implements CategoryVM {

	private Long id;
    private Long dbId;
    private String name;
    private Boolean isDerived;

    public CategoryVMImpl() {
    }

    @Override
    public Long getDbId() {
        return this.dbId;
    }
    @Override
    public void addDbId(Long dbId) {
       if(this.dbId == null) this.dbId = 0L;
       this.dbId += dbId;
    }
    @Override
    public String getName() {
        return this.name;
    }
    @Override
    public Boolean getIsDerived() {
        return this.isDerived;
    }
    @Override
    public void setDbId(Long dbId) {
        this.dbId = dbId;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public void setIsDerived(Boolean isDerived) {
        this.isDerived = isDerived;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getDbId() != null ? getDbId().hashCode() : 0);
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getIsDerived() != null ? getIsDerived().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        CategoryVM categoryVM = (CategoryVM) o;
    
        if (getDbId() != null ? !getDbId().equals(categoryVM.getDbId()) : categoryVM.getDbId() != null) return false;
        if (getName() != null ? !getName().equals(categoryVM.getName()) : categoryVM.getName() != null) return false;
        if (getIsDerived() != null ? !getIsDerived().equals(categoryVM.getIsDerived()) : categoryVM.getIsDerived() != null) return false;
    
        return true;
    }


    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
