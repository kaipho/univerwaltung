package de.kaipho.domain.core;

import de.kaipho.domain.constant.uni.RepVorlesung;
import de.kaipho.domain.db.StatistikVM;
import de.kaipho.domain.exception.ErrorVM;
import de.kaipho.domain.security.AnonymUser;
import de.kaipho.domain.security.CustomUser;
import de.kaipho.domain.security.LoginVM;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.domain.transaction.LongTransactionVM;
import de.kaipho.domain.uni.*;

import java.util.Optional;
import java.util.function.Function;

public class ToStringVisitor implements PersistentElementCompleteVisitor<String> {

    public static final ToStringVisitor INSTANCE = new ToStringVisitor();
    public static final String NULL_REPRESENTATION = "-";

    private <T> String getNullSafe(T obj, Function<T, Object> f) {
        if (obj == null)
            return NULL_REPRESENTATION;
        Object result = f.apply(obj);
        if (result == null)
            return NULL_REPRESENTATION;
        return result.toString();
    }

    private <T> String getNullSafe(Optional<T> optional, Function<T, Object> f) {
        if (optional == null)
            return NULL_REPRESENTATION;
        return optional.map(x -> getNullSafe(x, f)).orElse(NULL_REPRESENTATION);
    }

    @Override
    public String visit(ErrorVM errorVM) {
        return "-";
    }

    @Override
    public String visit(LongTransactionVM longTransactionVM) {
        return "-";
    }

    @Override
    public String visit(CategoryClassVM categoryClassVM) {
        return "-";
    }

    @Override
    public String visit(CategoryVM categoryVM) {
        return "-";
    }

    @Override
    public String visit(AnonymUser anonymUser) {
        return getNullSafe(anonymUser, AnonymUser::getUsername);
    }

    @Override
    public String visit(TechnicalUser technicalUser) {
        return getNullSafe(technicalUser, TechnicalUser::getUsername);
    }

    @Override
    public String visit(LoginVM loginVM) {
        return "-";
    }

    @Override
    public String visit(Notification notification) {
        return getNullSafe(notification, Notification::getSubject);
    }

    @Override
    public String visit(StatistikVM statistikVM) {
        return "-";
    }

    @Override
    public String visit(Person person) {
        return getNullSafe(person, Person::getVorname) + " " + getNullSafe(person, Person::getName) + "|" + getNullSafe(person.getRolle(),
                PersonRolle::toString);
    }

    @Override
    public String visit(Student student) {
        String person;
        if (student.getPerson().isPresent())
            person = getNullSafe(student.getPerson(), Person::getVorname) + " " + getNullSafe(student.getPerson(), Person::getName);
        else
            person = NULL_REPRESENTATION;
        return person + " (" + student.getMatrikelnummer() + " im " + student.getSemester() + ". Semester)";
    }

    @Override
    public String visit(Dozent dozent) {
        String person;
        if (dozent.getPerson().isPresent())
            person = getNullSafe(dozent.getPerson(), Person::getVorname) + " " + getNullSafe(dozent.getPerson(), Person::getName);
        else
            person = NULL_REPRESENTATION;
        return getNullSafe(dozent, Dozent::getTitel) + " " + person + " (Personalnummer: " + getNullSafe(dozent, Dozent::getPersonalnummer)
                + ")";
    }

    @Override
    public String visit(Vorlesung vorlesung) {
        return RepVorlesung.get(getNullSafe(vorlesung, Vorlesung::getBezeichnung), getNullSafe(vorlesung.getDozent(), Dozent::toString),
                vorlesung.getStudenten().size()).i18n().get();
    }

    @Override
    public String visit(Fachbereich fachbereich) {
        return getNullSafe(fachbereich, Fachbereich::getBezeichnung);
    }

    @Override
    public String visit(Raum raum) {
        return getNullSafe(raum, Raum::getBezeichnung);
    }
}
