package de.kaipho.domain.core.primitive;

/**
 * Created by Neo on 03.03.17.
 */
public class PersistentEmptyPrimitive<RANGE extends de.kaipho.domain.core.primitive.UnitClass> extends de.kaipho.domain.core.primitive.PrimitiveState<RANGE> {
    private final UnitInstance<RANGE> unit;

    PersistentEmptyPrimitive(PersistentPrimitive<RANGE> of, UnitInstance<RANGE> unit) {
        super(of);
        this.unit = unit;
    }

    @Override
    public PersistentPrimitive<RANGE> to(UnitInstance<RANGE> unit) {
        throw new NullPointerException();
    }

    @Override
    public de.kaipho.domain.core.primitive.Rational resolve() {
        return de.kaipho.domain.core.primitive.Rational.NO_VAL;
    }

    @Override
    public PersistentPrimitive<RANGE> add(PersistentPrimitive<RANGE> primitive) {
        return this.of;
    }

    @Override
    public PersistentPrimitive<?> mult(PersistentPrimitive<?> primitive) {
        return this.of;
    }

    @Override
    public PersistentPrimitive<?> minus(PersistentPrimitive<?> primitive) {
        return this.of;
    }

    @Override
    public PersistentPrimitive<?> div(PersistentPrimitive<?> primitive) {
        return this.of;
    }

    @Override
    public UnitInstance<RANGE> getUnitInstance() {
        return unit;
    }

    @Override
    public String toString() {
        return "Elementary: EMPTY!";
    }
    @Override
    public <R> R visit(PrimitiveStateVisitor<R> visitor) {
        return visitor.accept(this);
    }
}
