package de.kaipho.domain.core.primitive;

import java.util.Collection;

/**
 * A primitive combined by several elementary primitives in the same RANGE.
 */
public class PersistentCombinedPrimitive<RANGE extends de.kaipho.domain.core.primitive.UnitClass> extends de.kaipho.domain.core.primitive.PrimitiveState<RANGE> {
    private Collection<PersistentElementaryPrimitive<RANGE>> primitives;

    PersistentCombinedPrimitive(PersistentPrimitive<RANGE> of) {
        super(of);
    }

    @Override
    public PersistentPrimitive<RANGE> to(UnitInstance<RANGE> unit) {
        return null;
    }

    @Override
    public de.kaipho.domain.core.primitive.Rational resolve() {
        throw new RuntimeException("not implemented!");
    }

    @Override
    public PersistentPrimitive<RANGE> add(PersistentPrimitive<RANGE> primitive) {
        return null;
    }

    @Override
    public PersistentPrimitive<?> mult(PersistentPrimitive<?> primitive) {
        return null;
    }

    @Override
    public PersistentPrimitive<?> minus(PersistentPrimitive<?> primitive) {
        return null;
    }

    @Override
    public PersistentPrimitive<?> div(PersistentPrimitive<?> primitive) {
        return null;
    }

    @Override
    public UnitInstance<RANGE> getUnitInstance() {
        return of.getUnitInstance()
                 .getInstanceOf()
                 .getDefaultInstance();
    }

    @Override
    public <R> R visit(PrimitiveStateVisitor<R> visitor) {
        return visitor.accept(this);
    }
}
