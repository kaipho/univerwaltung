package de.kaipho.domain.core.primitive;

public interface PrimitiveStateVisitor<R> {
    R accept(de.kaipho.domain.core.primitive.PersistentCombinedPrimitive rangePersistentCombinedPrimitive);
    R accept(de.kaipho.domain.core.primitive.PersistentElementaryPrimitive rangePersistentElementaryPrimitive);
    R accept(de.kaipho.domain.core.primitive.PersistentEmptyPrimitive rangePersistentEmptyPrimitive);
}
