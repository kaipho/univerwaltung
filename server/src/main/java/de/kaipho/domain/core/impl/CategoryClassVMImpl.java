package de.kaipho.domain.core.impl;

import de.kaipho.domain.core.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import java.util.List;
import de.kaipho.domain.core.CategoryVM;
import java.util.ArrayList;

/**
 * VM für eine Kategorie Klasse.
 */
public class CategoryClassVMImpl implements CategoryClassVM {

	private Long id;
    private Long dbId;
    private String name;
    private List<CategoryVM> values;

    public CategoryClassVMImpl() {
        this.values = new ArrayList<>();
    }

    @Override
    public Long getDbId() {
        return this.dbId;
    }
    @Override
    public void addDbId(Long dbId) {
       if(this.dbId == null) this.dbId = 0L;
       this.dbId += dbId;
    }
    @Override
    public String getName() {
        return this.name;
    }
    @Override
    public List<CategoryVM> getValues() {
        return this.values;
    }
    @Override
    public void addSingleToValues(CategoryVM values) {
       this.values.add(values);
    }
    @Override
    public void setDbId(Long dbId) {
        this.dbId = dbId;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public void setValues(List<CategoryVM> values) {
        this.values = values;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getDbId() != null ? getDbId().hashCode() : 0);
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getValues() != null ? getValues().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        CategoryClassVM categoryClassVM = (CategoryClassVM) o;
    
        if (getDbId() != null ? !getDbId().equals(categoryClassVM.getDbId()) : categoryClassVM.getDbId() != null) return false;
        if (getName() != null ? !getName().equals(categoryClassVM.getName()) : categoryClassVM.getName() != null) return false;
        if (getValues() != null ? !getValues().equals(categoryClassVM.getValues()) : categoryClassVM.getValues() != null) return false;
    
        return true;
    }


    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
