package de.kaipho.domain.uni.impl;

import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import java.util.List;
import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import java.util.Optional;
import de.kaipho.domain.security.CustomUserCompleteVisitor;
import de.kaipho.domain.security.UserCompleteVisitor;
import de.kaipho.domain.security.CustomUserSimpleVisitor;
import de.kaipho.domain.security.CustomUserVisitor;
import de.kaipho.domain.category.Language;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.domain.security.impl.CustomUserImpl;
import java.util.ArrayList;

public class PersonImpl extends CustomUserImpl implements Person {

	private Long id;
    private String username;
    private String password;
    private Language lang;
    private List<Role> roles;
    private List<Notification> notifications;
    private String name;
    private String vorname;
    private Optional<PersonRolle> rolle;

    public PersonImpl() {
        this.roles = new ArrayList<>();
        this.notifications = new ArrayList<>();
    }

    @Override
    public String getUsername() {
        return this.username;
    }
    @Override
    public String getPassword() {
        return this.password;
    }
    @Override
    public Language getLang() {
        return this.lang;
    }
    @Override
    public List<Role> getRoles() {
        return this.roles;
    }
    @Override
    public void addSingleToRoles(Role roles) {
       this.roles.add(roles);
    }
    @Override
    public List<Notification> getNotifications() {
        return this.notifications;
    }
    @Override
    public void addSingleToNotifications(Notification notifications) {
       this.notifications.add(notifications);
    }
    @Override
    public String getName() {
        return this.name;
    }
    @Override
    public String getVorname() {
        return this.vorname;
    }
    @Override
    public Optional<PersonRolle> getRolle() {
        return this.rolle;
    }
    @Override
    public void setUsername(String username) {
        this.username = username;
    }
    @Override
    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public void setLang(Language lang) {
        this.lang = lang;
    }
    @Override
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    @Override
    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }
    @Override
    public void setRolle(Optional<PersonRolle> rolle) {
        this.rolle = rolle;
    }
    @Override
    public void setRolle(PersonRolle rolle) {
        this.rolle = Optional.of(rolle);
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getUsername() != null ? getUsername().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getLang() != null ? getLang().hashCode() : 0);
        result = 31 * result + (getRoles() != null ? getRoles().hashCode() : 0);
        result = 31 * result + (getNotifications() != null ? getNotifications().hashCode() : 0);
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getVorname() != null ? getVorname().hashCode() : 0);
        result = 31 * result + (getRolle() != null ? getRolle().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        Person person = (Person) o;
    
        if (getUsername() != null ? !getUsername().equals(person.getUsername()) : person.getUsername() != null) return false;
        if (getPassword() != null ? !getPassword().equals(person.getPassword()) : person.getPassword() != null) return false;
        if (getLang() != null ? !getLang().equals(person.getLang()) : person.getLang() != null) return false;
        if (getRoles() != null ? !getRoles().equals(person.getRoles()) : person.getRoles() != null) return false;
        if (getNotifications() != null ? !getNotifications().equals(person.getNotifications()) : person.getNotifications() != null) return false;
        if (getName() != null ? !getName().equals(person.getName()) : person.getName() != null) return false;
        if (getVorname() != null ? !getVorname().equals(person.getVorname()) : person.getVorname() != null) return false;
        if (getRolle() != null ? !getRolle().equals(person.getRolle()) : person.getRolle() != null) return false;
    
        return true;
    }

    @Override
    public <D> D accept(CustomUserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    @Override
    public void accept(CustomUserSimpleVisitor visitor) {
       visitor.visit(this);
    }
    @Override
    public <D> D accept(CustomUserCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }
    @Override
    public <D> D accept(UserCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
