package de.kaipho.domain.uni;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.uni.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import java.util.Optional;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.core.db.DbConnector;
import de.kaipho.domain.uni.PersonRolleCompleteVisitor;
import de.kaipho.domain.uni.impl.DozentRepositoryProxy;
import de.kaipho.domain.uni.PersonRolleSimpleVisitor;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.core.db.DbException;
import java.util.List;
import de.kaipho.domain.category.Titel;
import de.kaipho.core.db.DbFacade;
import java.sql.PreparedStatement;
import de.kaipho.domain.uni.PersonRolleVisitor;

public interface Dozent extends PersistentElement,PersonRolle {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static Dozent createUnchecked() {
            return new DozentImpl();
    }

    static Dozent create(
            Titel titel, String personalnummer, Optional<Fachbereich> fachbereich
    ) {
        Dozent result = new DozentValidationProxy(new DozentRepositoryProxy());
        result.setTitel(titel);
        result.setPersonalnummer(personalnummer);
        result.setFachbereich(fachbereich);
        return result;
    }

	static Dozent findOneById(Long id) {
	   if(id == null) return null;
	   return new DozentValidationProxy(new DozentRepositoryProxy(id));
	}
	static List<Dozent> findAll() throws DbException {
	    return DbFacade.getAll(23L, (classId, id) -> new DozentValidationProxy(new DozentRepositoryProxy(id)));
	}
	static List<Dozent> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(23L, association, s)
	                   .stream()
	                   .map(Dozent::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    Titel getTitel();
    
    String getPersonalnummer();
    
    Optional<Fachbereich> getFachbereich();
    
    void setTitel(Titel titel);
    
    void setPersonalnummer(String personalnummer);
    
    void setFachbereich(Optional<Fachbereich> fachbereich);
    
    void setFachbereich(Fachbereich fachbereich);
    

    public static DozentBuilder builder() {
        return new DozentBuilder();
    }
    
    public static class DoneableDozentBuilder<R> extends DozentBuilder {
        private MasterBuilder<R> master;
        public DoneableDozentBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableDozentBuilder<R> personalnummer(String personalnummer) {
           super.personalnummer(personalnummer);
           return this;
        }
        @Override
        public DoneableDozentBuilder<R> fachbereich(Optional<Fachbereich> fachbereich) {
           super.fachbereich(fachbereich);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class DozentBuilder extends MasterBuilder<DozentBuilder> {
        private DozentRepositoryProxy dozentRepositoryProxy;
        private Dozent dozent;
    
        DozentBuilder() {
            this.dozentRepositoryProxy = new DozentRepositoryProxy();
            this.dozent = new DozentValidationProxy(this.dozentRepositoryProxy);
        }
    
        public DozentBuilder titel(Titel titel) {
            this.dozent.setTitel(titel);
            return this;
        }
        public DozentBuilder personalnummer(String personalnummer) {
            this.dozent.setPersonalnummer(personalnummer);
            return this;
        }
        public DozentBuilder fachbereich(Optional<Fachbereich> fachbereich) {
            this.dozent.setFachbereich(fachbereich);
            return this;
        }
    
        public Dozent build() {
            this.dozentRepositoryProxy.constructAll();
            return this.dozent;
        }
    }

    public <D> D accept(PersonRolleVisitor<D> visitor);
    public void accept(PersonRolleSimpleVisitor visitor);
    public <D> D accept(PersonRolleCompleteVisitor<D> visitor);
}
