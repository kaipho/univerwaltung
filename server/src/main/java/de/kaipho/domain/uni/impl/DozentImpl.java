package de.kaipho.domain.uni.impl;

import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.domain.category.Titel;
import java.util.Optional;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.core.DerivedManager;
import de.kaipho.domain.uni.PersonRolleCompleteVisitor;
import de.kaipho.domain.uni.PersonRolleSimpleVisitor;
import de.kaipho.domain.uni.impl.PersonRolleImpl;
import de.kaipho.domain.uni.PersonRolleVisitor;

public class DozentImpl extends PersonRolleImpl implements Dozent {

	private Long id;
    private Titel titel;
    private String personalnummer;
    private Optional<Fachbereich> fachbereich;

    public DozentImpl() {
    }

    @Override
    public Titel getTitel() {
        return this.titel;
    }
    @Override
    public String getPersonalnummer() {
        return this.personalnummer;
    }
    @Override
    public Optional<Fachbereich> getFachbereich() {
        return this.fachbereich;
    }
    @Override
    public Optional<Person> getPerson() {
       return DerivedManager.getInstance().derivedPerson(this);
    }
    @Override
    public void setTitel(Titel titel) {
        this.titel = titel;
    }
    @Override
    public void setPersonalnummer(String personalnummer) {
        this.personalnummer = personalnummer;
    }
    @Override
    public void setFachbereich(Optional<Fachbereich> fachbereich) {
        this.fachbereich = fachbereich;
    }
    @Override
    public void setFachbereich(Fachbereich fachbereich) {
        this.fachbereich = Optional.of(fachbereich);
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getPerson() != null ? getPerson().hashCode() : 0);
        result = 31 * result + (getTitel() != null ? getTitel().hashCode() : 0);
        result = 31 * result + (getPersonalnummer() != null ? getPersonalnummer().hashCode() : 0);
        result = 31 * result + (getFachbereich() != null ? getFachbereich().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        Dozent dozent = (Dozent) o;
    
        if (getPerson() != null ? !getPerson().equals(dozent.getPerson()) : dozent.getPerson() != null) return false;
        if (getTitel() != null ? !getTitel().equals(dozent.getTitel()) : dozent.getTitel() != null) return false;
        if (getPersonalnummer() != null ? !getPersonalnummer().equals(dozent.getPersonalnummer()) : dozent.getPersonalnummer() != null) return false;
        if (getFachbereich() != null ? !getFachbereich().equals(dozent.getFachbereich()) : dozent.getFachbereich() != null) return false;
    
        return true;
    }

    @Override
    public <D> D accept(PersonRolleVisitor<D> visitor) {
       return visitor.visit(this);
    }
    @Override
    public void accept(PersonRolleSimpleVisitor visitor) {
       visitor.visit(this);
    }
    @Override
    public <D> D accept(PersonRolleCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
