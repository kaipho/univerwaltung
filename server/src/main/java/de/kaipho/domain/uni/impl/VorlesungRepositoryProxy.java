package de.kaipho.domain.uni.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;

import java.util.List;
import de.kaipho.domain.uni.Student;
import java.util.Optional;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.uni.PersonRolleSimpleVisitor;
import de.kaipho.domain.uni.PersonRolleVisitor;
import de.kaipho.domain.uni.Dozent;
import java.util.ArrayList;

public class VorlesungRepositoryProxy extends DbFacade implements Vorlesung {
    private VorlesungImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public VorlesungRepositoryProxy() {
        Long id= super.createObjInstance(UNI_VORLESUNG);
        real = new VorlesungImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public VorlesungRepositoryProxy(Long id) {
        this.real = new VorlesungImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setBezeichnung(cache.getString(links, UNI_VORLESUNG_BEZEICHNUNG));
        getObjList(real.getId(), UNI_VORLESUNG_STUDENTEN).forEach((key, val) ->
               real.getStudenten().add(Student.findOneById(val))
        );
        initLists();
    }

    private void initLists() {
        real.setStudenten(new StoredList<>(UNI_VORLESUNG_STUDENTEN, this, real.getId(), real.getStudenten()));
    }

    public void constructAll() {
        if(real.getDozent() == null) {
           this.setDozent(Optional.empty());
        }
        if(real.getFachbereich() == null) {
           this.setFachbereich(Optional.empty());
        }
        if(real.getRaum() == null) {
           this.setRaum(Optional.empty());
        }
    }

    @Override
    public String getBezeichnung() {
       return real.getBezeichnung();
    }
    @Override public Optional<Dozent> getDozent() {
       if(real.getDozent() == null) {
           real.setDozent(Optional.ofNullable(Dozent
               .findOneById(getObjectForAssociationsOptional(real.getId(), UNI_VORLESUNG_DOZENT))));
       }
       return real.getDozent();
    }
    @Override public Optional<Fachbereich> getFachbereich() {
       if(real.getFachbereich() == null) {
           real.setFachbereich(Optional.ofNullable(Fachbereich
               .findOneById(getObjectForAssociationsOptional(real.getId(), UNI_VORLESUNG_FACHBEREICH))));
       }
       return real.getFachbereich();
    }
    @Override public Optional<Raum> getRaum() {
       if(real.getRaum() == null) {
           real.setRaum(Optional.ofNullable(Raum
               .findOneById(getObjectForAssociationsOptional(real.getId(), UNI_VORLESUNG_RAUM))));
       }
       return real.getRaum();
    }
    @Override
    public List<Student> getStudenten() {
       return real.getStudenten();
    }
    @Override
    public void setBezeichnung(String bezeichnung) {
        setAssociation(real.getId(), bezeichnung, UNI_VORLESUNG_BEZEICHNUNG);
        real.setBezeichnung(bezeichnung);
    }
    @Override
    public void setDozent(Optional<Dozent> dozent) {
        setAssociation(real.getId(), dozent.orElse(null), UNI_VORLESUNG_DOZENT);
        real.setDozent(dozent);
    }
    @Override
    public void setDozent(Dozent dozent) {
        setAssociation(real.getId(), dozent, UNI_VORLESUNG_DOZENT);
        real.setDozent(dozent);
    }
    @Override
    public void setFachbereich(Optional<Fachbereich> fachbereich) {
        setAssociation(real.getId(), fachbereich.orElse(null), UNI_VORLESUNG_FACHBEREICH);
        real.setFachbereich(fachbereich);
    }
    @Override
    public void setFachbereich(Fachbereich fachbereich) {
        setAssociation(real.getId(), fachbereich, UNI_VORLESUNG_FACHBEREICH);
        real.setFachbereich(fachbereich);
    }
    @Override
    public void setRaum(Optional<Raum> raum) {
        setAssociation(real.getId(), raum.orElse(null), UNI_VORLESUNG_RAUM);
        real.setRaum(raum);
    }
    @Override
    public void setRaum(Raum raum) {
        setAssociation(real.getId(), raum, UNI_VORLESUNG_RAUM);
        real.setRaum(raum);
    }
    @Override
    public void setStudenten(List<Student> studenten) {
        setAssociation(real.getId(), studenten, UNI_VORLESUNG_STUDENTEN);
        real.setStudenten(studenten);
    }
    @Override
    public void addSingleToStudenten(Student studenten) {
       real.addSingleToStudenten(studenten);
    }

    public <D> D accept(PersonRolleVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(PersonRolleSimpleVisitor visitor) {
       visitor.visit(this);
    }

    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof VorlesungRepositoryProxy) return getId().equals(((VorlesungRepositoryProxy) obj).getId());
        return false;
    }
}
