package de.kaipho.domain.uni;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.uni.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import java.util.List;
import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.impl.FachbereichRepositoryProxy;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.core.DerivedManager;
import de.kaipho.core.db.DbConnector;
import java.sql.PreparedStatement;
import de.kaipho.core.db.DbException;
import java.util.ArrayList;
import de.kaipho.domain.uni.Dozent;

public interface Fachbereich extends PersistentElement {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static Fachbereich createUnchecked() {
            return new FachbereichImpl();
    }

    static Fachbereich create(
            String bezeichnung
    ) {
        Fachbereich result = new FachbereichRepositoryProxy();
        result.setBezeichnung(bezeichnung);
        return result;
    }

	static Fachbereich findOneById(Long id) {
	   if(id == null) return null;
	   return new FachbereichRepositoryProxy(id);
	}
	static List<Fachbereich> findAll() throws DbException {
	    return DbFacade.getAll(24L, (classId, id) -> new FachbereichRepositoryProxy(id));
	}
	static List<Fachbereich> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(24L, association, s)
	                   .stream()
	                   .map(Fachbereich::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    String getBezeichnung();
    
    List<Dozent> getDozenten();
    
    List<Vorlesung> getVorlesungen();
    
    void setBezeichnung(String bezeichnung);
    

    public static FachbereichBuilder builder() {
        return new FachbereichBuilder();
    }
    
    public static class DoneableFachbereichBuilder<R> extends FachbereichBuilder {
        private MasterBuilder<R> master;
        public DoneableFachbereichBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableFachbereichBuilder<R> bezeichnung(String bezeichnung) {
           super.bezeichnung(bezeichnung);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class FachbereichBuilder extends MasterBuilder<FachbereichBuilder> {
        private FachbereichRepositoryProxy fachbereichRepositoryProxy;
        private Fachbereich fachbereich;
    
        FachbereichBuilder() {
            this.fachbereichRepositoryProxy = new FachbereichRepositoryProxy();
            this.fachbereich = this.fachbereichRepositoryProxy;
        }
    
        public FachbereichBuilder bezeichnung(String bezeichnung) {
            this.fachbereich.setBezeichnung(bezeichnung);
            return this;
        }
    
        public Fachbereich build() {
            this.fachbereichRepositoryProxy.constructAll();
            return this.fachbereich;
        }
    }

}
