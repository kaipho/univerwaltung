package de.kaipho.domain.uni;

import de.kaipho.domain.category.Titel;
import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.category.Semester;
import de.kaipho.domain.uni.Dozent;

public interface PersonRolleVisitor<D> {
    D visit(Dozent dozent);
    D visit(Titel titel);
    D visit(Student student);
    D visit(Semester semester);
    D visit(Vorlesung vorlesung);
}
