package de.kaipho.domain.uni.impl;

import de.kaipho.core.exceptions.ValidationException;
import de.kaipho.domain.MetadataKeys;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElement;

import java.util.regex.Pattern;
import java.util.List;

import de.kaipho.domain.category.Titel;
import java.util.Optional;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.uni.PersonRolleCompleteVisitor;
import de.kaipho.domain.uni.PersonRolleSimpleVisitor;
import de.kaipho.domain.uni.PersonRolleVisitor;

public class DozentValidationProxy implements Dozent {
    private Dozent real;

    /**
     * Creates a proxy for validation. So the setter may throw a ValidationException
     */
    public DozentValidationProxy(Dozent real) {
        this.real = real;
    }

    @Override public Optional<Person> getPerson() {
       return real.getPerson();
    }
    @Override public Titel getTitel() {
       return real.getTitel();
    }
    @Override public String getPersonalnummer() {
       return real.getPersonalnummer();
    }
    @Override public Optional<Fachbereich> getFachbereich() {
       return real.getFachbereich();
    }
    @Override public void setTitel(Titel titel) {
       real.setTitel(titel);
    }
    private static final Pattern P_Personalnummer = Pattern.compile("\\d{5}");
    @Override public void setPersonalnummer(String personalnummer) {
       if(!P_Personalnummer.matcher(personalnummer).matches()) {
           throw new ValidationException("validation.errors.pattern.uni.personalnummer");
       }
       List<Dozent> foundingOfDozent = Dozent.findBy(MetadataKeys.UNI_DOZENT_PERSONALNUMMER, personalnummer);
       if (!foundingOfDozent.isEmpty()) {
          throw new ValidationException("validation.errors.unique.uni.personalnummer");
       }
       real.setPersonalnummer(personalnummer);
    }
    @Override public void setFachbereich(Optional<Fachbereich> fachbereich) {
       real.setFachbereich(fachbereich);
    }
    @Override public void setFachbereich(Fachbereich fachbereich) {
       this.setFachbereich(Optional.of(fachbereich));
    }


    @Override
    public Long getId() {
        return real.getId();
    }

    public <D> D accept(PersonRolleVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(PersonRolleSimpleVisitor visitor) {
       visitor.visit(this);
    }
    public <D> D accept(PersonRolleCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
