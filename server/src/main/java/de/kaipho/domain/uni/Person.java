package de.kaipho.domain.uni;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.uni.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import java.util.Optional;
import de.kaipho.domain.security.CustomUserCompleteVisitor;
import de.kaipho.core.db.DbConnector;
import de.kaipho.domain.security.CustomUserSimpleVisitor;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.core.db.DbException;
import java.util.ArrayList;
import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.uni.impl.PersonRepositoryProxy;
import de.kaipho.domain.security.UserCompleteVisitor;
import java.sql.PreparedStatement;
import de.kaipho.domain.security.CustomUserVisitor;
import de.kaipho.domain.category.Language;
import de.kaipho.domain.security.CustomUser;

public interface Person extends PersistentElement,CustomUser {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static Person createUnchecked() {
            return new PersonImpl();
    }

    static Person create(
            String username, String password, Language lang, String name, String vorname, Optional<PersonRolle> rolle
    ) {
        Person result = new PersonValidationProxy(new PersonRepositoryProxy());
        result.setUsername(username);
        result.setPassword(password);
        result.setLang(lang);
        result.setName(name);
        result.setVorname(vorname);
        result.setRolle(rolle);
        return result;
    }

	static Person findOneById(Long id) {
	   if(id == null) return null;
	   return new PersonValidationProxy(new PersonRepositoryProxy(id));
	}
	static List<Person> findAll() throws DbException {
	    return DbFacade.getAll(21L, (classId, id) -> new PersonValidationProxy(new PersonRepositoryProxy(id)));
	}
	static List<Person> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(21L, association, s)
	                   .stream()
	                   .map(Person::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    String getName();
    
    String getVorname();
    
    Optional<PersonRolle> getRolle();
    
    void setName(String name);
    
    void setVorname(String vorname);
    
    void setRolle(Optional<PersonRolle> rolle);
    
    void setRolle(PersonRolle rolle);
    

    public static PersonBuilder builder() {
        return new PersonBuilder();
    }
    
    public static class DoneablePersonBuilder<R> extends PersonBuilder {
        private MasterBuilder<R> master;
        public DoneablePersonBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneablePersonBuilder<R> username(String username) {
           super.username(username);
           return this;
        }
        @Override
        public DoneablePersonBuilder<R> password(String password) {
           super.password(password);
           return this;
        }
        @Override
        public DoneablePersonBuilder<R> roles(List<Role> roles) {
           super.roles(roles);
           return this;
        }
        @Override
        public DoneablePersonBuilder<R> notifications(List<Notification> notifications) {
           super.notifications(notifications);
           return this;
        }
        @Override
        public DoneablePersonBuilder<R> name(String name) {
           super.name(name);
           return this;
        }
        @Override
        public DoneablePersonBuilder<R> vorname(String vorname) {
           super.vorname(vorname);
           return this;
        }
        @Override
        public DoneablePersonBuilder<R> rolle(Optional<PersonRolle> rolle) {
           super.rolle(rolle);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class PersonBuilder extends MasterBuilder<PersonBuilder> {
        private PersonRepositoryProxy personRepositoryProxy;
        private Person person;
    
        PersonBuilder() {
            this.personRepositoryProxy = new PersonRepositoryProxy();
            this.person = new PersonValidationProxy(this.personRepositoryProxy);
        }
    
        public PersonBuilder username(String username) {
            this.person.setUsername(username);
            return this;
        }
        public PersonBuilder password(String password) {
            this.person.setPassword(password);
            return this;
        }
        public PersonBuilder lang(Language lang) {
            this.person.setLang(lang);
            return this;
        }
        public PersonBuilder roles(List<Role> roles) {
            this.person.setRoles(roles);
            return this;
        }
        public PersonBuilder notifications(List<Notification> notifications) {
            this.person.setNotifications(notifications);
            return this;
        }
        public PersonBuilder name(String name) {
            this.person.setName(name);
            return this;
        }
        public PersonBuilder vorname(String vorname) {
            this.person.setVorname(vorname);
            return this;
        }
        public PersonBuilder rolle(Optional<PersonRolle> rolle) {
            this.person.setRolle(rolle);
            return this;
        }
    
        public Person build() {
            this.personRepositoryProxy.constructAll();
            return this.person;
        }
    }

    public <D> D accept(CustomUserVisitor<D> visitor);
    public void accept(CustomUserSimpleVisitor visitor);
    public <D> D accept(CustomUserCompleteVisitor<D> visitor);
    public <D> D accept(UserCompleteVisitor<D> visitor);
}
