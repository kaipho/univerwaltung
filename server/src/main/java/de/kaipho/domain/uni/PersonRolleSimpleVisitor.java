package de.kaipho.domain.uni;

import de.kaipho.domain.category.Titel;
import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.uni.Student;
import de.kaipho.domain.category.Semester;
import de.kaipho.domain.uni.Dozent;

public interface PersonRolleSimpleVisitor {
    void visit(Dozent dozent);
    void visit(Titel titel);
    void visit(Student student);
    void visit(Semester semester);
    void visit(Vorlesung vorlesung);
}
