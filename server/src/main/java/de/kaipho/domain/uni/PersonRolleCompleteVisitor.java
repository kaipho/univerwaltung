package de.kaipho.domain.uni;

import de.kaipho.domain.uni.Student;
import de.kaipho.domain.uni.Dozent;

public interface PersonRolleCompleteVisitor<R> {
    R visit(Dozent dozent);
    R visit(Student student);
}
