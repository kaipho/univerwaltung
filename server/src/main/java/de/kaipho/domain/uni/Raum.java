package de.kaipho.domain.uni;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.uni.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.core.db.DbConnector;
import de.kaipho.domain.uni.impl.RaumRepositoryProxy;
import java.sql.PreparedStatement;
import de.kaipho.core.db.DbException;

public interface Raum extends PersistentElement {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static Raum createUnchecked() {
            return new RaumImpl();
    }

    static Raum create(
            String bezeichnung
    ) {
        Raum result = new RaumRepositoryProxy();
        result.setBezeichnung(bezeichnung);
        return result;
    }

	static Raum findOneById(Long id) {
	   if(id == null) return null;
	   return new RaumRepositoryProxy(id);
	}
	static List<Raum> findAll() throws DbException {
	    return DbFacade.getAll(25L, (classId, id) -> new RaumRepositoryProxy(id));
	}
	static List<Raum> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(25L, association, s)
	                   .stream()
	                   .map(Raum::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    String getBezeichnung();
    
    void setBezeichnung(String bezeichnung);
    

    public static RaumBuilder builder() {
        return new RaumBuilder();
    }
    
    public static class DoneableRaumBuilder<R> extends RaumBuilder {
        private MasterBuilder<R> master;
        public DoneableRaumBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableRaumBuilder<R> bezeichnung(String bezeichnung) {
           super.bezeichnung(bezeichnung);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class RaumBuilder extends MasterBuilder<RaumBuilder> {
        private RaumRepositoryProxy raumRepositoryProxy;
        private Raum raum;
    
        RaumBuilder() {
            this.raumRepositoryProxy = new RaumRepositoryProxy();
            this.raum = this.raumRepositoryProxy;
        }
    
        public RaumBuilder bezeichnung(String bezeichnung) {
            this.raum.setBezeichnung(bezeichnung);
            return this;
        }
    
        public Raum build() {
            this.raumRepositoryProxy.constructAll();
            return this.raum;
        }
    }

}
