package de.kaipho.domain.uni.impl;

import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import java.util.List;
import de.kaipho.domain.uni.Vorlesung;
import java.util.Optional;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.core.DerivedManager;
import de.kaipho.domain.uni.PersonRolleCompleteVisitor;
import de.kaipho.domain.uni.PersonRolleSimpleVisitor;
import de.kaipho.domain.category.Semester;
import de.kaipho.domain.uni.impl.PersonRolleImpl;
import de.kaipho.domain.uni.PersonRolleVisitor;
import java.util.ArrayList;

public class StudentImpl extends PersonRolleImpl implements Student {

	private Long id;
    private Long matrikelnummer;
    private Semester semester;

    public StudentImpl() {
    }

    @Override
    public Long getMatrikelnummer() {
        return this.matrikelnummer;
    }
    @Override
    public void addMatrikelnummer(Long matrikelnummer) {
       if(this.matrikelnummer == null) this.matrikelnummer = 0L;
       this.matrikelnummer += matrikelnummer;
    }
    @Override
    public Semester getSemester() {
        return this.semester;
    }
    @Override
    public Optional<Person> getPerson() {
       return DerivedManager.getInstance().derivedPerson(this);
    }
    @Override
    public List<Vorlesung> getBesuchteVorlesungen() {
       return DerivedManager.getInstance().derivedBesuchteVorlesungen(this);
    }
    @Override
    public void setMatrikelnummer(Long matrikelnummer) {
        this.matrikelnummer = matrikelnummer;
    }
    @Override
    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getPerson() != null ? getPerson().hashCode() : 0);
        result = 31 * result + (getMatrikelnummer() != null ? getMatrikelnummer().hashCode() : 0);
        result = 31 * result + (getSemester() != null ? getSemester().hashCode() : 0);
        result = 31 * result + (getBesuchteVorlesungen() != null ? getBesuchteVorlesungen().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        Student student = (Student) o;
    
        if (getPerson() != null ? !getPerson().equals(student.getPerson()) : student.getPerson() != null) return false;
        if (getMatrikelnummer() != null ? !getMatrikelnummer().equals(student.getMatrikelnummer()) : student.getMatrikelnummer() != null) return false;
        if (getSemester() != null ? !getSemester().equals(student.getSemester()) : student.getSemester() != null) return false;
        if (getBesuchteVorlesungen() != null ? !getBesuchteVorlesungen().equals(student.getBesuchteVorlesungen()) : student.getBesuchteVorlesungen() != null) return false;
    
        return true;
    }

    @Override
    public <D> D accept(PersonRolleVisitor<D> visitor) {
       return visitor.visit(this);
    }
    @Override
    public void accept(PersonRolleSimpleVisitor visitor) {
       visitor.visit(this);
    }
    @Override
    public <D> D accept(PersonRolleCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
