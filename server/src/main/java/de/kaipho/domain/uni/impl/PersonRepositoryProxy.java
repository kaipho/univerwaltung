package de.kaipho.domain.uni.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.security.UserVisitor;
import java.util.Optional;
import de.kaipho.domain.security.CustomUserCompleteVisitor;
import de.kaipho.domain.security.CustomUserSimpleVisitor;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.domain.security.UserSimpleVisitor;
import java.util.ArrayList;
import java.util.List;
import de.kaipho.domain.security.UserCompleteVisitor;
import de.kaipho.domain.security.CustomUserVisitor;
import de.kaipho.domain.category.Language;

public class PersonRepositoryProxy extends DbFacade implements Person {
    private PersonImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public PersonRepositoryProxy() {
        Long id= super.createObjInstance(UNI_PERSON);
        real = new PersonImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public PersonRepositoryProxy(Long id) {
        this.real = new PersonImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setUsername(cache.getString(links, SECURITY_USER_USERNAME));
        real.setPassword(cache.getString(links, SECURITY_USER_PASSWORD));
        real.setLang(Language.from(cache.getString(links, SECURITY_USER_LANG)));
        links.getOrDefault(SECURITY_USER_ROLES, new ArrayList<>()).forEach(link ->
               real.getRoles().add(Role.from(cache.getString(link.getToObj())))
        );
        getObjList(real.getId(), SECURITY_USER_NOTIFICATIONS).forEach((key, val) ->
               real.getNotifications().add(Notification.findOneById(val))
        );
        real.setName(cache.getString(links, UNI_PERSON_NAME));
        real.setVorname(cache.getString(links, UNI_PERSON_VORNAME));
        initLists();
    }

    private void initLists() {
        real.setRoles(new StoredList<>(SECURITY_USER_ROLES, this, real.getId(), real.getRoles()));
        real.setNotifications(new StoredList<>(SECURITY_USER_NOTIFICATIONS, this, real.getId(), real.getNotifications()));
    }

    public void constructAll() {
        if(real.getRolle() == null) {
           this.setRolle(Optional.empty());
        }
    }

    @Override
    public String getUsername() {
       return real.getUsername();
    }
    @Override
    public String getPassword() {
       return real.getPassword();
    }
    @Override
    public Language getLang() {
       return real.getLang();
    }
    @Override
    public List<Role> getRoles() {
       return real.getRoles();
    }
    @Override
    public List<Notification> getNotifications() {
       return real.getNotifications();
    }
    @Override
    public String getName() {
       return real.getName();
    }
    @Override
    public String getVorname() {
       return real.getVorname();
    }
    @Override public Optional<PersonRolle> getRolle() {
       if(real.getRolle() == null) {
           real.setRolle(Optional.ofNullable(PersonRolle
               .findOneById(getObjectForAssociationsOptional(real.getId(), UNI_PERSON_ROLLE))));
       }
       return real.getRolle();
    }
    @Override
    public void setUsername(String username) {
        setAssociation(real.getId(), username, SECURITY_USER_USERNAME);
        real.setUsername(username);
    }
    @Override
    public void setPassword(String password) {
        setAssociation(real.getId(), password, SECURITY_USER_PASSWORD);
        real.setPassword(password);
    }
    @Override
    public void setLang(Language lang) {
        setAssociation(real.getId(), lang, SECURITY_USER_LANG);
        real.setLang(lang);
    }
    @Override
    public void setRoles(List<Role> roles) {
        setAssociation(real.getId(), roles, SECURITY_USER_ROLES);
        real.setRoles(roles);
    }
    @Override
    public void addSingleToRoles(Role roles) {
       real.addSingleToRoles(roles);
    }
    @Override
    public void setNotifications(List<Notification> notifications) {
        setAssociation(real.getId(), notifications, SECURITY_USER_NOTIFICATIONS);
        real.setNotifications(notifications);
    }
    @Override
    public void addSingleToNotifications(Notification notifications) {
       real.addSingleToNotifications(notifications);
    }
    @Override
    public void setName(String name) {
        setAssociation(real.getId(), name, UNI_PERSON_NAME);
        real.setName(name);
    }
    @Override
    public void setVorname(String vorname) {
        setAssociation(real.getId(), vorname, UNI_PERSON_VORNAME);
        real.setVorname(vorname);
    }
    @Override
    public void setRolle(Optional<PersonRolle> rolle) {
        setAssociation(real.getId(), rolle.orElse(null), UNI_PERSON_ROLLE);
        real.setRolle(rolle);
    }
    @Override
    public void setRolle(PersonRolle rolle) {
        setAssociation(real.getId(), rolle, UNI_PERSON_ROLLE);
        real.setRolle(rolle);
    }

    public <D> D accept(CustomUserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(CustomUserSimpleVisitor visitor) {
       visitor.visit(this);
    }
    public <D> D accept(UserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(UserSimpleVisitor visitor) {
       visitor.visit(this);
    }
    public <D> D accept(CustomUserCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public <D> D accept(UserCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof PersonRepositoryProxy) return getId().equals(((PersonRepositoryProxy) obj).getId());
        return false;
    }
}
