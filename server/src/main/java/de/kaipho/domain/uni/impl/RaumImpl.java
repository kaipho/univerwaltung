package de.kaipho.domain.uni.impl;

import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;

public class RaumImpl implements Raum {

	private Long id;
    private String bezeichnung;

    public RaumImpl() {
    }

    @Override
    public String getBezeichnung() {
        return this.bezeichnung;
    }
    @Override
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getBezeichnung() != null ? getBezeichnung().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        Raum raum = (Raum) o;
    
        if (getBezeichnung() != null ? !getBezeichnung().equals(raum.getBezeichnung()) : raum.getBezeichnung() != null) return false;
    
        return true;
    }


    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
