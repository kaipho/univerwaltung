package de.kaipho.domain.uni.impl;

import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import java.util.List;
import de.kaipho.domain.uni.Student;
import java.util.Optional;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.uni.PersonRolleSimpleVisitor;
import de.kaipho.domain.uni.PersonRolleVisitor;
import de.kaipho.domain.uni.Dozent;
import java.util.ArrayList;

public class VorlesungImpl implements Vorlesung {

	private Long id;
    private String bezeichnung;
    private Optional<Dozent> dozent;
    private Optional<Fachbereich> fachbereich;
    private Optional<Raum> raum;
    private List<Student> studenten;

    public VorlesungImpl() {
        this.studenten = new ArrayList<>();
    }

    @Override
    public String getBezeichnung() {
        return this.bezeichnung;
    }
    @Override
    public Optional<Dozent> getDozent() {
        return this.dozent;
    }
    @Override
    public Optional<Fachbereich> getFachbereich() {
        return this.fachbereich;
    }
    @Override
    public Optional<Raum> getRaum() {
        return this.raum;
    }
    @Override
    public List<Student> getStudenten() {
        return this.studenten;
    }
    @Override
    public void addSingleToStudenten(Student studenten) {
       this.studenten.add(studenten);
    }
    @Override
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
    @Override
    public void setDozent(Optional<Dozent> dozent) {
        this.dozent = dozent;
    }
    @Override
    public void setDozent(Dozent dozent) {
        this.dozent = Optional.of(dozent);
    }
    @Override
    public void setFachbereich(Optional<Fachbereich> fachbereich) {
        this.fachbereich = fachbereich;
    }
    @Override
    public void setFachbereich(Fachbereich fachbereich) {
        this.fachbereich = Optional.of(fachbereich);
    }
    @Override
    public void setRaum(Optional<Raum> raum) {
        this.raum = raum;
    }
    @Override
    public void setRaum(Raum raum) {
        this.raum = Optional.of(raum);
    }
    @Override
    public void setStudenten(List<Student> studenten) {
        this.studenten = studenten;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getBezeichnung() != null ? getBezeichnung().hashCode() : 0);
        result = 31 * result + (getDozent() != null ? getDozent().hashCode() : 0);
        result = 31 * result + (getFachbereich() != null ? getFachbereich().hashCode() : 0);
        result = 31 * result + (getRaum() != null ? getRaum().hashCode() : 0);
        result = 31 * result + (getStudenten() != null ? getStudenten().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        Vorlesung vorlesung = (Vorlesung) o;
    
        if (getBezeichnung() != null ? !getBezeichnung().equals(vorlesung.getBezeichnung()) : vorlesung.getBezeichnung() != null) return false;
        if (getDozent() != null ? !getDozent().equals(vorlesung.getDozent()) : vorlesung.getDozent() != null) return false;
        if (getFachbereich() != null ? !getFachbereich().equals(vorlesung.getFachbereich()) : vorlesung.getFachbereich() != null) return false;
        if (getRaum() != null ? !getRaum().equals(vorlesung.getRaum()) : vorlesung.getRaum() != null) return false;
        if (getStudenten() != null ? !getStudenten().equals(vorlesung.getStudenten()) : vorlesung.getStudenten() != null) return false;
    
        return true;
    }

    @Override
    public <D> D accept(PersonRolleVisitor<D> visitor) {
       return visitor.visit(this);
    }
    @Override
    public void accept(PersonRolleSimpleVisitor visitor) {
       visitor.visit(this);
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
