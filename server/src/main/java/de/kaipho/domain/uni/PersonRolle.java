package de.kaipho.domain.uni;

import de.kaipho.domain.uni.impl.*;
import de.kaipho.domain.core.PersistentElement;

import java.util.Arrays;
import java.util.Optional;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.core.DerivedManager;
import de.kaipho.core.db.DbConnector;
import de.kaipho.domain.uni.impl.DozentRepositoryProxy;
import java.util.OptionalInt;
import de.kaipho.core.db.DbException;
import java.util.List;
import de.kaipho.core.db.DbFacade;
import java.util.stream.Collectors;
import java.sql.PreparedStatement;
import de.kaipho.domain.uni.impl.StudentRepositoryProxy;

public interface PersonRolle extends PersistentElement {
    static PersonRolle findOneById(Long id) throws DbException {
        if(id == null) return null;
        OptionalInt optionalClassId = DbFacade.getClassIdForInstanceId(id);
        int classId = optionalClassId.orElseThrow(DbException::new);
        return mapToInstance(classId, id);
    }
    
    static PersonRolle mapToInstance(Integer classId, Long id) {
        if(classId == 23) {
            return new DozentRepositoryProxy(id);
        }
        if(classId == 16) {
            return new StudentRepositoryProxy(id);
        }
        throw new DbException("Class id not handled in the case 'PersonRolle'!");
    }
    static List<PersonRolle> findAll() throws DbException {
        return DbFacade.getAll(22L, PersonRolle::mapToInstance);
    }
    
    static List<PersonRolle> findBy(Long association, Object s) throws DbException {
        return DbFacade.getByIdAndAssociationLike(22L, association, s)
                       .stream()
                       .map(PersonRolle::findOneById)
                       .collect(Collectors.toList());
    }

    Optional<Person> getPerson();
    

    <D> D accept(PersonRolleVisitor<D> visitor);
    void accept(PersonRolleSimpleVisitor visitor);
    <D> D accept(PersonRolleCompleteVisitor<D> visitor);
}
