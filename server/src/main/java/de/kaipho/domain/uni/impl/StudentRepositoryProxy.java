package de.kaipho.domain.uni.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;

import java.util.List;
import de.kaipho.domain.uni.Vorlesung;
import java.util.Optional;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.core.DerivedManager;
import de.kaipho.domain.uni.PersonRolleCompleteVisitor;
import de.kaipho.domain.uni.PersonRolleSimpleVisitor;
import de.kaipho.domain.category.Semester;
import de.kaipho.domain.uni.PersonRolleVisitor;
import java.util.ArrayList;

public class StudentRepositoryProxy extends DbFacade implements Student {
    private StudentImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public StudentRepositoryProxy() {
        Long id= super.createObjInstance(UNI_STUDENT);
        real = new StudentImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public StudentRepositoryProxy(Long id) {
        this.real = new StudentImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setMatrikelnummer(cache.getInteger(links, UNI_STUDENT_MATRIKELNUMMER));
        real.setSemester(Semester.from(cache.getString(links, UNI_STUDENT_SEMESTER)));
        initLists();
    }

    private void initLists() {
    }

    public void constructAll() {
    }

    @Override
    public Optional<Person> getPerson() {
       return DerivedManager.getInstance().derivedPerson(this);
    }
    @Override
    public Long getMatrikelnummer() {
       return real.getMatrikelnummer();
    }
    @Override
    public Semester getSemester() {
       return real.getSemester();
    }
    @Override
    public List<Vorlesung> getBesuchteVorlesungen() {
       return DerivedManager.getInstance().derivedBesuchteVorlesungen(this);
    }
    @Override
    public void setMatrikelnummer(Long matrikelnummer) {
        setAssociation(real.getId(), matrikelnummer, UNI_STUDENT_MATRIKELNUMMER);
        real.setMatrikelnummer(matrikelnummer);
    }
    @Override
    public void addMatrikelnummer(Long matrikelnummer) {
       real.addMatrikelnummer(matrikelnummer);
    }
    @Override
    public void setSemester(Semester semester) {
        setAssociation(real.getId(), semester, UNI_STUDENT_SEMESTER);
        real.setSemester(semester);
    }

    public <D> D accept(PersonRolleVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(PersonRolleSimpleVisitor visitor) {
       visitor.visit(this);
    }
    public <D> D accept(PersonRolleCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof StudentRepositoryProxy) return getId().equals(((StudentRepositoryProxy) obj).getId());
        return false;
    }
}
