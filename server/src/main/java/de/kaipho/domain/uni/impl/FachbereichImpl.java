package de.kaipho.domain.uni.impl;

import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import java.util.List;
import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.core.DerivedManager;
import java.util.ArrayList;
import de.kaipho.domain.uni.Dozent;

public class FachbereichImpl implements Fachbereich {

	private Long id;
    private String bezeichnung;

    public FachbereichImpl() {
    }

    @Override
    public String getBezeichnung() {
        return this.bezeichnung;
    }
    @Override
    public List<Dozent> getDozenten() {
       return DerivedManager.getInstance().derivedDozenten(this);
    }
    @Override
    public List<Vorlesung> getVorlesungen() {
       return DerivedManager.getInstance().derivedVorlesungen(this);
    }
    @Override
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getBezeichnung() != null ? getBezeichnung().hashCode() : 0);
        result = 31 * result + (getDozenten() != null ? getDozenten().hashCode() : 0);
        result = 31 * result + (getVorlesungen() != null ? getVorlesungen().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        Fachbereich fachbereich = (Fachbereich) o;
    
        if (getBezeichnung() != null ? !getBezeichnung().equals(fachbereich.getBezeichnung()) : fachbereich.getBezeichnung() != null) return false;
        if (getDozenten() != null ? !getDozenten().equals(fachbereich.getDozenten()) : fachbereich.getDozenten() != null) return false;
        if (getVorlesungen() != null ? !getVorlesungen().equals(fachbereich.getVorlesungen()) : fachbereich.getVorlesungen() != null) return false;
    
        return true;
    }


    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
