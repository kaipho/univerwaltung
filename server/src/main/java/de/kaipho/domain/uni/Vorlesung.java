package de.kaipho.domain.uni;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.uni.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import de.kaipho.domain.uni.Student;
import java.util.Optional;
import de.kaipho.domain.uni.Raum;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.core.db.DbConnector;
import de.kaipho.domain.uni.PersonRolleSimpleVisitor;
import de.kaipho.domain.uni.impl.VorlesungRepositoryProxy;
import de.kaipho.core.db.DbException;
import de.kaipho.domain.uni.Dozent;
import java.util.ArrayList;
import java.util.List;
import de.kaipho.core.db.DbFacade;
import java.sql.PreparedStatement;
import de.kaipho.domain.uni.PersonRolleVisitor;

public interface Vorlesung extends PersistentElement {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static Vorlesung createUnchecked() {
            return new VorlesungImpl();
    }

    static Vorlesung create(
            String bezeichnung, Optional<Dozent> dozent, Optional<Fachbereich> fachbereich, Optional<Raum> raum
    ) {
        Vorlesung result = new VorlesungRepositoryProxy();
        result.setBezeichnung(bezeichnung);
        result.setDozent(dozent);
        result.setFachbereich(fachbereich);
        result.setRaum(raum);
        return result;
    }

	static Vorlesung findOneById(Long id) {
	   if(id == null) return null;
	   return new VorlesungRepositoryProxy(id);
	}
	static List<Vorlesung> findAll() throws DbException {
	    return DbFacade.getAll(17L, (classId, id) -> new VorlesungRepositoryProxy(id));
	}
	static List<Vorlesung> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(17L, association, s)
	                   .stream()
	                   .map(Vorlesung::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    String getBezeichnung();
    
    Optional<Dozent> getDozent();
    
    Optional<Fachbereich> getFachbereich();
    
    Optional<Raum> getRaum();
    
    List<Student> getStudenten();
    
    void setBezeichnung(String bezeichnung);
    
    void setDozent(Optional<Dozent> dozent);
    
    void setDozent(Dozent dozent);
    
    void setFachbereich(Optional<Fachbereich> fachbereich);
    
    void setFachbereich(Fachbereich fachbereich);
    
    void setRaum(Optional<Raum> raum);
    
    void setRaum(Raum raum);
    
    void setStudenten(List<Student> studenten);
    
    void addSingleToStudenten(Student studenten);
    

    public static VorlesungBuilder builder() {
        return new VorlesungBuilder();
    }
    
    public static class DoneableVorlesungBuilder<R> extends VorlesungBuilder {
        private MasterBuilder<R> master;
        public DoneableVorlesungBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableVorlesungBuilder<R> bezeichnung(String bezeichnung) {
           super.bezeichnung(bezeichnung);
           return this;
        }
        @Override
        public DoneableVorlesungBuilder<R> dozent(Optional<Dozent> dozent) {
           super.dozent(dozent);
           return this;
        }
        @Override
        public DoneableVorlesungBuilder<R> fachbereich(Optional<Fachbereich> fachbereich) {
           super.fachbereich(fachbereich);
           return this;
        }
        @Override
        public DoneableVorlesungBuilder<R> raum(Optional<Raum> raum) {
           super.raum(raum);
           return this;
        }
        @Override
        public DoneableVorlesungBuilder<R> studenten(List<Student> studenten) {
           super.studenten(studenten);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class VorlesungBuilder extends MasterBuilder<VorlesungBuilder> {
        private VorlesungRepositoryProxy vorlesungRepositoryProxy;
        private Vorlesung vorlesung;
    
        VorlesungBuilder() {
            this.vorlesungRepositoryProxy = new VorlesungRepositoryProxy();
            this.vorlesung = this.vorlesungRepositoryProxy;
        }
    
        public VorlesungBuilder bezeichnung(String bezeichnung) {
            this.vorlesung.setBezeichnung(bezeichnung);
            return this;
        }
        public VorlesungBuilder dozent(Optional<Dozent> dozent) {
            this.vorlesung.setDozent(dozent);
            return this;
        }
        public VorlesungBuilder fachbereich(Optional<Fachbereich> fachbereich) {
            this.vorlesung.setFachbereich(fachbereich);
            return this;
        }
        public VorlesungBuilder raum(Optional<Raum> raum) {
            this.vorlesung.setRaum(raum);
            return this;
        }
        public VorlesungBuilder studenten(List<Student> studenten) {
            this.vorlesung.setStudenten(studenten);
            return this;
        }
    
        public Vorlesung build() {
            this.vorlesungRepositoryProxy.constructAll();
            return this.vorlesung;
        }
    }

    public <D> D accept(PersonRolleVisitor<D> visitor);
    public void accept(PersonRolleSimpleVisitor visitor);
}
