package de.kaipho.domain.uni;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.uni.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import de.kaipho.domain.uni.Vorlesung;
import java.util.Optional;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.core.DerivedManager;
import de.kaipho.core.db.DbConnector;
import de.kaipho.domain.uni.PersonRolleCompleteVisitor;
import de.kaipho.domain.uni.PersonRolleSimpleVisitor;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.core.db.DbException;
import java.util.ArrayList;
import java.util.List;
import de.kaipho.core.db.DbFacade;
import java.sql.PreparedStatement;
import de.kaipho.domain.category.Semester;
import de.kaipho.domain.uni.PersonRolleVisitor;
import de.kaipho.domain.uni.impl.StudentRepositoryProxy;

public interface Student extends PersistentElement,PersonRolle {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static Student createUnchecked() {
            return new StudentImpl();
    }

    static Student create(
            Long matrikelnummer, Semester semester
    ) {
        Student result = new StudentRepositoryProxy();
        result.setMatrikelnummer(matrikelnummer);
        result.setSemester(semester);
        return result;
    }

	static Student findOneById(Long id) {
	   if(id == null) return null;
	   return new StudentRepositoryProxy(id);
	}
	static List<Student> findAll() throws DbException {
	    return DbFacade.getAll(16L, (classId, id) -> new StudentRepositoryProxy(id));
	}
	static List<Student> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(16L, association, s)
	                   .stream()
	                   .map(Student::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    Long getMatrikelnummer();
    
    Semester getSemester();
    
    List<Vorlesung> getBesuchteVorlesungen();
    
    void setMatrikelnummer(Long matrikelnummer);
    
    void addMatrikelnummer(Long matrikelnummer);
    
    void setSemester(Semester semester);
    

    public static StudentBuilder builder() {
        return new StudentBuilder();
    }
    
    public static class DoneableStudentBuilder<R> extends StudentBuilder {
        private MasterBuilder<R> master;
        public DoneableStudentBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableStudentBuilder<R> matrikelnummer(Long matrikelnummer) {
           super.matrikelnummer(matrikelnummer);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class StudentBuilder extends MasterBuilder<StudentBuilder> {
        private StudentRepositoryProxy studentRepositoryProxy;
        private Student student;
    
        StudentBuilder() {
            this.studentRepositoryProxy = new StudentRepositoryProxy();
            this.student = this.studentRepositoryProxy;
        }
    
        public StudentBuilder matrikelnummer(Long matrikelnummer) {
            this.student.setMatrikelnummer(matrikelnummer);
            return this;
        }
        public StudentBuilder semester(Semester semester) {
            this.student.setSemester(semester);
            return this;
        }
    
        public Student build() {
            this.studentRepositoryProxy.constructAll();
            return this.student;
        }
    }

    public <D> D accept(PersonRolleVisitor<D> visitor);
    public void accept(PersonRolleSimpleVisitor visitor);
    public <D> D accept(PersonRolleCompleteVisitor<D> visitor);
}
