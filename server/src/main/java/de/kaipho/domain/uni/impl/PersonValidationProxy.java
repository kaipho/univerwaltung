package de.kaipho.domain.uni.impl;

import de.kaipho.core.exceptions.ValidationException;
import de.kaipho.domain.MetadataKeys;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElement;

import java.util.regex.Pattern;
import java.util.List;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.security.UserVisitor;
import java.util.Optional;
import de.kaipho.domain.security.CustomUserCompleteVisitor;
import de.kaipho.domain.security.CustomUserSimpleVisitor;
import de.kaipho.domain.uni.PersonRolle;
import de.kaipho.domain.security.UserSimpleVisitor;
import java.util.ArrayList;
import java.util.List;
import de.kaipho.domain.security.UserCompleteVisitor;
import de.kaipho.domain.security.CustomUserVisitor;
import de.kaipho.domain.category.Language;

public class PersonValidationProxy implements Person {
    private Person real;

    /**
     * Creates a proxy for validation. So the setter may throw a ValidationException
     */
    public PersonValidationProxy(Person real) {
        this.real = real;
    }

    @Override public String getUsername() {
       return real.getUsername();
    }
    @Override public String getPassword() {
       return real.getPassword();
    }
    @Override public Language getLang() {
       return real.getLang();
    }
    @Override public List<Role> getRoles() {
       return real.getRoles();
    }
    @Override public List<Notification> getNotifications() {
       return real.getNotifications();
    }
    @Override public String getName() {
       return real.getName();
    }
    @Override public String getVorname() {
       return real.getVorname();
    }
    @Override public Optional<PersonRolle> getRolle() {
       return real.getRolle();
    }
    @Override public void setUsername(String username) {
       List<Person> foundingOfPerson = Person.findBy(MetadataKeys.SECURITY_USER_USERNAME, username);
       if (!foundingOfPerson.isEmpty()) {
          throw new ValidationException("validation.errors.unique.uni.username");
       }
       real.setUsername(username);
    }
    @Override public void setPassword(String password) {
       real.setPassword(password);
    }
    @Override public void setLang(Language lang) {
       real.setLang(lang);
    }
    @Override public void setRoles(List<Role> roles) {
       real.setRoles(roles);
    }
    @Override public void addSingleToRoles(Role roles) {
    	real.addSingleToRoles(roles);
    }
    @Override public void setNotifications(List<Notification> notifications) {
       real.setNotifications(notifications);
    }
    @Override public void addSingleToNotifications(Notification notifications) {
    	real.addSingleToNotifications(notifications);
    }
    @Override public void setName(String name) {
       real.setName(name);
    }
    @Override public void setVorname(String vorname) {
       real.setVorname(vorname);
    }
    @Override public void setRolle(Optional<PersonRolle> rolle) {
       real.setRolle(rolle);
    }
    @Override public void setRolle(PersonRolle rolle) {
       this.setRolle(Optional.of(rolle));
    }


    @Override
    public Long getId() {
        return real.getId();
    }

    public <D> D accept(CustomUserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(CustomUserSimpleVisitor visitor) {
       visitor.visit(this);
    }
    public <D> D accept(UserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(UserSimpleVisitor visitor) {
       visitor.visit(this);
    }
    public <D> D accept(CustomUserCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public <D> D accept(UserCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
