package de.kaipho.domain.uni.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;

import de.kaipho.domain.category.Titel;
import java.util.Optional;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.uni.Fachbereich;
import de.kaipho.domain.core.DerivedManager;
import de.kaipho.domain.uni.PersonRolleCompleteVisitor;
import de.kaipho.domain.uni.PersonRolleSimpleVisitor;
import de.kaipho.domain.uni.PersonRolleVisitor;

public class DozentRepositoryProxy extends DbFacade implements Dozent {
    private DozentImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public DozentRepositoryProxy() {
        Long id= super.createObjInstance(UNI_DOZENT);
        real = new DozentImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public DozentRepositoryProxy(Long id) {
        this.real = new DozentImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setTitel(Titel.from(cache.getString(links, UNI_DOZENT_TITEL)));
        real.setPersonalnummer(cache.getString(links, UNI_DOZENT_PERSONALNUMMER));
        initLists();
    }

    private void initLists() {
    }

    public void constructAll() {
        if(real.getFachbereich() == null) {
           this.setFachbereich(Optional.empty());
        }
    }

    @Override
    public Optional<Person> getPerson() {
       return DerivedManager.getInstance().derivedPerson(this);
    }
    @Override
    public Titel getTitel() {
       return real.getTitel();
    }
    @Override
    public String getPersonalnummer() {
       return real.getPersonalnummer();
    }
    @Override public Optional<Fachbereich> getFachbereich() {
       if(real.getFachbereich() == null) {
           real.setFachbereich(Optional.ofNullable(Fachbereich
               .findOneById(getObjectForAssociationsOptional(real.getId(), UNI_DOZENT_FACHBEREICH))));
       }
       return real.getFachbereich();
    }
    @Override
    public void setTitel(Titel titel) {
        setAssociation(real.getId(), titel, UNI_DOZENT_TITEL);
        real.setTitel(titel);
    }
    @Override
    public void setPersonalnummer(String personalnummer) {
        setAssociation(real.getId(), personalnummer, UNI_DOZENT_PERSONALNUMMER);
        real.setPersonalnummer(personalnummer);
    }
    @Override
    public void setFachbereich(Optional<Fachbereich> fachbereich) {
        setAssociation(real.getId(), fachbereich.orElse(null), UNI_DOZENT_FACHBEREICH);
        real.setFachbereich(fachbereich);
    }
    @Override
    public void setFachbereich(Fachbereich fachbereich) {
        setAssociation(real.getId(), fachbereich, UNI_DOZENT_FACHBEREICH);
        real.setFachbereich(fachbereich);
    }

    public <D> D accept(PersonRolleVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(PersonRolleSimpleVisitor visitor) {
       visitor.visit(this);
    }
    public <D> D accept(PersonRolleCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof DozentRepositoryProxy) return getId().equals(((DozentRepositoryProxy) obj).getId());
        return false;
    }
}
