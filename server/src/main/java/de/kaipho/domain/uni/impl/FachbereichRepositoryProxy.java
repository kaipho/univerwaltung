package de.kaipho.domain.uni.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.uni.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;

import java.util.List;
import de.kaipho.domain.uni.Vorlesung;
import de.kaipho.domain.core.DerivedManager;
import java.util.ArrayList;
import de.kaipho.domain.uni.Dozent;

public class FachbereichRepositoryProxy extends DbFacade implements Fachbereich {
    private FachbereichImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public FachbereichRepositoryProxy() {
        Long id= super.createObjInstance(UNI_FACHBEREICH);
        real = new FachbereichImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public FachbereichRepositoryProxy(Long id) {
        this.real = new FachbereichImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setBezeichnung(cache.getString(links, UNI_FACHBEREICH_BEZEICHNUNG));
        initLists();
    }

    private void initLists() {
    }

    public void constructAll() {
    }

    @Override
    public String getBezeichnung() {
       return real.getBezeichnung();
    }
    @Override
    public List<Dozent> getDozenten() {
       return DerivedManager.getInstance().derivedDozenten(this);
    }
    @Override
    public List<Vorlesung> getVorlesungen() {
       return DerivedManager.getInstance().derivedVorlesungen(this);
    }
    @Override
    public void setBezeichnung(String bezeichnung) {
        setAssociation(real.getId(), bezeichnung, UNI_FACHBEREICH_BEZEICHNUNG);
        real.setBezeichnung(bezeichnung);
    }


    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof FachbereichRepositoryProxy) return getId().equals(((FachbereichRepositoryProxy) obj).getId());
        return false;
    }
}
