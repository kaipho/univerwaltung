package de.kaipho.domain.security.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.security.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;

import de.kaipho.domain.security.User;

/**
 * The view-model containing login information
 */
public class LoginVMRepositoryProxy extends DbFacade implements LoginVM {
    private LoginVMImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public LoginVMRepositoryProxy() {
        Long id= super.createObjInstance(SECURITY_LOGINVM);
        real = new LoginVMImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public LoginVMRepositoryProxy(Long id) {
        this.real = new LoginVMImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setToken(cache.getString(links, SECURITY_LOGINVM_TOKEN));
        initLists();
    }

    private void initLists() {
    }

    public void constructAll() {
    }

    @Override
    public String getToken() {
       return real.getToken();
    }
    @Override public User getUser() {
       if(real.getUser().getId() == null) {
           real.setUser(User.findOneById(getObjectForAssociations(real.getId(), SECURITY_LOGINVM_USER)));
       }
       return real.getUser();
    }
    @Override
    public void setToken(String token) {
        setAssociation(real.getId(), token, SECURITY_LOGINVM_TOKEN);
        real.setToken(token);
    }
    @Override
    public void setUser(User user) {
        setAssociation(real.getId(), user, SECURITY_LOGINVM_USER);
        real.setUser(user);
    }


    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof LoginVMRepositoryProxy) return getId().equals(((LoginVMRepositoryProxy) obj).getId());
        return false;
    }
}
