package de.kaipho.domain.security;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.category.Language;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.domain.security.AnonymUser;
import de.kaipho.domain.security.CustomUser;

public interface UserVisitor<D> {
    D visit(CustomUser customUser);
    D visit(Language language);
    D visit(Role role);
    D visit(Notification notification);
    D visit(AnonymUser anonymUser);
    D visit(TechnicalUser technicalUser);
}
