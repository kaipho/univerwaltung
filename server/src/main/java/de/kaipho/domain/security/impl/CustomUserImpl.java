package de.kaipho.domain.security.impl;

import de.kaipho.domain.security.*;

public abstract class CustomUserImpl implements CustomUser {
   @Override
   public <D> D accept(UserVisitor<D> visitor) {
      return visitor.visit(this);
   }
   @Override
   public void accept(UserSimpleVisitor visitor) {
      visitor.visit(this);
   }
}
