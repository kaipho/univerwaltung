package de.kaipho.domain.security;

import de.kaipho.domain.uni.Person;

public interface CustomUserCompleteVisitor<R> {
    R visit(Person person);
}
