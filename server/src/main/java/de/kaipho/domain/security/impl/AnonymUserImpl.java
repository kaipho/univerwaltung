package de.kaipho.domain.security.impl;

import de.kaipho.domain.security.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import java.util.List;
import de.kaipho.domain.category.Role;
import de.kaipho.domain.security.impl.UserImpl;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.security.UserVisitor;
import de.kaipho.domain.security.UserCompleteVisitor;
import de.kaipho.domain.category.Language;
import de.kaipho.domain.security.UserSimpleVisitor;
import java.util.ArrayList;

/**
 * User for unauthorized access or guest account
 */
public class AnonymUserImpl extends UserImpl implements AnonymUser {

	private Long id;
    private String username;
    private String password;
    private Language lang;
    private List<Role> roles;
    private List<Notification> notifications;

    public AnonymUserImpl() {
        this.roles = new ArrayList<>();
        this.notifications = new ArrayList<>();
    }

    @Override
    public String getUsername() {
        return this.username;
    }
    @Override
    public String getPassword() {
        return this.password;
    }
    @Override
    public Language getLang() {
        return this.lang;
    }
    @Override
    public List<Role> getRoles() {
        return this.roles;
    }
    @Override
    public void addSingleToRoles(Role roles) {
       this.roles.add(roles);
    }
    @Override
    public List<Notification> getNotifications() {
        return this.notifications;
    }
    @Override
    public void addSingleToNotifications(Notification notifications) {
       this.notifications.add(notifications);
    }
    @Override
    public void setUsername(String username) {
        this.username = username;
    }
    @Override
    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public void setLang(Language lang) {
        this.lang = lang;
    }
    @Override
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    @Override
    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getUsername() != null ? getUsername().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getLang() != null ? getLang().hashCode() : 0);
        result = 31 * result + (getRoles() != null ? getRoles().hashCode() : 0);
        result = 31 * result + (getNotifications() != null ? getNotifications().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        AnonymUser anonymUser = (AnonymUser) o;
    
        if (getUsername() != null ? !getUsername().equals(anonymUser.getUsername()) : anonymUser.getUsername() != null) return false;
        if (getPassword() != null ? !getPassword().equals(anonymUser.getPassword()) : anonymUser.getPassword() != null) return false;
        if (getLang() != null ? !getLang().equals(anonymUser.getLang()) : anonymUser.getLang() != null) return false;
        if (getRoles() != null ? !getRoles().equals(anonymUser.getRoles()) : anonymUser.getRoles() != null) return false;
        if (getNotifications() != null ? !getNotifications().equals(anonymUser.getNotifications()) : anonymUser.getNotifications() != null) return false;
    
        return true;
    }

    @Override
    public <D> D accept(UserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    @Override
    public void accept(UserSimpleVisitor visitor) {
       visitor.visit(this);
    }
    @Override
    public <D> D accept(UserCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
