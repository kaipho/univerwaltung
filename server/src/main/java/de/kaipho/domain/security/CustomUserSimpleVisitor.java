package de.kaipho.domain.security;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.category.Language;

public interface CustomUserSimpleVisitor {
    void visit(Person person);
    void visit(Language language);
    void visit(Role role);
    void visit(Notification notification);
}
