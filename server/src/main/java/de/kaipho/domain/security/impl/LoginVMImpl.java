package de.kaipho.domain.security.impl;

import de.kaipho.domain.security.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.domain.security.User;

/**
 * The view-model containing login information
 */
public class LoginVMImpl implements LoginVM {

	private Long id;
    private String token;
    private User user;

    public LoginVMImpl() {
    }

    @Override
    public String getToken() {
        return this.token;
    }
    @Override
    public User getUser() {
        return this.user;
    }
    @Override
    public void setToken(String token) {
        this.token = token;
    }
    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getToken() != null ? getToken().hashCode() : 0);
        result = 31 * result + (getUser() != null ? getUser().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        LoginVM loginVM = (LoginVM) o;
    
        if (getToken() != null ? !getToken().equals(loginVM.getToken()) : loginVM.getToken() != null) return false;
        if (getUser() != null ? !getUser().equals(loginVM.getUser()) : loginVM.getUser() != null) return false;
    
        return true;
    }


    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
