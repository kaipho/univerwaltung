package de.kaipho.domain.security;

import de.kaipho.domain.uni.Person;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.domain.security.AnonymUser;

public interface UserCompleteVisitor<R> {
    R visit(Person person);
    R visit(AnonymUser anonymUser);
    R visit(TechnicalUser technicalUser);
}
