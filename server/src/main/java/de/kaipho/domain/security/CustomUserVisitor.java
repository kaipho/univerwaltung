package de.kaipho.domain.security;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.uni.Person;
import de.kaipho.domain.category.Language;

public interface CustomUserVisitor<D> {
    D visit(Person person);
    D visit(Language language);
    D visit(Role role);
    D visit(Notification notification);
}
