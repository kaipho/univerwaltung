package de.kaipho.domain.security;

import de.kaipho.domain.security.impl.*;
import de.kaipho.domain.core.PersistentElement;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.security.impl.AnonymUserRepositoryProxy;
import java.util.Arrays;
import de.kaipho.core.db.DbConnector;
import de.kaipho.domain.security.impl.TechnicalUserRepositoryProxy;
import java.util.OptionalInt;
import de.kaipho.core.db.DbException;
import java.util.ArrayList;
import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.uni.impl.PersonRepositoryProxy;
import java.util.stream.Collectors;
import java.sql.PreparedStatement;
import de.kaipho.domain.category.Language;

public interface User extends PersistentElement {
    static User findOneById(Long id) throws DbException {
        if(id == null) return null;
        OptionalInt optionalClassId = DbFacade.getClassIdForInstanceId(id);
        int classId = optionalClassId.orElseThrow(DbException::new);
        return mapToInstance(classId, id);
    }
    
    static User mapToInstance(Integer classId, Long id) {
        if(classId == 21) {
            return new PersonRepositoryProxy(id);
        }
        if(classId == 11) {
            return new AnonymUserRepositoryProxy(id);
        }
        if(classId == 35) {
            return new TechnicalUserRepositoryProxy(id);
        }
        throw new DbException("Class id not handled in the case 'User'!");
    }
    static List<User> findAll() throws DbException {
        return DbFacade.getAll(9L, User::mapToInstance);
    }
    
    static List<User> findBy(Long association, Object s) throws DbException {
        return DbFacade.getByIdAndAssociationLike(9L, association, s)
                       .stream()
                       .map(User::findOneById)
                       .collect(Collectors.toList());
    }

    String getUsername();
    
    String getPassword();
    
    Language getLang();
    
    List<Role> getRoles();
    
    List<Notification> getNotifications();
    
    void setUsername(String username);
    
    void setPassword(String password);
    
    void setLang(Language lang);
    
    void setRoles(List<Role> roles);
    
    void addSingleToRoles(Role roles);
    
    void setNotifications(List<Notification> notifications);
    
    void addSingleToNotifications(Notification notifications);
    

    <D> D accept(UserVisitor<D> visitor);
    void accept(UserSimpleVisitor visitor);
    <D> D accept(UserCompleteVisitor<D> visitor);
}
