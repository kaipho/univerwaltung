package de.kaipho.domain.security.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.security.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;

import java.util.List;
import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.security.UserVisitor;
import de.kaipho.domain.security.UserCompleteVisitor;
import de.kaipho.domain.category.Language;
import de.kaipho.domain.security.UserSimpleVisitor;
import java.util.ArrayList;

/**
 * User for unauthorized access or guest account
 */
public class AnonymUserRepositoryProxy extends DbFacade implements AnonymUser {
    private AnonymUserImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public AnonymUserRepositoryProxy() {
        Long id= super.createObjInstance(SECURITY_ANONYMUSER);
        real = new AnonymUserImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public AnonymUserRepositoryProxy(Long id) {
        this.real = new AnonymUserImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setUsername(cache.getString(links, SECURITY_USER_USERNAME));
        real.setPassword(cache.getString(links, SECURITY_USER_PASSWORD));
        real.setLang(Language.from(cache.getString(links, SECURITY_USER_LANG)));
        links.getOrDefault(SECURITY_USER_ROLES, new ArrayList<>()).forEach(link ->
               real.getRoles().add(Role.from(cache.getString(link.getToObj())))
        );
        getObjList(real.getId(), SECURITY_USER_NOTIFICATIONS).forEach((key, val) ->
               real.getNotifications().add(Notification.findOneById(val))
        );
        initLists();
    }

    private void initLists() {
        real.setRoles(new StoredList<>(SECURITY_USER_ROLES, this, real.getId(), real.getRoles()));
        real.setNotifications(new StoredList<>(SECURITY_USER_NOTIFICATIONS, this, real.getId(), real.getNotifications()));
    }

    public void constructAll() {
    }

    @Override
    public String getUsername() {
       return real.getUsername();
    }
    @Override
    public String getPassword() {
       return real.getPassword();
    }
    @Override
    public Language getLang() {
       return real.getLang();
    }
    @Override
    public List<Role> getRoles() {
       return real.getRoles();
    }
    @Override
    public List<Notification> getNotifications() {
       return real.getNotifications();
    }
    @Override
    public void setUsername(String username) {
        setAssociation(real.getId(), username, SECURITY_USER_USERNAME);
        real.setUsername(username);
    }
    @Override
    public void setPassword(String password) {
        setAssociation(real.getId(), password, SECURITY_USER_PASSWORD);
        real.setPassword(password);
    }
    @Override
    public void setLang(Language lang) {
        setAssociation(real.getId(), lang, SECURITY_USER_LANG);
        real.setLang(lang);
    }
    @Override
    public void setRoles(List<Role> roles) {
        setAssociation(real.getId(), roles, SECURITY_USER_ROLES);
        real.setRoles(roles);
    }
    @Override
    public void addSingleToRoles(Role roles) {
       real.addSingleToRoles(roles);
    }
    @Override
    public void setNotifications(List<Notification> notifications) {
        setAssociation(real.getId(), notifications, SECURITY_USER_NOTIFICATIONS);
        real.setNotifications(notifications);
    }
    @Override
    public void addSingleToNotifications(Notification notifications) {
       real.addSingleToNotifications(notifications);
    }

    public <D> D accept(UserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(UserSimpleVisitor visitor) {
       visitor.visit(this);
    }
    public <D> D accept(UserCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof AnonymUserRepositoryProxy) return getId().equals(((AnonymUserRepositoryProxy) obj).getId());
        return false;
    }
}
