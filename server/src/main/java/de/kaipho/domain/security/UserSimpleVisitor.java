package de.kaipho.domain.security;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.category.Language;
import de.kaipho.domain.security.TechnicalUser;
import de.kaipho.domain.security.AnonymUser;
import de.kaipho.domain.security.CustomUser;

public interface UserSimpleVisitor {
    void visit(CustomUser customUser);
    void visit(Language language);
    void visit(Role role);
    void visit(Notification notification);
    void visit(AnonymUser anonymUser);
    void visit(TechnicalUser technicalUser);
}
