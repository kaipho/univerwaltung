package de.kaipho.domain.security;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.security.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.security.UserVisitor;
import de.kaipho.core.db.DbConnector;
import de.kaipho.domain.security.impl.TechnicalUserRepositoryProxy;
import de.kaipho.core.db.DbException;
import de.kaipho.domain.security.UserSimpleVisitor;
import java.util.ArrayList;
import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.security.UserCompleteVisitor;
import java.sql.PreparedStatement;
import de.kaipho.domain.category.Language;
import de.kaipho.domain.security.User;

public interface TechnicalUser extends PersistentElement,User {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static TechnicalUser createUnchecked() {
            return new TechnicalUserImpl();
    }

    static TechnicalUser create(
            String username, String password, Language lang, String description
    ) {
        TechnicalUser result = new TechnicalUserValidationProxy(new TechnicalUserRepositoryProxy());
        result.setUsername(username);
        result.setPassword(password);
        result.setLang(lang);
        result.setDescription(description);
        return result;
    }

	static TechnicalUser findOneById(Long id) {
	   if(id == null) return null;
	   return new TechnicalUserValidationProxy(new TechnicalUserRepositoryProxy(id));
	}
	static List<TechnicalUser> findAll() throws DbException {
	    return DbFacade.getAll(35L, (classId, id) -> new TechnicalUserValidationProxy(new TechnicalUserRepositoryProxy(id)));
	}
	static List<TechnicalUser> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(35L, association, s)
	                   .stream()
	                   .map(TechnicalUser::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    String getDescription();
    
    void setDescription(String description);
    

    public static TechnicalUserBuilder builder() {
        return new TechnicalUserBuilder();
    }
    
    public static class DoneableTechnicalUserBuilder<R> extends TechnicalUserBuilder {
        private MasterBuilder<R> master;
        public DoneableTechnicalUserBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableTechnicalUserBuilder<R> username(String username) {
           super.username(username);
           return this;
        }
        @Override
        public DoneableTechnicalUserBuilder<R> password(String password) {
           super.password(password);
           return this;
        }
        @Override
        public DoneableTechnicalUserBuilder<R> roles(List<Role> roles) {
           super.roles(roles);
           return this;
        }
        @Override
        public DoneableTechnicalUserBuilder<R> notifications(List<Notification> notifications) {
           super.notifications(notifications);
           return this;
        }
        @Override
        public DoneableTechnicalUserBuilder<R> description(String description) {
           super.description(description);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class TechnicalUserBuilder extends MasterBuilder<TechnicalUserBuilder> {
        private TechnicalUserRepositoryProxy technicalUserRepositoryProxy;
        private TechnicalUser technicalUser;
    
        TechnicalUserBuilder() {
            this.technicalUserRepositoryProxy = new TechnicalUserRepositoryProxy();
            this.technicalUser = new TechnicalUserValidationProxy(this.technicalUserRepositoryProxy);
        }
    
        public TechnicalUserBuilder username(String username) {
            this.technicalUser.setUsername(username);
            return this;
        }
        public TechnicalUserBuilder password(String password) {
            this.technicalUser.setPassword(password);
            return this;
        }
        public TechnicalUserBuilder lang(Language lang) {
            this.technicalUser.setLang(lang);
            return this;
        }
        public TechnicalUserBuilder roles(List<Role> roles) {
            this.technicalUser.setRoles(roles);
            return this;
        }
        public TechnicalUserBuilder notifications(List<Notification> notifications) {
            this.technicalUser.setNotifications(notifications);
            return this;
        }
        public TechnicalUserBuilder description(String description) {
            this.technicalUser.setDescription(description);
            return this;
        }
    
        public TechnicalUser build() {
            this.technicalUserRepositoryProxy.constructAll();
            return this.technicalUser;
        }
    }

    public <D> D accept(UserVisitor<D> visitor);
    public void accept(UserSimpleVisitor visitor);
    public <D> D accept(UserCompleteVisitor<D> visitor);
}
