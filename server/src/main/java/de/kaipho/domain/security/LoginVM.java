package de.kaipho.domain.security;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.security.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.core.db.DbConnector;
import java.sql.PreparedStatement;
import de.kaipho.domain.security.impl.LoginVMRepositoryProxy;
import de.kaipho.core.db.DbException;
import de.kaipho.domain.security.User;

/**
 * The view-model containing login information
 */
public interface LoginVM extends PersistentElement {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static LoginVM createUnchecked() {
            return new LoginVMImpl();
    }

    static LoginVM create(
            String token, User user
    ) {
        LoginVM result = new LoginVMRepositoryProxy();
        result.setToken(token);
        result.setUser(user);
        return result;
    }

	static LoginVM findOneById(Long id) {
	   if(id == null) return null;
	   return new LoginVMRepositoryProxy(id);
	}
	static List<LoginVM> findAll() throws DbException {
	    return DbFacade.getAll(12L, (classId, id) -> new LoginVMRepositoryProxy(id));
	}
	static List<LoginVM> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(12L, association, s)
	                   .stream()
	                   .map(LoginVM::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    String getToken();
    
    User getUser();
    
    void setToken(String token);
    
    void setUser(User user);
    

    public static LoginVMBuilder builder() {
        return new LoginVMBuilder();
    }
    
    public static class DoneableLoginVMBuilder<R> extends LoginVMBuilder {
        private MasterBuilder<R> master;
        public DoneableLoginVMBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableLoginVMBuilder<R> token(String token) {
           super.token(token);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class LoginVMBuilder extends MasterBuilder<LoginVMBuilder> {
        private LoginVMRepositoryProxy loginVMRepositoryProxy;
        private LoginVM loginVM;
    
        LoginVMBuilder() {
            this.loginVMRepositoryProxy = new LoginVMRepositoryProxy();
            this.loginVM = this.loginVMRepositoryProxy;
        }
    
        public LoginVMBuilder token(String token) {
            this.loginVM.setToken(token);
            return this;
        }
        public LoginVMBuilder user(User user) {
            this.loginVM.setUser(user);
            return this;
        }
    
        public LoginVM build() {
            this.loginVMRepositoryProxy.constructAll();
            return this.loginVM;
        }
    }

}
