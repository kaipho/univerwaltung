package de.kaipho.domain.security;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.security.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.security.UserVisitor;
import de.kaipho.domain.security.impl.AnonymUserRepositoryProxy;
import de.kaipho.core.db.DbConnector;
import de.kaipho.core.db.DbException;
import de.kaipho.domain.security.UserSimpleVisitor;
import java.util.ArrayList;
import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.security.UserCompleteVisitor;
import java.sql.PreparedStatement;
import de.kaipho.domain.category.Language;
import de.kaipho.domain.security.User;

/**
 * User for unauthorized access or guest account
 */
public interface AnonymUser extends PersistentElement,User {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static AnonymUser createUnchecked() {
            return new AnonymUserImpl();
    }

    static AnonymUser create(
            String username, String password, Language lang
    ) {
        AnonymUser result = new AnonymUserValidationProxy(new AnonymUserRepositoryProxy());
        result.setUsername(username);
        result.setPassword(password);
        result.setLang(lang);
        return result;
    }

	static AnonymUser findOneById(Long id) {
	   if(id == null) return null;
	   return new AnonymUserValidationProxy(new AnonymUserRepositoryProxy(id));
	}
	static List<AnonymUser> findAll() throws DbException {
	    return DbFacade.getAll(11L, (classId, id) -> new AnonymUserValidationProxy(new AnonymUserRepositoryProxy(id)));
	}
	static List<AnonymUser> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(11L, association, s)
	                   .stream()
	                   .map(AnonymUser::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}


    public static AnonymUserBuilder builder() {
        return new AnonymUserBuilder();
    }
    
    public static class DoneableAnonymUserBuilder<R> extends AnonymUserBuilder {
        private MasterBuilder<R> master;
        public DoneableAnonymUserBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableAnonymUserBuilder<R> username(String username) {
           super.username(username);
           return this;
        }
        @Override
        public DoneableAnonymUserBuilder<R> password(String password) {
           super.password(password);
           return this;
        }
        @Override
        public DoneableAnonymUserBuilder<R> roles(List<Role> roles) {
           super.roles(roles);
           return this;
        }
        @Override
        public DoneableAnonymUserBuilder<R> notifications(List<Notification> notifications) {
           super.notifications(notifications);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class AnonymUserBuilder extends MasterBuilder<AnonymUserBuilder> {
        private AnonymUserRepositoryProxy anonymUserRepositoryProxy;
        private AnonymUser anonymUser;
    
        AnonymUserBuilder() {
            this.anonymUserRepositoryProxy = new AnonymUserRepositoryProxy();
            this.anonymUser = new AnonymUserValidationProxy(this.anonymUserRepositoryProxy);
        }
    
        public AnonymUserBuilder username(String username) {
            this.anonymUser.setUsername(username);
            return this;
        }
        public AnonymUserBuilder password(String password) {
            this.anonymUser.setPassword(password);
            return this;
        }
        public AnonymUserBuilder lang(Language lang) {
            this.anonymUser.setLang(lang);
            return this;
        }
        public AnonymUserBuilder roles(List<Role> roles) {
            this.anonymUser.setRoles(roles);
            return this;
        }
        public AnonymUserBuilder notifications(List<Notification> notifications) {
            this.anonymUser.setNotifications(notifications);
            return this;
        }
    
        public AnonymUser build() {
            this.anonymUserRepositoryProxy.constructAll();
            return this.anonymUser;
        }
    }

    public <D> D accept(UserVisitor<D> visitor);
    public void accept(UserSimpleVisitor visitor);
    public <D> D accept(UserCompleteVisitor<D> visitor);
}
