package de.kaipho.domain.security;

import de.kaipho.domain.security.impl.*;
import de.kaipho.domain.core.PersistentElement;

import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.security.UserVisitor;
import java.util.Arrays;
import de.kaipho.core.db.DbConnector;
import java.util.OptionalInt;
import de.kaipho.core.db.DbException;
import de.kaipho.domain.security.UserSimpleVisitor;
import java.util.ArrayList;
import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.uni.impl.PersonRepositoryProxy;
import java.util.stream.Collectors;
import java.sql.PreparedStatement;
import de.kaipho.domain.category.Language;

public interface CustomUser extends PersistentElement, User {
    static CustomUser findOneById(Long id) throws DbException {
        if(id == null) return null;
        OptionalInt optionalClassId = DbFacade.getClassIdForInstanceId(id);
        int classId = optionalClassId.orElseThrow(DbException::new);
        return mapToInstance(classId, id);
    }
    
    static CustomUser mapToInstance(Integer classId, Long id) {
        if(classId == 21) {
            return new PersonRepositoryProxy(id);
        }
        throw new DbException("Class id not handled in the case 'CustomUser'!");
    }
    static List<CustomUser> findAll() throws DbException {
        return DbFacade.getAll(10L, CustomUser::mapToInstance);
    }
    
    static List<CustomUser> findBy(Long association, Object s) throws DbException {
        return DbFacade.getByIdAndAssociationLike(10L, association, s)
                       .stream()
                       .map(CustomUser::findOneById)
                       .collect(Collectors.toList());
    }


    <D> D accept(CustomUserVisitor<D> visitor);
    void accept(CustomUserSimpleVisitor visitor);
    <D> D accept(CustomUserCompleteVisitor<D> visitor);
    <D> D accept(UserVisitor<D> visitor);
    void accept(UserSimpleVisitor visitor);
    <D> D accept(UserCompleteVisitor<D> visitor);
}
