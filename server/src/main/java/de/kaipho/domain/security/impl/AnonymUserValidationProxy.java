package de.kaipho.domain.security.impl;

import de.kaipho.core.exceptions.ValidationException;
import de.kaipho.domain.MetadataKeys;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.domain.security.*;
import de.kaipho.domain.core.PersistentElement;

import java.util.regex.Pattern;
import java.util.List;

import java.util.List;
import de.kaipho.domain.category.Role;
import de.kaipho.domain.core.Notification;
import de.kaipho.domain.security.UserVisitor;
import de.kaipho.domain.security.UserCompleteVisitor;
import de.kaipho.domain.category.Language;
import de.kaipho.domain.security.UserSimpleVisitor;
import java.util.ArrayList;

/**
 * User for unauthorized access or guest account
 */
public class AnonymUserValidationProxy implements AnonymUser {
    private AnonymUser real;

    /**
     * Creates a proxy for validation. So the setter may throw a ValidationException
     */
    public AnonymUserValidationProxy(AnonymUser real) {
        this.real = real;
    }

    @Override public String getUsername() {
       return real.getUsername();
    }
    @Override public String getPassword() {
       return real.getPassword();
    }
    @Override public Language getLang() {
       return real.getLang();
    }
    @Override public List<Role> getRoles() {
       return real.getRoles();
    }
    @Override public List<Notification> getNotifications() {
       return real.getNotifications();
    }
    @Override public void setUsername(String username) {
       List<AnonymUser> foundingOfAnonymUser = AnonymUser.findBy(MetadataKeys.SECURITY_USER_USERNAME, username);
       if (!foundingOfAnonymUser.isEmpty()) {
          throw new ValidationException("validation.errors.unique.security.username");
       }
       real.setUsername(username);
    }
    @Override public void setPassword(String password) {
       real.setPassword(password);
    }
    @Override public void setLang(Language lang) {
       real.setLang(lang);
    }
    @Override public void setRoles(List<Role> roles) {
       real.setRoles(roles);
    }
    @Override public void addSingleToRoles(Role roles) {
    	real.addSingleToRoles(roles);
    }
    @Override public void setNotifications(List<Notification> notifications) {
       real.setNotifications(notifications);
    }
    @Override public void addSingleToNotifications(Notification notifications) {
    	real.addSingleToNotifications(notifications);
    }


    @Override
    public Long getId() {
        return real.getId();
    }

    public <D> D accept(UserVisitor<D> visitor) {
       return visitor.visit(this);
    }
    public void accept(UserSimpleVisitor visitor) {
       visitor.visit(this);
    }
    public <D> D accept(UserCompleteVisitor<D> visitor) {
       return visitor.visit(this);
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
