package de.kaipho.domain.constant.uni;

import de.kaipho.i18n.I18nContext;
import de.kaipho.i18n.I18nInstance;
import de.kaipho.i18n.I18nModelContext;

public interface RepVorlesung extends I18nInstance {
    Long ID = 27L;

    static RepVorlesung get(Object bezeichnung, Object dozent, Object anzStudenten) {
        return new RepVorlesungImpl(bezeichnung, dozent, anzStudenten);
    }
    
    static I18nContext i18nModel() {
        return I18nModelContext.get(ID);
    }

}
