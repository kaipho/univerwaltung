package de.kaipho.domain.constant.uni;

import de.kaipho.domain.constant.core.PersistentConstant;
import de.kaipho.i18n.I18nContext;
import de.kaipho.i18n.I18nModelConstantContext;

import java.util.HashMap;
import java.util.Map;

class RepVorlesungImpl extends PersistentConstant implements RepVorlesung {
    private final Map<String, Object> data;

    RepVorlesungImpl(Object bezeichnung, Object dozent, Object anzStudenten) {
        super(RepVorlesung.ID);
        data = new HashMap<>();
        data.put("bezeichnung", bezeichnung);
        data.put("dozent", dozent);
        data.put("anzStudenten", anzStudenten);
    }

    @Override
    public I18nContext i18n() {
        return I18nModelConstantContext.get(ID, data);
    }
}
