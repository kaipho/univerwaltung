package de.kaipho.domain.category;

public class Titel extends PersistentCategory {
    public final static Long ID = 20L;

    private Titel(String value) {
        super(ID, value);
    }

    public static Titel empty() {
        return new Titel(null);
    }

    public static Titel from(String s) {
        return new Titel(s);
    }
}
