package de.kaipho.domain.category;

public class NotificationLevel extends PersistentCategory {
    public final static Long ID = 33L;

    private NotificationLevel(String value) {
        super(ID, value);
    }

    public static NotificationLevel empty() {
        return new NotificationLevel(null);
    }

    public static NotificationLevel from(String s) {
        return new NotificationLevel(s);
    }
}
