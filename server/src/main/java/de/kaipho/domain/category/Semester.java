package de.kaipho.domain.category;

public class Semester extends PersistentCategory {
    public final static Long ID = 19L;

    private Semester(String value) {
        super(ID, value);
    }

    public static Semester empty() {
        return new Semester(null);
    }

    public static Semester from(String s) {
        return new Semester(s);
    }
}
