package de.kaipho.domain.category;

public class Role extends PersistentCategory {
    public final static Long ID = 5L;

    private Role(String value) {
        super(ID, value);
    }

    public static Role empty() {
        return new Role(null);
    }

    public static Role from(String s) {
        return new Role(s);
    }

    public static Role ADMIN;
    public static Role EDIT;
    public static Role VERWALTUNG;
    public static Role DOZENT;
}
