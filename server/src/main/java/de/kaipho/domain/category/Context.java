package de.kaipho.domain.category;

public class Context extends PersistentCategory {
    public final static Long ID = 37L;

    private Context(String value) {
        super(ID, value);
    }

    public static Context empty() {
        return new Context(null);
    }

    public static Context from(String s) {
        return new Context(s);
    }
}
