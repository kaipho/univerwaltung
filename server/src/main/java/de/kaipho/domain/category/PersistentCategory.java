package de.kaipho.domain.category;

import de.kaipho.core.db.DbFacade;
import de.kaipho.service.core.CategoryNotExistInClassException;

/**
 * An instance of a Category, instance of a CategoryClass and pints to a Singleton Category in the DB!
 */
public class PersistentCategory {
    private final Long instanceOf;

    private final String value;

    public PersistentCategory(Long instanceOf, String value) {
        if (value != null && !DbFacade.validateStringReachedFromCategoryClass(instanceOf, value)) {
            throw new CategoryNotExistInClassException(value);
        }
        this.instanceOf = instanceOf;
        this.value = value;
    }

    public Long getInstanceOf() {
        return instanceOf;
    }

    public String getValue() {
        return value;
    }

    public boolean isEmpty() {
        return value == null;
    }
        
    @Override
    public String toString() {
        return getValue();
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersistentCategory that = (PersistentCategory) o;

        if (instanceOf != null ? !instanceOf.equals(that.instanceOf) : that.instanceOf != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = instanceOf != null ? instanceOf.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
