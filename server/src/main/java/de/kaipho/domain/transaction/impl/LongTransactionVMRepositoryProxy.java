package de.kaipho.domain.transaction.impl;

import de.kaipho.core.db.DbLink;
import de.kaipho.core.db.StoredList;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.core.db.primitives.PrimitivesStore;
import de.kaipho.domain.transaction.*;
import de.kaipho.domain.core.PersistentElement;
import de.kaipho.domain.core.ToStringVisitor;
import de.kaipho.core.db.cache.SingletonCache;
import de.kaipho.core.db.DbFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.kaipho.domain.MetadataKeys.*;


public class LongTransactionVMRepositoryProxy extends DbFacade implements LongTransactionVM {
    private LongTransactionVMImpl real;
    private PrimitivesStore primitivesStore = PrimitivesStore.getStore();

    /**
     * Creates the proxy for a new created object. This way, a new object in the db will be created.
     */
    public LongTransactionVMRepositoryProxy() {
        Long id= super.createObjInstance(TRANSACTION_LONGTRANSACTIONVM);
        real = new LongTransactionVMImpl();
        real.setId(id);
        initLists();
    }

    /**
     * Creates a proxy for a known object. This way, the id is needed to access the object in the db.
     */
    public LongTransactionVMRepositoryProxy(Long id) {
        this.real = new LongTransactionVMImpl();
        real.setId(id);

        Map<Long, List<DbLink>> links = DbFacade.loadLinks(id);
        SingletonCache cache = SingletonCache.getInstance();

        real.setLongTaId(cache.getInteger(links, TRANSACTION_LONGTRANSACTIONVM_LONGTAID));
        initLists();
    }

    private void initLists() {
    }

    public void constructAll() {
    }

    @Override
    public Long getLongTaId() {
       return real.getLongTaId();
    }
    @Override
    public void setLongTaId(Long longTaId) {
        setAssociation(real.getId(), longTaId, TRANSACTION_LONGTRANSACTIONVM_LONGTAID);
        real.setLongTaId(longTaId);
    }
    @Override
    public void addLongTaId(Long longTaId) {
       real.addLongTaId(longTaId);
    }


    @Override
    public Long getId() {
        return real.getId();
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj instanceof LongTransactionVMRepositoryProxy) return getId().equals(((LongTransactionVMRepositoryProxy) obj).getId());
        return false;
    }
}
