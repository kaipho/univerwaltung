package de.kaipho.domain.transaction;

import de.kaipho.domain.core.MasterBuilder;
import de.kaipho.domain.transaction.impl.*;
import de.kaipho.domain.core.PersistentElement;
import java.util.Arrays;
import java.util.stream.Collectors;

import java.util.List;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.transaction.impl.LongTransactionVMRepositoryProxy;
import de.kaipho.core.db.DbConnector;
import java.sql.PreparedStatement;
import de.kaipho.core.db.DbException;

public interface LongTransactionVM extends PersistentElement {

    /**
     * Creates an instance of this class without initial validations (e.g. required associations).
     * With this method, the instance is not stored in the DB.
     * Also there can be an error while storing it manually.
     */
    static LongTransactionVM createUnchecked() {
            return new LongTransactionVMImpl();
    }

    static LongTransactionVM create(
            Long longTaId
    ) {
        LongTransactionVM result = new LongTransactionVMRepositoryProxy();
        result.setLongTaId(longTaId);
        return result;
    }

	static LongTransactionVM findOneById(Long id) {
	   if(id == null) return null;
	   return new LongTransactionVMRepositoryProxy(id);
	}
	static List<LongTransactionVM> findAll() throws DbException {
	    return DbFacade.getAll(29L, (classId, id) -> new LongTransactionVMRepositoryProxy(id));
	}
	static List<LongTransactionVM> findBy(Long association, Object s) throws DbException {
	    return DbFacade.getByIdAndAssociationLike(29L, association, s)
	                   .stream()
	                   .map(LongTransactionVM::findOneById) // TODO needs optimisation!
	                   .collect(Collectors.toList());
	}

    Long getLongTaId();
    
    void setLongTaId(Long longTaId);
    
    void addLongTaId(Long longTaId);
    

    public static LongTransactionVMBuilder builder() {
        return new LongTransactionVMBuilder();
    }
    
    public static class DoneableLongTransactionVMBuilder<R> extends LongTransactionVMBuilder {
        private MasterBuilder<R> master;
        public DoneableLongTransactionVMBuilder(MasterBuilder<R> master) {
            super();
            this.master = master;
        }
    
        @Override
        public DoneableLongTransactionVMBuilder<R> longTaId(Long longTaId) {
           super.longTaId(longTaId);
           return this;
        }
    
        public R done() {
            return master.visit(this.build());
        }
    }
    
    public static class LongTransactionVMBuilder extends MasterBuilder<LongTransactionVMBuilder> {
        private LongTransactionVMRepositoryProxy longTransactionVMRepositoryProxy;
        private LongTransactionVM longTransactionVM;
    
        LongTransactionVMBuilder() {
            this.longTransactionVMRepositoryProxy = new LongTransactionVMRepositoryProxy();
            this.longTransactionVM = this.longTransactionVMRepositoryProxy;
        }
    
        public LongTransactionVMBuilder longTaId(Long longTaId) {
            this.longTransactionVM.setLongTaId(longTaId);
            return this;
        }
    
        public LongTransactionVM build() {
            this.longTransactionVMRepositoryProxy.constructAll();
            return this.longTransactionVM;
        }
    }

}
