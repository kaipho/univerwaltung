package de.kaipho.domain.transaction.impl;

import de.kaipho.domain.transaction.*;
import de.kaipho.domain.core.PersistentElementCompleteVisitor;
import de.kaipho.domain.core.ToStringVisitor;

public class LongTransactionVMImpl implements LongTransactionVM {

	private Long id;
    private Long longTaId;

    public LongTransactionVMImpl() {
    }

    @Override
    public Long getLongTaId() {
        return this.longTaId;
    }
    @Override
    public void addLongTaId(Long longTaId) {
       if(this.longTaId == null) this.longTaId = 0L;
       this.longTaId += longTaId;
    }
    @Override
    public void setLongTaId(Long longTaId) {
        this.longTaId = longTaId;
    }

    @Override
    public int hashCode() {
        int result = 1;
    
        result = 31 * result + (getLongTaId() != null ? getLongTaId().hashCode() : 0);
    
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
    
        LongTransactionVM longTransactionVM = (LongTransactionVM) o;
    
        if (getLongTaId() != null ? !getLongTaId().equals(longTransactionVM.getLongTaId()) : longTransactionVM.getLongTaId() != null) return false;
    
        return true;
    }


    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}
