-- model
CREATE TABLE IF NOT EXISTS class (
  id       INT PRIMARY KEY           NOT NULL,
  name     VARCHAR(255)              NOT NULL,
  abstract INT                       NOT NULL,
  type     VARCHAR(255)              NOT NULL
);

CREATE TABLE IF NOT EXISTS hierarchy (
  super   INT NOT NULL,
  sub     INT NOT NULL,
  derived INT NOT NULL,
  CONSTRAINT fk_super FOREIGN KEY (super) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT fk_sub FOREIGN KEY (sub) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT pk_hierarchy PRIMARY KEY (super, sub)
);

CREATE TABLE IF NOT EXISTS association (
  id     INT          NOT NULL,
  source INT          NOT NULL,
  target INT          NOT NULL,
  name   VARCHAR(255) NOT NULL,
  CONSTRAINT fk_source FOREIGN KEY (source) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT fk_target FOREIGN KEY (target) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT pk_association PRIMARY KEY (id, source, target, name)
);

CREATE TABLE IF NOT EXISTS callable (
  id     INT          NOT NULL,
  source INT          NOT NULL,
  target INT          NOT NULL,
  name   VARCHAR(255) NOT NULL,
  CONSTRAINT fk_callable_source FOREIGN KEY (source) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT fk_callable_target FOREIGN KEY (target) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT pk_callable_association PRIMARY KEY (id)
);

DELETE FROM class;
DELETE FROM hierarchy;
DELETE FROM association;
INSERT INTO class (id, name, abstract, type) VALUES (-1, 'PersistentElement', 1, 'class');
INSERT INTO class (id, name, abstract, type) VALUES (-2, 'GenString', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-3, 'GenDouble', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-4, 'GenInteger', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-5, 'GenDate', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-6, 'GenBoolean', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-7, 'GenText', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-8, 'GenPassword', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-9, 'GenUnit', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-10, 'GenBlob', 0, 'gen-primitive');

INSERT INTO association (id, source, target, name) VALUES (-1, -1, -4, 'id');
INSERT INTO callable (id, source, target, name) VALUES (0, -9, -9, 'default');

INSERT INTO class(id, name, abstract, type) VALUES (21, 'Person', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (22, 'PersonRolle', 1, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (16, 'Student', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (23, 'Dozent', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (17, 'Vorlesung', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (24, 'Fachbereich', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (25, 'Raum', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (6, 'ErrorVM', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (29, 'LongTransactionVM', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (7, 'CategoryClassVM', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (8, 'CategoryVM', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (9, 'User', 1, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (10, 'CustomUser', 1, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (11, 'AnonymUser', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (35, 'TechnicalUser', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (12, 'LoginVM', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (13, 'Notification', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (31, 'StatistikVM', 0, 'class');
INSERT INTO class(id, name, abstract, type) VALUES (19, 'Semester', 0, 'category_class');
INSERT INTO class(id, name, abstract, type) VALUES (20, 'Titel', 0, 'category_class');
INSERT INTO class(id, name, abstract, type) VALUES (4, 'Language', 0, 'category_class');
INSERT INTO class(id, name, abstract, type) VALUES (5, 'Role', 0, 'category_class');
INSERT INTO class(id, name, abstract, type) VALUES (33, 'NotificationLevel', 0, 'category_class');
INSERT INTO class(id, name, abstract, type) VALUES (37, 'Context', 0, 'category_class');
INSERT INTO class(id, name, abstract, type) VALUES (27, 'RepVorlesung', 0, 'constant');
INSERT INTO hierarchy(super, sub, derived) VALUES (21, 21, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 21, 1);
INSERT INTO hierarchy(super, sub, derived) VALUES (10, 21, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (9, 21, 1);
INSERT INTO hierarchy(super, sub, derived) VALUES (22, 22, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 22, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (16, 16, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 16, 1);
INSERT INTO hierarchy(super, sub, derived) VALUES (22, 16, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (23, 23, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 23, 1);
INSERT INTO hierarchy(super, sub, derived) VALUES (22, 23, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (17, 17, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 17, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (24, 24, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 24, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (25, 25, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 25, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (6, 6, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 6, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (29, 29, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 29, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (7, 7, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 7, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (8, 8, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 8, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (9, 9, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 9, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (10, 10, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 10, 1);
INSERT INTO hierarchy(super, sub, derived) VALUES (9, 10, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (11, 11, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 11, 1);
INSERT INTO hierarchy(super, sub, derived) VALUES (9, 11, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (35, 35, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 35, 1);
INSERT INTO hierarchy(super, sub, derived) VALUES (9, 35, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (12, 12, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 12, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (13, 13, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 13, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (31, 31, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (-1, 31, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (19, 19, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (20, 20, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (4, 4, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (5, 5, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (33, 33, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (37, 37, 0);
INSERT INTO hierarchy(super, sub, derived) VALUES (27, 27, 0);
INSERT INTO association (id, source, target, name) VALUES (32, 21, -2, 'name');
INSERT INTO association (id, source, target, name) VALUES (33, 21, -2, 'vorname');
INSERT INTO association (id, source, target, name) VALUES (34, 21, 22, 'rolle');
INSERT INTO association (id, source, target, name) VALUES (46, 22, 21, 'person');
INSERT INTO association (id, source, target, name) VALUES (35, 16, -4, 'matrikelnummer');
INSERT INTO association (id, source, target, name) VALUES (36, 16, 19, 'semester');
INSERT INTO association (id, source, target, name) VALUES (28, 16, 17, 'besuchteVorlesungen');
INSERT INTO association (id, source, target, name) VALUES (37, 23, 20, 'titel');
INSERT INTO association (id, source, target, name) VALUES (38, 23, -2, 'personalnummer');
INSERT INTO association (id, source, target, name) VALUES (39, 23, 24, 'fachbereich');
INSERT INTO association (id, source, target, name) VALUES (29, 17, -2, 'bezeichnung');
INSERT INTO association (id, source, target, name) VALUES (40, 17, 23, 'dozent');
INSERT INTO association (id, source, target, name) VALUES (41, 17, 24, 'fachbereich');
INSERT INTO association (id, source, target, name) VALUES (42, 17, 25, 'raum');
INSERT INTO association (id, source, target, name) VALUES (30, 17, 16, 'studenten');
INSERT INTO association (id, source, target, name) VALUES (43, 24, -2, 'bezeichnung');
INSERT INTO association (id, source, target, name) VALUES (48, 24, 23, 'dozenten');
INSERT INTO association (id, source, target, name) VALUES (49, 24, 17, 'vorlesungen');
INSERT INTO association (id, source, target, name) VALUES (44, 25, -2, 'bezeichnung');
INSERT INTO association (id, source, target, name) VALUES (1, 6, -4, 'code');
INSERT INTO association (id, source, target, name) VALUES (2, 6, -2, 'message');
INSERT INTO association (id, source, target, name) VALUES (51, 29, -4, 'longTaId');
INSERT INTO association (id, source, target, name) VALUES (3, 7, -4, 'dbId');
INSERT INTO association (id, source, target, name) VALUES (4, 7, -2, 'name');
INSERT INTO association (id, source, target, name) VALUES (5, 7, 8, 'values');
INSERT INTO association (id, source, target, name) VALUES (6, 8, -4, 'dbId');
INSERT INTO association (id, source, target, name) VALUES (7, 8, -2, 'name');
INSERT INTO association (id, source, target, name) VALUES (8, 8, -6, 'isDerived');
INSERT INTO association (id, source, target, name) VALUES (9, 9, -2, 'username');
INSERT INTO association (id, source, target, name) VALUES (10, 9, -8, 'password');
INSERT INTO association (id, source, target, name) VALUES (11, 9, 4, 'lang');
INSERT INTO association (id, source, target, name) VALUES (12, 9, 5, 'roles');
INSERT INTO association (id, source, target, name) VALUES (13, 9, 13, 'notifications');
INSERT INTO association (id, source, target, name) VALUES (56, 35, -7, 'description');
INSERT INTO association (id, source, target, name) VALUES (14, 12, -2, 'token');
INSERT INTO association (id, source, target, name) VALUES (15, 12, 9, 'user');
INSERT INTO association (id, source, target, name) VALUES (16, 13, -2, 'subject');
INSERT INTO association (id, source, target, name) VALUES (17, 13, -7, 'message');
INSERT INTO association (id, source, target, name) VALUES (18, 13, -5, 'creation');
INSERT INTO association (id, source, target, name) VALUES (19, 13, 33, 'type');
INSERT INTO association (id, source, target, name) VALUES (20, 13, -6, 'isRead');
INSERT INTO association (id, source, target, name) VALUES (53, 31, -2, 'name');
INSERT INTO association (id, source, target, name) VALUES (54, 31, -4, 'value');
INSERT INTO callable (id, source, target, name) VALUES (58, 23, -9, 'setFachbereich');
INSERT INTO callable (id, source, target, name) VALUES (59, 21, -9, 'setRolle');
INSERT INTO callable (id, source, target, name) VALUES (60, 16, -9, 'addVorlesung');
INSERT INTO callable (id, source, target, name) VALUES (61, 17, -9, 'setRaum');
INSERT INTO callable (id, source, target, name) VALUES (62, 17, -9, 'setFachbereichVorlesung');
INSERT INTO callable (id, source, target, name) VALUES (63, 17, -9, 'setDozent');
INSERT INTO callable (id, source, target, name) VALUES (64, -9, 12, 'login');
INSERT INTO callable (id, source, target, name) VALUES (65, -9, -9, 'logout');
INSERT INTO callable (id, source, target, name) VALUES (66, -9, -9, 'changePassword');
INSERT INTO callable (id, source, target, name) VALUES (67, 9, -2, 'resetPassword');
INSERT INTO callable (id, source, target, name) VALUES (68, -9, 12, 'getUserFromToken');
INSERT INTO callable (id, source, target, name) VALUES (69, -9, -3, 'convertTo');
INSERT INTO callable (id, source, target, name) VALUES (70, -9, -2, 'getModelForClass');
INSERT INTO callable (id, source, target, name) VALUES (71, -9, -2, 'getAllClasses');
INSERT INTO callable (id, source, target, name) VALUES (72, -9, 31, 'modelStatistics');
INSERT INTO callable (id, source, target, name) VALUES (73, -9, -2, 'getToken');
INSERT INTO callable (id, source, target, name) VALUES (74, -9, 9, 'parseToken');
INSERT INTO callable (id, source, target, name) VALUES (75, -9, -2, 'hashPassword');
INSERT INTO callable (id, source, target, name) VALUES (76, -9, -6, 'matchPasswordWithHash');
INSERT INTO callable (id, source, target, name) VALUES (77, 9, -9, 'addNotification');
INSERT INTO callable (id, source, target, name) VALUES (78, -9, 13, 'getNotifications');
INSERT INTO callable (id, source, target, name) VALUES (79, -9, -2, 'decrypt');
INSERT INTO callable (id, source, target, name) VALUES (80, -9, -2, 'encrypt');
INSERT INTO callable (id, source, target, name) VALUES (81, -9, -2, 'getKey');
INSERT INTO callable (id, source, target, name) VALUES (82, -9, -2, 'decrypt');
INSERT INTO callable (id, source, target, name) VALUES (83, -9, -2, 'encrypt');
INSERT INTO callable (id, source, target, name) VALUES (84, -9, -2, 'getValuesFor');
INSERT INTO callable (id, source, target, name) VALUES (85, -9, -9, 'addValueTo');
INSERT INTO callable (id, source, target, name) VALUES (86, -9, -9, 'deleteValue');
INSERT INTO callable (id, source, target, name) VALUES (87, -9, 7, 'getAllCategoryClasses');
INSERT INTO callable (id, source, target, name) VALUES (88, -9, -9, 'commit');
INSERT INTO callable (id, source, target, name) VALUES (89, -9, -9, 'startTransaction');
INSERT INTO callable (id, source, target, name) VALUES (90, -9, 29, 'openTransactions');
INSERT INTO callable (id, source, target, name) VALUES (91, -9, 31, 'transactionStatistics');

-- instance
CREATE SEQUENCE globalseq;

CREATE TABLE IF NOT EXISTS longTransaction (
  id       INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  userId   INT                                       NOT NULL,
  commited TIMESTAMP
);

CREATE TABLE IF NOT EXISTS shortTransaction (
  id         INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  instanceof INT                                       NOT NULL,
  belongsTo  INT                                       NOT NULL,
  CONSTRAINT fk_sTaBelongsTo FOREIGN KEY (belongsTo) REFERENCES longTransaction ON DELETE CASCADE,
  CONSTRAINT fk_sTaInstanceOf FOREIGN KEY (instanceof) REFERENCES callable ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS accessed (
  type        CHAR NOT NULL,
  transaction INT  NOT NULL,
  object      INT  NOT NULL,
  CONSTRAINT fk_accessed_transaction FOREIGN KEY (transaction) REFERENCES shortTransaction ON DELETE CASCADE,
  CONSTRAINT pk_accessed PRIMARY KEY (transaction, object)
);

CREATE TABLE IF NOT EXISTS obj (
  id         INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  instanceof INT                                       NOT NULL,
  createdBy  INT                                       NOT NULL,
  CONSTRAINT fk_objCreatedBy FOREIGN KEY (createdBy) REFERENCES shortTransaction ON DELETE CASCADE
);
CREATE INDEX i_obj_instanceof
  ON obj (instanceof);

CREATE TABLE IF NOT EXISTS val_integer (
  id  INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  val INT,
);
CREATE UNIQUE INDEX i_val_integer
  ON val_integer (val);

CREATE TABLE IF NOT EXISTS val_string (
  id  INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  val VARCHAR(2000),
);
CREATE UNIQUE INDEX i_val_string
  ON val_string (val);

CREATE TABLE IF NOT EXISTS val_datetime (
  id  INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  val DATETIME,
);
CREATE UNIQUE INDEX i_val_datetime
  ON val_datetime (val);

CREATE TABLE IF NOT EXISTS val_double (
  id  INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  val DOUBLE,
);
CREATE UNIQUE INDEX i_val_double
  ON val_double (val);

CREATE TABLE IF NOT EXISTS val_blob (
  id  INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  val BLOB,
);

CREATE TABLE IF NOT EXISTS val_rational (
  id          INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  numerator   BIGINT,
  denominator BIGINT
);
CREATE UNIQUE INDEX i_val_rational
  ON val_rational (numerator, denominator);

CREATE TABLE IF NOT EXISTS persistent_primitive (
  id       INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  fraction INT,
  unit     INT
);

CREATE TABLE IF NOT EXISTS category (
  instanceOf INT NOT NULL,
  fk_string  INT NOT NULL,
  CONSTRAINT fk_category_string FOREIGN KEY (fk_string) REFERENCES val_string ON DELETE CASCADE,
  CONSTRAINT pk_category PRIMARY KEY (instanceOf, fk_string)
);

CREATE TABLE IF NOT EXISTS internationalisation (
  rep        INT NOT NULL,
  lang       INT NOT NULL,
  translates INT NOT NULL,
  CONSTRAINT fk_in_rep FOREIGN KEY (rep) REFERENCES val_string ON DELETE CASCADE,
  CONSTRAINT fk_in_lang FOREIGN KEY (lang) REFERENCES val_string ON DELETE CASCADE,
  CONSTRAINT pk_in PRIMARY KEY (translates, lang)
);

CREATE TABLE IF NOT EXISTS link (
  id         INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  pos        INT                                       NOT NULL,
  fromObj    INT                                       NOT NULL,
  toObj      INT,
  named      INT,
  instanceof INT                                       NOT NULL,
  createdBy  INT                                       NOT NULL,
  CONSTRAINT fk_fromObj FOREIGN KEY (fromObj) REFERENCES obj ON DELETE CASCADE,
  CONSTRAINT fk_createdBy FOREIGN KEY (createdBy) REFERENCES shortTransaction ON DELETE CASCADE
);

CREATE INDEX i_link_instanceof
  ON link (instanceof);
CREATE INDEX i_fromObj
  ON link (fromObj);
CREATE INDEX i_toObj
  ON link (toObj);

CREATE VIEW transactionCommitted AS
  SELECT
    LONGTRANSACTION.commited,
    SHORTTRANSACTION.id
  FROM LONGTRANSACTION, SHORTTRANSACTION
  WHERE SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
        AND LONGTRANSACTION.COMMITED IS NOT NULL;

CREATE VIEW transactionOpen AS
  SELECT
    LONGTRANSACTION.ID  AS LTA_ID,
    SHORTTRANSACTION.id AS STA_ID
  FROM LONGTRANSACTION, SHORTTRANSACTION
  WHERE SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
        AND LONGTRANSACTION.COMMITED IS NULL;

CREATE VIEW linksNotDeleted AS
  SELECT LINK.id
  FROM transactionCommitted, LINK
  WHERE LINK.createdBy = transactionCommitted.id
        AND LINK.id NOT IN (SELECT OBJECT
                            FROM ACCESSED, transactionCommitted
                            WHERE type = 'D' AND transactionCommitted.id = ACCESSED.transaction);
