SELECT obj.id
FROM obj, val_string, link
WHERE obj.id = link.fromobj
      AND link.toobj = val_string.id
      AND link.instanceof = ?
      AND val_string.val LIKE ?
      AND obj.instanceof IN (SELECT SUB
                             FROM HIERARCHY
                             WHERE SUPER = ?)
      AND link.id IN (SELECT LINK.id
                      FROM LONGTRANSACTION, SHORTTRANSACTION, LINK
                      WHERE LINK.createdBy = SHORTTRANSACTION.id
                            AND SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
                            AND (LONGTRANSACTION.COMMITED IS NOT NULL OR LONGTRANSACTION.ID = ?))
      AND LINK.id NOT IN (SELECT OBJECT
                          FROM ACCESSED, LONGTRANSACTION, SHORTTRANSACTION
                          WHERE type = 'D'
                                AND SHORTTRANSACTION.id = ACCESSED.transaction
                                AND SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
                                AND (LONGTRANSACTION.COMMITED IS NOT NULL OR LONGTRANSACTION.ID = ?));
