SELECT link.toObj
FROM link
WHERE link.fromObj = ?
      AND link.instanceOf = ?
      AND link.id IN (SELECT LINK.id
                      FROM LONGTRANSACTION, SHORTTRANSACTION, LINK
                      WHERE LINK.createdBy = SHORTTRANSACTION.id
                            AND SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
                            AND (LONGTRANSACTION.COMMITED IS NOT NULL OR LONGTRANSACTION.ID = ?))
      AND LINK.id NOT IN (SELECT OBJECT
                          FROM ACCESSED, LONGTRANSACTION, SHORTTRANSACTION
                          WHERE type = 'D'
                                AND SHORTTRANSACTION.id = ACCESSED.transaction
                                AND SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
                                AND (LONGTRANSACTION.COMMITED IS NOT NULL OR LONGTRANSACTION.ID = ?));
