SELECT
  link.id,
  toobj,
  instanceof,
  pos,
  named
FROM link
WHERE link.fromObj = ?
      AND link.id IN (SELECT LINK.id
                      FROM LONGTRANSACTION, SHORTTRANSACTION, LINK
                      WHERE LINK.createdBy = SHORTTRANSACTION.id
                            AND SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
                            AND (LONGTRANSACTION.COMMITED IS NOT NULL OR LONGTRANSACTION.ID = ?))
      AND LINK.id NOT IN (SELECT OBJECT
                          FROM ACCESSED, LONGTRANSACTION, SHORTTRANSACTION
                          WHERE type = 'D'
                                AND SHORTTRANSACTION.id = ACCESSED.transaction
                                AND SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
                                AND (LONGTRANSACTION.COMMITED IS NOT NULL OR LONGTRANSACTION.ID = ?));
