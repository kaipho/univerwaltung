SELECT
  id,
  instanceof
FROM obj, HIERARCHY
WHERE HIERARCHY.SUPER = ?
      AND obj.INSTANCEOF = HIERARCHY.SUB
      AND obj.id IN (SELECT obj.id
                     FROM LONGTRANSACTION, SHORTTRANSACTION, obj
                     WHERE obj.createdBy = SHORTTRANSACTION.id
                           AND SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
                           AND (LONGTRANSACTION.COMMITED IS NOT NULL OR LONGTRANSACTION.ID = ?))
      AND obj.id NOT IN (SELECT OBJECT
                         FROM ACCESSED, LONGTRANSACTION, SHORTTRANSACTION
                         WHERE type = 'D'
                               AND SHORTTRANSACTION.id = ACCESSED.transaction
                               AND SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
                               AND (LONGTRANSACTION.COMMITED IS NOT NULL OR LONGTRANSACTION.ID = ?));
