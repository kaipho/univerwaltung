package de.kaipho.i18n;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class I18nParserTest {


    @Test
    void parseStringWithConstant() {
        String s = "{test} | {test2}|{test}";
        Map<String, Object> data = new HashMap<>();
        data.put("test", "...");
        data.put("test2", "!!!");

        String result = new I18nParser(s, data).parseString();
        assertEquals("... | !!!|...", result);
    }

}