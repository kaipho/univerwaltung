package de.kaipho.core.transaction;

import de.kaipho.BootstrapContext;
import de.kaipho.core.db.DbConnector;
import de.kaipho.core.db.DbFacade;
import de.kaipho.domain.MetadataKeys;
import de.kaipho.domain.uni.Person;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TransactionTest {


    @Test
    void testDataIsExclusiveBeforeCommit() {
        // Should run in TA1, gets committed
        BootstrapContext.bootstrap();
        {
            List<Person> p = Person.findBy(MetadataKeys.UNI_PERSON_NAME, "Schmidt");
            assertEquals(1, p.size());
            Person person = p.get(0);
            assertEquals("Schmidt", person.getName());
        }

        // TA2 changes data, do not commit
        TransactionExecutor<Object> transaction = Transaction.getInstance()
                                                             .run(null, () -> {
                                                                 List<Person> p = Person.findBy(MetadataKeys.UNI_PERSON_NAME, "Schmidt");
                                                                 p.get(0).setName("SSSS");
                                                                 return null;
                                                             });
        transaction.resolve();
        TransactionContext.reset();
        // Data from TA1 is read without TA context
        {
            List<Person> p = Person.findBy(MetadataKeys.UNI_PERSON_NAME, "Schmidt");
            assertEquals(1, p.size());
            Person person = p.get(0);
            assertEquals("Schmidt", person.getName());
        }
        {
            List<Person> p = Person.findBy(MetadataKeys.UNI_PERSON_NAME, "SSSS");
            assertEquals(0, p.size());
        }
        // In TA2, new Data is read
        TransactionContext.setLongTransaction(transaction.getRunsInTransaction());
        Transaction.getInstance()
                   .run(null, () -> {
                       List<Person> p = Person.findBy(MetadataKeys.UNI_PERSON_NAME, "SSSS");
                       assertEquals(1, p.size());
                       Person person = p.get(0);
                       assertEquals("SSSS", person.getName());
                       return null;
                   }).resolve();

        TransactionContext.reset();
        // TA3 should act like no TA
        Transaction.getInstance()
                   .run(null, () -> {
                       List<Person> p = Person.findBy(MetadataKeys.UNI_PERSON_NAME, "Schmidt");
                       assertEquals(1, p.size());
                       Person person = p.get(0);
                       assertEquals("Schmidt", person.getName());
                       return null;
                   }).resolve();

        transaction.getRunsInTransaction().commit();
        // Data from TA2 is read without TA context
        {
            List<Person> p = Person.findBy(MetadataKeys.UNI_PERSON_NAME, "SSSS");
            assertEquals(1, p.size());
            Person person = p.get(0);
            assertEquals("SSSS", person.getName());
        }
        DbConnector.getConnector().closeConnection();
    }
}