import {NgModule} from "@angular/core";
import {AppComponent} from "./app.component";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule, MatCommonModule, MatDatepickerModule,
    MatDialogModule, MatDividerModule, MatSnackBarModule,
    MatExpansionModule, MatIconModule, MatInputModule, MatTabsModule,
    MatListModule, MatMenuModule, MatOptionModule, MatSelectModule, MatSidenavModule, MatToolbarModule, MatTooltipModule
} from "@angular/material";
import {
    CovalentCommonModule,
    CovalentFileModule,
    CovalentMenuModule,
    CovalentNotificationsModule
} from "@covalent/core";
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {StompConfig, StompService} from "@stomp/ng2-stompjs";

import {SERVICE_COMPONENTS} from "./components/components";
import {ROUTING} from "./app.routes";
import {AuthenticationGuard} from "./guard/authentication.guard";
import {SaveDialog, SaveGuard} from "./guard/save.guard";
import {GENERATED_SERVICES} from "./service/domain/http/services";
import {TitleService} from "./service/core/title.service";
import {SidenavComponent} from './components/core/sidenav/sidenav.component';
import {RouterModule} from '@angular/router';
import {LoginService} from './service/core/login.service';
import {TokenStore} from "./service/core/token.store";
import {MenuService} from "./components/framework/gen-service/menu.service";
import {GenService} from "./components/framework/gen-service/gen-service.component";
import {GenDomainrListComponent} from "./components/framework/gen-obj-list/gen-domainr-list.component";
import {GenObjListComponent} from "./components/framework/gen-obj-list/gen-obj-list.component";
import {Ng2Webstorage} from "ngx-webstorage";
import {FormService} from "./components/framework/form/form.service";
import {SafeHtml} from "./components/framework/pipes/safeHtml.pipe";
import {GenSecureComponent} from "./components/framework/gen-secure/gen-secure.component";
import {MonacoEditorComponent} from './components/framework/editor/monaco-editor.component';
import {GenPrimitiveComponent} from "./components/framework/gen-obj/gen-primitive";
import {GenPrimitiveForServiceComponent} from "./components/framework/gen-obj/gen-primitive-s";
import {GenBlobComponent} from "./components/framework/gen-obj/gen-blob";
import {GenCategoryComponent} from "./components/framework/form/gen-category";
import {GenObjComponent} from "./components/framework/gen-obj/gen-obj.component";
import {AddElementComponent, GenSimpleObjListComponent} from "./components/framework/gen-obj-list/gen-simple-obj-list.component";
import {
    AddElementToCategoryComponent,
    GenCategoryObjListComponent
} from "./components/framework/gen-obj-list/gen-category-obj-list.component";
import {TransactionStore} from "./service/core/transaction.store";
import {SidenavAreaComponent} from "./components/core/sidenav/sidenav-area.component";
import {ADMIN_COMPONENTS} from "./components/admin/admin.components";
import {environment} from "../environments/environment";


import { FachbereichComponent } from './components/domain/uni/fachbereich.component';
import { CustomUserListComponent } from './components/domain/security/customUser.list.component';
import { PersonComponent } from './components/domain/uni/person.component';
import { StudentListComponent } from './components/domain/uni/student.list.component';
import { CategoryClassVMDetailComponent } from './components/domain/core/categoryClassVM.detail.component';
import { ErrorVMDetailComponent } from './components/domain/exception/errorVM.detail.component';
import { StatistikVMDetailComponent } from './components/domain/db/statistikVM.detail.component';
import { RaumListComponent } from './components/domain/uni/raum.list.component';
import { StudentDetailComponent } from './components/domain/uni/student.detail.component';
import { CategoryClassVMComponent } from './components/domain/core/categoryClassVM.component';
import { NotificationDetailComponent } from './components/domain/core/notification.detail.component';
import { CategoryVMDetailComponent } from './components/domain/core/categoryVM.detail.component';
import { VorlesungListComponent } from './components/domain/uni/vorlesung.list.component';
import { UserListComponent } from './components/domain/security/user.list.component';
import { NotificationListComponent } from './components/domain/core/notification.list.component';
import { RaumDetailComponent } from './components/domain/uni/raum.detail.component';
import { TechnicalUserListComponent } from './components/domain/security/technicalUser.list.component';
import { TechnicalUserComponent } from './components/domain/security/technicalUser.component';
import { LongTransactionVMComponent } from './components/domain/transaction/longTransactionVM.component';
import { LongTransactionVMListComponent } from './components/domain/transaction/longTransactionVM.list.component';
import { VorlesungDetailComponent } from './components/domain/uni/vorlesung.detail.component';
import { FachbereichDetailComponent } from './components/domain/uni/fachbereich.detail.component';
import { StatistikVMListComponent } from './components/domain/db/statistikVM.list.component';
import { DozentListComponent } from './components/domain/uni/dozent.list.component';
import { VorlesungComponent } from './components/domain/uni/vorlesung.component';
import { PersonListComponent } from './components/domain/uni/person.list.component';
import { AnonymUserComponent } from './components/domain/security/anonymUser.component';
import { PersonDetailComponent } from './components/domain/uni/person.detail.component';
import { AnonymUserDetailComponent } from './components/domain/security/anonymUser.detail.component';
import { TechnicalUserDetailComponent } from './components/domain/security/technicalUser.detail.component';
import { CategoryClassVMListComponent } from './components/domain/core/categoryClassVM.list.component';
import { CategoryVMComponent } from './components/domain/core/categoryVM.component';
import { ErrorVMListComponent } from './components/domain/exception/errorVM.list.component';
import { LongTransactionVMDetailComponent } from './components/domain/transaction/longTransactionVM.detail.component';
import { PersonRolleListComponent } from './components/domain/uni/personRolle.list.component';
import { DozentDetailComponent } from './components/domain/uni/dozent.detail.component';
import { LoginVMDetailComponent } from './components/domain/security/loginVM.detail.component';
import { LoginVMComponent } from './components/domain/security/loginVM.component';
import { ErrorVMComponent } from './components/domain/exception/errorVM.component';
import { LoginVMListComponent } from './components/domain/security/loginVM.list.component';
import { DozentComponent } from './components/domain/uni/dozent.component';
import { NotificationComponent } from './components/domain/core/notification.component';
import { AnonymUserListComponent } from './components/domain/security/anonymUser.list.component';
import { CategoryVMListComponent } from './components/domain/core/categoryVM.list.component';
import { StudentComponent } from './components/domain/uni/student.component';
import { RaumComponent } from './components/domain/uni/raum.component';
import { StatistikVMComponent } from './components/domain/db/statistikVM.component';
import { FachbereichListComponent } from './components/domain/uni/fachbereich.list.component';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

let location = environment.server == '' ? "ws://" + window.location.host : environment.server;
location = location.replace('http://', "ws://");
location = location.replace('https://', "wss://");
const stompConfig: StompConfig = {
    url:  location + "/ws",
    headers: {},
    heartbeat_in: 0,
    heartbeat_out: 20000,
    reconnect_delay: 5000,
    debug: true
};

@NgModule({
    imports: [
        HttpClientModule,
        FormsModule,
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        Ng2Webstorage,
        RouterModule.forRoot(ROUTING),
        MatButtonModule,
        MatChipsModule,
        MatCardModule,
        MatExpansionModule,
        MatDialogModule,
        MatCheckboxModule,
        MatCommonModule,
        MatIconModule,
        MatInputModule,
        MatDatepickerModule,
        MatDividerModule,
        MatOptionModule,
        MatMenuModule,
        MatToolbarModule,
        MatTooltipModule,
        MatTabsModule,
        MatSelectModule,
        MatSidenavModule,
        MatListModule,
        MatSnackBarModule,
        CovalentCommonModule,
        CovalentNotificationsModule,
        CovalentFileModule,
        CovalentMenuModule,
        NgxChartsModule
    ],
    declarations: [
        ADMIN_COMPONENTS,
        MonacoEditorComponent,
        GenSecureComponent,
        GenPrimitiveComponent,
        GenPrimitiveForServiceComponent,
        GenBlobComponent,
        GenCategoryComponent,
        GenObjComponent,
        SafeHtml,
        AppComponent,
        SidenavComponent,
        SidenavAreaComponent,
        GenService,
        SaveDialog,
        GenDomainrListComponent,
        GenObjListComponent,
        GenSimpleObjListComponent,
        AddElementComponent,
        GenCategoryObjListComponent,
        AddElementToCategoryComponent,
        SERVICE_COMPONENTS,
        PersonComponent,
        PersonDetailComponent,
        StudentComponent,
        StudentDetailComponent,
        DozentComponent,
        DozentDetailComponent,
        VorlesungComponent,
        VorlesungDetailComponent,
        FachbereichComponent,
        FachbereichDetailComponent,
        RaumComponent,
        RaumDetailComponent,
        ErrorVMComponent,
        ErrorVMDetailComponent,
        LongTransactionVMComponent,
        LongTransactionVMDetailComponent,
        CategoryClassVMComponent,
        CategoryClassVMDetailComponent,
        CategoryVMComponent,
        CategoryVMDetailComponent,
        AnonymUserComponent,
        AnonymUserDetailComponent,
        TechnicalUserComponent,
        TechnicalUserDetailComponent,
        LoginVMComponent,
        LoginVMDetailComponent,
        NotificationComponent,
        NotificationDetailComponent,
        StatistikVMComponent,
        StatistikVMDetailComponent,
        PersonListComponent,
        PersonRolleListComponent,
        StudentListComponent,
        DozentListComponent,
        VorlesungListComponent,
        FachbereichListComponent,
        RaumListComponent,
        ErrorVMListComponent,
        LongTransactionVMListComponent,
        CategoryClassVMListComponent,
        CategoryVMListComponent,
        UserListComponent,
        CustomUserListComponent,
        AnonymUserListComponent,
        TechnicalUserListComponent,
        LoginVMListComponent,
        NotificationListComponent,
        StatistikVMListComponent,
    ],
    providers: [
        AuthenticationGuard,
        SaveGuard,
        LoginService,
        GENERATED_SERVICES,
        TitleService,
        TokenStore,
        TransactionStore,
        FormService,
        MenuService,
        StompService,
        {
            provide: StompConfig,
            useValue: stompConfig
        }
    ],
    bootstrap: [AppComponent],
    entryComponents: [
        AddElementComponent,
        AddElementToCategoryComponent,
        SaveDialog,
        SERVICE_COMPONENTS
    ]})
export class AppModule {}
