import { Dozent } from './dozent';
import { RepVisitor } from '../rep.visitor';
import { Vorlesung } from './vorlesung';
import { DomainObjectVisitor } from '../domain.object.visitor';
import { DomainObject } from '../domain.object';

export class Fachbereich implements DomainObject {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  bezeichnung:string;
  dozenten:Array<Dozent>;
  vorlesungen:Array<Vorlesung>;

  toString():string {
    return new RepVisitor().visitFachbereich(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitFachbereich(this);
  }
}
