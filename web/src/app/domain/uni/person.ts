import { Notification } from '../core/notification';
import { RepVisitor } from '../rep.visitor';
import { CustomUser } from '../security/customUser';
import { DomainObjectVisitor } from '../domain.object.visitor';

export class Person implements CustomUser {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  username:string;
  password:string;
  roles:Array<string>;
  notifications:Array<Notification>;
  name:string;
  vorname:string;

  toString():string {
    return new RepVisitor().visitPerson(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitPerson(this);
  }
}
