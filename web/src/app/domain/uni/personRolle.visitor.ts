import { Dozent } from './dozent';
import { Student } from './student';

/**
 * Visitor to visit every PersonRolle element.
 */
export interface PersonRolleVisitor<D> {
    visitDozent(obj: Dozent):D
    visitStudent(obj: Student):D
}
