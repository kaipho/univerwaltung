import { RepVisitor } from '../rep.visitor';
import { DomainObjectVisitor } from '../domain.object.visitor';
import { DomainObject } from '../domain.object';
import { Student } from './student';

export class Vorlesung implements DomainObject {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  bezeichnung:string;
  studenten:Array<Student>;

  toString():string {
    return new RepVisitor().visitVorlesung(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitVorlesung(this);
  }
}
