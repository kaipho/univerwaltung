import { DomainObject } from '../domain.object';
import { PersonRolleVisitor } from './personRolle.visitor';

export interface PersonRolle extends DomainObject {
    self:string;
    id:number;


    accept<D>(visitor: PersonRolleVisitor<D>): D;

}
