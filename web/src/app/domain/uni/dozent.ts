import { PersonRolle } from './personRolle';
import { RepVisitor } from '../rep.visitor';
import { DomainObjectVisitor } from '../domain.object.visitor';

export class Dozent implements PersonRolle {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  personalnummer:string;

  toString():string {
    return new RepVisitor().visitDozent(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitDozent(this);
  }
}
