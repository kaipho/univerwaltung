import { PersonRolle } from './personRolle';
import { RepVisitor } from '../rep.visitor';
import { Vorlesung } from './vorlesung';
import { DomainObjectVisitor } from '../domain.object.visitor';

export class Student implements PersonRolle {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  matrikelnummer:number;
  besuchteVorlesungen:Array<Vorlesung>;

  toString():string {
    return new RepVisitor().visitStudent(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitStudent(this);
  }
}
