import { RepVisitor } from '../rep.visitor';
import { DomainObjectVisitor } from '../domain.object.visitor';
import { DomainObject } from '../domain.object';

export class Raum implements DomainObject {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  bezeichnung:string;

  toString():string {
    return new RepVisitor().visitRaum(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitRaum(this);
  }
}
