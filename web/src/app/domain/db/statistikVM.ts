import { RepVisitor } from '../rep.visitor';
import { DomainObjectVisitor } from '../domain.object.visitor';
import { DomainObject } from '../domain.object';

export class StatistikVM implements DomainObject {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  name:string;
  value:number;

  toString():string {
    return new RepVisitor().visitStatistikVM(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitStatistikVM(this);
  }
}
