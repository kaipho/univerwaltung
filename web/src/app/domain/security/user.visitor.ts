import { AnonymUser } from './anonymUser';
import { TechnicalUser } from './technicalUser';
import { CustomUser } from './customUser';

/**
 * Visitor to visit every User element.
 */
export interface UserVisitor<D> {
    visitCustomUser(obj: CustomUser):D
    visitAnonymUser(obj: AnonymUser):D
    visitTechnicalUser(obj: TechnicalUser):D
}
