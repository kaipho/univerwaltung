import { Person } from '../uni/person';

/**
 * Visitor to visit every CustomUser element.
 */
export interface CustomUserVisitor<D> {
    visitPerson(obj: Person):D
}
