import { DomainObject } from '../domain.object';
import { UserVisitor } from './user.visitor';
import { Notification } from '../core/notification';

export interface User extends DomainObject {
    self:string;
    id:number;

    username:string;
    password:string;
    roles:Array<string>;
    notifications:Array<Notification>;

    accept<D>(visitor: UserVisitor<D>): D;

}
