import { Notification } from '../core/notification';
import { RepVisitor } from '../rep.visitor';
import { User } from './user';
import { DomainObjectVisitor } from '../domain.object.visitor';

/**
 * User for unauthorized access or guest account
 */
export class AnonymUser implements User {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  username:string;
  password:string;
  roles:Array<string>;
  notifications:Array<Notification>;

  toString():string {
    return new RepVisitor().visitAnonymUser(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitAnonymUser(this);
  }
}
