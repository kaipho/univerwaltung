import { RepVisitor } from '../rep.visitor';
import { DomainObjectVisitor } from '../domain.object.visitor';
import { DomainObject } from '../domain.object';

/**
 * The view-model containing login information
 */
export class LoginVM implements DomainObject {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  token:string;

  toString():string {
    return new RepVisitor().visitLoginVM(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitLoginVM(this);
  }
}
