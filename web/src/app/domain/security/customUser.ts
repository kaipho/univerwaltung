import { DomainObject } from '../domain.object';
import { CustomUserVisitor } from './customUser.visitor';
import { Notification } from '../core/notification';

export interface CustomUser extends DomainObject {
    self:string;
    id:number;

    username:string;
    password:string;
    roles:Array<string>;
    notifications:Array<Notification>;

    accept<D>(visitor: CustomUserVisitor<D>): D;

}
