import { Notification } from '../core/notification';
import { RepVisitor } from '../rep.visitor';
import { User } from './user';
import { DomainObjectVisitor } from '../domain.object.visitor';

export class TechnicalUser implements User {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  username:string;
  password:string;
  roles:Array<string>;
  notifications:Array<Notification>;
  description:string;

  toString():string {
    return new RepVisitor().visitTechnicalUser(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitTechnicalUser(this);
  }
}
