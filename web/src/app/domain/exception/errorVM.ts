import { RepVisitor } from '../rep.visitor';
import { DomainObjectVisitor } from '../domain.object.visitor';
import { DomainObject } from '../domain.object';

export class ErrorVM implements DomainObject {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  code:number;
  message:string;

  toString():string {
    return new RepVisitor().visitErrorVM(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitErrorVM(this);
  }
}
