import { DomainObjectVisitor } from "./domain.object.visitor";
import { Person } from './uni/person';
import { PersonRolle } from './uni/personRolle';
import { Student } from './uni/student';
import { Dozent } from './uni/dozent';
import { Vorlesung } from './uni/vorlesung';
import { Fachbereich } from './uni/fachbereich';
import { Raum } from './uni/raum';
import { ErrorVM } from './exception/errorVM';
import { LongTransactionVM } from './transaction/longTransactionVM';
import { CategoryClassVM } from './core/categoryClassVM';
import { CategoryVM } from './core/categoryVM';
import { User } from './security/user';
import { CustomUser } from './security/customUser';
import { AnonymUser } from './security/anonymUser';
import { TechnicalUser } from './security/technicalUser';
import { LoginVM } from './security/loginVM';
import { Notification } from './core/notification';
import { StatistikVM } from './db/statistikVM';
// imports_end

/**
 * Visitor to create a string for the object preview
 */
export class RepVisitor implements DomainObjectVisitor<string> {
    // editable_area_start
    public visitTechnicalUser(obj: TechnicalUser):string {
       // TODO implement visitTechnicalUser(obj: TechnicalUser):string
       return 'todo';
    }
    public visitStatistikVM(obj: StatistikVM):string {
       // TODO implement visitStatistikVM(obj: StatistikVM):string
       return 'todo';
    }
    public visitLongTransactionVM(obj: LongTransactionVM):string {
       // TODO implement visitLongTransactionVM(obj: LongTransactionVM):string
       return 'todo';
    }
    public visitPerson(obj: Person):string {
       // TODO implement visitPerson(obj: Person):string
       return 'todo';
    }
    public visitPersonRolle(obj: PersonRolle):string {
       // TODO implement visitPersonRolle(obj: PersonRolle):string
       return 'todo';
    }
    public visitRaum(obj: Raum):string {
       // TODO implement visitRaum(obj: Raum):string
       return 'todo';
    }
    public visitDozent(obj: Dozent):string {
       // TODO implement visitDozent(obj: Dozent):string
       return 'todo';
    }
    public visitFachbereich(obj: Fachbereich):string {
       // TODO implement visitFachbereich(obj: Fachbereich):string
       return 'todo';
    }
    public visitStudent(obj: Student):string {
       // TODO implement visitStudent(obj: Student):string
       return 'todo';
    }
    public visitVorlesung(obj: Vorlesung):string {
       // TODO implement visitVorlesung(obj: Vorlesung):string
       return 'todo';
    }

    public visitErrorVM(obj: ErrorVM):string {
       // TODO implement visitErrorVM(obj: ErrorVM):string
       return 'todo';
    }
    public visitCategoryClassVM(obj: CategoryClassVM):string {
       // TODO implement visitCategoryClassVM(obj: CategoryClassVM):string
       return 'todo';
    }
    public visitCategoryVM(obj: CategoryVM):string {
       // TODO implement visitCategoryVM(obj: CategoryVM):string
       return 'todo';
    }
    public visitUser(obj: User):string {
       // TODO implement visitUser(obj: User):string
       return 'todo';
    }
    public visitCustomUser(obj: CustomUser):string {
       // TODO implement visitCustomUser(obj: CustomUser):string
       return 'todo';
    }
    public visitAnonymUser(obj: AnonymUser):string {
       // TODO implement visitAnonymUser(obj: AnonymUser):string
       return 'todo';
    }
    public visitLoginVM(obj: LoginVM):string {
       // TODO implement visitLoginVM(obj: LoginVM):string
       return 'todo';
    }
    public visitNotification(obj: Notification):string {
       // TODO implement visitNotification(obj: Notification):string
       return 'todo';
    }

    // editable_area_end
}
