import { Person } from './uni/person';
import { PersonRolle } from './uni/personRolle';
import { Student } from './uni/student';
import { Dozent } from './uni/dozent';
import { Vorlesung } from './uni/vorlesung';
import { Fachbereich } from './uni/fachbereich';
import { Raum } from './uni/raum';
import { ErrorVM } from './exception/errorVM';
import { LongTransactionVM } from './transaction/longTransactionVM';
import { CategoryClassVM } from './core/categoryClassVM';
import { CategoryVM } from './core/categoryVM';
import { User } from './security/user';
import { CustomUser } from './security/customUser';
import { AnonymUser } from './security/anonymUser';
import { TechnicalUser } from './security/technicalUser';
import { LoginVM } from './security/loginVM';
import { Notification } from './core/notification';
import { StatistikVM } from './db/statistikVM';

/**
 * Visitor to visit every domain element.
 */
export interface DomainObjectVisitor<D> {
    visitPerson(obj: Person):D
    visitPersonRolle(obj: PersonRolle):D
    visitStudent(obj: Student):D
    visitDozent(obj: Dozent):D
    visitVorlesung(obj: Vorlesung):D
    visitFachbereich(obj: Fachbereich):D
    visitRaum(obj: Raum):D
    visitErrorVM(obj: ErrorVM):D
    visitLongTransactionVM(obj: LongTransactionVM):D
    visitCategoryClassVM(obj: CategoryClassVM):D
    visitCategoryVM(obj: CategoryVM):D
    visitUser(obj: User):D
    visitCustomUser(obj: CustomUser):D
    visitAnonymUser(obj: AnonymUser):D
    visitTechnicalUser(obj: TechnicalUser):D
    visitLoginVM(obj: LoginVM):D
    visitNotification(obj: Notification):D
    visitStatistikVM(obj: StatistikVM):D
}
