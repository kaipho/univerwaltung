import { DomainObjectVisitor } from "./domain.object.visitor";
import { Person } from './uni/person';
import { PersonRolle } from './uni/personRolle';
import { Student } from './uni/student';
import { Dozent } from './uni/dozent';
import { Vorlesung } from './uni/vorlesung';
import { Fachbereich } from './uni/fachbereich';
import { Raum } from './uni/raum';
import { ErrorVM } from './exception/errorVM';
import { LongTransactionVM } from './transaction/longTransactionVM';
import { CategoryClassVM } from './core/categoryClassVM';
import { CategoryVM } from './core/categoryVM';
import { User } from './security/user';
import { CustomUser } from './security/customUser';
import { AnonymUser } from './security/anonymUser';
import { TechnicalUser } from './security/technicalUser';
import { LoginVM } from './security/loginVM';
import { Notification } from './core/notification';
import { StatistikVM } from './db/statistikVM';

/**
 * Visitor to get the Type as string for the object
 */
export class TypeVisitor implements DomainObjectVisitor<string> {
    public visitPerson(obj: Person):string {
       return 'person';
    }
    public visitPersonRolle(obj: PersonRolle):string {
       return 'personRolle';
    }
    public visitStudent(obj: Student):string {
       return 'student';
    }
    public visitDozent(obj: Dozent):string {
       return 'dozent';
    }
    public visitVorlesung(obj: Vorlesung):string {
       return 'vorlesung';
    }
    public visitFachbereich(obj: Fachbereich):string {
       return 'fachbereich';
    }
    public visitRaum(obj: Raum):string {
       return 'raum';
    }
    public visitErrorVM(obj: ErrorVM):string {
       return 'errorVM';
    }
    public visitLongTransactionVM(obj: LongTransactionVM):string {
       return 'longTransactionVM';
    }
    public visitCategoryClassVM(obj: CategoryClassVM):string {
       return 'categoryClassVM';
    }
    public visitCategoryVM(obj: CategoryVM):string {
       return 'categoryVM';
    }
    public visitUser(obj: User):string {
       return 'user';
    }
    public visitCustomUser(obj: CustomUser):string {
       return 'customUser';
    }
    public visitAnonymUser(obj: AnonymUser):string {
       return 'anonymUser';
    }
    public visitTechnicalUser(obj: TechnicalUser):string {
       return 'technicalUser';
    }
    public visitLoginVM(obj: LoginVM):string {
       return 'loginVM';
    }
    public visitNotification(obj: Notification):string {
       return 'notification';
    }
    public visitStatistikVM(obj: StatistikVM):string {
       return 'statistikVM';
    }
}
