import { RepVisitor } from '../rep.visitor';
import { DomainObjectVisitor } from '../domain.object.visitor';
import { DomainObject } from '../domain.object';

/**
 * VM für eine einzelne Kategorie.
 */
export class CategoryVM implements DomainObject {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  dbId:number;
  name:string;
  isDerived:boolean;

  toString():string {
    return new RepVisitor().visitCategoryVM(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitCategoryVM(this);
  }
}
