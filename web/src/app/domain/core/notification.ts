import { RepVisitor } from '../rep.visitor';
import { DomainObjectVisitor } from '../domain.object.visitor';
import { DomainObject } from '../domain.object';

/**
 * A $Notification belongs to one to n $User. A Notification should inform about errors, events and so on.
 */
export class Notification implements DomainObject {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  subject:string;
  message:string;
  creation:string;
  isRead:boolean;

  toString():string {
    return new RepVisitor().visitNotification(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitNotification(this);
  }
}
