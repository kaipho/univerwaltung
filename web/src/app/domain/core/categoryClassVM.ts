import { CategoryVM } from './categoryVM';
import { RepVisitor } from '../rep.visitor';
import { DomainObjectVisitor } from '../domain.object.visitor';
import { DomainObject } from '../domain.object';

/**
 * VM für eine Kategorie Klasse.
 */
export class CategoryClassVM implements DomainObject {
  self:string;
  id:number;
  _rep: string;
  _links:any;

  dbId:number;
  name:string;
  values:Array<CategoryVM>;

  toString():string {
    return new RepVisitor().visitCategoryClassVM(this);
  }

  accept<D>(visitor: DomainObjectVisitor<D>): D {
    return visitor.visitCategoryClassVM(this);
  }
}
