import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { Person } from '../../../domain/uni/person';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes Person
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class PersonHttpService extends AbstractHttpService<Person>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<Person>> {
		return this.http.get('/api/domainr/21').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<Person>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<Person>):Observable<Pageable<Person>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<Person>):Observable<Pageable<Person>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<Person> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<Person>> {
        return this.http.get(`/api/domainr/21/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(person:Person):Observable<Person> {
		return this.http.put(person.self, person).catch((error) => this.handleError(error));
	}

	delete(person:Person):Observable<any> {
		return this.http.delete(person.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):Person {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new Person();
		}
		return AbstractHttpService.extractOne(body, new Person());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<Person> {
		const body = res.body;
		if (body == null) {
			return new Pageable<Person>();
		}
		const result = new Pageable<Person>();
		if(body._embedded.hasOwnProperty('person')) {
            body._embedded.person.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new Person());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
