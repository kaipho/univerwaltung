import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { CategoryClassVM } from '../../../domain/core/categoryClassVM';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes CategoryClassVM
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class CategoryClassVMHttpService extends AbstractHttpService<CategoryClassVM>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<CategoryClassVM>> {
		return this.http.get('/api/domainr/7').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<CategoryClassVM>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<CategoryClassVM>):Observable<Pageable<CategoryClassVM>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<CategoryClassVM>):Observable<Pageable<CategoryClassVM>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<CategoryClassVM> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<CategoryClassVM>> {
        return this.http.get(`/api/domainr/7/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(categoryClassVM:CategoryClassVM):Observable<CategoryClassVM> {
		return this.http.put(categoryClassVM.self, categoryClassVM).catch((error) => this.handleError(error));
	}

	delete(categoryClassVM:CategoryClassVM):Observable<any> {
		return this.http.delete(categoryClassVM.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):CategoryClassVM {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new CategoryClassVM();
		}
		return AbstractHttpService.extractOne(body, new CategoryClassVM());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<CategoryClassVM> {
		const body = res.body;
		if (body == null) {
			return new Pageable<CategoryClassVM>();
		}
		const result = new Pageable<CategoryClassVM>();
		if(body._embedded.hasOwnProperty('categoryClassVM')) {
            body._embedded.categoryClassVM.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new CategoryClassVM());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
