import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { Vorlesung } from '../../../domain/uni/vorlesung';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes Vorlesung
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class VorlesungHttpService extends AbstractHttpService<Vorlesung>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<Vorlesung>> {
		return this.http.get('/api/domainr/17').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<Vorlesung>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<Vorlesung>):Observable<Pageable<Vorlesung>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<Vorlesung>):Observable<Pageable<Vorlesung>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<Vorlesung> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<Vorlesung>> {
        return this.http.get(`/api/domainr/17/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(vorlesung:Vorlesung):Observable<Vorlesung> {
		return this.http.put(vorlesung.self, vorlesung).catch((error) => this.handleError(error));
	}

	delete(vorlesung:Vorlesung):Observable<any> {
		return this.http.delete(vorlesung.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):Vorlesung {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new Vorlesung();
		}
		return AbstractHttpService.extractOne(body, new Vorlesung());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<Vorlesung> {
		const body = res.body;
		if (body == null) {
			return new Pageable<Vorlesung>();
		}
		const result = new Pageable<Vorlesung>();
		if(body._embedded.hasOwnProperty('vorlesung')) {
            body._embedded.vorlesung.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new Vorlesung());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
