import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { TechnicalUser } from '../../../domain/security/technicalUser';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes TechnicalUser
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class TechnicalUserHttpService extends AbstractHttpService<TechnicalUser>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<TechnicalUser>> {
		return this.http.get('/api/domainr/35').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<TechnicalUser>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<TechnicalUser>):Observable<Pageable<TechnicalUser>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<TechnicalUser>):Observable<Pageable<TechnicalUser>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<TechnicalUser> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<TechnicalUser>> {
        return this.http.get(`/api/domainr/35/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(technicalUser:TechnicalUser):Observable<TechnicalUser> {
		return this.http.put(technicalUser.self, technicalUser).catch((error) => this.handleError(error));
	}

	delete(technicalUser:TechnicalUser):Observable<any> {
		return this.http.delete(technicalUser.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):TechnicalUser {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new TechnicalUser();
		}
		return AbstractHttpService.extractOne(body, new TechnicalUser());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<TechnicalUser> {
		const body = res.body;
		if (body == null) {
			return new Pageable<TechnicalUser>();
		}
		const result = new Pageable<TechnicalUser>();
		if(body._embedded.hasOwnProperty('technicalUser')) {
            body._embedded.technicalUser.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new TechnicalUser());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
