import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { Raum } from '../../../domain/uni/raum';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes Raum
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class RaumHttpService extends AbstractHttpService<Raum>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<Raum>> {
		return this.http.get('/api/domainr/25').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<Raum>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<Raum>):Observable<Pageable<Raum>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<Raum>):Observable<Pageable<Raum>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<Raum> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<Raum>> {
        return this.http.get(`/api/domainr/25/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(raum:Raum):Observable<Raum> {
		return this.http.put(raum.self, raum).catch((error) => this.handleError(error));
	}

	delete(raum:Raum):Observable<any> {
		return this.http.delete(raum.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):Raum {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new Raum();
		}
		return AbstractHttpService.extractOne(body, new Raum());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<Raum> {
		const body = res.body;
		if (body == null) {
			return new Pageable<Raum>();
		}
		const result = new Pageable<Raum>();
		if(body._embedded.hasOwnProperty('raum')) {
            body._embedded.raum.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new Raum());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
