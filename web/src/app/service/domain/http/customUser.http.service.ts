import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { CustomUser } from '../../../domain/security/customUser';
import { HttpService } from '../../core/http.service';
import { Person } from '../../../domain/uni/person';


/**
 * Service zum Abrufen des exportierten Domain Objektes CustomUser
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class CustomUserHttpService extends AbstractHttpService<CustomUser>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<CustomUser>> {
		return this.http.get('/api/domainr/10').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<CustomUser>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<CustomUser>):Observable<Pageable<CustomUser>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<CustomUser>):Observable<Pageable<CustomUser>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<CustomUser> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<CustomUser>> {
        return this.http.get(`/api/domainr/10/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(customUser:CustomUser):Observable<CustomUser> {
		return this.http.put(customUser.self, customUser).map(this._extractObj).catch((error) => this.handleError(error));
	}

	delete(customUser:CustomUser):Observable<any> {
		return this.http.delete(customUser.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):CustomUser {
        const body = res.body;
        const type = CustomUserHttpService._createInstanceOf(CustomUserHttpService._extractTypeOfUrl(body._links.self.href));

		if (body == null) {
			return type;
		}
		return AbstractHttpService.extractOne(body, type);
	}

	_extractObjList(res:HttpResponse<any>):Pageable<CustomUser> {
		const body = res.body;
		if (body == null) {
			return new Pageable<CustomUser>();
		}
		const result = new Pageable<CustomUser>();

        for(var type in body._embedded) {
            if(body._embedded.hasOwnProperty(type)) {
                body._embedded[type].forEach(actualClass => {
                    const extracted = AbstractHttpService.extractOne(actualClass, CustomUserHttpService._createInstanceOf(type));
                    result.data.push(extracted);
                });
            }
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}

    static _extractTypeOfUrl(url: string):string {
        const parts = url.split("/")
        if (parts.length < 3) {
            throw new Error("Can not process URL!")
        }
        return parts[3];
    }

    static _createInstanceOf(type: string): any {
        switch (type) {
            case 'person':
               return new Person();
            default:
                throw new Error("Type not OK!");
        }
    }
}
