import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { StatistikVM } from '../../../domain/db/statistikVM';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes StatistikVM
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class StatistikVMHttpService extends AbstractHttpService<StatistikVM>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<StatistikVM>> {
		return this.http.get('/api/domainr/31').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<StatistikVM>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<StatistikVM>):Observable<Pageable<StatistikVM>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<StatistikVM>):Observable<Pageable<StatistikVM>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<StatistikVM> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<StatistikVM>> {
        return this.http.get(`/api/domainr/31/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(statistikVM:StatistikVM):Observable<StatistikVM> {
		return this.http.put(statistikVM.self, statistikVM).catch((error) => this.handleError(error));
	}

	delete(statistikVM:StatistikVM):Observable<any> {
		return this.http.delete(statistikVM.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):StatistikVM {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new StatistikVM();
		}
		return AbstractHttpService.extractOne(body, new StatistikVM());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<StatistikVM> {
		const body = res.body;
		if (body == null) {
			return new Pageable<StatistikVM>();
		}
		const result = new Pageable<StatistikVM>();
		if(body._embedded.hasOwnProperty('statistikVM')) {
            body._embedded.statistikVM.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new StatistikVM());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
