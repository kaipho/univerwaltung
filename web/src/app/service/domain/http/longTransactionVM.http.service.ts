import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { LongTransactionVM } from '../../../domain/transaction/longTransactionVM';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes LongTransactionVM
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class LongTransactionVMHttpService extends AbstractHttpService<LongTransactionVM>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<LongTransactionVM>> {
		return this.http.get('/api/domainr/29').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<LongTransactionVM>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<LongTransactionVM>):Observable<Pageable<LongTransactionVM>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<LongTransactionVM>):Observable<Pageable<LongTransactionVM>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<LongTransactionVM> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<LongTransactionVM>> {
        return this.http.get(`/api/domainr/29/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(longTransactionVM:LongTransactionVM):Observable<LongTransactionVM> {
		return this.http.put(longTransactionVM.self, longTransactionVM).catch((error) => this.handleError(error));
	}

	delete(longTransactionVM:LongTransactionVM):Observable<any> {
		return this.http.delete(longTransactionVM.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):LongTransactionVM {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new LongTransactionVM();
		}
		return AbstractHttpService.extractOne(body, new LongTransactionVM());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<LongTransactionVM> {
		const body = res.body;
		if (body == null) {
			return new Pageable<LongTransactionVM>();
		}
		const result = new Pageable<LongTransactionVM>();
		if(body._embedded.hasOwnProperty('longTransactionVM')) {
            body._embedded.longTransactionVM.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new LongTransactionVM());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
