import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { Student } from '../../../domain/uni/student';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes Student
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class StudentHttpService extends AbstractHttpService<Student>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<Student>> {
		return this.http.get('/api/domainr/16').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<Student>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<Student>):Observable<Pageable<Student>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<Student>):Observable<Pageable<Student>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<Student> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<Student>> {
        return this.http.get(`/api/domainr/16/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(student:Student):Observable<Student> {
		return this.http.put(student.self, student).catch((error) => this.handleError(error));
	}

	delete(student:Student):Observable<any> {
		return this.http.delete(student.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):Student {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new Student();
		}
		return AbstractHttpService.extractOne(body, new Student());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<Student> {
		const body = res.body;
		if (body == null) {
			return new Pageable<Student>();
		}
		const result = new Pageable<Student>();
		if(body._embedded.hasOwnProperty('student')) {
            body._embedded.student.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new Student());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
