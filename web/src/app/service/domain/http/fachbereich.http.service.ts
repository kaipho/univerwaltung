import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { Fachbereich } from '../../../domain/uni/fachbereich';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes Fachbereich
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class FachbereichHttpService extends AbstractHttpService<Fachbereich>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<Fachbereich>> {
		return this.http.get('/api/domainr/24').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<Fachbereich>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<Fachbereich>):Observable<Pageable<Fachbereich>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<Fachbereich>):Observable<Pageable<Fachbereich>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<Fachbereich> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<Fachbereich>> {
        return this.http.get(`/api/domainr/24/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(fachbereich:Fachbereich):Observable<Fachbereich> {
		return this.http.put(fachbereich.self, fachbereich).catch((error) => this.handleError(error));
	}

	delete(fachbereich:Fachbereich):Observable<any> {
		return this.http.delete(fachbereich.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):Fachbereich {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new Fachbereich();
		}
		return AbstractHttpService.extractOne(body, new Fachbereich());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<Fachbereich> {
		const body = res.body;
		if (body == null) {
			return new Pageable<Fachbereich>();
		}
		const result = new Pageable<Fachbereich>();
		if(body._embedded.hasOwnProperty('fachbereich')) {
            body._embedded.fachbereich.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new Fachbereich());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
