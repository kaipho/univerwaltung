import {HttpService} from "../../core/http.service";
import {Observable} from "rxjs/Rx";
import {Pageable} from "./pageable";
import {Router} from "@angular/router";
import {ErrorObservable} from "rxjs/observable/ErrorObservable";

import { StatistikVM } from '../../../domain/db/statistikVM';
import { Raum } from '../../../domain/uni/raum';
import { LongTransactionVM } from '../../../domain/transaction/longTransactionVM';
import { AnonymUser } from '../../../domain/security/anonymUser';
import { LoginVM } from '../../../domain/security/loginVM';
import { Student } from '../../../domain/uni/student';
import { Vorlesung } from '../../../domain/uni/vorlesung';
import { Person } from '../../../domain/uni/person';
import { CategoryClassVM } from '../../../domain/core/categoryClassVM';
import { ErrorVM } from '../../../domain/exception/errorVM';
import { Notification } from '../../../domain/core/notification';
import { Dozent } from '../../../domain/uni/dozent';
import { TechnicalUser } from '../../../domain/security/technicalUser';
import { Fachbereich } from '../../../domain/uni/fachbereich';
import { CategoryVM } from '../../../domain/core/categoryVM';

export abstract class AbstractHttpService<D> {
    http: HttpService;
    router: Router;

    constructor(http: HttpService, router: Router) {
        this.http = http;
        this.router = router;
    }

    abstract getFirst(): Observable<Pageable<D>>;

    abstract getAll(link: string): Observable<Pageable<D>>;

    abstract getNext(data: Pageable<D>): Observable<Pageable<D>>;

    abstract getPrev(data: Pageable<D>): Observable<Pageable<D>>;

    abstract get(link: string): Observable<D>;

    abstract create(type: number): Observable<String>;

    abstract update(data: D): Observable<D>;

    abstract delete(data: D): Observable<any>;

    abstract find(id: number, searchText: string): Observable<Pageable<D>>;

    static extractOne(data: any, obj: any): any {
        let _this = data;
        if (data._embedded)
            _this = data._embedded;

        for (let property in _this) {
            if (_this.hasOwnProperty(property)) {
                if (Object.prototype.toString.call(_this[property]) === '[object Object]') {
                    const _type: number = _this[property]._type;
                    if (_type == null) {
                        obj[property] = _this[property];
                    } else
                        obj[property] = AbstractHttpService.mapTo(_type, _this[property]);
                } else if (AbstractHttpService.typedArray(_this[property])) {
                    const array = [];
                    _this[property].forEach(item => {
                        const _type: number = item._type;
                        array.push(AbstractHttpService.mapTo(_type, item));
                    });
                    obj[property] = array;
                } else {
                    obj[property] = _this[property] || null;
                }
            }
        }
        if (data._links)
            obj.self = data._links.self;
        return obj;
    }

    static typedArray(data: any): boolean {
        return Object.prototype.toString.call(data) === '[object Array]' && data.length > 0 && Object.prototype.toString.call(data[0]) === '[object Object]';
    }

    static mapTo(type: number, item: any): any {
        switch (type) {
            case 21:
               return AbstractHttpService.extractOne(item, new Person());
            case 16:
               return AbstractHttpService.extractOne(item, new Student());
            case 23:
               return AbstractHttpService.extractOne(item, new Dozent());
            case 17:
               return AbstractHttpService.extractOne(item, new Vorlesung());
            case 24:
               return AbstractHttpService.extractOne(item, new Fachbereich());
            case 25:
               return AbstractHttpService.extractOne(item, new Raum());
            case 6:
               return AbstractHttpService.extractOne(item, new ErrorVM());
            case 29:
               return AbstractHttpService.extractOne(item, new LongTransactionVM());
            case 7:
               return AbstractHttpService.extractOne(item, new CategoryClassVM());
            case 8:
               return AbstractHttpService.extractOne(item, new CategoryVM());
            case 11:
               return AbstractHttpService.extractOne(item, new AnonymUser());
            case 35:
               return AbstractHttpService.extractOne(item, new TechnicalUser());
            case 12:
               return AbstractHttpService.extractOne(item, new LoginVM());
            case 13:
               return AbstractHttpService.extractOne(item, new Notification());
            case 31:
               return AbstractHttpService.extractOne(item, new StatistikVM());
        }
    }

    handleError(error: any): ErrorObservable {
        return Observable.throw(error);
    }
}
