import {HttpService} from "../../core/http.service";
export {HttpService} from "../../core/http.service";
import { CustomUserHttpService } from './customUser.http.service';
import { SecureValueAccessorHttpService } from '../../functions/http/secureValueAccessor.http.service';
import { ModelServiceHttpService } from '../../functions/http/modelService.http.service';
import { CategoryClassVMHttpService } from './categoryClassVM.http.service';
import { PersonRolleHttpService } from './personRolle.http.service';
import { RaumHttpService } from './raum.http.service';
import { LoginVMHttpService } from './loginVM.http.service';
import { UserServiceHttpService } from '../../functions/http/userService.http.service';
import { FachbereichHttpService } from './fachbereich.http.service';
import { TransactionServiceHttpService } from '../../functions/http/transactionService.http.service';
import { AnonymUserHttpService } from './anonymUser.http.service';
import { StatistikVMHttpService } from './statistikVM.http.service';
import { UniServiceHttpService } from '../../functions/http/uniService.http.service';
import { ErrorVMHttpService } from './errorVM.http.service';
import { PersonHttpService } from './person.http.service';
import { CategoryServiceHttpService } from '../../functions/http/categoryService.http.service';
import { TechnicalUserHttpService } from './technicalUser.http.service';
import { UserHttpService } from './user.http.service';
import { CategoryVMHttpService } from './categoryVM.http.service';
import { NotificationServiceHttpService } from '../../functions/http/notificationService.http.service';
import { LongTransactionVMHttpService } from './longTransactionVM.http.service';
import { StudentHttpService } from './student.http.service';
import { DozentHttpService } from './dozent.http.service';
import { VorlesungHttpService } from './vorlesung.http.service';
import { NotificationHttpService } from './notification.http.service';
import { PrimitivesServiceHttpService } from '../../functions/http/primitivesService.http.service';
export { CustomUserHttpService } from './customUser.http.service';
export { SecureValueAccessorHttpService } from '../../functions/http/secureValueAccessor.http.service';
export { ModelServiceHttpService } from '../../functions/http/modelService.http.service';
export { CategoryClassVMHttpService } from './categoryClassVM.http.service';
export { PersonRolleHttpService } from './personRolle.http.service';
export { RaumHttpService } from './raum.http.service';
export { LoginVMHttpService } from './loginVM.http.service';
export { UserServiceHttpService } from '../../functions/http/userService.http.service';
export { FachbereichHttpService } from './fachbereich.http.service';
export { TransactionServiceHttpService } from '../../functions/http/transactionService.http.service';
export { AnonymUserHttpService } from './anonymUser.http.service';
export { StatistikVMHttpService } from './statistikVM.http.service';
export { UniServiceHttpService } from '../../functions/http/uniService.http.service';
export { ErrorVMHttpService } from './errorVM.http.service';
export { PersonHttpService } from './person.http.service';
export { CategoryServiceHttpService } from '../../functions/http/categoryService.http.service';
export { TechnicalUserHttpService } from './technicalUser.http.service';
export { UserHttpService } from './user.http.service';
export { CategoryVMHttpService } from './categoryVM.http.service';
export { NotificationServiceHttpService } from '../../functions/http/notificationService.http.service';
export { LongTransactionVMHttpService } from './longTransactionVM.http.service';
export { StudentHttpService } from './student.http.service';
export { DozentHttpService } from './dozent.http.service';
export { VorlesungHttpService } from './vorlesung.http.service';
export { NotificationHttpService } from './notification.http.service';
export { PrimitivesServiceHttpService } from '../../functions/http/primitivesService.http.service';

export const GENERATED_SERVICES:any[] = [
    HttpService,
    PersonHttpService,
    PersonRolleHttpService,
    StudentHttpService,
    DozentHttpService,
    VorlesungHttpService,
    FachbereichHttpService,
    RaumHttpService,
    ErrorVMHttpService,
    LongTransactionVMHttpService,
    CategoryClassVMHttpService,
    CategoryVMHttpService,
    UserHttpService,
    CustomUserHttpService,
    AnonymUserHttpService,
    TechnicalUserHttpService,
    LoginVMHttpService,
    NotificationHttpService,
    StatistikVMHttpService,
    UniServiceHttpService,
    UserServiceHttpService,
    PrimitivesServiceHttpService,
    ModelServiceHttpService,
    NotificationServiceHttpService,
    SecureValueAccessorHttpService,
    CategoryServiceHttpService,
    TransactionServiceHttpService,
];
