import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { User } from '../../../domain/security/user';
import { HttpService } from '../../core/http.service';
import { Person } from '../../../domain/uni/person';
import { AnonymUser } from '../../../domain/security/anonymUser';
import { TechnicalUser } from '../../../domain/security/technicalUser';


/**
 * Service zum Abrufen des exportierten Domain Objektes User
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class UserHttpService extends AbstractHttpService<User>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<User>> {
		return this.http.get('/api/domainr/9').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<User>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<User>):Observable<Pageable<User>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<User>):Observable<Pageable<User>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<User> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<User>> {
        return this.http.get(`/api/domainr/9/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(user:User):Observable<User> {
		return this.http.put(user.self, user).map(this._extractObj).catch((error) => this.handleError(error));
	}

	delete(user:User):Observable<any> {
		return this.http.delete(user.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):User {
        const body = res.body;
        const type = UserHttpService._createInstanceOf(UserHttpService._extractTypeOfUrl(body._links.self.href));

		if (body == null) {
			return type;
		}
		return AbstractHttpService.extractOne(body, type);
	}

	_extractObjList(res:HttpResponse<any>):Pageable<User> {
		const body = res.body;
		if (body == null) {
			return new Pageable<User>();
		}
		const result = new Pageable<User>();

        for(var type in body._embedded) {
            if(body._embedded.hasOwnProperty(type)) {
                body._embedded[type].forEach(actualClass => {
                    const extracted = AbstractHttpService.extractOne(actualClass, UserHttpService._createInstanceOf(type));
                    result.data.push(extracted);
                });
            }
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}

    static _extractTypeOfUrl(url: string):string {
        const parts = url.split("/")
        if (parts.length < 3) {
            throw new Error("Can not process URL!")
        }
        return parts[3];
    }

    static _createInstanceOf(type: string): any {
        switch (type) {
            case 'person':
               return new Person();
            case 'anonymUser':
               return new AnonymUser();
            case 'technicalUser':
               return new TechnicalUser();
            default:
                throw new Error("Type not OK!");
        }
    }
}
