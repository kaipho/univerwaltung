import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { Notification } from '../../../domain/core/notification';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes Notification
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class NotificationHttpService extends AbstractHttpService<Notification>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<Notification>> {
		return this.http.get('/api/domainr/13').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<Notification>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<Notification>):Observable<Pageable<Notification>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<Notification>):Observable<Pageable<Notification>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<Notification> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<Notification>> {
        return this.http.get(`/api/domainr/13/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(notification:Notification):Observable<Notification> {
		return this.http.put(notification.self, notification).catch((error) => this.handleError(error));
	}

	delete(notification:Notification):Observable<any> {
		return this.http.delete(notification.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):Notification {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new Notification();
		}
		return AbstractHttpService.extractOne(body, new Notification());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<Notification> {
		const body = res.body;
		if (body == null) {
			return new Pageable<Notification>();
		}
		const result = new Pageable<Notification>();
		if(body._embedded.hasOwnProperty('notification')) {
            body._embedded.notification.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new Notification());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
