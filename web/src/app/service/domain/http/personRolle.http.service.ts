import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { PersonRolle } from '../../../domain/uni/personRolle';
import { HttpService } from '../../core/http.service';
import { Dozent } from '../../../domain/uni/dozent';
import { Student } from '../../../domain/uni/student';


/**
 * Service zum Abrufen des exportierten Domain Objektes PersonRolle
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class PersonRolleHttpService extends AbstractHttpService<PersonRolle>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<PersonRolle>> {
		return this.http.get('/api/domainr/22').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<PersonRolle>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<PersonRolle>):Observable<Pageable<PersonRolle>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<PersonRolle>):Observable<Pageable<PersonRolle>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<PersonRolle> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<PersonRolle>> {
        return this.http.get(`/api/domainr/22/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(personRolle:PersonRolle):Observable<PersonRolle> {
		return this.http.put(personRolle.self, personRolle).map(this._extractObj).catch((error) => this.handleError(error));
	}

	delete(personRolle:PersonRolle):Observable<any> {
		return this.http.delete(personRolle.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):PersonRolle {
        const body = res.body;
        const type = PersonRolleHttpService._createInstanceOf(PersonRolleHttpService._extractTypeOfUrl(body._links.self.href));

		if (body == null) {
			return type;
		}
		return AbstractHttpService.extractOne(body, type);
	}

	_extractObjList(res:HttpResponse<any>):Pageable<PersonRolle> {
		const body = res.body;
		if (body == null) {
			return new Pageable<PersonRolle>();
		}
		const result = new Pageable<PersonRolle>();

        for(var type in body._embedded) {
            if(body._embedded.hasOwnProperty(type)) {
                body._embedded[type].forEach(actualClass => {
                    const extracted = AbstractHttpService.extractOne(actualClass, PersonRolleHttpService._createInstanceOf(type));
                    result.data.push(extracted);
                });
            }
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}

    static _extractTypeOfUrl(url: string):string {
        const parts = url.split("/")
        if (parts.length < 3) {
            throw new Error("Can not process URL!")
        }
        return parts[3];
    }

    static _createInstanceOf(type: string): any {
        switch (type) {
            case 'dozent':
               return new Dozent();
            case 'student':
               return new Student();
            default:
                throw new Error("Type not OK!");
        }
    }
}
