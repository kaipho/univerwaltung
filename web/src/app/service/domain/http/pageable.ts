export class Pageable<D> {
    data: Array<D>;
    prev: string;
    next: string;
    total:number;

    constructor() {
        this.data = [];
    }
}
