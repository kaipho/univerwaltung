import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { CategoryVM } from '../../../domain/core/categoryVM';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes CategoryVM
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class CategoryVMHttpService extends AbstractHttpService<CategoryVM>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<CategoryVM>> {
		return this.http.get('/api/domainr/8').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<CategoryVM>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<CategoryVM>):Observable<Pageable<CategoryVM>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<CategoryVM>):Observable<Pageable<CategoryVM>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<CategoryVM> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<CategoryVM>> {
        return this.http.get(`/api/domainr/8/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(categoryVM:CategoryVM):Observable<CategoryVM> {
		return this.http.put(categoryVM.self, categoryVM).catch((error) => this.handleError(error));
	}

	delete(categoryVM:CategoryVM):Observable<any> {
		return this.http.delete(categoryVM.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):CategoryVM {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new CategoryVM();
		}
		return AbstractHttpService.extractOne(body, new CategoryVM());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<CategoryVM> {
		const body = res.body;
		if (body == null) {
			return new Pageable<CategoryVM>();
		}
		const result = new Pageable<CategoryVM>();
		if(body._embedded.hasOwnProperty('categoryVM')) {
            body._embedded.categoryVM.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new CategoryVM());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
