import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { AnonymUser } from '../../../domain/security/anonymUser';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes AnonymUser
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class AnonymUserHttpService extends AbstractHttpService<AnonymUser>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<AnonymUser>> {
		return this.http.get('/api/domainr/11').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<AnonymUser>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<AnonymUser>):Observable<Pageable<AnonymUser>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<AnonymUser>):Observable<Pageable<AnonymUser>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<AnonymUser> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<AnonymUser>> {
        return this.http.get(`/api/domainr/11/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(anonymUser:AnonymUser):Observable<AnonymUser> {
		return this.http.put(anonymUser.self, anonymUser).catch((error) => this.handleError(error));
	}

	delete(anonymUser:AnonymUser):Observable<any> {
		return this.http.delete(anonymUser.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):AnonymUser {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new AnonymUser();
		}
		return AbstractHttpService.extractOne(body, new AnonymUser());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<AnonymUser> {
		const body = res.body;
		if (body == null) {
			return new Pageable<AnonymUser>();
		}
		const result = new Pageable<AnonymUser>();
		if(body._embedded.hasOwnProperty('anonymUser')) {
            body._embedded.anonymUser.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new AnonymUser());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
