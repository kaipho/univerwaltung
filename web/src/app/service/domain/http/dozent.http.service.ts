import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { Dozent } from '../../../domain/uni/dozent';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes Dozent
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class DozentHttpService extends AbstractHttpService<Dozent>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<Dozent>> {
		return this.http.get('/api/domainr/23').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<Dozent>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<Dozent>):Observable<Pageable<Dozent>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<Dozent>):Observable<Pageable<Dozent>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<Dozent> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<Dozent>> {
        return this.http.get(`/api/domainr/23/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(dozent:Dozent):Observable<Dozent> {
		return this.http.put(dozent.self, dozent).catch((error) => this.handleError(error));
	}

	delete(dozent:Dozent):Observable<any> {
		return this.http.delete(dozent.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):Dozent {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new Dozent();
		}
		return AbstractHttpService.extractOne(body, new Dozent());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<Dozent> {
		const body = res.body;
		if (body == null) {
			return new Pageable<Dozent>();
		}
		const result = new Pageable<Dozent>();
		if(body._embedded.hasOwnProperty('dozent')) {
            body._embedded.dozent.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new Dozent());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
