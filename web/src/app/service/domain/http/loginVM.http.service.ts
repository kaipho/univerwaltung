import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { LoginVM } from '../../../domain/security/loginVM';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes LoginVM
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class LoginVMHttpService extends AbstractHttpService<LoginVM>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<LoginVM>> {
		return this.http.get('/api/domainr/12').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<LoginVM>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<LoginVM>):Observable<Pageable<LoginVM>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<LoginVM>):Observable<Pageable<LoginVM>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<LoginVM> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<LoginVM>> {
        return this.http.get(`/api/domainr/12/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(loginVM:LoginVM):Observable<LoginVM> {
		return this.http.put(loginVM.self, loginVM).catch((error) => this.handleError(error));
	}

	delete(loginVM:LoginVM):Observable<any> {
		return this.http.delete(loginVM.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):LoginVM {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new LoginVM();
		}
		return AbstractHttpService.extractOne(body, new LoginVM());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<LoginVM> {
		const body = res.body;
		if (body == null) {
			return new Pageable<LoginVM>();
		}
		const result = new Pageable<LoginVM>();
		if(body._embedded.hasOwnProperty('loginVM')) {
            body._embedded.loginVM.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new LoginVM());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
