import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Pageable } from "./pageable";
import { AbstractHttpService } from "./abstract.http.service";
import { Router } from "@angular/router";
import { ErrorVM } from '../../../domain/exception/errorVM';
import { HttpService } from '../../core/http.service';


/**
 * Service zum Abrufen des exportierten Domain Objektes ErrorVM
 * Läd das Objekt samt aller Abhängigkeiten
 */
@Injectable()
export class ErrorVMHttpService extends AbstractHttpService<ErrorVM>{

    constructor(http:HttpService, router:Router) {
        super(http, router);
    }

	getFirst():Observable<Pageable<ErrorVM>> {
		return this.http.get('/api/domainr/6').map(this._extractObjList).catch((error) => this.handleError(error));
	}

    getAll(link: string):Observable<Pageable<ErrorVM>> {
        return this.http.get(link).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getNext(data: Pageable<ErrorVM>):Observable<Pageable<ErrorVM>> {
        return this.http.get(data.next).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	getPrev(data: Pageable<ErrorVM>):Observable<Pageable<ErrorVM>> {
        return this.http.get(data.prev).map(this._extractObjList).catch((error) => this.handleError(error));
    }

	get(link:string):Observable<ErrorVM> {
		return this.http.get(link).map(this._extractObj).catch((error) => this.handleError(error));
	}

	// TODO repository functions

    find(id:number, searchText:string):Observable<Pageable<ErrorVM>> {
        return this.http.get(`/api/domainr/6/find?associationId=${id}&val=${searchText}`).map(this._extractObjList).catch((error) => this.handleError(error));
    }

    create(type:number):Observable<String> {
        return this.http.get(`/api/domainr/${type}/create`).map(it => it.body).catch((error) => this.handleError(error));
    }

	update(errorVM:ErrorVM):Observable<ErrorVM> {
		return this.http.put(errorVM.self, errorVM).catch((error) => this.handleError(error));
	}

	delete(errorVM:ErrorVM):Observable<any> {
		return this.http.delete(errorVM.self).catch((error) => this.handleError(error));
	}

	_extractObj(res:HttpResponse<any>):ErrorVM {
		const body = res.body;
		if (body == null) {
		    // TODO Decide which to instanciate
			return new ErrorVM();
		}
		return AbstractHttpService.extractOne(body, new ErrorVM());
	}

	_extractObjList(res:HttpResponse<any>):Pageable<ErrorVM> {
		const body = res.body;
		if (body == null) {
			return new Pageable<ErrorVM>();
		}
		const result = new Pageable<ErrorVM>();
		if(body._embedded.hasOwnProperty('errorVM')) {
            body._embedded.errorVM.forEach(actualClass => {
                const extracted = AbstractHttpService.extractOne(actualClass, new ErrorVM());
                result.data.push(extracted);
            });
        }
        if(body.page != null) {
            result.total = body.page.totalElements;
            if(body._links.prev) {
                result.prev = body._links.prev.href;
            }
            if(body._links.next) {
                result.next = body._links.next.href;
            }
		}
		return result;
	}
}
