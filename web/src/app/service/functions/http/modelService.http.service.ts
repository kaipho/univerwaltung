import {Injectable} from "@angular/core";
import {HttpService} from "../../../service/core/http.service";
import {Observable} from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";
import { StatistikVM } from '../../../domain/db/statistikVM';

@Injectable()
export class ModelServiceHttpService {
    private _http: HttpService;

    constructor(http:HttpService) {
        this._http = http;
    }

	public getModelForClass(clazz: string): Observable<any> {
	   return this._http.post(`/db/getModelForClass`, {clazz : clazz, }).map(res => res.body);
	}
	public getAllClasses(): Observable<any> {
	   return this._http.post(`/db/getAllClasses`, {}).map(res => res.body);
	}
	public modelStatistics(): Observable<any> {
	   return this._http.post(`/db/modelStatistics`, {}).map(res => ModelServiceHttpService._extractObjList(res, 'statistikVM'));
	}

    static _extractOne(data: any, obj: any):any {
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                obj[property] = data[property];
            }
        }
        return obj;
    }

    static _extractObjList(res:HttpResponse<any>, obj: string):Array<any> {
        const body = res.body;
        if (body == null) {
            return [];
        }
        const result = [];

        body.forEach(actualClass => {
            const extracted = ModelServiceHttpService._extractOne(actualClass, ModelServiceHttpService._getInstance(obj));
            result.push(extracted);
        });
        return result;
    }

    static _getInstance(obj: string):any {
		switch (obj) {
			case 'statistikVM': return new StatistikVM();
            default: throw new Error("Type not OK!");
        }
    }
}
