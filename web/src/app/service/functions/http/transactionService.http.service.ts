import {Injectable} from "@angular/core";
import {HttpService} from "../../../service/core/http.service";
import {Observable} from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";
import { StatistikVM } from '../../../domain/db/statistikVM';
import { LongTransactionVM } from '../../../domain/transaction/longTransactionVM';

@Injectable()
export class TransactionServiceHttpService {
    private _http: HttpService;

    constructor(http:HttpService) {
        this._http = http;
    }

	public commit(): Observable<any> {
	   return this._http.post(`/transaction/commit`, {}).map(res => res.body);
	}
	public startTransaction(): Observable<any> {
	   return this._http.post(`/transaction/startTransaction`, {}).map(res => res.body);
	}
	public openTransactions(): Observable<any> {
	   return this._http.post(`/transaction/openTransactions`, {}).map(res => TransactionServiceHttpService._extractObjList(res, 'longTransactionVM'));
	}
	public transactionStatistics(): Observable<any> {
	   return this._http.post(`/transaction/transactionStatistics`, {}).map(res => TransactionServiceHttpService._extractObjList(res, 'statistikVM'));
	}

    static _extractOne(data: any, obj: any):any {
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                obj[property] = data[property];
            }
        }
        return obj;
    }

    static _extractObjList(res:HttpResponse<any>, obj: string):Array<any> {
        const body = res.body;
        if (body == null) {
            return [];
        }
        const result = [];

        body.forEach(actualClass => {
            const extracted = TransactionServiceHttpService._extractOne(actualClass, TransactionServiceHttpService._getInstance(obj));
            result.push(extracted);
        });
        return result;
    }

    static _getInstance(obj: string):any {
		switch (obj) {
			case 'statistikVM': return new StatistikVM();
			case 'longTransactionVM': return new LongTransactionVM();
            default: throw new Error("Type not OK!");
        }
    }
}
