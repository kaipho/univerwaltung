import {Injectable} from "@angular/core";
import {HttpService} from "../../../service/core/http.service";
import {Observable} from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";
import { CategoryClassVM } from '../../../domain/core/categoryClassVM';

@Injectable()
export class CategoryServiceHttpService {
    private _http: HttpService;

    constructor(http:HttpService) {
        this._http = http;
    }

	public getValuesFor(categoryClassId: number): Observable<any> {
	   return this._http.post(`/core/getValuesFor`, {categoryClassId : categoryClassId, }).map(res => res.body);
	}
	public addValueTo(value: string, categoryClassId: number): Observable<any> {
	   return this._http.post(`/core/addValueTo`, {value : value, categoryClassId : categoryClassId, }).map(res => res.body);
	}
	public deleteValue(value: string, categoryClassId: number): Observable<any> {
	   return this._http.post(`/core/deleteValue`, {value : value, categoryClassId : categoryClassId, }).map(res => res.body);
	}
	public getAllCategoryClasses(): Observable<any> {
	   return this._http.post(`/core/getAllCategoryClasses`, {}).map(res => CategoryServiceHttpService._extractObjList(res, 'categoryClassVM'));
	}

    static _extractOne(data: any, obj: any):any {
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                obj[property] = data[property];
            }
        }
        return obj;
    }

    static _extractObjList(res:HttpResponse<any>, obj: string):Array<any> {
        const body = res.body;
        if (body == null) {
            return [];
        }
        const result = [];

        body.forEach(actualClass => {
            const extracted = CategoryServiceHttpService._extractOne(actualClass, CategoryServiceHttpService._getInstance(obj));
            result.push(extracted);
        });
        return result;
    }

    static _getInstance(obj: string):any {
		switch (obj) {
			case 'categoryClassVM': return new CategoryClassVM();
            default: throw new Error("Type not OK!");
        }
    }
}
