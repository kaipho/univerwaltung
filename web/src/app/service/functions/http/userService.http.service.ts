import {Injectable} from "@angular/core";
import {HttpService} from "../../../service/core/http.service";
import {Observable} from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";
import { LoginVM } from '../../../domain/security/loginVM';

@Injectable()
export class UserServiceHttpService {
    private _http: HttpService;

    constructor(http:HttpService) {
        this._http = http;
    }

	public login(username: string, password: string): Observable<any> {
	   return this._http.post(`/security/login`, {username : username, password : password, }).map(res => UserServiceHttpService._extractOne(res.body, new LoginVM()));
	}
	public logout(): Observable<any> {
	   return this._http.post(`/security/logout`, {}).map(res => res.body);
	}
	public changePassword(old: string, newPassword: string, repeatIt: string): Observable<any> {
	   return this._http.post(`/security/changePassword`, {old : old, newPassword : newPassword, repeatIt : repeatIt, }).map(res => res.body);
	}
	public resetPassword(user: number): Observable<any> {
	   return this._http.post(`/security/resetPassword`, {user : user, }).map(res => res.body);
	}
	public getUserFromToken(token: string): Observable<any> {
	   return this._http.post(`/security/getUserFromToken`, {token : token, }).map(res => UserServiceHttpService._extractOne(res.body, new LoginVM()));
	}

    static _extractOne(data: any, obj: any):any {
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                obj[property] = data[property];
            }
        }
        return obj;
    }

    static _extractObjList(res:HttpResponse<any>, obj: string):Array<any> {
        const body = res.body;
        if (body == null) {
            return [];
        }
        const result = [];

        body.forEach(actualClass => {
            const extracted = UserServiceHttpService._extractOne(actualClass, UserServiceHttpService._getInstance(obj));
            result.push(extracted);
        });
        return result;
    }

    static _getInstance(obj: string):any {
		switch (obj) {
			case 'loginVM': return new LoginVM();
            default: throw new Error("Type not OK!");
        }
    }
}
