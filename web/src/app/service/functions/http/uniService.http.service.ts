import {Injectable} from "@angular/core";
import {HttpService} from "../../../service/core/http.service";
import {Observable} from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";

@Injectable()
export class UniServiceHttpService {
    private _http: HttpService;

    constructor(http:HttpService) {
        this._http = http;
    }

	public setFachbereich(dozent: number, fachbereich: number): Observable<any> {
	   return this._http.post(`/uni/setFachbereich`, {dozent : dozent, fachbereich : fachbereich, }).map(res => res.body);
	}
	public setRolle(person: number, rolle: number): Observable<any> {
	   return this._http.post(`/uni/setRolle`, {person : person, rolle : rolle, }).map(res => res.body);
	}
	public addVorlesung(student: number, vorlesung: number): Observable<any> {
	   return this._http.post(`/uni/addVorlesung`, {student : student, vorlesung : vorlesung, }).map(res => res.body);
	}
	public setRaum(vorlesung: number, raum: number): Observable<any> {
	   return this._http.post(`/uni/setRaum`, {vorlesung : vorlesung, raum : raum, }).map(res => res.body);
	}
	public setFachbereichVorlesung(vorlesung: number, fachbereich: number): Observable<any> {
	   return this._http.post(`/uni/setFachbereichVorlesung`, {vorlesung : vorlesung, fachbereich : fachbereich, }).map(res => res.body);
	}
	public setDozent(vorlesung: number, dozent: number): Observable<any> {
	   return this._http.post(`/uni/setDozent`, {vorlesung : vorlesung, dozent : dozent, }).map(res => res.body);
	}

    static _extractOne(data: any, obj: any):any {
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                obj[property] = data[property];
            }
        }
        return obj;
    }

    static _extractObjList(res:HttpResponse<any>, obj: string):Array<any> {
        const body = res.body;
        if (body == null) {
            return [];
        }
        const result = [];

        body.forEach(actualClass => {
            const extracted = UniServiceHttpService._extractOne(actualClass, UniServiceHttpService._getInstance(obj));
            result.push(extracted);
        });
        return result;
    }

    static _getInstance(obj: string):any {
		switch (obj) {
            default: throw new Error("Type not OK!");
        }
    }
}
