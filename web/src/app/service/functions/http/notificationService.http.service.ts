import {Injectable} from "@angular/core";
import {HttpService} from "../../../service/core/http.service";
import {Observable} from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";
import { Notification } from '../../../domain/core/notification';

@Injectable()
export class NotificationServiceHttpService {
    private _http: HttpService;

    constructor(http:HttpService) {
        this._http = http;
    }

	public addNotification(user: number, notification: number): Observable<any> {
	   return this._http.post(`/core/addNotification`, {user : user, notification : notification, }).map(res => res.body);
	}
	public getNotifications(): Observable<any> {
	   return this._http.post(`/core/getNotifications`, {}).map(res => NotificationServiceHttpService._extractObjList(res, 'notification'));
	}

    static _extractOne(data: any, obj: any):any {
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                obj[property] = data[property];
            }
        }
        return obj;
    }

    static _extractObjList(res:HttpResponse<any>, obj: string):Array<any> {
        const body = res.body;
        if (body == null) {
            return [];
        }
        const result = [];

        body.forEach(actualClass => {
            const extracted = NotificationServiceHttpService._extractOne(actualClass, NotificationServiceHttpService._getInstance(obj));
            result.push(extracted);
        });
        return result;
    }

    static _getInstance(obj: string):any {
		switch (obj) {
			case 'notification': return new Notification();
            default: throw new Error("Type not OK!");
        }
    }
}
