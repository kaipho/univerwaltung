import {Injectable} from "@angular/core";
import {HttpService} from "../../../service/core/http.service";
import {Observable} from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";

@Injectable()
export class SecureValueAccessorHttpService {
    private _http: HttpService;

    constructor(http:HttpService) {
        this._http = http;
    }

	public decrypt(text: string, password: string): Observable<any> {
	   return this._http.post(`/security/decrypt`, {text : text, password : password, }).map(res => res.body);
	}
	public encrypt(text: string, password: string): Observable<any> {
	   return this._http.post(`/security/encrypt`, {text : text, password : password, }).map(res => res.body);
	}

    static _extractOne(data: any, obj: any):any {
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                obj[property] = data[property];
            }
        }
        return obj;
    }

    static _extractObjList(res:HttpResponse<any>, obj: string):Array<any> {
        const body = res.body;
        if (body == null) {
            return [];
        }
        const result = [];

        body.forEach(actualClass => {
            const extracted = SecureValueAccessorHttpService._extractOne(actualClass, SecureValueAccessorHttpService._getInstance(obj));
            result.push(extracted);
        });
        return result;
    }

    static _getInstance(obj: string):any {
		switch (obj) {
            default: throw new Error("Type not OK!");
        }
    }
}
