import {Injectable} from "@angular/core";
import {HttpService} from "../../../service/core/http.service";
import {Observable} from "rxjs/Rx";
import { HttpResponse } from "@angular/common/http";

@Injectable()
export class PrimitivesServiceHttpService {
    private _http: HttpService;

    constructor(http:HttpService) {
        this._http = http;
    }

	public convertTo(primitiveId: number, unitId: number): Observable<any> {
	   return this._http.post(`/primitive/convertTo`, {primitiveId : primitiveId, unitId : unitId, }).map(res => res.body);
	}

    static _extractOne(data: any, obj: any):any {
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                obj[property] = data[property];
            }
        }
        return obj;
    }

    static _extractObjList(res:HttpResponse<any>, obj: string):Array<any> {
        const body = res.body;
        if (body == null) {
            return [];
        }
        const result = [];

        body.forEach(actualClass => {
            const extracted = PrimitivesServiceHttpService._extractOne(actualClass, PrimitivesServiceHttpService._getInstance(obj));
            result.push(extracted);
        });
        return result;
    }

    static _getInstance(obj: string):any {
		switch (obj) {
            default: throw new Error("Type not OK!");
        }
    }
}
