import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Rx";
import { Component, Injectable, OnInit } from "@angular/core";
import { FormService } from "../components/framework/form/form.service";
import { MatDialog, MatDialogRef } from "@angular/material";

/**
 * Guard, der Routes mit einer authentifizierung ausstattet.
 * Falls keine Anmeldung vorhanden ist, wird der Nutzer auf die Anmeldeseite weitergeleitet.
 */
@Injectable()
export class SaveGuard implements CanDeactivate<OnInit> {
    constructor(public formService: FormService,
                public dialog: MatDialog) {

    }

    canDeactivate(component: OnInit, route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>
        | Promise<boolean>
        | boolean {
        if (!this.formService.isFormTouched()) {
            return true;
        }

        return this.dialog.open(SaveDialog).afterClosed().map(
            it => {
                if(it) {
                    this.formService.save();
                }
                return true;
            }
        );
    }
}

@Component({
    template: `
    <mat-toolbar>Speichern?</mat-toolbar>
    <mat-card-actions align="end">
        <div>
            <button mat-raised-button type="submit" color="primary" (click)="save()">
                <mat-icon>check</mat-icon>
                Speichern
            </button>
            <button mat-button mat-dialog-close>
                <mat-icon>close</mat-icon>
                Verwerfen
            </button>
        </div>
    </mat-card-actions>
    `
})
export class SaveDialog {
    constructor(public dialogRef: MatDialogRef<SaveDialog>) {}

    save() {
        this.dialogRef.close(true);
    }
}
