import { Routes } from '@angular/router';
import { AuthenticationGuard } from './guard/authentication.guard';
import { MasterviewComponent } from "./components/admin/masterview/masterview.component";
import { TransactionsComponent } from "./components/admin/transactions/transactions.component";
import { StatisticComponent } from "./components/admin/statistic/statistic.component";

import { CategoryVMDomainRoutes } from './components/domain/core/categoryVM.routes';
import { UserDomainRoutes } from './components/domain/security/user.routes';
import { CustomUserDomainRoutes } from './components/domain/security/customUser.routes';
import { PersonDomainRoutes } from './components/domain/uni/person.routes';
import { PersonRolleDomainRoutes } from './components/domain/uni/personRolle.routes';
import { NotificationDomainRoutes } from './components/domain/core/notification.routes';
import { FachbereichDomainRoutes } from './components/domain/uni/fachbereich.routes';
import { StudentDomainRoutes } from './components/domain/uni/student.routes';
import { RaumDomainRoutes } from './components/domain/uni/raum.routes';
import { DozentDomainRoutes } from './components/domain/uni/dozent.routes';
import { ErrorVMDomainRoutes } from './components/domain/exception/errorVM.routes';
import { CategoryClassVMDomainRoutes } from './components/domain/core/categoryClassVM.routes';
import { AnonymUserDomainRoutes } from './components/domain/security/anonymUser.routes';
import { TechnicalUserDomainRoutes } from './components/domain/security/technicalUser.routes';
import { LongTransactionVMDomainRoutes } from './components/domain/transaction/longTransactionVM.routes';
import { VorlesungDomainRoutes } from './components/domain/uni/vorlesung.routes';
import { StatistikVMDomainRoutes } from './components/domain/db/statistikVM.routes';
import { LoginVMDomainRoutes } from './components/domain/security/loginVM.routes';

export const ROUTING: Routes = [
    {
        path: '',
        redirectTo: "/web/admin/categories",
        pathMatch: 'full'
    },
    {
        path: 'web/admin/categories',
        canActivate: [AuthenticationGuard],
        component: MasterviewComponent
    },
    {
        path: 'web/admin/transactions',
        canActivate: [AuthenticationGuard],
        component: TransactionsComponent
    },
    {
        path: 'web/admin/statistics',
        canActivate: [AuthenticationGuard],
        component: StatisticComponent
    },
    {
        path: 'web',
        canActivate: [AuthenticationGuard],
        children: [
            ...PersonDomainRoutes,
            ...PersonRolleDomainRoutes,
            ...StudentDomainRoutes,
            ...DozentDomainRoutes,
            ...VorlesungDomainRoutes,
            ...FachbereichDomainRoutes,
            ...RaumDomainRoutes,
            ...ErrorVMDomainRoutes,
            ...LongTransactionVMDomainRoutes,
            ...CategoryClassVMDomainRoutes,
            ...CategoryVMDomainRoutes,
            ...UserDomainRoutes,
            ...CustomUserDomainRoutes,
            ...AnonymUserDomainRoutes,
            ...TechnicalUserDomainRoutes,
            ...LoginVMDomainRoutes,
            ...NotificationDomainRoutes,
            ...StatistikVMDomainRoutes,
        ]
    }
];
