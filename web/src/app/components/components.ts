import { UserServiceChangePasswordUnitComponent } from './service/security/UserService.changePassword.Unit.component';
import { ModelServiceModelStatisticsCollectionComponent } from './service/db/ModelService.modelStatistics.Collection.component';
import { CategoryServiceAddValueToUnitComponent } from './service/core/CategoryService.addValueTo.Unit.component';
import { UniServiceSetFachbereichVorlesungUnitComponent } from './service/uni/UniService.setFachbereichVorlesung.Unit.component';
import { UserServiceGetUserFromTokenLoginVMComponent } from './service/security/UserService.getUserFromToken.LoginVM.component';
import { UniServiceSetFachbereichUnitComponent } from './service/uni/UniService.setFachbereich.Unit.component';
import { NotificationServiceGetNotificationsCollectionComponent } from './service/core/NotificationService.getNotifications.Collection.component';
import { CategoryServiceGetValuesForCollectionComponent } from './service/core/CategoryService.getValuesFor.Collection.component';
import { TransactionServiceOpenTransactionsCollectionComponent } from './service/transaction/TransactionService.openTransactions.Collection.component';
import { UserServiceLoginLoginVMComponent } from './service/security/UserService.login.LoginVM.component';
import { CategoryServiceGetAllCategoryClassesCollectionComponent } from './service/core/CategoryService.getAllCategoryClasses.Collection.component';
import { UniServiceSetDozentUnitComponent } from './service/uni/UniService.setDozent.Unit.component';
import { CategoryServiceDeleteValueUnitComponent } from './service/core/CategoryService.deleteValue.Unit.component';
import { TransactionServiceCommitUnitComponent } from './service/transaction/TransactionService.commit.Unit.component';
import { TransactionServiceStartTransactionUnitComponent } from './service/transaction/TransactionService.startTransaction.Unit.component';
import { UniServiceAddVorlesungUnitComponent } from './service/uni/UniService.addVorlesung.Unit.component';
import { PrimitivesServiceConvertToDoubleComponent } from './service/primitive/PrimitivesService.convertTo.Double.component';
import { NotificationServiceAddNotificationUnitComponent } from './service/core/NotificationService.addNotification.Unit.component';
import { ModelServiceGetAllClassesCollectionComponent } from './service/db/ModelService.getAllClasses.Collection.component';
import { SecureValueAccessorEncryptStringComponent } from './service/security/SecureValueAccessor.encrypt.String.component';
import { UserServiceResetPasswordStringComponent } from './service/security/UserService.resetPassword.String.component';
import { SecureValueAccessorDecryptStringComponent } from './service/security/SecureValueAccessor.decrypt.String.component';
import { ModelServiceGetModelForClassStringComponent } from './service/db/ModelService.getModelForClass.String.component';
import { UniServiceSetRaumUnitComponent } from './service/uni/UniService.setRaum.Unit.component';
import { TransactionServiceTransactionStatisticsCollectionComponent } from './service/transaction/TransactionService.transactionStatistics.Collection.component';
import { UniServiceSetRolleUnitComponent } from './service/uni/UniService.setRolle.Unit.component';
import { UserServiceLogoutUnitComponent } from './service/security/UserService.logout.Unit.component';
export { UserServiceChangePasswordUnitComponent } from './service/security/UserService.changePassword.Unit.component';
export { ModelServiceModelStatisticsCollectionComponent } from './service/db/ModelService.modelStatistics.Collection.component';
export { CategoryServiceAddValueToUnitComponent } from './service/core/CategoryService.addValueTo.Unit.component';
export { UniServiceSetFachbereichVorlesungUnitComponent } from './service/uni/UniService.setFachbereichVorlesung.Unit.component';
export { UserServiceGetUserFromTokenLoginVMComponent } from './service/security/UserService.getUserFromToken.LoginVM.component';
export { UniServiceSetFachbereichUnitComponent } from './service/uni/UniService.setFachbereich.Unit.component';
export { NotificationServiceGetNotificationsCollectionComponent } from './service/core/NotificationService.getNotifications.Collection.component';
export { CategoryServiceGetValuesForCollectionComponent } from './service/core/CategoryService.getValuesFor.Collection.component';
export { TransactionServiceOpenTransactionsCollectionComponent } from './service/transaction/TransactionService.openTransactions.Collection.component';
export { UserServiceLoginLoginVMComponent } from './service/security/UserService.login.LoginVM.component';
export { CategoryServiceGetAllCategoryClassesCollectionComponent } from './service/core/CategoryService.getAllCategoryClasses.Collection.component';
export { UniServiceSetDozentUnitComponent } from './service/uni/UniService.setDozent.Unit.component';
export { CategoryServiceDeleteValueUnitComponent } from './service/core/CategoryService.deleteValue.Unit.component';
export { TransactionServiceCommitUnitComponent } from './service/transaction/TransactionService.commit.Unit.component';
export { TransactionServiceStartTransactionUnitComponent } from './service/transaction/TransactionService.startTransaction.Unit.component';
export { UniServiceAddVorlesungUnitComponent } from './service/uni/UniService.addVorlesung.Unit.component';
export { PrimitivesServiceConvertToDoubleComponent } from './service/primitive/PrimitivesService.convertTo.Double.component';
export { NotificationServiceAddNotificationUnitComponent } from './service/core/NotificationService.addNotification.Unit.component';
export { ModelServiceGetAllClassesCollectionComponent } from './service/db/ModelService.getAllClasses.Collection.component';
export { SecureValueAccessorEncryptStringComponent } from './service/security/SecureValueAccessor.encrypt.String.component';
export { UserServiceResetPasswordStringComponent } from './service/security/UserService.resetPassword.String.component';
export { SecureValueAccessorDecryptStringComponent } from './service/security/SecureValueAccessor.decrypt.String.component';
export { ModelServiceGetModelForClassStringComponent } from './service/db/ModelService.getModelForClass.String.component';
export { UniServiceSetRaumUnitComponent } from './service/uni/UniService.setRaum.Unit.component';
export { TransactionServiceTransactionStatisticsCollectionComponent } from './service/transaction/TransactionService.transactionStatistics.Collection.component';
export { UniServiceSetRolleUnitComponent } from './service/uni/UniService.setRolle.Unit.component';
export { UserServiceLogoutUnitComponent } from './service/security/UserService.logout.Unit.component';



export const SERVICE_COMPONENTS:any[] = [
	UniServiceSetFachbereichUnitComponent,
	UniServiceSetRolleUnitComponent,
	UniServiceAddVorlesungUnitComponent,
	UniServiceSetRaumUnitComponent,
	UniServiceSetFachbereichVorlesungUnitComponent,
	UniServiceSetDozentUnitComponent,
	UserServiceLoginLoginVMComponent,
	UserServiceLogoutUnitComponent,
	UserServiceChangePasswordUnitComponent,
	UserServiceResetPasswordStringComponent,
	UserServiceGetUserFromTokenLoginVMComponent,
	PrimitivesServiceConvertToDoubleComponent,
	ModelServiceGetModelForClassStringComponent,
	ModelServiceGetAllClassesCollectionComponent,
	ModelServiceModelStatisticsCollectionComponent,
	NotificationServiceAddNotificationUnitComponent,
	NotificationServiceGetNotificationsCollectionComponent,
	SecureValueAccessorDecryptStringComponent,
	SecureValueAccessorEncryptStringComponent,
	CategoryServiceGetValuesForCollectionComponent,
	CategoryServiceAddValueToUnitComponent,
	CategoryServiceDeleteValueUnitComponent,
	CategoryServiceGetAllCategoryClassesCollectionComponent,
	TransactionServiceCommitUnitComponent,
	TransactionServiceStartTransactionUnitComponent,
	TransactionServiceOpenTransactionsCollectionComponent,
	TransactionServiceTransactionStatisticsCollectionComponent,
];
