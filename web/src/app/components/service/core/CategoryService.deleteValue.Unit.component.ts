import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewContainerRef } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';
import { CategoryServiceHttpService } from '../../../service/functions/http/categoryService.http.service';

@Component({
    templateUrl: './CategoryService.deleteValue.Unit.component.html',
})
export class CategoryServiceDeleteValueUnitComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<CategoryServiceDeleteValueUnitComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<CategoryServiceDeleteValueUnitComponent>, builder:FormBuilder
            , public http: CategoryServiceHttpService) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            value: ['', Validators.required],
            categoryClassId: ['', Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    submit() {
       this.submitting = true;
       this.http.deleteValue(
               this.serviceForm.get('value').value,
               this.serviceForm.get('categoryClassId').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               this.dialogRef.close(ok);
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
