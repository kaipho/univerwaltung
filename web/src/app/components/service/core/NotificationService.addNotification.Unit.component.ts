import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { User } from '../../../domain/security/user';
import { Notification } from '../../../domain/core/notification';
import { NotificationServiceHttpService } from '../../../service/functions/http/notificationService.http.service';
import { NotificationListComponent } from '../../domain/core/notification.list.component';
import { ViewContainerRef } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';

@Component({
    templateUrl: './NotificationService.addNotification.Unit.component.html',
})
export class NotificationServiceAddNotificationUnitComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<NotificationServiceAddNotificationUnitComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<NotificationServiceAddNotificationUnitComponent>, builder:FormBuilder
            , public http: NotificationServiceHttpService, public dialog: MatDialog, public viewContainerRef: ViewContainerRef) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            notification: [{}, Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    selectNotification() {
       let config = new MatDialogConfig();
       config.viewContainerRef = this.viewContainerRef;
       const ref = this.dialog.open(NotificationListComponent, config);
       ref.afterClosed().subscribe(result => {
           this.serviceForm.patchValue({notification: result.id});
       });
    }
    submit() {
       this.submitting = true;
       this.http.addNotification(
               this._actualId, 
               this.serviceForm.get('notification').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               this.dialogRef.close(ok);
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
