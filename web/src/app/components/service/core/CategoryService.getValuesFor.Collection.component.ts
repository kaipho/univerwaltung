import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewContainerRef } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';
import { CategoryServiceHttpService } from '../../../service/functions/http/categoryService.http.service';

@Component({
    templateUrl: './CategoryService.getValuesFor.Collection.component.html',
})
export class CategoryServiceGetValuesForCollectionComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<CategoryServiceGetValuesForCollectionComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<CategoryServiceGetValuesForCollectionComponent>, builder:FormBuilder
            , public http: CategoryServiceHttpService) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            categoryClassId: ['', Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    submit() {
       this.submitting = true;
       this.http.getValuesFor(
               this.serviceForm.get('categoryClassId').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               if (this.closeSilent) this.dialogRef.close(ok);
               this.serviceForm.patchValue({result: ok});
               this._result = ok;
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
