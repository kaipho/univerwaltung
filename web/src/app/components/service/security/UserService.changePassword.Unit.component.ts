import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewContainerRef } from '@angular/core';
import { UserServiceHttpService } from '../../../service/functions/http/userService.http.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';

@Component({
    templateUrl: './UserService.changePassword.Unit.component.html',
})
export class UserServiceChangePasswordUnitComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<UserServiceChangePasswordUnitComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<UserServiceChangePasswordUnitComponent>, builder:FormBuilder
            , public http: UserServiceHttpService) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            old: ['', Validators.required],
            newPassword: ['', Validators.required],
            repeatIt: ['', Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    submit() {
       this.submitting = true;
       this.http.changePassword(
               this.serviceForm.get('old').value,
               this.serviceForm.get('newPassword').value,
               this.serviceForm.get('repeatIt').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               this.dialogRef.close(ok);
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
