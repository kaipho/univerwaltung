import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserServiceHttpService } from '../../../service/functions/http/userService.http.service';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';

@Component({
    templateUrl: './UserService.logout.Unit.component.html',
})
export class UserServiceLogoutUnitComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<UserServiceLogoutUnitComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<UserServiceLogoutUnitComponent>, builder:FormBuilder
            , public http: UserServiceHttpService) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
        })
    }

    ngOnInit() {
        this.submit()
    }

    close() {
        this.dialogRef.close(this._result);
    }

    submit() {
       this.submitting = true;
       this.http.logout(
       ).subscribe(
           (ok) => {
               this.submitting = false;
               this.dialogRef.close(ok);
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
