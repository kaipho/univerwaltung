import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewContainerRef } from '@angular/core';
import { UserServiceHttpService } from '../../../service/functions/http/userService.http.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';

@Component({
    templateUrl: './UserService.getUserFromToken.LoginVM.component.html',
})
export class UserServiceGetUserFromTokenLoginVMComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<UserServiceGetUserFromTokenLoginVMComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<UserServiceGetUserFromTokenLoginVMComponent>, builder:FormBuilder
            , public http: UserServiceHttpService) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            token: ['', Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    submit() {
       this.submitting = true;
       this.http.getUserFromToken(
               this.serviceForm.get('token').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               if (this.closeSilent) this.dialogRef.close(ok);
               this.serviceForm.patchValue({result: ok});
               this._result = ok;
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
