import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewContainerRef } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { PrimitivesServiceHttpService } from '../../../service/functions/http/primitivesService.http.service';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';

@Component({
    templateUrl: './PrimitivesService.convertTo.Double.component.html',
})
export class PrimitivesServiceConvertToDoubleComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<PrimitivesServiceConvertToDoubleComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<PrimitivesServiceConvertToDoubleComponent>, builder:FormBuilder
            , public http: PrimitivesServiceHttpService) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            primitiveId: ['', Validators.required],
            unitId: ['', Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    submit() {
       this.submitting = true;
       this.http.convertTo(
               this.serviceForm.get('primitiveId').value,
               this.serviceForm.get('unitId').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               if (this.closeSilent) this.dialogRef.close(ok);
               this.serviceForm.patchValue({result: ok});
               this._result = ok;
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
