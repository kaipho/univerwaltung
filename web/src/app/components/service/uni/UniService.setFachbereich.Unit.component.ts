import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UniServiceHttpService } from '../../../service/functions/http/uniService.http.service';
import { Dozent } from '../../../domain/uni/dozent';
import { ViewContainerRef } from '@angular/core';
import { FachbereichListComponent } from '../../domain/uni/fachbereich.list.component';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';
import { Fachbereich } from '../../../domain/uni/fachbereich';

@Component({
    templateUrl: './UniService.setFachbereich.Unit.component.html',
})
export class UniServiceSetFachbereichUnitComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<UniServiceSetFachbereichUnitComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<UniServiceSetFachbereichUnitComponent>, builder:FormBuilder
            , public http: UniServiceHttpService, public dialog: MatDialog, public viewContainerRef: ViewContainerRef) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            fachbereich: [{}, Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    selectFachbereich() {
       let config = new MatDialogConfig();
       config.viewContainerRef = this.viewContainerRef;
       const ref = this.dialog.open(FachbereichListComponent, config);
       ref.afterClosed().subscribe(result => {
           this.serviceForm.patchValue({fachbereich: result.id});
       });
    }
    submit() {
       this.submitting = true;
       this.http.setFachbereich(
               this._actualId, 
               this.serviceForm.get('fachbereich').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               this.dialogRef.close(ok);
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
