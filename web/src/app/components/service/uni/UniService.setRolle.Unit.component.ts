import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UniServiceHttpService } from '../../../service/functions/http/uniService.http.service';
import { PersonRolle } from '../../../domain/uni/personRolle';
import { ViewContainerRef } from '@angular/core';
import { PersonRolleListComponent } from '../../domain/uni/personRolle.list.component';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';
import { Person } from '../../../domain/uni/person';

@Component({
    templateUrl: './UniService.setRolle.Unit.component.html',
})
export class UniServiceSetRolleUnitComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<UniServiceSetRolleUnitComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<UniServiceSetRolleUnitComponent>, builder:FormBuilder
            , public http: UniServiceHttpService, public dialog: MatDialog, public viewContainerRef: ViewContainerRef) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            rolle: [{}, Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    selectRolle() {
       let config = new MatDialogConfig();
       config.viewContainerRef = this.viewContainerRef;
       const ref = this.dialog.open(PersonRolleListComponent, config);
       ref.afterClosed().subscribe(result => {
           this.serviceForm.patchValue({rolle: result.id});
       });
    }
    submit() {
       this.submitting = true;
       this.http.setRolle(
               this._actualId, 
               this.serviceForm.get('rolle').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               this.dialogRef.close(ok);
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
