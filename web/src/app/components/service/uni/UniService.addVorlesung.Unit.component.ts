import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UniServiceHttpService } from '../../../service/functions/http/uniService.http.service';
import { VorlesungListComponent } from '../../domain/uni/vorlesung.list.component';
import { ViewContainerRef } from '@angular/core';
import { Student } from '../../../domain/uni/student';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';
import { Vorlesung } from '../../../domain/uni/vorlesung';

@Component({
    templateUrl: './UniService.addVorlesung.Unit.component.html',
})
export class UniServiceAddVorlesungUnitComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<UniServiceAddVorlesungUnitComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<UniServiceAddVorlesungUnitComponent>, builder:FormBuilder
            , public http: UniServiceHttpService, public dialog: MatDialog, public viewContainerRef: ViewContainerRef) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            vorlesung: [{}, Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    selectVorlesung() {
       let config = new MatDialogConfig();
       config.viewContainerRef = this.viewContainerRef;
       const ref = this.dialog.open(VorlesungListComponent, config);
       ref.afterClosed().subscribe(result => {
           this.serviceForm.patchValue({vorlesung: result.id});
       });
    }
    submit() {
       this.submitting = true;
       this.http.addVorlesung(
               this._actualId, 
               this.serviceForm.get('vorlesung').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               this.dialogRef.close(ok);
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
