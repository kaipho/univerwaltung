import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UniServiceHttpService } from '../../../service/functions/http/uniService.http.service';
import { Raum } from '../../../domain/uni/raum';
import { ViewContainerRef } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';
import { Vorlesung } from '../../../domain/uni/vorlesung';
import { RaumListComponent } from '../../domain/uni/raum.list.component';

@Component({
    templateUrl: './UniService.setRaum.Unit.component.html',
})
export class UniServiceSetRaumUnitComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<UniServiceSetRaumUnitComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<UniServiceSetRaumUnitComponent>, builder:FormBuilder
            , public http: UniServiceHttpService, public dialog: MatDialog, public viewContainerRef: ViewContainerRef) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            raum: [{}, Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    selectRaum() {
       let config = new MatDialogConfig();
       config.viewContainerRef = this.viewContainerRef;
       const ref = this.dialog.open(RaumListComponent, config);
       ref.afterClosed().subscribe(result => {
           this.serviceForm.patchValue({raum: result.id});
       });
    }
    submit() {
       this.submitting = true;
       this.http.setRaum(
               this._actualId, 
               this.serviceForm.get('raum').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               this.dialogRef.close(ok);
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
