import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UniServiceHttpService } from '../../../service/functions/http/uniService.http.service';
import { ViewContainerRef } from '@angular/core';
import { FachbereichListComponent } from '../../domain/uni/fachbereich.list.component';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';
import { Vorlesung } from '../../../domain/uni/vorlesung';
import { Fachbereich } from '../../../domain/uni/fachbereich';

@Component({
    templateUrl: './UniService.setFachbereichVorlesung.Unit.component.html',
})
export class UniServiceSetFachbereichVorlesungUnitComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<UniServiceSetFachbereichVorlesungUnitComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<UniServiceSetFachbereichVorlesungUnitComponent>, builder:FormBuilder
            , public http: UniServiceHttpService, public dialog: MatDialog, public viewContainerRef: ViewContainerRef) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            fachbereich: [{}, Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    selectFachbereich() {
       let config = new MatDialogConfig();
       config.viewContainerRef = this.viewContainerRef;
       const ref = this.dialog.open(FachbereichListComponent, config);
       ref.afterClosed().subscribe(result => {
           this.serviceForm.patchValue({fachbereich: result.id});
       });
    }
    submit() {
       this.submitting = true;
       this.http.setFachbereichVorlesung(
               this._actualId, 
               this.serviceForm.get('fachbereich').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               this.dialogRef.close(ok);
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
