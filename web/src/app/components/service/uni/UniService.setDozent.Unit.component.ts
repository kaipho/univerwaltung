import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UniServiceHttpService } from '../../../service/functions/http/uniService.http.service';
import { Dozent } from '../../../domain/uni/dozent';
import { ViewContainerRef } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';
import { Vorlesung } from '../../../domain/uni/vorlesung';
import { DozentListComponent } from '../../domain/uni/dozent.list.component';

@Component({
    templateUrl: './UniService.setDozent.Unit.component.html',
})
export class UniServiceSetDozentUnitComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<UniServiceSetDozentUnitComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<UniServiceSetDozentUnitComponent>, builder:FormBuilder
            , public http: UniServiceHttpService, public dialog: MatDialog, public viewContainerRef: ViewContainerRef) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            dozent: [{}, Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        this.dialogRef.close(this._result);
    }

    selectDozent() {
       let config = new MatDialogConfig();
       config.viewContainerRef = this.viewContainerRef;
       const ref = this.dialog.open(DozentListComponent, config);
       ref.afterClosed().subscribe(result => {
           this.serviceForm.patchValue({dozent: result.id});
       });
    }
    submit() {
       this.submitting = true;
       this.http.setDozent(
               this._actualId, 
               this.serviceForm.get('dozent').value,
       ).subscribe(
           (ok) => {
               this.submitting = false;
               this.dialogRef.close(ok);
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
