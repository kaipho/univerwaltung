import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TransactionServiceHttpService } from '../../../service/functions/http/transactionService.http.service';
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';

@Component({
    templateUrl: './TransactionService.transactionStatistics.Collection.component.html',
})
export class TransactionServiceTransactionStatisticsCollectionComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<TransactionServiceTransactionStatisticsCollectionComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<TransactionServiceTransactionStatisticsCollectionComponent>, builder:FormBuilder
            , public http: TransactionServiceHttpService) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
        })
    }

    ngOnInit() {
        this.submit()
    }

    close() {
        this.dialogRef.close(this._result);
    }

    submit() {
       this.submitting = true;
       this.http.transactionStatistics(
       ).subscribe(
           (ok) => {
               this.submitting = false;
               if (this.closeSilent) this.dialogRef.close(ok);
               this.serviceForm.patchValue({result: ok});
               this._result = ok;
           },
           (error) => {
               this.submitting = false;
               this._error = error.body.message;
           }
       );
    }

    private _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
