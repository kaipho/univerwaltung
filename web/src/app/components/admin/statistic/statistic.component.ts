import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {MatSnackBar} from "@angular/material";
import {StatistikVM} from "../../../domain/db/statistikVM";
import {ModelServiceHttpService} from "../../../service/functions/http/modelService.http.service";

@Component({
    selector: 'app-transactions',
    templateUrl: './statistic.component.html',
    styleUrls: ['./statistic.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class StatisticComponent implements OnInit {
    data: Array<StatistikVM> = [];

    constructor(public html: ModelServiceHttpService,
                public snackBar: MatSnackBar) {

    }

    blueScheme: any = {
        domain: [
            '#0D47A1',
            '#1976D2',
            '#039BE5',
            '#29B6F6',
            '#81D4FA',
            '#B2EBF2'],
    };

    view: any[] = [850, 500];

    ngOnInit(): void {
        this.html.modelStatistics().subscribe(
            result => this.data = result,
            error => {
                this.snackBar.open(error)
            }
        );
    }
}
