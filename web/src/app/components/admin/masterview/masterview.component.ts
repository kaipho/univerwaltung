import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { CategoryServiceHttpService } from "../../../service/functions/http/categoryService.http.service";
import { CategoryClassVM } from "../../../domain/core/categoryClassVM";
import { MatDialog, MatSnackBar } from "@angular/material";
import {CategoryServiceAddValueToUnitComponent} from "../../service/core/CategoryService.addValueTo.Unit.component";

@Component({
    selector: 'app-masterview',
    templateUrl: './masterview.component.html',
    styleUrls: ['./masterview.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MasterviewComponent implements OnInit {
    classes: Array<CategoryClassVM> = [];

    constructor(public html: CategoryServiceHttpService,
                public snackBar: MatSnackBar,
                public dialog: MatDialog) {

    }

    ngOnInit(): void {
        this.html.getAllCategoryClasses().subscribe(
            result => {
                if(this.classes.length != result.length) {
                    this.classes = result
                } else {
                    result.forEach( (actual, index)=> {
                        this.classes[index].values = result[index].values;
                    });
                }
            },
            error => {
                this.snackBar.open(error)
            }
        );
    }

    removeElement(clazz: number, category: string) {
        this.html.deleteValue(category, clazz).subscribe(
            ok => this.ngOnInit(),
            error => {
                this.snackBar.open(error)
            }
        );
    }

    addElement(clazz: number) {
        const dialog = this.dialog.open(CategoryServiceAddValueToUnitComponent);
        dialog.componentInstance.serviceForm.patchValue({'categoryClassId': clazz});
        dialog.afterClosed().subscribe(
            () => this.ngOnInit()
        );
    }
}
