import {Component, Input} from "@angular/core";
import {LoginService} from "../../../service/core/login.service";

@Component({
  selector: 'gen-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent {
  @Input()
  entries: Array<SidenavEntry> = [];

  @Input()
  title: string;

  toggleDiv: boolean = false;

  filteredEntries: Array<SidenavEntry> = [];

  constructor(public loginService: LoginService) {
    this.filteredEntries = this.entries.filter(actual => {
      this.loginService.hasRole(actual.roles)
    });
    loginService.userChange.subscribe(
        user => {
          this.filteredEntries = this.entries.filter(actual => {
            return actual.roles == null || this.loginService.hasRole(actual.roles);
          })
        }
    );
  }

  toggle(): void {
    this.toggleDiv = !this.toggleDiv;
  }
}

export interface SidenavEntry {
  name: string;
  icon: string;
  link: (string | number)[];
  roles?: string[];
}
