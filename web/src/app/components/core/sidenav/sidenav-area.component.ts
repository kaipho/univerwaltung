import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import {SidenavEntry} from "./sidenav.component";
import {Http} from "@angular/http";
import {HttpService} from "../../../service/core/http.service";

@Component({
    selector: 'gen-sidenav-area',
    templateUrl: './sidenav-area.component.html',
    styleUrls: ['./sidenav-area.component.scss']
})
export class SidenavAreaComponent {
    entries : { [key:string]:Array<SidenavEntry>; } = {};

    @Output()
    close: EventEmitter<any> = new EventEmitter();

    constructor(http: HttpService) {
        http.get("./assets/ui/sidenav.json").subscribe(
            ok => this.entries = ok.body
        )
    }

    keys() : Array<string> {
        const keys =  Object.keys(this.entries);
        return keys;
    }
}
