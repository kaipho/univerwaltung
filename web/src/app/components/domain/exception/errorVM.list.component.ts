import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { ListComponent } from '../../framework/title/list.component';
import { TitleService } from '../../../service/core/title.service';
import { ErrorVMHttpService } from '../../../service/domain/http/errorVM.http.service';
import { ErrorVM } from '../../../domain/exception/errorVM';

@Component({
    templateUrl: './errorVM.list.component.html'
})
export class ErrorVMListComponent extends ListComponent<ErrorVM> implements OnInit {

    constructor(titleService: TitleService,
        http: ErrorVMHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<ErrorVMListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
