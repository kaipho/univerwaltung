import { SaveGuard } from "../../../guard/save.guard";
import { ErrorVMComponent } from './errorVM.component';
import { ErrorVMListComponent } from './errorVM.list.component';
import { ErrorVMDetailComponent } from './errorVM.detail.component';

export const ErrorVMDomainRoutes = [
	{
	    path: 'domainr/6/:id',
	    component: ErrorVMDetailComponent,
	    children: [
	        {path: '', component: ErrorVMComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/6',
		component: ErrorVMListComponent,
	}
];
