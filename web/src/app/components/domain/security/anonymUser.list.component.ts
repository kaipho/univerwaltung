import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { AnonymUser } from '../../../domain/security/anonymUser';
import { ListComponent } from '../../framework/title/list.component';
import { AnonymUserHttpService } from '../../../service/domain/http/anonymUser.http.service';
import { TitleService } from '../../../service/core/title.service';

@Component({
    templateUrl: './anonymUser.list.component.html'
})
export class AnonymUserListComponent extends ListComponent<AnonymUser> implements OnInit {

    constructor(titleService: TitleService,
        http: AnonymUserHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<AnonymUserListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
