import { SaveGuard } from "../../../guard/save.guard";
import { TechnicalUserComponent } from './technicalUser.component';
import { NotificationComponent } from '../core/notification.component';
import { PersonDetailComponent } from '../uni/person.detail.component';
import { AnonymUserComponent } from './anonymUser.component';
import { UserListComponent } from './user.list.component';
import { AnonymUserDetailComponent } from './anonymUser.detail.component';
import { PersonComponent } from '../uni/person.component';
import { TechnicalUserDetailComponent } from './technicalUser.detail.component';

export const UserDomainRoutes = [
	{
	    path: 'domainr/21/:id',
	    component: PersonDetailComponent,
	    children: [
	        {path: '13', component: NotificationComponent, canDeactivate: [SaveGuard]},
	        {path: '', component: PersonComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
	    path: 'domainr/11/:id',
	    component: AnonymUserDetailComponent,
	    children: [
	        {path: '13', component: NotificationComponent, canDeactivate: [SaveGuard]},
	        {path: '', component: AnonymUserComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
	    path: 'domainr/35/:id',
	    component: TechnicalUserDetailComponent,
	    children: [
	        {path: '13', component: NotificationComponent, canDeactivate: [SaveGuard]},
	        {path: '', component: TechnicalUserComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/9',
		component: UserListComponent,
	}
];
