import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { User } from '../../../domain/security/user';
import { ListComponent } from '../../framework/title/list.component';
import { UserHttpService } from '../../../service/domain/http/user.http.service';
import { TitleService } from '../../../service/core/title.service';

@Component({
    templateUrl: './user.list.component.html'
})
export class UserListComponent extends ListComponent<User> implements OnInit {

    constructor(titleService: TitleService,
        http: UserHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<UserListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
