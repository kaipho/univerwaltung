import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { ListComponent } from '../../framework/title/list.component';
import { LoginVM } from '../../../domain/security/loginVM';
import { TitleService } from '../../../service/core/title.service';
import { LoginVMHttpService } from '../../../service/domain/http/loginVM.http.service';

@Component({
    templateUrl: './loginVM.list.component.html'
})
export class LoginVMListComponent extends ListComponent<LoginVM> implements OnInit {

    constructor(titleService: TitleService,
        http: LoginVMHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<LoginVMListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
