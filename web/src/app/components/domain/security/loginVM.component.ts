import { Component, OnInit } from "@angular/core"
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { FormService } from "../../framework/form/form.service";

import { MenuService } from '../../framework/gen-service/menu.service';
import { TitleComponent } from '../../framework/title/title.component';
import { LoginVM } from '../../../domain/security/loginVM';
import { TypeVisitor } from '../../../domain/type.visitor';
import { TitleService } from '../../../service/core/title.service';
import { LoginVMHttpService } from '../../../service/domain/http/loginVM.http.service';

@Component({
	templateUrl: './loginVM.component.html',
})
export class LoginVMComponent extends TitleComponent implements OnInit {
	_http:LoginVMHttpService;
	_router:Router;
	

	obj:LoginVM = new LoginVM();
	crudForm: FormGroup;

	constructor(titleService:TitleService, http:LoginVMHttpService, builder:FormBuilder, public menu: MenuService,
		router:Router, public activeRoute: ActivatedRoute, private formService: FormService) {
		super(titleService);
		this._http = http;
		this._router = router;
		

		this.crudForm = builder.group({
		    
		    token: [''],
		    user: [''],
        });
	}

	ngOnInit() {
	    this.activeRoute.params.subscribe( data =>
            this._http.get(this._router.url.replace("/web/", "/api/")).subscribe(result => {
				this.obj = result;
                this.crudForm.reset(result);
                this.titleService.addTitle(result._rep.split("|")[0], 1);

                this.formService.form = this.crudForm;
                this.formService.component = this;
                this.formService.url = result['_self'];

                
                this.menu.addEntries(this, result.id, result,
                );
            })
        );
	}

	
}
