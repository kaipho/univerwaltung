import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { ListComponent } from '../../framework/title/list.component';
import { TitleService } from '../../../service/core/title.service';
import { TechnicalUser } from '../../../domain/security/technicalUser';
import { TechnicalUserHttpService } from '../../../service/domain/http/technicalUser.http.service';

@Component({
    templateUrl: './technicalUser.list.component.html'
})
export class TechnicalUserListComponent extends ListComponent<TechnicalUser> implements OnInit {

    constructor(titleService: TitleService,
        http: TechnicalUserHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<TechnicalUserListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
