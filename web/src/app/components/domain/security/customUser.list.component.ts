import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { CustomUserHttpService } from '../../../service/domain/http/customUser.http.service';
import { CustomUser } from '../../../domain/security/customUser';
import { ListComponent } from '../../framework/title/list.component';
import { TitleService } from '../../../service/core/title.service';

@Component({
    templateUrl: './customUser.list.component.html'
})
export class CustomUserListComponent extends ListComponent<CustomUser> implements OnInit {

    constructor(titleService: TitleService,
        http: CustomUserHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<CustomUserListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
