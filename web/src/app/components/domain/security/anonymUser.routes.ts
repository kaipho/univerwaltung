import { SaveGuard } from "../../../guard/save.guard";
import { NotificationComponent } from '../core/notification.component';
import { AnonymUserListComponent } from './anonymUser.list.component';
import { AnonymUserComponent } from './anonymUser.component';
import { AnonymUserDetailComponent } from './anonymUser.detail.component';

export const AnonymUserDomainRoutes = [
	{
	    path: 'domainr/11/:id',
	    component: AnonymUserDetailComponent,
	    children: [
	        {path: '13', component: NotificationComponent, canDeactivate: [SaveGuard]},
	        {path: '', component: AnonymUserComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/11',
		component: AnonymUserListComponent,
	}
];
