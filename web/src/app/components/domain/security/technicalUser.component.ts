import { Component, OnInit } from "@angular/core"
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { FormService } from "../../framework/form/form.service";

import { MenuService } from '../../framework/gen-service/menu.service';
import { Notification } from '../../../domain/core/notification';
import { TitleComponent } from '../../framework/title/title.component';
import { NotificationServiceAddNotificationUnitComponent } from '../../service/core/NotificationService.addNotification.Unit.component';
import { UserServiceResetPasswordStringComponent } from '../../service/security/UserService.resetPassword.String.component';
import { TypeVisitor } from '../../../domain/type.visitor';
import { TitleService } from '../../../service/core/title.service';
import { TechnicalUser } from '../../../domain/security/technicalUser';
import { NotificationHttpService } from '../../../service/domain/http/notification.http.service';
import { TechnicalUserHttpService } from '../../../service/domain/http/technicalUser.http.service';

@Component({
	templateUrl: './technicalUser.component.html',
})
export class TechnicalUserComponent extends TitleComponent implements OnInit {
	_http:TechnicalUserHttpService;
	_router:Router;
	

	obj:TechnicalUser = new TechnicalUser();
	crudForm: FormGroup;

	constructor(titleService:TitleService, http:TechnicalUserHttpService, builder:FormBuilder, public menu: MenuService,
		router:Router, public activeRoute: ActivatedRoute, private formService: FormService) {
		super(titleService);
		this._http = http;
		this._router = router;
		

		this.crudForm = builder.group({
		    
		    username: [''],
		    password: [''],
		    lang: [''],
		    roles: [''],
		    notifications: [''],
		    description: [''],
        });
	}

	ngOnInit() {
	    this.activeRoute.params.subscribe( data =>
            this._http.get(this._router.url.replace("/web/", "/api/")).subscribe(result => {
				this.obj = result;
                this.crudForm.reset(result);
                this.titleService.addTitle(result._rep.split("|")[0], 1);

                this.formService.form = this.crudForm;
                this.formService.component = this;
                this.formService.url = result['_self'];

                
                this.menu.addEntries(this, result.id, result,
                  { name: 'functions.security.resetPassword.title', dialog: UserServiceResetPasswordStringComponent},
                  { name: 'functions.core.addNotification.title', dialog: NotificationServiceAddNotificationUnitComponent},
                );
            })
        );
	}

	
}
