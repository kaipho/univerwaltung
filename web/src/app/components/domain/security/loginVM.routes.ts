import { SaveGuard } from "../../../guard/save.guard";
import { LoginVMComponent } from './loginVM.component';
import { LoginVMListComponent } from './loginVM.list.component';
import { LoginVMDetailComponent } from './loginVM.detail.component';

export const LoginVMDomainRoutes = [
	{
	    path: 'domainr/12/:id',
	    component: LoginVMDetailComponent,
	    children: [
	        {path: '', component: LoginVMComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/12',
		component: LoginVMListComponent,
	}
];
