import { SaveGuard } from "../../../guard/save.guard";
import { NotificationComponent } from '../core/notification.component';
import { PersonDetailComponent } from '../uni/person.detail.component';
import { CustomUserListComponent } from './customUser.list.component';
import { PersonComponent } from '../uni/person.component';

export const CustomUserDomainRoutes = [
	{
	    path: 'domainr/21/:id',
	    component: PersonDetailComponent,
	    children: [
	        {path: '13', component: NotificationComponent, canDeactivate: [SaveGuard]},
	        {path: '', component: PersonComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/10',
		component: CustomUserListComponent,
	}
];
