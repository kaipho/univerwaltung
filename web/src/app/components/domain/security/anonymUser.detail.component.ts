import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { FormService } from "../../framework/form/form.service";
import { Location } from '@angular/common';
import {DetailComponent} from "../../framework/title/detail.component";

import { LoginService } from '../../../service/core/login.service';
import { TitleComponent } from '../../framework/title/title.component';
import { AnonymUser } from '../../../domain/security/anonymUser';
import { HttpService } from '../../../service/core/http.service';
import { AnonymUserHttpService } from '../../../service/domain/http/anonymUser.http.service';
import { TitleService } from '../../../service/core/title.service';

@Component({
    templateUrl: './anonymUser.detail.component.html'
})
export class AnonymUserDetailComponent extends DetailComponent {
    tabs = [
        {route: './', title: 'title.security.anonymUser', name: 'anonymUser', realname: 'anonymUser', area: '.security'},
    ];

    constructor(router: Router,
                route: ActivatedRoute,
                public http: AnonymUserHttpService,
                login: LoginService,
                httpService: HttpService,
                formService: FormService,
                location: Location,
                titleService: TitleService) {
        super(router, route, login, httpService, formService, location, titleService);
    }

    doNav(to) {
        this.router.navigate([this.tabs[to.index].route], {relativeTo: this.route});
    }
}
