import { SaveGuard } from "../../../guard/save.guard";
import { TechnicalUserListComponent } from './technicalUser.list.component';
import { TechnicalUserComponent } from './technicalUser.component';
import { NotificationComponent } from '../core/notification.component';
import { TechnicalUserDetailComponent } from './technicalUser.detail.component';

export const TechnicalUserDomainRoutes = [
	{
	    path: 'domainr/35/:id',
	    component: TechnicalUserDetailComponent,
	    children: [
	        {path: '13', component: NotificationComponent, canDeactivate: [SaveGuard]},
	        {path: '', component: TechnicalUserComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/35',
		component: TechnicalUserListComponent,
	}
];
