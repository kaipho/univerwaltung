import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { StatistikVM } from '../../../domain/db/statistikVM';
import { ListComponent } from '../../framework/title/list.component';
import { StatistikVMHttpService } from '../../../service/domain/http/statistikVM.http.service';
import { TitleService } from '../../../service/core/title.service';

@Component({
    templateUrl: './statistikVM.list.component.html'
})
export class StatistikVMListComponent extends ListComponent<StatistikVM> implements OnInit {

    constructor(titleService: TitleService,
        http: StatistikVMHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<StatistikVMListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
