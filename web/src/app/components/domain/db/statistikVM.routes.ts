import { SaveGuard } from "../../../guard/save.guard";
import { StatistikVMComponent } from './statistikVM.component';
import { StatistikVMListComponent } from './statistikVM.list.component';
import { StatistikVMDetailComponent } from './statistikVM.detail.component';

export const StatistikVMDomainRoutes = [
	{
	    path: 'domainr/31/:id',
	    component: StatistikVMDetailComponent,
	    children: [
	        {path: '', component: StatistikVMComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/31',
		component: StatistikVMListComponent,
	}
];
