import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { FormService } from "../../framework/form/form.service";
import { Location } from '@angular/common';
import {DetailComponent} from "../../framework/title/detail.component";

import { LoginService } from '../../../service/core/login.service';
import { Notification } from '../../../domain/core/notification';
import { TitleComponent } from '../../framework/title/title.component';
import { HttpService } from '../../../service/core/http.service';
import { TitleService } from '../../../service/core/title.service';
import { NotificationHttpService } from '../../../service/domain/http/notification.http.service';

@Component({
    templateUrl: './notification.detail.component.html'
})
export class NotificationDetailComponent extends DetailComponent {
    tabs = [
        {route: './', title: 'title.core.notification', name: 'notification', realname: 'notification', area: '.core'},
    ];

    constructor(router: Router,
                route: ActivatedRoute,
                public http: NotificationHttpService,
                login: LoginService,
                httpService: HttpService,
                formService: FormService,
                location: Location,
                titleService: TitleService) {
        super(router, route, login, httpService, formService, location, titleService);
    }

    doNav(to) {
        this.router.navigate([this.tabs[to.index].route], {relativeTo: this.route});
    }
}
