import { SaveGuard } from "../../../guard/save.guard";
import { CategoryClassVMListComponent } from './categoryClassVM.list.component';
import { CategoryClassVMDetailComponent } from './categoryClassVM.detail.component';
import { CategoryVMComponent } from './categoryVM.component';
import { CategoryClassVMComponent } from './categoryClassVM.component';

export const CategoryClassVMDomainRoutes = [
	{
	    path: 'domainr/7/:id',
	    component: CategoryClassVMDetailComponent,
	    children: [
	        {path: '', component: CategoryClassVMComponent, canDeactivate: [SaveGuard]},
	        {path: '5', component: CategoryVMComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/7',
		component: CategoryClassVMListComponent,
	}
];
