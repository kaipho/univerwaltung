import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { ListComponent } from '../../framework/title/list.component';
import { CategoryClassVMHttpService } from '../../../service/domain/http/categoryClassVM.http.service';
import { TitleService } from '../../../service/core/title.service';
import { CategoryClassVM } from '../../../domain/core/categoryClassVM';

@Component({
    templateUrl: './categoryClassVM.list.component.html'
})
export class CategoryClassVMListComponent extends ListComponent<CategoryClassVM> implements OnInit {

    constructor(titleService: TitleService,
        http: CategoryClassVMHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<CategoryClassVMListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
