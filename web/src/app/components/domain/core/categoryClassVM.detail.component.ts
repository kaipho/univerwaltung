import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { FormService } from "../../framework/form/form.service";
import { Location } from '@angular/common';
import {DetailComponent} from "../../framework/title/detail.component";

import { LoginService } from '../../../service/core/login.service';
import { TitleComponent } from '../../framework/title/title.component';
import { CategoryClassVMHttpService } from '../../../service/domain/http/categoryClassVM.http.service';
import { HttpService } from '../../../service/core/http.service';
import { TitleService } from '../../../service/core/title.service';
import { CategoryClassVM } from '../../../domain/core/categoryClassVM';

@Component({
    templateUrl: './categoryClassVM.detail.component.html'
})
export class CategoryClassVMDetailComponent extends DetailComponent {
    tabs = [
        {route: './', title: 'title.core.categoryClassVM', name: 'categoryClassVM', realname: 'categoryClassVM', area: '.core'},
    ];

    constructor(router: Router,
                route: ActivatedRoute,
                public http: CategoryClassVMHttpService,
                login: LoginService,
                httpService: HttpService,
                formService: FormService,
                location: Location,
                titleService: TitleService) {
        super(router, route, login, httpService, formService, location, titleService);
    }

    doNav(to) {
        this.router.navigate([this.tabs[to.index].route], {relativeTo: this.route});
    }
}
