import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { FormService } from "../../framework/form/form.service";
import { Location } from '@angular/common';
import {DetailComponent} from "../../framework/title/detail.component";

import { LoginService } from '../../../service/core/login.service';
import { TitleComponent } from '../../framework/title/title.component';
import { HttpService } from '../../../service/core/http.service';
import { TitleService } from '../../../service/core/title.service';
import { CategoryVMHttpService } from '../../../service/domain/http/categoryVM.http.service';
import { CategoryVM } from '../../../domain/core/categoryVM';

@Component({
    templateUrl: './categoryVM.detail.component.html'
})
export class CategoryVMDetailComponent extends DetailComponent {
    tabs = [
        {route: './', title: 'title.core.categoryVM', name: 'categoryVM', realname: 'categoryVM', area: '.core'},
    ];

    constructor(router: Router,
                route: ActivatedRoute,
                public http: CategoryVMHttpService,
                login: LoginService,
                httpService: HttpService,
                formService: FormService,
                location: Location,
                titleService: TitleService) {
        super(router, route, login, httpService, formService, location, titleService);
    }

    doNav(to) {
        this.router.navigate([this.tabs[to.index].route], {relativeTo: this.route});
    }
}
