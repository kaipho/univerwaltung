import { SaveGuard } from "../../../guard/save.guard";
import { CategoryVMComponent } from './categoryVM.component';
import { CategoryVMListComponent } from './categoryVM.list.component';
import { CategoryVMDetailComponent } from './categoryVM.detail.component';

export const CategoryVMDomainRoutes = [
	{
	    path: 'domainr/8/:id',
	    component: CategoryVMDetailComponent,
	    children: [
	        {path: '', component: CategoryVMComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/8',
		component: CategoryVMListComponent,
	}
];
