import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { Notification } from '../../../domain/core/notification';
import { ListComponent } from '../../framework/title/list.component';
import { TitleService } from '../../../service/core/title.service';
import { NotificationHttpService } from '../../../service/domain/http/notification.http.service';

@Component({
    templateUrl: './notification.list.component.html'
})
export class NotificationListComponent extends ListComponent<Notification> implements OnInit {

    constructor(titleService: TitleService,
        http: NotificationHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<NotificationListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
