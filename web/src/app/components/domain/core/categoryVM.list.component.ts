import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { ListComponent } from '../../framework/title/list.component';
import { TitleService } from '../../../service/core/title.service';
import { CategoryVMHttpService } from '../../../service/domain/http/categoryVM.http.service';
import { CategoryVM } from '../../../domain/core/categoryVM';

@Component({
    templateUrl: './categoryVM.list.component.html'
})
export class CategoryVMListComponent extends ListComponent<CategoryVM> implements OnInit {

    constructor(titleService: TitleService,
        http: CategoryVMHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<CategoryVMListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
