import { SaveGuard } from "../../../guard/save.guard";
import { NotificationListComponent } from './notification.list.component';
import { NotificationDetailComponent } from './notification.detail.component';
import { NotificationComponent } from './notification.component';

export const NotificationDomainRoutes = [
	{
	    path: 'domainr/13/:id',
	    component: NotificationDetailComponent,
	    children: [
	        {path: '', component: NotificationComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/13',
		component: NotificationListComponent,
	}
];
