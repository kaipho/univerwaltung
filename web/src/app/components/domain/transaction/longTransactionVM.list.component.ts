import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { LongTransactionVM } from '../../../domain/transaction/longTransactionVM';
import { ListComponent } from '../../framework/title/list.component';
import { LongTransactionVMHttpService } from '../../../service/domain/http/longTransactionVM.http.service';
import { TitleService } from '../../../service/core/title.service';

@Component({
    templateUrl: './longTransactionVM.list.component.html'
})
export class LongTransactionVMListComponent extends ListComponent<LongTransactionVM> implements OnInit {

    constructor(titleService: TitleService,
        http: LongTransactionVMHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<LongTransactionVMListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
