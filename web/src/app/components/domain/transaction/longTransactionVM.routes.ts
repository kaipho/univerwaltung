import { SaveGuard } from "../../../guard/save.guard";
import { LongTransactionVMComponent } from './longTransactionVM.component';
import { LongTransactionVMDetailComponent } from './longTransactionVM.detail.component';
import { LongTransactionVMListComponent } from './longTransactionVM.list.component';

export const LongTransactionVMDomainRoutes = [
	{
	    path: 'domainr/29/:id',
	    component: LongTransactionVMDetailComponent,
	    children: [
	        {path: '', component: LongTransactionVMComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/29',
		component: LongTransactionVMListComponent,
	}
];
