import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { FormService } from "../../framework/form/form.service";
import { Location } from '@angular/common';
import {DetailComponent} from "../../framework/title/detail.component";

import { LoginService } from '../../../service/core/login.service';
import { TitleComponent } from '../../framework/title/title.component';
import { LongTransactionVM } from '../../../domain/transaction/longTransactionVM';
import { HttpService } from '../../../service/core/http.service';
import { LongTransactionVMHttpService } from '../../../service/domain/http/longTransactionVM.http.service';
import { TitleService } from '../../../service/core/title.service';

@Component({
    templateUrl: './longTransactionVM.detail.component.html'
})
export class LongTransactionVMDetailComponent extends DetailComponent {
    tabs = [
        {route: './', title: 'title.transaction.longTransactionVM', name: 'longTransactionVM', realname: 'longTransactionVM', area: '.transaction'},
    ];

    constructor(router: Router,
                route: ActivatedRoute,
                public http: LongTransactionVMHttpService,
                login: LoginService,
                httpService: HttpService,
                formService: FormService,
                location: Location,
                titleService: TitleService) {
        super(router, route, login, httpService, formService, location, titleService);
    }

    doNav(to) {
        this.router.navigate([this.tabs[to.index].route], {relativeTo: this.route});
    }
}
