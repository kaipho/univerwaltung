import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { FormService } from "../../framework/form/form.service";
import { Location } from '@angular/common';
import {DetailComponent} from "../../framework/title/detail.component";

import { LoginService } from '../../../service/core/login.service';
import { TitleComponent } from '../../framework/title/title.component';
import { VorlesungHttpService } from '../../../service/domain/http/vorlesung.http.service';
import { HttpService } from '../../../service/core/http.service';
import { TitleService } from '../../../service/core/title.service';
import { Vorlesung } from '../../../domain/uni/vorlesung';

@Component({
    templateUrl: './vorlesung.detail.component.html'
})
export class VorlesungDetailComponent extends DetailComponent {
    tabs = [
        {route: './', title: 'title.uni.vorlesung', name: 'vorlesung', realname: 'vorlesung', area: '.uni'},
    ];

    constructor(router: Router,
                route: ActivatedRoute,
                public http: VorlesungHttpService,
                login: LoginService,
                httpService: HttpService,
                formService: FormService,
                location: Location,
                titleService: TitleService) {
        super(router, route, login, httpService, formService, location, titleService);
    }

    doNav(to) {
        this.router.navigate([this.tabs[to.index].route], {relativeTo: this.route});
    }
}
