import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { ListComponent } from '../../framework/title/list.component';
import { TitleService } from '../../../service/core/title.service';
import { PersonHttpService } from '../../../service/domain/http/person.http.service';
import { Person } from '../../../domain/uni/person';

@Component({
    templateUrl: './person.list.component.html'
})
export class PersonListComponent extends ListComponent<Person> implements OnInit {

    constructor(titleService: TitleService,
        http: PersonHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<PersonListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
