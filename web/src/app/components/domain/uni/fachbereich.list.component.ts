import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { FachbereichHttpService } from '../../../service/domain/http/fachbereich.http.service';
import { ListComponent } from '../../framework/title/list.component';
import { TitleService } from '../../../service/core/title.service';
import { Fachbereich } from '../../../domain/uni/fachbereich';

@Component({
    templateUrl: './fachbereich.list.component.html'
})
export class FachbereichListComponent extends ListComponent<Fachbereich> implements OnInit {

    constructor(titleService: TitleService,
        http: FachbereichHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<FachbereichListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
