import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { Raum } from '../../../domain/uni/raum';
import { ListComponent } from '../../framework/title/list.component';
import { TitleService } from '../../../service/core/title.service';
import { RaumHttpService } from '../../../service/domain/http/raum.http.service';

@Component({
    templateUrl: './raum.list.component.html'
})
export class RaumListComponent extends ListComponent<Raum> implements OnInit {

    constructor(titleService: TitleService,
        http: RaumHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<RaumListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
