import { SaveGuard } from "../../../guard/save.guard";
import { StudentListComponent } from './student.list.component';
import { StudentComponent } from './student.component';
import { StudentDetailComponent } from './student.detail.component';
import { VorlesungComponent } from './vorlesung.component';

export const StudentDomainRoutes = [
	{
	    path: 'domainr/16/:id',
	    component: StudentDetailComponent,
	    children: [
	        {path: '', component: StudentComponent, canDeactivate: [SaveGuard]},
	        {path: '28', component: VorlesungComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/16',
		component: StudentListComponent,
	}
];
