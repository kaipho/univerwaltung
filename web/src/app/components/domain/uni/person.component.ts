import { Component, OnInit } from "@angular/core"
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { FormService } from "../../framework/form/form.service";

import { MenuService } from '../../framework/gen-service/menu.service';
import { Notification } from '../../../domain/core/notification';
import { TitleComponent } from '../../framework/title/title.component';
import { NotificationServiceAddNotificationUnitComponent } from '../../service/core/NotificationService.addNotification.Unit.component';
import { UserServiceResetPasswordStringComponent } from '../../service/security/UserService.resetPassword.String.component';
import { TypeVisitor } from '../../../domain/type.visitor';
import { TitleService } from '../../../service/core/title.service';
import { PersonHttpService } from '../../../service/domain/http/person.http.service';
import { NotificationHttpService } from '../../../service/domain/http/notification.http.service';
import { Person } from '../../../domain/uni/person';
import { UniServiceSetRolleUnitComponent } from '../../service/uni/UniService.setRolle.Unit.component';

@Component({
	templateUrl: './person.component.html',
})
export class PersonComponent extends TitleComponent implements OnInit {
	_http:PersonHttpService;
	_router:Router;
	

	obj:Person = new Person();
	crudForm: FormGroup;

	constructor(titleService:TitleService, http:PersonHttpService, builder:FormBuilder, public menu: MenuService,
		router:Router, public activeRoute: ActivatedRoute, private formService: FormService) {
		super(titleService);
		this._http = http;
		this._router = router;
		

		this.crudForm = builder.group({
		    
		    username: [''],
		    password: [''],
		    lang: [''],
		    roles: [''],
		    notifications: [''],
		    name: [''],
		    vorname: [''],
		    rolle: [''],
        });
	}

	ngOnInit() {
	    this.activeRoute.params.subscribe( data =>
            this._http.get(this._router.url.replace("/web/", "/api/")).subscribe(result => {
				this.obj = result;
                this.crudForm.reset(result);
                this.titleService.addTitle(result._rep.split("|")[0], 1);

                this.formService.form = this.crudForm;
                this.formService.component = this;
                this.formService.url = result['_self'];

                
                this.menu.addEntries(this, result.id, result,
                  { name: 'functions.uni.setRolle.title', dialog: UniServiceSetRolleUnitComponent},
                  { name: 'functions.security.resetPassword.title', dialog: UserServiceResetPasswordStringComponent},
                  { name: 'functions.core.addNotification.title', dialog: NotificationServiceAddNotificationUnitComponent},
                );
            })
        );
	}

	
}
