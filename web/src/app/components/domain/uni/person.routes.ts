import { SaveGuard } from "../../../guard/save.guard";
import { PersonComponent } from './person.component';
import { NotificationComponent } from '../core/notification.component';
import { PersonDetailComponent } from './person.detail.component';
import { PersonListComponent } from './person.list.component';

export const PersonDomainRoutes = [
	{
	    path: 'domainr/21/:id',
	    component: PersonDetailComponent,
	    children: [
	        {path: '13', component: NotificationComponent, canDeactivate: [SaveGuard]},
	        {path: '', component: PersonComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/21',
		component: PersonListComponent,
	}
];
