import { SaveGuard } from "../../../guard/save.guard";
import { DozentDetailComponent } from './dozent.detail.component';
import { StudentComponent } from './student.component';
import { StudentDetailComponent } from './student.detail.component';
import { PersonRolleListComponent } from './personRolle.list.component';
import { VorlesungComponent } from './vorlesung.component';
import { DozentComponent } from './dozent.component';

export const PersonRolleDomainRoutes = [
	{
	    path: 'domainr/23/:id',
	    component: DozentDetailComponent,
	    children: [
	        {path: '', component: DozentComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
	    path: 'domainr/16/:id',
	    component: StudentDetailComponent,
	    children: [
	        {path: '', component: StudentComponent, canDeactivate: [SaveGuard]},
	        {path: '28', component: VorlesungComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/22',
		component: PersonRolleListComponent,
	}
];
