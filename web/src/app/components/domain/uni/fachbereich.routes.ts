import { SaveGuard } from "../../../guard/save.guard";
import { FachbereichDetailComponent } from './fachbereich.detail.component';
import { FachbereichListComponent } from './fachbereich.list.component';
import { FachbereichComponent } from './fachbereich.component';
import { VorlesungComponent } from './vorlesung.component';
import { DozentComponent } from './dozent.component';

export const FachbereichDomainRoutes = [
	{
	    path: 'domainr/24/:id',
	    component: FachbereichDetailComponent,
	    children: [
	        {path: '', component: FachbereichComponent, canDeactivate: [SaveGuard]},
	        {path: '48', component: DozentComponent, canDeactivate: [SaveGuard]},
	        {path: '49', component: VorlesungComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/24',
		component: FachbereichListComponent,
	}
];
