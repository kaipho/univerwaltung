import { SaveGuard } from "../../../guard/save.guard";
import { DozentDetailComponent } from './dozent.detail.component';
import { DozentListComponent } from './dozent.list.component';
import { DozentComponent } from './dozent.component';

export const DozentDomainRoutes = [
	{
	    path: 'domainr/23/:id',
	    component: DozentDetailComponent,
	    children: [
	        {path: '', component: DozentComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/23',
		component: DozentListComponent,
	}
];
