import { SaveGuard } from "../../../guard/save.guard";
import { RaumDetailComponent } from './raum.detail.component';
import { RaumListComponent } from './raum.list.component';
import { RaumComponent } from './raum.component';

export const RaumDomainRoutes = [
	{
	    path: 'domainr/25/:id',
	    component: RaumDetailComponent,
	    children: [
	        {path: '', component: RaumComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/25',
		component: RaumListComponent,
	}
];
