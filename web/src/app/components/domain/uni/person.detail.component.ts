import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { FormService } from "../../framework/form/form.service";
import { Location } from '@angular/common';
import {DetailComponent} from "../../framework/title/detail.component";

import { LoginService } from '../../../service/core/login.service';
import { TitleComponent } from '../../framework/title/title.component';
import { HttpService } from '../../../service/core/http.service';
import { TitleService } from '../../../service/core/title.service';
import { PersonHttpService } from '../../../service/domain/http/person.http.service';
import { Person } from '../../../domain/uni/person';

@Component({
    templateUrl: './person.detail.component.html'
})
export class PersonDetailComponent extends DetailComponent {
    tabs = [
        {route: './', title: 'title.uni.person', name: 'person', realname: 'person', area: '.uni'},
    ];

    constructor(router: Router,
                route: ActivatedRoute,
                public http: PersonHttpService,
                login: LoginService,
                httpService: HttpService,
                formService: FormService,
                location: Location,
                titleService: TitleService) {
        super(router, route, login, httpService, formService, location, titleService);
    }

    doNav(to) {
        this.router.navigate([this.tabs[to.index].route], {relativeTo: this.route});
    }
}
