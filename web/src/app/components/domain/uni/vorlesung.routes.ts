import { SaveGuard } from "../../../guard/save.guard";
import { StudentComponent } from './student.component';
import { VorlesungDetailComponent } from './vorlesung.detail.component';
import { VorlesungListComponent } from './vorlesung.list.component';
import { VorlesungComponent } from './vorlesung.component';

export const VorlesungDomainRoutes = [
	{
	    path: 'domainr/17/:id',
	    component: VorlesungDetailComponent,
	    children: [
	        {path: '', component: VorlesungComponent, canDeactivate: [SaveGuard]},
	        {path: '30', component: StudentComponent, canDeactivate: [SaveGuard]},
	    ]
	},
	{
		path: 'domainr/17',
		component: VorlesungListComponent,
	}
];
