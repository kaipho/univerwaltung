import { Component, OnInit } from "@angular/core"
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { FormService } from "../../framework/form/form.service";

import { StudentHttpService } from '../../../service/domain/http/student.http.service';
import { UniServiceAddVorlesungUnitComponent } from '../../service/uni/UniService.addVorlesung.Unit.component';
import { MenuService } from '../../framework/gen-service/menu.service';
import { TitleComponent } from '../../framework/title/title.component';
import { VorlesungHttpService } from '../../../service/domain/http/vorlesung.http.service';
import { Student } from '../../../domain/uni/student';
import { TypeVisitor } from '../../../domain/type.visitor';
import { TitleService } from '../../../service/core/title.service';
import { Vorlesung } from '../../../domain/uni/vorlesung';

@Component({
	templateUrl: './student.component.html',
})
export class StudentComponent extends TitleComponent implements OnInit {
	_http:StudentHttpService;
	_router:Router;
	

	obj:Student = new Student();
	crudForm: FormGroup;

	constructor(titleService:TitleService, http:StudentHttpService, builder:FormBuilder, public menu: MenuService,
		router:Router, public activeRoute: ActivatedRoute, private formService: FormService) {
		super(titleService);
		this._http = http;
		this._router = router;
		

		this.crudForm = builder.group({
		    
		    person: [{value:'', disabled: true}],
		    matrikelnummer: [''],
		    semester: [''],
		    besuchteVorlesungen: [{value:'', disabled: true}],
        });
	}

	ngOnInit() {
	    this.activeRoute.params.subscribe( data =>
            this._http.get(this._router.url.replace("/web/", "/api/")).subscribe(result => {
				this.obj = result;
                this.crudForm.reset(result);
                this.titleService.addTitle(result._rep.split("|")[0], 1);

                this.formService.form = this.crudForm;
                this.formService.component = this;
                this.formService.url = result['_self'];

                
                this.menu.addEntries(this, result.id, result,
                  { name: 'functions.uni.addVorlesung.title', dialog: UniServiceAddVorlesungUnitComponent},
                );
            })
        );
	}

	
}
