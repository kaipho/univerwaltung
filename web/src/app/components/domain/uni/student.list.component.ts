import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { StudentHttpService } from '../../../service/domain/http/student.http.service';
import { ListComponent } from '../../framework/title/list.component';
import { Student } from '../../../domain/uni/student';
import { TitleService } from '../../../service/core/title.service';

@Component({
    templateUrl: './student.list.component.html'
})
export class StudentListComponent extends ListComponent<Student> implements OnInit {

    constructor(titleService: TitleService,
        http: StudentHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<StudentListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
