import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { ListComponent } from '../../framework/title/list.component';
import { PersonRolle } from '../../../domain/uni/personRolle';
import { PersonRolleHttpService } from '../../../service/domain/http/personRolle.http.service';
import { TitleService } from '../../../service/core/title.service';

@Component({
    templateUrl: './personRolle.list.component.html'
})
export class PersonRolleListComponent extends ListComponent<PersonRolle> implements OnInit {

    constructor(titleService: TitleService,
        http: PersonRolleHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<PersonRolleListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
