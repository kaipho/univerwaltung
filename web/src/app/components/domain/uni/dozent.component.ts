import { Component, OnInit } from "@angular/core"
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { FormService } from "../../framework/form/form.service";

import { UniServiceSetFachbereichUnitComponent } from '../../service/uni/UniService.setFachbereich.Unit.component';
import { MenuService } from '../../framework/gen-service/menu.service';
import { TitleComponent } from '../../framework/title/title.component';
import { Dozent } from '../../../domain/uni/dozent';
import { TypeVisitor } from '../../../domain/type.visitor';
import { TitleService } from '../../../service/core/title.service';
import { DozentHttpService } from '../../../service/domain/http/dozent.http.service';

@Component({
	templateUrl: './dozent.component.html',
})
export class DozentComponent extends TitleComponent implements OnInit {
	_http:DozentHttpService;
	_router:Router;
	

	obj:Dozent = new Dozent();
	crudForm: FormGroup;

	constructor(titleService:TitleService, http:DozentHttpService, builder:FormBuilder, public menu: MenuService,
		router:Router, public activeRoute: ActivatedRoute, private formService: FormService) {
		super(titleService);
		this._http = http;
		this._router = router;
		

		this.crudForm = builder.group({
		    
		    person: [{value:'', disabled: true}],
		    titel: [''],
		    personalnummer: [''],
		    fachbereich: [''],
        });
	}

	ngOnInit() {
	    this.activeRoute.params.subscribe( data =>
            this._http.get(this._router.url.replace("/web/", "/api/")).subscribe(result => {
				this.obj = result;
                this.crudForm.reset(result);
                this.titleService.addTitle(result._rep.split("|")[0], 1);

                this.formService.form = this.crudForm;
                this.formService.component = this;
                this.formService.url = result['_self'];

                
                this.menu.addEntries(this, result.id, result,
                  { name: 'functions.uni.setFachbereich.title', dialog: UniServiceSetFachbereichUnitComponent},
                );
            })
        );
	}

	
}
