import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { FormService } from "../../framework/form/form.service";
import { Location } from '@angular/common';
import {DetailComponent} from "../../framework/title/detail.component";

import { LoginService } from '../../../service/core/login.service';
import { FachbereichHttpService } from '../../../service/domain/http/fachbereich.http.service';
import { TitleComponent } from '../../framework/title/title.component';
import { HttpService } from '../../../service/core/http.service';
import { TitleService } from '../../../service/core/title.service';
import { Fachbereich } from '../../../domain/uni/fachbereich';

@Component({
    templateUrl: './fachbereich.detail.component.html'
})
export class FachbereichDetailComponent extends DetailComponent {
    tabs = [
        {route: './', title: 'title.uni.fachbereich', name: 'fachbereich', realname: 'fachbereich', area: '.uni'},
    ];

    constructor(router: Router,
                route: ActivatedRoute,
                public http: FachbereichHttpService,
                login: LoginService,
                httpService: HttpService,
                formService: FormService,
                location: Location,
                titleService: TitleService) {
        super(router, route, login, httpService, formService, location, titleService);
    }

    doNav(to) {
        this.router.navigate([this.tabs[to.index].route], {relativeTo: this.route});
    }
}
