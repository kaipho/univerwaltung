import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder } from "@angular/forms";
import { ListComponent } from '../../framework/title/list.component';
import { VorlesungHttpService } from '../../../service/domain/http/vorlesung.http.service';
import { TitleService } from '../../../service/core/title.service';
import { Vorlesung } from '../../../domain/uni/vorlesung';

@Component({
    templateUrl: './vorlesung.list.component.html'
})
export class VorlesungListComponent extends ListComponent<Vorlesung> implements OnInit {

    constructor(titleService: TitleService,
        http: VorlesungHttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<VorlesungListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }
    
}
