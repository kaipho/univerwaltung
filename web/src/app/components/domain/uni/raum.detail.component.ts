import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { FormService } from "../../framework/form/form.service";
import { Location } from '@angular/common';
import {DetailComponent} from "../../framework/title/detail.component";

import { LoginService } from '../../../service/core/login.service';
import { TitleComponent } from '../../framework/title/title.component';
import { Raum } from '../../../domain/uni/raum';
import { HttpService } from '../../../service/core/http.service';
import { TitleService } from '../../../service/core/title.service';
import { RaumHttpService } from '../../../service/domain/http/raum.http.service';

@Component({
    templateUrl: './raum.detail.component.html'
})
export class RaumDetailComponent extends DetailComponent {
    tabs = [
        {route: './', title: 'title.uni.raum', name: 'raum', realname: 'raum', area: '.uni'},
    ];

    constructor(router: Router,
                route: ActivatedRoute,
                public http: RaumHttpService,
                login: LoginService,
                httpService: HttpService,
                formService: FormService,
                location: Location,
                titleService: TitleService) {
        super(router, route, login, httpService, formService, location, titleService);
    }

    doNav(to) {
        this.router.navigate([this.tabs[to.index].route], {relativeTo: this.route});
    }
}
