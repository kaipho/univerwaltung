import { Component, OnInit } from "@angular/core"
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { FormService } from "../../framework/form/form.service";

import { MenuService } from '../../framework/gen-service/menu.service';
import { FachbereichHttpService } from '../../../service/domain/http/fachbereich.http.service';
import { TitleComponent } from '../../framework/title/title.component';
import { Dozent } from '../../../domain/uni/dozent';
import { VorlesungHttpService } from '../../../service/domain/http/vorlesung.http.service';
import { TypeVisitor } from '../../../domain/type.visitor';
import { TitleService } from '../../../service/core/title.service';
import { Fachbereich } from '../../../domain/uni/fachbereich';
import { Vorlesung } from '../../../domain/uni/vorlesung';
import { DozentHttpService } from '../../../service/domain/http/dozent.http.service';

@Component({
	templateUrl: './fachbereich.component.html',
})
export class FachbereichComponent extends TitleComponent implements OnInit {
	_http:FachbereichHttpService;
	_router:Router;
	

	obj:Fachbereich = new Fachbereich();
	crudForm: FormGroup;

	constructor(titleService:TitleService, http:FachbereichHttpService, builder:FormBuilder, public menu: MenuService,
		router:Router, public activeRoute: ActivatedRoute, private formService: FormService) {
		super(titleService);
		this._http = http;
		this._router = router;
		

		this.crudForm = builder.group({
		    
		    bezeichnung: [''],
		    dozenten: [{value:'', disabled: true}],
		    vorlesungen: [{value:'', disabled: true}],
        });
	}

	ngOnInit() {
	    this.activeRoute.params.subscribe( data =>
            this._http.get(this._router.url.replace("/web/", "/api/")).subscribe(result => {
				this.obj = result;
                this.crudForm.reset(result);
                this.titleService.addTitle(result._rep.split("|")[0], 1);

                this.formService.form = this.crudForm;
                this.formService.component = this;
                this.formService.url = result['_self'];

                
                this.menu.addEntries(this, result.id, result,
                );
            })
        );
	}

	
}
