import { Component, OnInit } from "@angular/core"
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { FormService } from "../../framework/form/form.service";

import { StudentHttpService } from '../../../service/domain/http/student.http.service';
import { MenuService } from '../../framework/gen-service/menu.service';
import { TitleComponent } from '../../framework/title/title.component';
import { UniServiceSetRaumUnitComponent } from '../../service/uni/UniService.setRaum.Unit.component';
import { VorlesungHttpService } from '../../../service/domain/http/vorlesung.http.service';
import { Student } from '../../../domain/uni/student';
import { TypeVisitor } from '../../../domain/type.visitor';
import { UniServiceSetFachbereichVorlesungUnitComponent } from '../../service/uni/UniService.setFachbereichVorlesung.Unit.component';
import { TitleService } from '../../../service/core/title.service';
import { UniServiceSetDozentUnitComponent } from '../../service/uni/UniService.setDozent.Unit.component';
import { Vorlesung } from '../../../domain/uni/vorlesung';

@Component({
	templateUrl: './vorlesung.component.html',
})
export class VorlesungComponent extends TitleComponent implements OnInit {
	_http:VorlesungHttpService;
	_router:Router;
	

	obj:Vorlesung = new Vorlesung();
	crudForm: FormGroup;

	constructor(titleService:TitleService, http:VorlesungHttpService, builder:FormBuilder, public menu: MenuService,
		router:Router, public activeRoute: ActivatedRoute, private formService: FormService) {
		super(titleService);
		this._http = http;
		this._router = router;
		

		this.crudForm = builder.group({
		    
		    bezeichnung: [''],
		    dozent: [''],
		    fachbereich: [''],
		    raum: [''],
		    studenten: [''],
        });
	}

	ngOnInit() {
	    this.activeRoute.params.subscribe( data =>
            this._http.get(this._router.url.replace("/web/", "/api/")).subscribe(result => {
				this.obj = result;
                this.crudForm.reset(result);
                this.titleService.addTitle(result._rep.split("|")[0], 1);

                this.formService.form = this.crudForm;
                this.formService.component = this;
                this.formService.url = result['_self'];

                
                this.menu.addEntries(this, result.id, result,
                  { name: 'functions.uni.setRaum.title', dialog: UniServiceSetRaumUnitComponent},
                  { name: 'functions.uni.setFachbereichVorlesung.title', dialog: UniServiceSetFachbereichVorlesungUnitComponent},
                  { name: 'functions.uni.setDozent.title', dialog: UniServiceSetDozentUnitComponent},
                );
            })
        );
	}

	
}
