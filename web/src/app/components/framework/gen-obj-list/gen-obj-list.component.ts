import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: 'gen-obj-list',
    template: `
<mat-card>
    <mat-toolbar color="primary">{{ name | translate }}</mat-toolbar>
    <mat-nav-list>
		<div *ngFor="let actual of obj; let i = index">
			<mat-list-item (click)="_emitSelect(i)">
				<h2 mat-line> {{actual._rep.split("|")[0]}} </h2>
                <p mat-line> {{actual._rep.split("|")[1]}} </p>
			</mat-list-item>
			<mat-divider></mat-divider>
		</div>
		<mat-list-item (click)="_emitSelect(i)" *ngIf="obj.length == 0">
				{{ 'noEntries' | translate }}
		</mat-list-item>
	</mat-nav-list></mat-card>
    `,
    styles: ['mat-card { margin: 0; }']
})
export class GenObjListComponent {
    @Input()
    name: string;
    @Input()
    obj: Array<any>;
    @Output()
    select: EventEmitter<Number> = new EventEmitter<Number>();

    constructor(private router:Router) {
    }

    private _emitSelect(index: number) {
        this.router.navigateByUrl("/web" + this.obj[index]._self);
    }
}
