import {TitleComponent} from "./title.component";
import {TitleService} from "../../../service/core/title.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AbstractHttpService} from "../../../service/domain/http/abstract.http.service";
import {DomainObject} from "../../../domain/domain.object";
import {Pageable} from "../../../service/domain/http/pageable";
import {MatDialog, MatDialogRef, MatSnackBar} from "@angular/material";
import {ActivatedRoute, Router} from "@angular/router";
import {GenDialogComponent} from "../gen-dialog/gen-dialog.component";

export abstract class ListComponent<T extends DomainObject> extends TitleComponent {
    public objects: Pageable<T> = new Pageable<T>();

    searchForm: FormGroup;
    searchHidden: boolean = true;

    constructor(titleService: TitleService,
                protected http: AbstractHttpService<T>,
                builder: FormBuilder,
                protected snackBar: MatSnackBar,
                protected router: Router,
                protected route: ActivatedRoute,
                public dialogRef: MatDialogRef<ListComponent<DomainObject>>,
                public dialog: MatDialog) {
        super(titleService);

        this.searchForm = builder.group({
            association: ['', Validators.required],
            text: ['', Validators.required],
        });
    }

    search() {
        this.http.find(this.searchForm.get("association").value, encodeURIComponent(this.searchForm.get("text").value)).subscribe(
            result => this.objects = result,
            error => this.snackBar.open(error.toString(), 'Close')
        )
    }

    fadeSearch(): void {
        if (!this.searchHidden) {
            this.loadAll();
        }
        this.searchHidden = !this.searchHidden;
    }

    open(it: T) {
        if (this.dialogRef) {
            this.dialogRef.close(it);
        } else {
            this.router.navigateByUrl("web/" + it.self);
        }
    }

    create(component: any, typeid: number) {
        const openDialog = this.dialog.open(component);
        (<any>openDialog.componentInstance).closeSilent = true;
        openDialog.afterClosed().subscribe(
            result => this.router.navigate(["web", "domainr", typeid, result.id])
        );
    }

    loadAll() {
        this.http.getFirst().subscribe(
            result => this.objects = result,
            error => this.snackBar.open(error.toString(), 'Close')
        )
    }
}
