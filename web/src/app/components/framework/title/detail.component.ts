import {OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {LoginService} from "../../../service/core/login.service";
import {HttpService} from "../../../service/core/http.service";
import {FormService} from "../form/form.service";
import {TitleService} from "../../../service/core/title.service";

export abstract class DetailComponent implements OnInit {
    saved: boolean;
    abstract tabs;

    constructor(public router: Router,
                public route: ActivatedRoute,
                public login: LoginService,
                public httpService: HttpService,
                public formService: FormService,
                public location: Location,
                public titleService: TitleService) {
    }

    ngOnInit() {
        this.saved = false;
    }

    canEdit(): boolean {
        return this.login.canWrite();
    }

    save() {
        this.formService.save();
    }

    public getSelectedIndex(): number {
        const url = this.router.url.split("/");
        const active = url[url.length - 1];
        return this.tabs.findIndex(actualClass => actualClass.route == active);
    }

    cancel() {
        this.formService.component.ngOnInit();
    }
}
