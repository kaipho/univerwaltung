import {Injectable, OnInit} from "@angular/core";

@Injectable()
export class MenuService {
    private _entries:Array<MenuEntry>;
    private _component:OnInit;
    private _actualId:number;
    private _data:any;

    constructor() {
        this._entries = []
    }

    get entries():Array<MenuEntry> {
        return this._entries;
    }

    get component():OnInit {
        return this._component;
    }

    get actualId():number {
        return this._actualId;
    }

    get data():any {
        return this._data;
    }

    addEntries(component:OnInit, actualId:number, _data:any, ...entrie:MenuEntry[]) {
        this._entries = entrie;
        this._component = component;
        this._actualId = actualId;
        this._data = _data;
    }

    destroy() {
        this._entries = [];
    }
}

export interface MenuEntry {
    dialog:any;
    name:string;
    icon?:string;
}
