import { Injectable, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { HttpService } from "../../../service/core/http.service";
import { MatSnackBar } from "@angular/material";
import { TranslateService } from "@ngx-translate/core";

@Injectable()
export class FormService {
    form: FormGroup;
    component: OnInit;
    url: string;

    constructor(private httpService: HttpService,
                private snackbar: MatSnackBar,
                private translate: TranslateService) {

    }

    isFormTouched(): boolean {
        return this.form && this.form.dirty
    }

    save() {
        this.httpService.put('/api/' + this.url, this.form.value).subscribe(
            ok => {
                this.snackbar.open("Speichern erfolgreich", 'close');
                this.component.ngOnInit();
            },
            error => {
                this.translate.get(error.body.message).subscribe((res: string) => {
                    this.snackbar.open(res, 'close')
                });
            }
        );
    }
}
