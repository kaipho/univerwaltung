import { Component, EventEmitter, Input, Optional, Self, ViewChild } from "@angular/core";
import { ControlValueAccessor, NgControl } from "@angular/forms";
import { PrimitivesServiceHttpService } from "../../../service/functions/http/primitivesService.http.service";
import { BLOB } from "../../../domain/blob";
import { TdFileInputComponent, TdFileService } from "@covalent/core";
import { HttpService } from "../../../service/core/http.service";
import { environment } from "../../../../environments/environment";


@Component({
    selector: 'gen-blob',
    template: `
        <div layout="row">
            <mat-input-container tdFileDrop
                                (fileDrop)="file = $event"
                                (click)="fileInput.inputElement.click()"
                                (keyup.enter)="fileInput.inputElement.click()"
                                (keyup.delete)="fileInput.clear()"
                                (keyup.backspace)="fileInput.clear()"
                                flex>
                <span mdPrefix><mat-icon>attach_file</mat-icon></span>
                <input matInput
                       [placeholder]="placeholder"
                       [ngModel]="value.name"
                       readonly/>
            </mat-input-container>
            <button mat-icon-button [disabled]="!value.blobId" (click)="clear()" (keyup.enter)="clear()">
                <mat-icon>cancel</mat-icon>
            </button>
            <td-file-input style="display: none" [(ngModel)]="file" #fileInput (select)="selectFile($event)"
                           name="file"></td-file-input>
            <button mat-icon-button [disabled]="!file" (click)="uploadBlob()">
                <mat-icon>file_upload</mat-icon>
            </button>
            <button mat-icon-button [disabled]="!value.blobId" (click)="downloadBlob()">
                <mat-icon>file_download</mat-icon>
            </button>
        </div>
    `
})
export class GenBlobComponent implements ControlValueAccessor {
    @Input()
    placeholder: string;

    @ViewChild(TdFileInputComponent)
    fileInput: TdFileInputComponent;

    file: File;

    value: BLOB;

    _onChange = (value: any) => {
    };

    constructor(@Self() @Optional() public control: NgControl,
                public http: PrimitivesServiceHttpService,
                public fileUpload: TdFileService) {
        this.control.valueAccessor = this;
    }

    writeValue(obj: any): void {
        this.value = obj;
    }

    registerOnChange(fn: (value: any) => void): void {
        this._onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    selectFile(event: File) {
        this.file = event;
        this._onChange({name: event.name, id: this.value.id, blobId: this.value.blobId});
        this.writeValue({name: event.name, id: this.value.id, blobId: this.value.blobId})
    }

    /**
     * Uploads the file to the server and stored it to the DB. Returns the ID (blobId) the blob is stored under.
     */
    uploadBlob() {
        this.fileUpload.upload({
            url: environment.server + '/api/file',
            method: 'post',
            file: this.file
        }).subscribe(
            ok => {
                this._onChange({name: this.value.name, id: this.value.id, blobId: ok});
                this.writeValue({name: this.value.name, id: this.value.id, blobId: ok})
            },
            error =>  {console.log(error)}
        );
        console.log('voooodoooo')
    }

    downloadBlob() {
        window.open(`${environment.server}/api/file/${this.value.blobId}`)
    }

    /**
     *  Clears the FileInput and the linking to the BLOB.
     */
    clear() {
        this.fileInput.clear();
        this.value.name = '';
        this.value.blobId = null;
    }
}
